import AxiosClient from './api'
import END_POINT from './constants'

async function getProfile({ userId }) {
    const res = await AxiosClient.get(`user/get?userId=${userId}`)
    return res.data;
}
async function postLogin(email, password) {
    const res = await AxiosClient.post('/auth/login', { email, password });
    return res.data;
}

async function postRegister(name, email, password, company, gender , dob) {

    const res = await AxiosClient.post('/auth/register', { name, email, password, company, gender , dob });
    console.log(res.data);

    return res.data;
}

async function searchUser(queryString) {
    const res = await AxiosClient.get(`/user/search?q=${queryString}`);
    return res.data;
}

export {
    getProfile,
    postLogin,
    postRegister,
    searchUser
}