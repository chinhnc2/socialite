import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'

function getComments(amount) {
    AxiosClient.get('/getComment/:idPost')
        .then((res) => {
            console.log(res);
            return res.data
        })
}
async function createComment(data) {
    console.log(data);
    const res = await AxiosClient.post('/comment/createComment', data)
    return res.data;
}

async function deleteComment(id) {
    const res = await AxiosClient.get(`/comment/deleteComment/${id}`)
    return res.data;
}

export {
    getComments,
    createComment,
    deleteComment
}
