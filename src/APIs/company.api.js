
import AxiosClient from './api'

async function getAllCompany() {
    const res = await AxiosClient.get('/company/getAllCompany/cli');
    return res.data;
}

async function getCompany(companyID) {
    console.log(companyID)
    const res = await AxiosClient.get(`/company/getCompany/${companyID}`);
    return res.data;
}

async function searchCompany(queryString) {
    const res = await AxiosClient.get(`/company/search/key=${queryString}`);
    return res.data;
}

async function joinInCompany(companyID, userID) {
    const res = await AxiosClient.post(`/company/request/${companyID}`, { userID });
    return res.data;
}

async function getCICOByUser(userID) {
    const res = await AxiosClient.get(`/cico/getByUser?userID=${userID}`);
    return res.data;
}

async function checkInApi(userID) {
    const res = await AxiosClient.post('/cico/checkin', { userID });
    return res.data;
}

async function checkOutApi(userID, CIID) {
    const res = await AxiosClient.post(`/cico/checkout/${CIID}`, { userID });
    return res.data;
}

async function getAllCICO(companyID) {
    const res = await AxiosClient.get(`/cico/getByCompany?companyID=${companyID}`);
    return res.data;
}

async function allowJoinInCompany(payload) {
    const res = await AxiosClient.post(`/company/addMember/cli/${payload.companyID}`, {userID: payload.userID})
    return res.data;
}

async function updateAvatarCompany(payload) {
    const res = await AxiosClient.post(`/company/updateAvatarCompany/${payload.companyID}`, { avatar: payload.avatar});
    return res.data;
}

export {
    getAllCompany,
    getCompany,
    searchCompany,
    joinInCompany,
    getCICOByUser,
    checkInApi,
    checkOutApi,
    getAllCICO,
    allowJoinInCompany,
    updateAvatarCompany
}