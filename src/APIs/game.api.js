import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'

async function getGames() {
    const res = await AxiosClient.get('/game/getallgame')
    return res.data
}
async function getGamesId(id) {
    console.log(id)
    const res = await AxiosClient.get('/game/getgame/'+id)
    return res.data
}

async function addGames(data) {
    const res = await AxiosClient.post('/game/addgame',data)
    return res.data
}
  
export {
    getGames,
    addGames,
    getGamesId
}