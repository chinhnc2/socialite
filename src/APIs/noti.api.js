import { parseFilter } from 'Utils'
import AxiosClient from './api'

async function createNoti(noti) {
    const res = await AxiosClient.post('/noti/createNoti', noti);
    return res.data;
}

async function getMyNotis(userID) {
  const res = await AxiosClient.get(`/noti/getNoti/${userID}`);
  return res.data
}

async function checkedNoti(id) {
  const res = await AxiosClient.get(`/noti/checkNoti/${id}`)
  return res.data
}

async function deleteNoti(id) {
  const res = await AxiosClient.get(`/noti/deleteNoti/${id}`)
  return res.data
}




export {
    createNoti,
  getMyNotis,
  checkedNoti,
  deleteNoti,
}
