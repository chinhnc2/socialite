import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'

async function getPlayer() {
    const res = await AxiosClient.get('/player/info/62458066104426474c8a6b50')
    return res.data 
}

async function playedGame(data) {
    const res = await AxiosClient.post(`player/info/${data.id}`,data) //player/info/ádkasdkaskd12312asfd
    return res.data 
}

async function sellItem(data) {
    const res = await AxiosClient.post('/player/deletePlayerItem',data)
    return res.data 
}

async function getItemdepot(data) {
    const res = await AxiosClient.post('/player/getItemdepot',data)
    return res.data 
}
    
export {
    getPlayer,
    playedGame,
    sellItem,
    getItemdepot
}