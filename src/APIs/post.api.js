import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'



async function getPosts(amount, begin) {
    console.log(amount, begin);
    const res = await AxiosClient.get(`/post/getPostFriend?amount=${amount}&begin=${begin}`)
    return res.data;
}

async function getPostInCompany(amount, begin, companyID) {
    const res = await AxiosClient.get(`/post/getPostCompany/${companyID}?amount=${amount}&begin=${begin}`);
    return res.data;
}

async function getPostId(id) {
    const res = await AxiosClient.get(`/post/get/${id}`)
    return res.data
}

async function createPost(payload) {
    const res = await AxiosClient.post('post/createPost/', payload)
    return res.data;
}

async function editPost(id, data) {
    const res = await AxiosClient.post(`/post/updatePost/${id}`, { content: data })
    return res.data
}

async function delPost(id) {
    const res = await AxiosClient.get(`/post/deletePost/${id}`)
    return res.data
}

async function likePost(postID, userID) {
    const res = await AxiosClient.post(`/post/likePost/${postID}`, { user_like: userID });
    return res.data
}

export {
    getPosts,
    getPostInCompany,
    editPost,
    delPost,
    likePost,
    createPost,
    getPostId,
}
