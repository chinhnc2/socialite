import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'

async function updateProfle(userID, updateBody) {
    const res = await AxiosClient.post(`/user/update/${userID}`, updateBody);
    return res.data;
}

async function sendRequestFriend(userID, friendID) {
    console.log(userID, friendID);
    const res = await AxiosClient.post(`/user/requestFriend/${userID}`, { req_friend_id: friendID })
    return res.data;
}

async function allowRequestFriend(userID, friendID) {
    const res = await AxiosClient.post(`/user/addFriend/${userID}`, { friendID });
    return res.data;
}

async function removeRequestFriend(notiID) {
    const res = await AxiosClient.get(`/noti/deleteNoti/${notiID}`);
    return res.data;
}

export {
    updateProfle,
    sendRequestFriend,
    allowRequestFriend,
    removeRequestFriend
}
