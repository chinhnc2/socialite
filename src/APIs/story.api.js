import { parseFilter } from 'Utils'
import AxiosClient from './api'
import END_POINT from './constants'


async function createStory(payload) {
    const res = await AxiosClient.post('story/createStory/', payload)
    return res.data;
}

async function getStories() {
    const res = await AxiosClient.get(`/story/getStories/`)
    return res.data
}

export {
    createStory,
    getStories
}
