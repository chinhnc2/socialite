import React, { useState } from 'react';
import { useForm, Controller } from "react-hook-form";
import uploadCloudinary from 'Utils/cloudinary';
import { usePost } from 'Hooks/home';
import styled from 'styled-components';
import './formUploadMedia.scss';
import { useAuth } from 'Hooks';

const FormUpload = styled.form`
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 8px;
`

const FormUploadMedia = ({ type }) => {
    const form = useForm();
    const { handleSubmit, reset, watch, register, control } = useForm();
    const [imgURL, setImgURL] = useState(null);

    const { profile } = useAuth();
    const { createPostAction } = usePost();

    const onSubmitUpload = async data => {
        let resURL = await uploadCloudinary(imgURL);
        if (resURL) {
            if (profile.company) {
                if (type == 'company') {
                    createPostAction({isPhoto: true, post_content: resURL, user_id: (profile.id || profile._id), companyID: profile.company._id})
                } else {
                    createPostAction({isPhoto: true, post_content: resURL, user_id: (profile.id || profile._id)})
                }
            }
            
            
        }
        reset();
    }

    

    const handleChangeFile = (e) => {
        let reader = new FileReader();
        console.log(e.target.files[0])
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function (e) {
            setImgURL(reader.result);
        };
    }

    // useEffect(() => {
        
    // }, [imgURL])

    


    return(
        <div className="FormUploadMedia">
            <FormUpload onSubmit={handleSubmit(onSubmitUpload)}>
                <div className="btn__upload">
                    <Controller
                        name="media"
                        control={control}
                        render={({ field }) => {
                            // sending integer instead of string.
                            return <input type='file'  name='media'
                            {...field} onChange={(e) => handleChangeFile(e)} />;
                        }}
                    />         
                    <i className="fa-solid fa-image" style={{color : '#1e6dd2'}} />
                    <span style={{marginLeft : '3px'}}>Photo/Video</span>
                </div>
                <div className="btn__cam">
                    <i className="fa-solid fa-face-grin"  style={{color : ' #e7438c'}}/>
                    <span  style={{marginLeft : '3px'}}>Feeling</span>
                </div>
                <div className="btn__tag">
                    <i className="fa-solid fa-user-tag"  style={{color : '#ff8e4a'}}/>
                    <span  style={{marginLeft : '3px'}}>Tag Friend</span>
                </div>
                {imgURL && <input type='submit' /> }
            </FormUpload>       
            <div className="upload__img">
                {imgURL && <img src={imgURL} alt='img_temp' />}
            </div>
        </div>
    )
}

export default FormUploadMedia;