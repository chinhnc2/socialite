import React, { useState } from "react";
import FormUploadMedia from "./formUploadMedia";
import "./postCreate.scss";
import EnterPost from "./modal";
import { useAuth, useProfile } from "Hooks";

const PostCreate = ({ handleShowHideModalEnterPost , profileUser, type }) => {

  //const { profileUser} = useProfile();
  const {profile} = useAuth();
  return (
    <div className="post-create">
      <div className="post__create-top">
        <div className="post__create-avt">
          <img
            src={profile && profile.avatar ? profile.avatar : "https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg"}
            alt=""
          />
        </div>
        <div className="post__create-input">
          <input
            type="text"
            placeholder="What's Your Mind ?"
            onFocus={() => handleShowHideModalEnterPost()}
          />
        </div>
      </div>
      <FormUploadMedia type={type} />
    </div>
  );
};

export default PostCreate;
