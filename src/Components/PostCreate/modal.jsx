import React, { useCallback, useEffect, useRef } from "react";
import "./modalPostCreate.scss";
import { useForm, FormProvider } from "react-hook-form";
import { useHome, usePost, useAuth } from "Hooks";
import {
    Submit,
    ClickAble,
    Input,
    Text,
    FormInput,
    FormTextArea,
} from "Components";


const EnterPost = ({ handleShowHideModalEnterPost, type ,setShowModal , showModal}) => {
    const { createPostAction } = usePost();
    const inputRef = useRef(null);

    const form = useForm();
    const { profile } = useAuth();
    const { handleSubmit, reset } = form;

    //* tự động focus sau khi được render
    useEffect(() => {
        if (inputRef.current) {
            inputRef.current.focus();
        }
    }, []);

    const onSubmit = useCallback((data) => {
        if (type == 'company') {
            createPostAction({ post_content: data.post_content, user_id: profile._id || profile.id, companyID: profile.company && profile.company._id });
            
        } else {
            createPostAction({ post_content: data.post_content, user_id: profile._id || profile.id });
            
        }
        reset();
        handleShowHideModalEnterPost();
    }, []);
    return (
        <div className="modal">
            <FormProvider {...form}>
                <form className="modal-content" onSubmit={handleSubmit(onSubmit)} >
                    <div className="modal__content-top">
                        <h2>Create post</h2>
                        <i
                            className="bx bx-x"
                            onClick={() => handleShowHideModalEnterPost()}
                        ></i>
                    </div>
                    <div className="modal__content-body">
                        <div className="modal__content-avt">
                            <img
                                src={profile.avatar}
                                alt=""
                            />
                            <div className="modal__content-input">
                                {/* <input type="text" placeholder="What's your msind?"/> */}
                                <FormTextArea
                                    name="post_content"
                                    required
                                    cols="46"
                                    rows="6"
                                    placeholder="What's your mind?"
                                    spellcheck="false"
                                    ref={inputRef}
                                ></FormTextArea>
                            </div>
                        </div>
                        <div className="modal__content-addFile">
                            <p>Add to your post</p>
                            <ul className="modal__content-addList">
                                <li style={{ backgroundColor: "rgba(219,234,254,1)" }}>
                                    <i
                                        className="bx bx-image-alt"
                                        style={{ color: "rgba(37,99,235,1)" }}
                                    ></i>
                                    {/* <i class='bx bx-photo-album icon-photo' style={{color: 'rgba(37,99,235,1)'}}></i> */}
                                </li>
                                <li style={{ backgroundColor: "rgba(254,226,226,1)" }}>
                                    <i
                                        className="bx bxs-videos"
                                        style={{ color: "rgba(220,38,38,1)" }}
                                    ></i>
                                    {/* <i class='bx bxs-tag-alt' style={{color: '#059669'}}></i> */}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="modal__content-footer">
                        <button onClick={() => handleShowHideModalEnterPost()}>
                            Cancel
                        </button>
                        <input type="submit" style={{
                            padding: '6px 14px',
                            borderRadius: '8px',
                            border: 'none',
                            color: 'white',
                            backgroundColor: '#4169e1f7'
                        }} />
                        {/* <button>Share</button> */}
                    </div>
                </form>
            </FormProvider>
        </div>
    );
};

export default EnterPost;
