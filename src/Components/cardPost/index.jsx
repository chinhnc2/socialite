import { CommentInput, Comment, FormTextArea, TimeSince } from "Components";
import React, { useState, useCallback, useEffect, useRef } from "react";
import { useAuth, usePost } from "Hooks";
import { useHistory, Link } from "react-router-dom";
import "./cardPost.scss";
import { FormProvider, useForm } from "react-hook-form";
import styled from "styled-components";
import avatar from '../../Assets/images/avt_default.png';
import ModalCustom from "Components/modal_custom";
import { element } from "prop-types";
const LikeBtn = styled.div`
    color: ${(props) => props.liked ? '#ee0a73f2' : '#656C76'};
    display: flex;
    justify-content: center;
    align-items: center;
  `
const ListLikeModalContent = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  width: 500px;
  height: 400px;
`
const EachDetailLike = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  width: 100%;
  padding: 15px 30px;
  font-size: 15px;
  color: black;
`
const BtnCancel = styled.button`
  border: none;
  outline: none;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
`
const CardPost = ({ x }) => {
  const [isDropdownOption, setIsDropdownOption] = useState(false);
  const [editingMode, setEditingMode] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [showTextMore , setShowTextMore] = useState(false);
  const [height , setHeight] = useState("");
  const [heightComment , setHeightComment] = useState("");
  const [showCommentMore , setShowCommentMore] = useState(false);
  const [initialHeightComment , setInitialHeightComment] = useState("");
  const  [initialHeight , setInitialHeight] = useState("");
  const [commentAvail, setCommentAvil] = useState([]);
//   const [ showButtonTextMore , setShowButtonTextMore] = useState(false);
  const { profile } = useAuth();
  const { likePostAction, editPostAction, deletePostAction } = usePost();
  const form = useForm();
  const { handleSubmit, reset } = form;
  const onSubmitEdit = useCallback((data) => {
    editPostAction({...x, post_content: data.editPost});
    setEditingMode(false);
  }, []);
  const handleToggleModalCustom = () => {
    setIsShowModal(prev => !prev);
  }
  const handleFocusComment = () => {
   
  }
  const onLikeHandle = (postID, profile, x) => {
    likePostAction({ postID, profile, post: x });   
  }
    const textContentRef = useRef(null);
    const commentContentRef = useRef(null);
    const handleShowTextMore = () => {
        setHeight(initialHeight);
        setShowTextMore(false)
    }
    const handleShowCommentMore = () => {
        setCommentAvil(x.comment);
        setShowCommentMore(false);
    }
    useEffect( () => {
        const heightRef = textContentRef?.current?.clientHeight;
        setInitialHeight(heightRef);
        if (heightRef > 200){
            setHeight(200);   
            setShowTextMore(true)
        }else {
            handleShowTextMore();
        }
    },[])
    useEffect(() => {
      if (x && x.comment) {
        if (x.comment.length > 5){
          setCommentAvil([...x.comment.slice(x.comment.length - 5)])
          setShowCommentMore(true);
        }
        else setCommentAvil(x.comment);
      }
    }, [x]);
  return (
    <div className="CardPost">
      <div className="header_post">
        <Link to={ x.user_id ? `/u/${x.user_id}` : "/"}>
            <div className="avt_user_post">
                <img src={x.user.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'} alt="avt_user_post" />
            </div>
        </Link>
        <div className="main_header">
           <Link to={x.user_id ? `/u/${x.user_id}` : "/"}>
                <div className="name_user_post">
                    <p>{x.user.name}</p>
                </div>
           </Link>
          <div className="create_post">
            <p>{TimeSince(new Date(x.createdAt))}</p>
          </div>
          <div className="option" onClick={() => setIsDropdownOption(prev => !prev)}>
            <box-icon name='dots-horizontal-rounded'></box-icon>
          </div>
          {isDropdownOption && (profile.id || profile._id) == x.user._id &&
            <div className="dropdownOption">
              <div className="item_option" onClick={() => deletePostAction(x)}>
                <p>Delete this post</p>
              </div>
              <div className="item_option" onClick={() => {
                setIsDropdownOption(false);
                setEditingMode(true)}}>
                <p>Edit this post</p>
              </div>
            </div>
          }
        </div>
      </div>
      {!editingMode ? <div className="content_post">
        {x.isPhoto ? (
          <img src={x.post_content} alt="image_post" />
        ) : (
          <>
            <p ref={textContentRef} style={{ height: height, overflow: 'hidden', textOverflow: 'ellipsis' }} >{x.post_content}</p>
            <div style={{
                color: '#5B059A',
                marginLeft: '8px', marginTop: '-6px',}}> {showTextMore && <button style={{ border: 'none', backgroundColor: 'transparent'}}
                onClick={handleShowTextMore}>...load more</button>}
            </div>
          </>
        )}
      </div>
      :
        <FormProvider {...form}>
          <form onSubmit={handleSubmit(onSubmitEdit)}>
            <FormTextArea name='editPost' required placeholder={x.post_content} />
            <input type="submit" />
          </form>
        </FormProvider>
      }
      <div className="tool_post">
        <div className="btn_favorite" onClick={() => onLikeHandle((x._id||x.id), profile, x)}>
          <LikeBtn liked={x.likes.findIndex(element => {
            return element ? (element._id || element.id) === (profile._id || profile.id) : -1
          }) >= 0}>
            <i className='bx bxs-heart'></i>
            <span>like</span>
          </LikeBtn>
        </div>
        <div className="btn_comment" onClick={handleFocusComment}>
          <i className="fa-solid fa-comment" />
          <span>Comment</span>
        </div>
        <div className="btn_share">
          <i className="fa-solid fa-share" />
          <span>Share</span>
        </div>
      </div>
      <div className="like_in_post" onClick={handleToggleModalCustom}>
        <p>{x.likes.length} người khác đã thích</p>
      </div>
      {isShowModal &&
        <ModalCustom func={handleToggleModalCustom}>
          <ListLikeModalContent>
            <div className="btnCancel">
              <BtnCancel onClick={setIsShowModal}>
                X
              </BtnCancel>
            </div>
            {x.likes.map(element => (
              <EachDetailLike>
                <p>Like: </p>
                <Link to={ x.user_id ? `/u/${x.user_id}` : "/"}>
                    <div className="img-avt">
                    <img src={element.avatar|| avatar} alt="avt" />
                    </div>
                </Link>
                <Link to={ x.user_id ? `/u/${x.user_id}` : "/"}>
                    <div className="name">
                    <p>{element.name}</p>
                    </div>
                </Link>
              </EachDetailLike>))}
          </ListLikeModalContent>
        </ModalCustom>}
        <div>
            {
            showCommentMore && <button onClick={handleShowCommentMore} style={{ border: 'none', backgroundColor: 'transparent', marginLeft: '13px', color: '#6F6F6F',fontWeight: '500',fontSize: '15.5px',
                } }>See more comment</button>
            }
        </div>
          <div className="comment_in_post" style={{ height: 'fit-content' , overflow: 'hidden' }}>
              {commentAvail && commentAvail.length > 0 && commentAvail.map(item => item.user && <Comment key={item._id || item.id} x={item} />)}
      </div>
      <div className="comment_type">
        <CommentInput x={x} handleFocusComment={handleFocusComment} />
      </div>
    </div>
  );
};
export default CardPost;