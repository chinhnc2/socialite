import { CommentInput, Comment, FormTextArea, TimeSince } from "Components";
import React, { useState, useCallback, useEffect, useRef } from "react";
import { useAuth, usePost, useHome } from "Hooks";
import { useHistory, Link } from "react-router-dom";
import { FormProvider, useForm } from "react-hook-form";
import styled from "styled-components";
import avatar from '../../Assets/images/avt_default.png';
import ModalCustom from "Components/modal_custom";
import { element } from "prop-types";
import "./styled.scss"

const LikeBtn = styled.div`
    color: ${(props) => props.liked ? '#ee0a73f2' : '#656c76'};
    display: flex;
    justify-content: center;
    align-items: center;
`
const ListLikeModalContent = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  width: 500px;
  height: 400px;
`

const EachDetailLike = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  width: 100%;
  padding: 15px 30px;
  font-size: 15px;
  color: black;
`

const BtnCancel = styled.button`
  border: none;
  outline: none;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
`

const CardPostSingle = ({ pSin, notiFromId }) => {
  const [isDropdownOption, setIsDropdownOption] = useState(false);
  const [editingMode, setEditingMode] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const [showTextMore , setShowTextMore] = useState(false);
  const [height , setHeight] = useState("");

  const [heightComment , setHeightComment] = useState("");
  const [showCommentMore , setShowCommentMore] = useState(false);
  const [initialHeightComment , setInitialHeightComment] = useState("");

  const  [initialHeight , setInitialHeight] = useState("");
//   const [ showButtonTextMore , setShowButtonTextMore] = useState(false);
  const { profile } = useAuth();
  const { posts } = useAuth();
  const { likePostAction, editPostAction, deletePostAction, getPostIdAction } = usePost();
  const form = useForm();
  const { handleSubmit, reset } = form;

  const onSubmitEdit = useCallback((data) => {
    console.log(data);
    editPostAction({...pSin, post_content: data.editPost});
  }, []);

  const handleToggleModalCustom = () => {
    setIsShowModal(prev => !prev);
  }

  const handleFocusComment = () => {
    
  }
  
  const onLikeHandle = (postID, profile, pSin) => {
    console.log("awwwwwwwwwwwwwwwww", pSin);
    likePostAction({ postID, profile, post: pSin, type: 'single' });    
  }
    const textContentRef = useRef(null);
    const commentContentRef = useRef(null);

    const handleShowTextMore = () => {
        setHeight(initialHeight);
        setShowTextMore(false)
    }
    const handleShowCommentMore = () => {
        setHeightComment(initialHeightComment);
        setShowCommentMore(false);
    }
    console.log("Sinnnnnnnnnnnnnnnnnnnn", pSin)
    useEffect( () => {

        const heightRef = textContentRef?.current?.clientHeight;
        setInitialHeight(heightRef);
        if (heightRef > 200){
            setHeight(200);    
            setShowTextMore(true)
        }else {
            handleShowTextMore();
        }
    },[])

    // useEffect( () => {
    //     const heightcommentRef = commentContentRef?.current?.clientHeight;
    //     setInitialHeightComment(heightcommentRef);

    //     if (heightcommentRef  > 200){
    //         setHeightComment(200);
    //         setShowCommentMore(true);

    //     }else{
    //         handleShowCommentMore();
    //     }
    // },[])

  return (
    <div className="cardPost">
      <>
        <div className="header_post">
          <Link to={ pSin[0].user_id ? `/u/${pSin[0].user_id}` : "/"}>
              <div className="avt_user_post">
                  <img src={pSin[0].user.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'} alt="avt_user_post" />
              </div>
          </Link>
      
          <div className="main_header">
            <Link to={pSin[0].user_id ? `/u/${pSin[0].user_id}` : "/"}>
                  <div className="name_user_post">
                      <p>{pSin[0].user.name}</p>
                  </div>
            </Link>
          
            <div className="create_post">
              <p>{TimeSince(new Date(pSin[0].createdAt))}</p>
            </div>
            <div className="option" onClick={() => setIsDropdownOption(prev => !prev)}>
              <box-icon name='dots-horizontal-rounded'></box-icon>
            </div>
            {isDropdownOption && (profile.id || profile._id) == pSin[0].user._id && 
              <div className="dropdownOption">
                <div className="item_option" onClick={() => deletePostAction(pSin[0])}>
                  <p>Delete this post</p>
                </div>
                <div className="item_option" onClick={() => {
                  setIsDropdownOption(false);
                  setEditingMode(true)}}>
                  <p>Edit this post</p>
                </div>    
              </div>
            }
          </div>
        </div>
        
        {!editingMode ? <div className="content_post">
          {pSin[0].isPhoto ? (
            <img src={pSin[0].post_content} alt="image_post" />
          ) : (
            <>
              <p ref={textContentRef} style={{ height: height, overflow: 'hidden', textOverflow: 'ellipsis' }} >{pSin[0].post_content}</p>
              <div style={{
                  color: '#5b059a',
                  marginLeft: '8px', marginTop: '-6px',}}> {showTextMore && <button style={{ border: 'none', backgroundColor: 'transparent'}} 
                  onClick={handleShowTextMore}>...load more</button>} 
              </div>
            </>
          )}
        </div> 
        : 
          <FormProvider {...form}>
            <form onSubmit={handleSubmit(onSubmitEdit)}>
              <FormTextArea name='editPost' required placeholder={pSin[0].post_content} />
              <input type="submit" />
            </form>
          </FormProvider> 
        }

        <div className="tool_post">
          <div className="btn_favorite" onClick={() => onLikeHandle((pSin[0]._id||pSin[0].id), profile, pSin[0])}>
              <LikeBtn liked={pSin[0].likes.findIndex(element => {
                return element ? (element._id || element.id) === (profile._id || profile.id) : -1
              }) >= 0}>
                <i className='bx bxs-heart'></i>
                <span>like</span>
              </LikeBtn>
          </div>
          <div className="btn_comment" onClick={handleFocusComment}>
            <i className="fa-solid fa-comment" />
            <span>Comment</span>
          </div>
          <div className="btn_share">
            <i className="fa-solid fa-share" />
            <span>Share</span>
          </div>
        </div>
        <div className="like_in_post" onClick={handleToggleModalCustom}>
          <p>{pSin[0].likes.length} người khác đã thích</p>
        </div>

        {isShowModal && 
          <ModalCustom func={handleToggleModalCustom}>
            <ListLikeModalContent>
              <div className="btnCancel">
                <BtnCancel onClick={setIsShowModal}>
                  X
                </BtnCancel>
              </div>
              {pSin[0].likes.map(element => (
                <EachDetailLike>
                  <p>Like: </p>
                  <Link to={ pSin[0].user_id ? `/u/${pSin[0].user_id}` : "/"}>
                      <div className="img-avt">
                      <img src={element.avatar|| avatar} alt="avt" />
                      </div>
                  </Link>
                  
                  <Link to={ pSin[0].user_id ? `/u/${pSin[0].user_id}` : "/"}>
                      <div className="name">
                      <p>{element.name}</p>
                      </div>
                  </Link>
                </EachDetailLike>))}
            </ListLikeModalContent>
          </ModalCustom>
        }
        <div className="comment_in_post" ref={commentContentRef} style={{ height: heightComment , overflow: 'hidden' }}>
          {pSin[0].comment && pSin[0].comment.length > 0 && pSin[0].comment.map(item => item.user && <Comment x={item} />)}
        </div>
          
        <div>
            {/* {showCommentMore && 
              <button onClick={handleShowCommentMore} style={{ border: 'none', backgroundColor: 'transparent', marginLeft: '13px', color: '#6f6f6f',fontWeight: '500',fontSize: '15.5px',}}>
                See more comment
              </button>
            } */}
        </div>
        <div className="comment_type">
          <CommentInput x={{...pSin[0], type: 'single'}}  handleFocusComment={handleFocusComment} />
        </div>
      </>
    </div>
  );
}

export default CardPostSingle
