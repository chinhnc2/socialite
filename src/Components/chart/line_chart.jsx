import React from 'react';
import styled from 'styled-components';
import { Line } from 'react-chartjs-2';
import setupData from 'Utils/settupData_chart';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

const LineChartContain = styled.div`
    width: 100%;
    height: 100%;
    overflow: hidden;
`

const LineChart = ({ labels, data, title }) => {
    return(
        <LineChartContain className='LineChart'>
            <Line data={setupData(labels, data, 'line', title)} />
        </LineChartContain>
    )
}

export default LineChart