import React, { useState } from 'react';
import './comment.scss';
import avatar from '../../Assets/images/avt_default.png';
import { TimeSince } from "Components";
import { useHistory, Link } from "react-router-dom";
import { useAuth, usePost } from 'Hooks';


const Comment = ({ x }) => {

    const [showDate, setShowDate] = useState(false);
    
    const { deleteCommentAction }  = usePost();
    const { profile } = useAuth();

    return(
        x.user && <div className="Comment">
            {profile && (profile.id || profile._id) == x.user._id && <div className="delete_btn" onClick={() => deleteCommentAction(x)}>
                    <p>X</p>
                </div>}
            <Link to={ x.user._id ? `/u/${x.user._id}` : "/"}>
                <div className="avt_comment">
                    <img src={x.user.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'} alt="avt_comment" />
                </div>
            </Link>
            
            <div className="content_comment" onMouseOver={() => setShowDate(true)} onMouseOut={() => setShowDate(false)} >
                
                <div className="main_content_comment">
                    <Link to={ x.user._id ? `/u/${x.user._id}` : "/"}>
                        <div className="user_comment">
                            <p>{x.user.name}</p>
                        </div>
                    </Link>
                    
                    <p>{x.content}</p>
                    {/* <div className="favorite">
                        <p>Tim</p>
                        <p>{x.likes.length}</p>
                    </div> */}
                </div>
                {showDate && <div className="sub_content_comment">
                    {/* <p>Thả tym</p> */}
                    <p>{TimeSince(new Date(x.createdAt))}</p>
                </div> }
            </div>
        </div>
    )
}
export default Comment;