import React, { useCallback, useRef, useEffect } from "react";
import { FormInput } from "Components";
import { useForm, FormProvider } from "react-hook-form";
import { useHome, useAuth, usePost } from "Hooks";
import { SendOutlined } from '@ant-design/icons'
import "./styled.scss"


const CommentInput = ({ x }) => {
 
  const form = useForm();
  const { profile } = useAuth();
  const { createCommentAction } = usePost();
  const { handleSubmit, reset } = form;

  
  const inputRef = useRef();
  
  const onSubmit = useCallback((data) => {
    // * x là bài post
    let userId = profile._id || profile.id;
    x.type ? createCommentAction({
      content: data.comment,
      user_id: (profile.id || profile._id),
      post_id: (x.id || x._id),
      post: x,
      type: x.type
    }) :
    createCommentAction({
      content: data.comment,
      user_id: (profile.id || profile._id),
      post_id: (x.id || x._id),
      post: x
    })
    reset();
  }, []);

  return (
    <div className="cmtInput">
      <div className="cmt__ava">
        <img src={profile.avatar} alt="" />
      </div>
      <FormProvider {...form}>
        <form onSubmit={handleSubmit(onSubmit)} style={{margin : '0' , width : '98%' }}>
          <FormInput 
            name="comment" 
            placeholder="Add your comment..." 
            ref={inputRef} 
            suffix={
              <SendOutlined 
                onClick={handleSubmit(onSubmit)}
                style={{cursor: 'pointer', fontSize: '16px'}}
              />
            }  
          />
          {/* <input type='submit' /> */}
        </form>
      </FormProvider>
    </div>
  );
};
export default CommentInput;
