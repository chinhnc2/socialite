import React, { useState, useCallback , useEffect , useRef} from "react";
import "./header.scss";
import logo from "../../Assets/images/logo/SCEP_6.png";
import { useForm, FormProvider } from "react-hook-form";
import {useAuth, useNoti, useProfile} from "Hooks"
import styled from "styled-components";
import { FormInput, TimeSince } from "Components";
import { useHistory, Link } from "react-router-dom";
import { useHome, usePost, useCompany } from "Hooks";

const NotificationsBtn = styled.div`
  width: 100%:
  height: 100%;
  position: relative;
  cursor: pointer;
  color: red;
`
const HighLightNotiUnseen = styled.p`
  position: absolute;
  top: -2px;
  right: -2px;
  color: red;
  font-weight: 600;
  font-size: 13px;
`
const Header = ({ handleShowHideDropdown, showDropdown, handleShowHideBarNew }) => {
  const { profile, searchUserAction, userQueries , logoutAction } = useAuth();
  const { checkNotifyAction } = useNoti();
  const { allowRequestFriendAction, denyRequestFriendAction } = useProfile();


  const [isShowNotifications, setIsShowNotifications] = useState(false);
  const [checkUnseenNoti, setCheckUnseenNoti] = useState(true);
  const [isShowSearchBoard, setIsShowSearchBoard] = useState(false);
  const { cico, getCICOAction } = useCompany();

  let history = useHistory();

  const onHandleChangeSearch = (data) => {
    // if (data.queryString.length > 0) {
      searchUserAction(data);
    //} 
    return data;
  }

    const hangleLogOut = useCallback( () => {
          // console.log("logout", profile.id)
          // const userId = profile.id;
          logoutAction();
          history.replace('/auth/login')
    },[])
    
  const form = useForm({
    resolver: onHandleChangeSearch ,
    mode: "onChange"
  });
  const { handleSubmit, reset } = form;

  const handleToggleShowNotifications = () => {
    setIsShowNotifications(prev => !prev);
  }

  const handleEachNoti = (noti) => {
    //console.log("notiiiiiiiiiiiiiii", noti)
    if (noti.type === 'post') {
      checkNotifyAction(noti);
      history.push({
        pathname: `/post/${noti.from_id}`,
        state: { 
          notiFromId: noti.from_id
        }
      });
    } else {
      history.push("/")
    }
  }
  const search_boardRef = useRef(null);
  const notiRef = useRef(null);
  const handleClickOutSearchBoard = (e) => {
    
      if (search_boardRef.current && !search_boardRef.current.contains(e.target)) {
          setIsShowSearchBoard(false);
      }
  }
//   const handleClickOutNoti = (e) => {
//       console.log(e.target);
//       if(!notiRef.current && !notiRef.current.contains(e.target)) {
//           console.log("vao");
//       }
//   }
//   useEffect(() => {
//     document.addEventListener("click", handleClickOutNoti, false);
//     return () => document.removeEventListener("click", handleClickOutNoti, false);
//   })
  useEffect(() => {
    document.addEventListener("click", handleClickOutSearchBoard, false);
    return () => document.removeEventListener("click", handleClickOutSearchBoard, false);
  })

  useEffect(() => {
      //console.log("profile", profile);

  },[])

  return (
    <header>
      <div className="top-bar">
        <div className="top-left-bar">
          <i
            onClick={handleShowHideBarNew}
            className="bx bx-menu"
            style={{ color: "#9ca3af", fontSize: "30px", cursor: "pointer" }}
          ></i>
          
          <Link to="/">
            <div className="top-bar-logo">
                <img src={logo} alt="" className="img-logo" />
            </div>
          </Link>
         
        </div>
        <div className="top-bar-list">
            <div className="top-bar-search" ref={search_boardRef} >
                <box-icon name="search" color="#9ca3af" style={{marginLeft: '8px'}}></box-icon>
                <FormProvider {...form}>
                <form onSubmit={handleSubmit()} >
                    <FormInput
                    name='queryString'
                    onFocus={() => setIsShowSearchBoard(true) }
                    //onBlur={() => setIsShowSearchBoard(false)}
                    //onChange={onHandleChangeSearch}
                    type="text"
                    className="top-bar-search"
                    placeholder="Search..."
                    />
                </form>
                </FormProvider>
                
            </div>

          <div className="top-bar-list__icon">
            {/* <button>
              <box-icon
                name="message-square-add"
                rotate="180"
                color="#ffffff"
                fontSize="25px"
              ></box-icon>
              <p>Upload</p>
            </button> */}
            <ul>
              <li>
                <NotificationsBtn onClick={handleToggleShowNotifications}>
                  <i
                    className="bx bx-bell"
                    style={{ fontSize: "26px", color: "#464646" }}
                  ></i>
                  <HighLightNotiUnseen>{profile.noti && profile.noti.length > 0 ? profile.noti.filter(x => !x.seen).length : 0}</HighLightNotiUnseen>
                </NotificationsBtn>
            
              </li>
              <Link to="/chat">
                <li>
                  <i
                    className="bx bx-message-detail"
                    style={{ fontSize: "26px", color: "#464646" }}
                    ></i>
                </li>
              </Link>
              <li onClick={handleShowHideDropdown}>
                <img
                 style={{objectFit : 'cover'}}
                  src={profile.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'}
                  alt="image user"
                />
                <div className="user__noti"></div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {isShowSearchBoard &&  userQueries && userQueries.length > 0 &&(
        <div className="search_user_board" >
            {userQueries  &&  userQueries.map (x => 
                <div className="each_user_search">
                <Link to={`/u/${(x._id || x.id)}`}><p>{x.name}</p></Link>
                </div>
            )}
        </div>
      )}
      {isShowNotifications && (
        <div className='notifications_board' onMouseLeave={() => setIsShowNotifications(false)}>
          <div className="header_noti">
            <div onClick={() => setCheckUnseenNoti(true)} className={checkUnseenNoti && "active"}><p>Unseen</p></div>
            <div onClick={() => setCheckUnseenNoti(false)} className={!checkUnseenNoti && "active"}><p>Seen</p></div>
          </div>
          <div className="list_noti">
            {profile.noti && profile.noti.length > 0 && checkUnseenNoti ? profile.noti.filter(x => !x.seen).reverse().map(element => (
              <div className='each_noti' key={element._id} onClick={() => handleEachNoti(element)}>
                {element.type === 'friend' ? <i className='bx bxs-user'></i> : <i className='bx bx-comment-detail'></i>}
                <div className="main_each_noti">
                  <p>{element.content}</p>
                  <p>{TimeSince(new Date(element.createdAt)) || 'now'}</p>
                  {element.type === 'friend' && 
                    <div className='request_add_friend'>
                      <button onClick={() => allowRequestFriendAction(element)}>Accept</button>
                      <button onClick={() => denyRequestFriendAction(element)}>Deny</button>
                    </div>}
                </div>
              </div>)) : 
            profile.noti && profile.noti.length > 0 && profile.noti.filter(x => x.seen).reverse().map(element => (
              <div className='each_noti' key={element._id} onClick={() => handleEachNoti(element)}>
                {/* {element.type == 'friend' && <img src={} />} */}
                {element.type === 'friend' ? <i className='bx bxs-user'></i> : <i className='bx bx-comment-detail'></i>}
                  <div className="main_each_noti">
                    <p>{element.content}</p>
                    <p>{TimeSince(new Date(element.createdAt)) || 'now'}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
      {showDropdown && (
        <div className="header-dropdown">
          <div className="header-dropdown-top">
            <div className="dropdown-top-avt">
              <img
                style={{objectFit : 'cover'}}
                src={profile.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'}
                alt=""
              />
            </div>
            <Link to={`/u/${profile._id}`}>
              <div className="dropdown-top-profle">
                <h3>{ profile.name}</h3>
                <p>my profile</p>
              </div>
            </Link>
          </div>
          <ul className="header-dropdown-body">
            <li className="li__cico">
              <i class="fa-solid fa-clipboard-check"></i>
              {(profile.company && cico.length > 0 && new Date(Date.now()).getHours() >= 8 &&  // Kiểm tra user có gia nhập company và danh sách cico lớn hơn 0, kiểm tra thời điểm hiện tại có quá 8h sáng không
                (   (new Date(Date.now())).getDate() -(new Date(cico[0]?.createdAt)).getDate() >= 1 ) ? // Kiểm tra xem cico mới nhất là hôm nay hay là các hôm trước
                <div className='cico_noti'> {/* Nếu là các hôm trước thì sẽ hiện thông báo Check In cho hôm nay */}
                  <Link to={`/c/cico/${profile.company && (profile.id || profile._id)}`}>Check In</Link>
                </div>
                : (!cico[0]?.checkOut && (Date.now() - (new Date(cico[0]?.createdAt)).getTime() >= 3600000*9 ) ? //Nếu không (cico mới nhất là hôm nay(đã checkIn)), kiểm tra có checkOut hay không nếu không có và nếu thời gian hiện tại trừ đi thời gian checkIn là 9 tiếng thì
                <div className='checkin_noti'> {/* Hiển thị noti checkout */}
                  <Link to={`/c/cico/${profile.id || profile._id}`}>Check out</Link>
                </div>
                : <div></div> //Nếu không (đã checkIn, checkOut), thì không hiện gì.
              ))}
              {cico.length == 0 && 
                <div className="noti__cico">
                  <Link to={`/c/cico/${profile.company && (profile.id || profile._id)}`}>Check In first</Link>
                </div>
              }
            </li>
            <li>
              <Link to="/noti">
                <i class="bx bx-bell"></i>
                <p>Notifications</p>
                <p className="noti__count">{profile.noti && profile.noti.length > 0 ? profile.noti.filter(x => !x.seen).length : 0}</p>
              </Link>
            </li>
            <li  onClick={hangleLogOut}>
              <i class="bx bx-log-out"></i>
              <p>Log out</p>
            </li>
          </ul>
        </div>
      )}
    </header>
  );
};

export default Header;
