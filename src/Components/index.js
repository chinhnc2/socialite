import Header from './header'
import Footer from './footer'
import SiderBar from './siderBar'
import SideBarNew from './sideBarNew'
import Table from './table'
import Modal from './modal'
import Title from './title'
import HeaderSearch from './HeaderSearch'
import Loading from './loading'
import Comment from './comment';
import CardPost from './cardPost';
import CommentInput from './commentInput';
import PostCreate from './PostCreate/index.jsx';
import EnterPost from './PostCreate/modal.jsx';
import Stories from './stories/index.jsx';
import Related from './related/index.jsx'
import ListFriendOnl from './listFriendOnl'
import CardPostSingle from './cardPostSingle'
import TimeSince from './timeSince'
import CreateStory from './stories/modal.jsx'
import NotiPage from './notiPage'

import {
    FormRadio,
    FormSelect,
    FormInput,
    FormUpload,
    FormEditor,
    FormNumber,
    FormUploadImage,
    FormTextArea,
    FormDatePicker,
    FormTimePicker,
    FormAutocomplete,
    FormInputNumber,
    FormLabel,
    FormTreeSelect,
    FormUploadDD,
    FormCheckbox,
    FormRangePicker,

} from './form'

export {
    Header,
    Footer,
    SiderBar,
    PostCreate,
    SideBarNew,
    Related,
    Table,
    Stories,
    EnterPost,
    Modal,
    Title,
    Comment, //new
    CommentInput, //new
    CardPost, //new
    CardPostSingle, // new
    TimeSince, // new
    CreateStory, // new
    NotiPage, //new
    ListFriendOnl,
    HeaderSearch,
    Loading,
    FormRadio,
    FormSelect,
    FormInput,
    FormUpload,
    FormEditor,
    FormNumber,
    FormUploadImage,
    FormTextArea,
    FormDatePicker,
    FormTimePicker,
    FormAutocomplete,
    FormInputNumber,
    FormLabel,
    FormTreeSelect,
    FormUploadDD,
    FormCheckbox,
    FormRangePicker
}
