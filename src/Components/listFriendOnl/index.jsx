import React from 'react';
import './listFriendOnl.scss';
import { useHistory, Link } from "react-router-dom";
const ListFriendOnl = ({ friends }) => {
    return(
        <div className="ListFriendOnl">
            { friends && friends.length && friends.length > 0 && friends.map(x =>
            (
                <Link to={ `/u/${x._id}` || "/"}>
                    <div className="each_friend">
                        <div className="avt_friend">
                            <img src={ x.avatar || 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg'} alt="avt_friend" />
                        </div>
                        <div className="name_friend">
                            <p>{x.name}</p>
                        </div>
                    </div>
                </Link>
               
            )
            )}
        </div>
    )
}
export default ListFriendOnl