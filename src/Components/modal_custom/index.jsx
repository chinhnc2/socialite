import React, { useEffect, useRef } from 'react';
import './ModalCustom.scss';
const ModalCustom = ({ children, func }) => {
    const modalRef = useRef(null);

    const handleClickOutSide = (e) => {
        if (modalRef.current && !modalRef.current.contains(e.target)) {
            func(false);
        }
    }

    useEffect(() => {
        document.addEventListener("click", handleClickOutSide, false);
        return () => document.removeEventListener("click", handleClickOutSide, false);
    })
    return(
        
        <div className="ModalCustom">
            <div className="modalContentChild" ref={modalRef} >
                {children}
            </div>
        </div>

    )
}
export default ModalCustom