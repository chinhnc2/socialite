import React, { useState, useRef } from 'react'
import { useAuth, useProfile } from "Hooks"
import { useHistory } from "react-router-dom";
import { TimeSince, Header } from "Components";
import './styled.scss'


function NotiPage() {
    const [isShowNotifications, setIsShowNotifications] = useState(false);
    const [showDropdown, setShowDropdown] = useState(false);
    const [checkUnseenNoti, setCheckUnseenNoti] = useState(true);
    const [showBarNew, setShowBarNew] = useState(false);
    const { allowRequestFriendAction, denyRequestFriendAction } = useProfile();
    const { profile } = useAuth();
    let history = useHistory();
    
    const unseenRef = useRef()
    const seenRef = useRef()

    const handleShowHideDropdown = () => {
      setShowDropdown(!showDropdown);
    };

    const handleHideDropdown = () => {
      setShowDropdown(false);
    };

    const handleShowHideBarNew = () => {
      setShowBarNew(!showBarNew)
    }
  

    const handleEachNoti = (noti) => {
        if (noti.type === 'post') {
          history.push({
            pathname: `/post/${noti.from_id}`,
            state: { 
              notiFromId: noti.from_id
            }
          });
        } else {
          history.push("/")
        }
      }

    return (
        <div>
          <Header 
            showDropdown={showDropdown}
            handleShowHideDropdown={handleShowHideDropdown}
            handleShowHideBarNew={handleShowHideBarNew}
          />
          <div className='notifications_board'>
            <div className="header_noti">
                <div onClick={() => setCheckUnseenNoti(true)} className={checkUnseenNoti && "active"}><p>Unseen</p></div>
                <div onClick={() => setCheckUnseenNoti(false)} className={!checkUnseenNoti && "active"}><p>Seen</p></div>
            </div>
            <div className="list_noti">
                {profile.noti && profile.noti.length > 0 && checkUnseenNoti ? profile.noti.filter(x => !x.seen).reverse().map(element => (
                <div className='each_noti' key={element._id} onClick={() => handleEachNoti(element)}>
                    {element.type === 'friend' ? <i className='bx bxs-user'></i> : <i className='bx bx-comment-detail'></i>}
                    <div className="main_each_noti">
                    <p>{element.content}</p>
                    <p>{TimeSince(new Date(element.createdAt)) || 'now'}</p>
                    {element.type === 'friend' && 
                        <div className='request_add_friend'>
                        <button onClick={() => allowRequestFriendAction(element)}>Accept</button>
                        <button onClick={() => denyRequestFriendAction(element)}>Deny</button>
                        </div>}
                    </div>
                </div>)) : 
                profile.noti && profile.noti.length > 0 && profile.noti.filter(x => x.seen).reverse().map(element => (
                <div className='each_noti' key={element._id} onClick={() => handleEachNoti(element)}>
                    {/* {element.type == 'friend' && <img src={} />} */}
                    {element.type === 'friend' ? <i className='bx bxs-user'></i> : <i className='bx bx-comment-detail'></i>}
                    <div className="main_each_noti">
                        <p>{element.content}</p>
                        <p>{TimeSince(new Date(element.createdAt)) || 'now'}</p>
                    </div>
                </div>
                ))}
            </div>
            </div>
        </div>
    )
}

export default NotiPage