/* eslint-disable array-callback-return */
import React, { useEffect } from 'react'
import styled from 'styled-components'
import { useLocation } from "react-router-dom";
import { CardPostSingle } from "Components";
import { useHome, usePost } from "Hooks";



const PostScreen = styled.div`
  width: 75%;
  padding: 0px 50px;
  top: 25px;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  overflow: scroll;
  border-radius: 14px;
  position: absolute;
  left: 20%;
  height: 100vh;

  @media only screen and (max-width: 480px) {
    width: 100%;
    padding: 0px 20px;
    left: 0;
  } 

`

const ContentCenter = styled.div`
  width: 100%;
  position: relative;
  top: 0;
  left: 0;
  padding-right: 25px;
  padding-bottom: 100px;
  height: fit-content;

  @media only screen and (max-width: 480px) {
    padding-right: 0;
  } 
`;

const PostSingle = () => {
    let location = useLocation();
    const { getPostIdAction } = usePost();
    const { notiFromId } = location.state;
    
    useEffect(() => {
      getPostIdAction(notiFromId);
    }, [notiFromId])

    const { postSingle } = useHome();
    useEffect(() => {
        console.log("postSingleeeeeeeee", postSingle)
    }, [postSingle])

    return (
        <PostScreen>
            <ContentCenter>
                {postSingle && <CardPostSingle pSin={postSingle} notiFromId={notiFromId}/>}
            </ContentCenter>
        </PostScreen>
    )
}

export default PostSingle