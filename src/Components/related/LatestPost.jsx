import React from "react";
import "./LatestPost.scss";
const LatesPost = ({arrContainPhotoLatePost}) => {
  const arrLatePost = arrContainPhotoLatePost.slice(0,4);
    
  return (
    <div id="Latespost">
      <div className="Latespost-top">
        <h3>Lates post</h3>
        <p>See all</p>
      </div>
      <div className="Latespost-list">
        {
            arrLatePost && arrLatePost.map( x =>(
                <div className="Latespost-item">
                    <img
                        src={x.post_content}
                        alt=""
                    />
                </div>
            ))
        }
      </div>
    </div>
  );
};

export default LatesPost;
