import React from "react";
import LatestPost from "./LatestPost";
import "./related.scss";
import {  usePost , useHome   } from "Hooks"
const Related = ({posts}) => {
//   const { posts } = usePost();
  let arrContainPhotoLatePost = posts.filter(post => post.isPhoto);
  arrContainPhotoLatePost.sort(function(a, b) {
    const c = new Date(a.createdAt);
    const d = new Date(b.createdAt);
    return d-c;
  });

  return (
    <div id="related">
        {
            arrContainPhotoLatePost && 
             <LatestPost arrContainPhotoLatePost={arrContainPhotoLatePost}  />
        }
    </div>
  );
};
export default Related;
