/* eslint-disable react/prop-types */
import React, { useEffect } from "react";
import { Route, Redirect } from "react-router-dom";

import HomeLayout from "Layouts/home";
import { useAuth } from "Hooks";
import { history } from 'Stores/reducers';

function PublicRoute({
  component: Component,
  layout: Layout = HomeLayout,
  ...rest
}) {
  const { authenticated } = useAuth();

  console.log("authenticated ->", authenticated);

  // const requireAuth = (nextState, replace) => {
  //   if (!authenticated) {
  //     replace({
  //       pathname: '/auth/login',
  //       state: { nextPathname: nextState.location.pathname}
  //     })
  //   }
  // }
  

  return (
    <Route
      {...rest}
      render={(props) =>
        !authenticated ? (
          <Layout>
            <Component {...props} />
          </Layout>
        ) : (
          <Redirect

            to={{ pathname: history.location.state ? history.location.state.from.pathname : '/'  , state: { from: props.location } }}
          />
        )
      }
    />
  );
}

export default PublicRoute;
