import React, { useState, useEffect, useRef } from "react";
import "./sideBar.scss";
import { Link } from "react-router-dom";
import { useForm, FormProvider } from "react-hook-form";
import { useAuth } from "Hooks"
import { FormInput } from "Components";


const SideBarNew = ({ showBarNew, setShowBarNew }) => {
  const { searchUserAction, profile, userQueries } = useAuth();
  const [isShowSearchBoard, setIsShowSearchBoard] = useState(false);
  const [deviceSize, setDeviceSize] = useState(window.innerWidth);
  

  useEffect(() => {
    const resizeW = () => setDeviceSize(window.innerWidth);

    window.addEventListener("resize", resizeW); // Update the width on resize

    return () => window.removeEventListener("resize", resizeW);
  });

  useEffect(() => {
    const changeBgColor = () => {
      let barNew = true;
      if (deviceSize === 768) {
        barNew = true;
      } else if (deviceSize <= 480) {
        barNew = false;
      }
  
      setShowBarNew(barNew);
    }
    changeBgColor()
  }, [deviceSize])

  const onHandleChangeSearch = (data) => {
    if (data.queryString.length > 0) {
      searchUserAction(data);
    }
    return data;
  }

  const form = useForm({
    resolver: onHandleChangeSearch ,
    mode: "onChange"
  });
  const { handleSubmit, reset } = form;

  const search_boardRef = useRef(null);
  const handleClickOutSearchBoard = (e) => {
    
    if (search_boardRef.current && !search_boardRef.current.contains(e.target)) {
        setIsShowSearchBoard(false);
    }
}

  useEffect(() => {
    document.addEventListener("click", handleClickOutSearchBoard, false);
    return () => document.removeEventListener("click", handleClickOutSearchBoard, false);
  })

  return (
    <>
      {showBarNew && (
        <div className="SideBarNew">
        {/* <div className='header_side_bar'>
                  <div className='img_side_bar'>
                      <img src="http://demo.foxthemes.net/instellohtml/assets/images/avatars/avatar-2.jpg" alt="avt" />
                  </div>
                  <div className='name_user'><p>
                      'KAKAAKAKA'
                  </p></div>
                  <div className='name_company'>
                      <p>B.A.P</p>
                  </div>
              </div> */}
          <div className="nav_side_bar">
            <div className="top-bar-search" ref={search_boardRef}>
              <box-icon name="search" color="#9ca3af" style={{marginLeft: '8px'}}></box-icon>
              <FormProvider {...form}>
                <form onSubmit={handleSubmit()} >
                  <FormInput
                    name='queryString'
                    onFocus={() => setIsShowSearchBoard(true) }
                    // onBlur={() => setIsShowSearchBoard(false)}
                    // onChange={onHandleChangeSearch}
                    type="text"
                    className="top-bar-search"
                    placeholder="Search..."
                  />
                </form>
              </FormProvider>
              
            </div>
            {isShowSearchBoard && (
              <div className="search_user_board" >
                  {userQueries && userQueries.length > 0 && userQueries.map(x => 
                    <div className="each_user_search">
                      <Link to={`/u/${(x._id || x.id)}`}><p>{x.name}</p></Link>
                    </div>
                  )}
              </div>
            )}
            <Link to="/" className="item">
              <div className="icon_item">
                <i className="bx bxs-home"></i>
              </div>
              <div className="name_item">
                <p style={{ color: "#eb2f96" }}>Feed</p>
              </div>
            </Link>
            <Link to="/chat" className="item">
              <div className="icon_item">
                <i className="bx bxs-conversation"></i>
              </div>
              <div className="name_item">
                <p style={{ color: "#0d5ccb" }}>Chats</p>
              </div>
            </Link>
            <Link to="/stores" className="item">
              <div className="icon_item">
                <i className="bx bxs-store"></i>
              </div>
              <div className="name_item">
                <p style={{ color: "#891ac4" }}>Marketplace</p>
              </div>
            </Link>
            <Link to={profile && profile.company && profile.company._id ? `/c/nf/${profile.company._id}` : '/c/source' } className="item">
              <div className="icon_item">
                <i className="bx bxs-group"></i>
              </div>
              <div className="name_item">
                <p style={{ color: "#891ac4" }}>Company</p>
              </div>
            </Link>
            <Link to="/nft-game" className="item">
              <div className="icon_item">
                <i class='bx bx-game'></i>
              </div>
              <div className="name_item">
                <p style={{ color: "#891ac4" }}>GameNFT</p>
              </div>
            </Link>
          </div>
            {/* <div className="logout_btn">
              <button>Log Out</button>
            </div> */}
      </div>
      )}
    </>
  );
};
export default SideBarNew;
