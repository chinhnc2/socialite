import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import StoryReel from "Components/storyReel";
import { useHistory } from "react-router-dom";
import { useAuth, useHome } from "Hooks";
import "./Stories.scss";

// const breakPoints = [
//   { width: 1, itemsToShow: 9 },
//   { width: 320, itemsToShow: 2, itemsToScroll: 1 },
//   { width: 300, itemsToShow: 5 },
//   { width: 200, itemsToShow: 5 },
// ];

const Stories = ({ handleShowAddStory }) => {
  const { profile } = useAuth()
  const { stories } = useHome();

  const [addStory, setAddStory] = useState(false)
  let history = useHistory();
  
  return (
    <div id="stories">
      <div className="carousel-wrapper">
        <Carousel itemsToShow={4} className="carousel-wrapper_all">
          <div className="story-item" onClick={handleShowAddStory}>
            <div className="story-img">
              <img src={profile.avatar} alt="" />
            </div>
            <div className="story-name">
              <p>Bạn</p>
            </div>
            <div className="story-add">+</div>
          </div>

          {stories.filter((value, index, self) => 
            index === self.findIndex((t) => (
              t.user_id === value.user_id
            ))
          ).map(story => (
            <div 
              className="story-item" 
              key={story._id}
              onClick={() => history.push({
                pathname: `/storyreel`,
                state: {  // location state
                  story: story,
                  stories: stories
                },
              })}
            >
              <div className="story-img">
                <img src={story.user.avatar} alt="" />
              </div>
              <div className="story-name">
                <p>{story.user.name}</p>
              </div>
            </div>
          ))
          }
        </Carousel>
      </div>
    </div>
  );
};

export default Stories;
