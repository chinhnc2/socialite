import React, { useState, useCallback, useEffect } from "react";
import { usePost, useAuth } from "Hooks";
import { useForm, FormProvider, Controller } from "react-hook-form";
import uploadCloudinary from 'Utils/cloudinary';
import { FormTextArea, FormInput } from "Components";
import styled from 'styled-components';
import "./modalStyled.scss"

const FormUpload = styled.form`
    width: 100%;
    height: 100%;
    margin-bottom: 8px;
    text-align: center;
`

const CreateStory = ({ handleShowAddStory }) => {
    const [addText, setAddText] = useState(true)
    const [addImg, setAddImg] = useState(false)
    const [bgText, setBgText] = useState("#000")
    const [imgURL, setImgURL] = useState(null);

    const { createStoryAction } = usePost()

    const form = useForm();
    const { profile } = useAuth();
    const { handleSubmit, reset, control } = form;

    const handleShowText = () => {
        setAddText(true)
        setAddImg(false)
    }

    const handleShowImg = () => {
        setAddText(false)
        setAddImg(true)
    }

    const onSubmit = useCallback((data) => {
        createStoryAction({ content : data.content , color: bgText, user_id : profile._id || profile.id });
        reset();
        setAddImg(!addImg)
    }, [bgText]);

    const onSubmitUpload = async data => {
        let resURL = await uploadCloudinary(imgURL);
        if (resURL) {
            createStoryAction({ isPhoto: true, content: resURL, user_id: (profile.id || profile._id) })
        }
        reset();
        setImgURL(!imgURL)
    }

    const handleChangeBgColor = (e) => {
        setBgText(e.target.value);
    }

    const handleChangeFile = (e) => {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function (e) {
            setImgURL(reader.result);
        };
    }

    useEffect(() => {

    }, [bgText])

    return (
        <div className="modal">
            <div>
                <div className="modal__menu">
                    <h4 onClick={handleShowText} className={addText && "active"}>Add Text</h4>
                    <h4 onClick={handleShowImg} className={addImg && "active"}>Add Img</h4>
                    <i className="bx bx-x" onClick={() => handleShowAddStory()}></i>
                </div>
                {addText && (
                    <div className="modal__text">
                        <FormProvider {...form}>
                            <form className="modal__text-content" onSubmit={handleSubmit(onSubmit)} >
                                <div className="modal__text-body">
                                    <div className="modal__text-avt">
                                        <img
                                            src={profile.avatar}
                                            alt="avatar"
                                        />
                                        <p className="modal__text-name">{profile.name}</p>
                                    </div>
                                    <div className="modal__text-input">
                                        {/* <input type="text" placeholder="What's your msind?"/> */}
                                        <FormTextArea
                                            style={{backgroundColor: `${bgText}`, color: '#fff' }}
                                            name="content"
                                            required
                                            cols="30"
                                            rows="6"
                                            placeholder="What's your mind?"
                                            spellCheck="false"
                                            // ref={inputRef}
                                        ></FormTextArea>
                                    </div>
                                    <div className="modal__text-color">
                                        <h2>Choose your background color</h2>
                                        
                                        <FormInput
                                            value={bgText}
                                            name="color"
                                            type='color'
                                            placeholder="Select your background color"
                                            onChange={(e) => handleChangeBgColor(e)}
                                            // list="data1"
                                        />
                                    </div>
                                </div>
                                <div className="modal__text-footer">
                                    <button onClick={() => handleShowAddStory()} className="btn-danger">
                                        Cancel
                                    </button>
                                    <input type="submit" className="btn btn-success"/>
                                    {/* <button>Share</button> */}
                                </div>
                            </form>
                        </FormProvider>
                    </div>
                )}

                {addImg && (
                    <div className="modal__img">
                        <FormUpload onSubmit={handleSubmit(onSubmitUpload)}>
                            <div className="modal__text-avt">
                                <img
                                    src={profile.avatar}
                                    alt="avatar"
                                />
                                <p className="modal__text-name">{profile.name}</p>
                            </div>
                            <div className="btn__upload">
                                <Controller
                                    name="media"
                                    control={control}
                                    render={({ field }) => {
                                        // sending integer instead of string.
                                        return <input type='file'  name='media'
                                        {...field} onChange={(e) => handleChangeFile(e)} />;
                                    }}
                                />  
                                <i className="fa fa-arrow-up"></i>
                            </div>
                            {imgURL && <input type='submit' className="btn btn-success" /> }
                        </FormUpload>       
                        <div className="upload__img">
                            {imgURL && <img src={imgURL} alt='img_temp' />}
                        </div>
                    </div>
                )}
            </div>
        </div>
    )
}


export default CreateStory