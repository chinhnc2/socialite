import React, { useState, useEffect, useRef } from 'react';
import './styled.scss'
import { Input } from 'antd'
import { SendOutlined } from '@ant-design/icons'
import { useHistory, useLocation } from "react-router-dom";
import { useAuth } from "Hooks";
import {
    animated,
    useSpring,
    SpringValue
} from '@react-spring/web'
import { Header, TimeSince } from 'Components'
import { info } from 'sass';

const StoryReel = () => {
    const [showPrev, setShowPrev] = useState(true);
    const [showNext, setShowNext] = useState(true);
    
    const location = useLocation();
    const { story, stories } = location.state
    const [showStory, setShowStory] = useState(story);

    const { profile } = useAuth();

    const styles = useSpring({
        loop: { reverse: false },
        config: { duration: 5000 },
        from: { width: '0%' },
        to: { width: '100%' },
        reset: true,
    })   


    console.log("storyyyyyy", story)
    // console.log("storiesssssssss", stories)

    let history = useHistory();

    const handleShowHideStoryReel = () => {
        history.push("/");
    } 

    const handleChangeStory = (story) => {
        setShowStory(story)
    } 

    return (
        <div>
            <Header
                /* showDropdown={showDropdown}
                handleShowHideDropdown={handleShowHideDropdown} */
            />
            <div className='story'>
                <div className="story__left">
                    <div className="story__left-title">
                        <h2>All Story</h2>
                        <span>Setting</span>
                    </div>
                    <div className="story__left-all">
                        <h3>your story</h3>
                        <div className="story__left-create">
                            {stories.map(story => {
                                if ((story.user._id || story.user.id) == (profile._id || profile.id)) {
                                    return (
                                        <div className="story__left-friend" key={story.id || story._id} onClick={() => handleChangeStory(story)}>
                                            <div className="story__left-friend--ava">
                                                <img src={story.user.avatar} alt="" />
                                            </div>
                                            <div className="story__left-friend--text">
                                                <h5>{story.user.name}</h5>
                                                <p>
                                                    <span className="story__left-friend--time">{TimeSince(new Date(story.createdAt))}</span>
                                                </p>
                                            </div>
                                        </div>
                                    )
                                }
                            })}
                        </div>
                        <h3>Friends Story</h3>
                        {stories.map(story => (
                            <div className="story__left-friend" key={story.id || story._id} onClick={() => handleChangeStory(story)}>
                                <div className="story__left-friend--ava">
                                    <img src={story.user.avatar} alt="" />
                                </div>
                                <div className="story__left-friend--text">
                                    <h5>{story.user.name}</h5>
                                    <p>
                                        <span className="story__left-friend--time">{TimeSince(new Date(story.createdAt))}</span>
                                    </p>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="story__right">
                    <div className="story__right-close" onClick={handleShowHideStoryReel}>
                        <i className="fa-solid fa-xmark" />
                    </div>

                    {showStory.isPhoto ? (
                        <div className="story__right-reel">
                            <div className="story__right-img">
                                <img src={showStory.content} alt="" />
                            </div>

                            <div className="story__right-time">
                                <animated.div 
                                    className="story__right-active" 
                                    style={{
                                        height: '100%',
                                        width: '100%',
                                        backgroundColor: '#fff',  
                                        ...styles
                                    }}
                                />
                            </div>

                            <div className="story__right-info">
                                <div className="story__right-img">
                                    <img src={showStory.user.avatar} alt="avatar" />
                                </div>
                                <div className="story__right-name">
                                    <h4>{showStory.user.name}</h4>
                                    <span>{TimeSince(new Date(story.createdAt))}</span>
                                </div>
                            </div>
                        </div> 
                    ) : (
                        <>
                            <div className="story__right-reel">
                                <div className="story__right-text" style={{backgroundColor: `${showStory.color}`}}>
                                    <p style={{color: "#fff"}}>
                                        {showStory.content} 
                                    </p>
                                </div>

                                <div className="story__right-time">
                                    <animated.div 
                                        className="story__right-active" 
                                        style={{
                                            height: '100%',
                                            width: '100%',
                                            backgroundColor: '#fff',  
                                            ...styles
                                        }}
                                    />
                                </div>

                                <div className="story__right-info">
                                    <div className="story__right-img">
                                        <img src={showStory.user.avatar} alt="avatar" />
                                    </div>
                                    <div className="story__right-name">
                                        <h4>{showStory.user.name}</h4>
                                        <span>{TimeSince(new Date(showStory.createdAt))}</span>
                                    </div>
                                </div>
                            </div> 
                        </>
                    )}

                    <div className="story__right-mess">
                        <Input
                            style={{backgroundColor: '#fff'}}
                            className="story__right-input"
                            type="text"
                            placeholder="Trả lời..."
                            suffix={
                                <SendOutlined 
                                    style={{cursor: 'pointer', fontSize: '16px'}}
                                />
                            }                        
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default StoryReel