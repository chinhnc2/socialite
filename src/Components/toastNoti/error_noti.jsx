import { notification } from "antd";


const errorNotification = (noti) => {
  console.log(noti);
    notification.open({
      message: noti.type,
      description:
        noti.content.message,
      placement: 'bottomRight',
      style: {
        width: 500,
        backgroundColor: "#ca1637",
        color: "white"
      }
    });
  };

export default errorNotification