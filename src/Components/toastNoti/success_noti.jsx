import { notification } from "antd";


const successNotification = (noti) => {
    if (noti.from_id)
    notification.success({
      message: noti.type,
      description:
        noti.content,
      placement: 'bottomRight',
      style: {
        width: 500,
        backgroundColor: "#55e824",
        color: "white"
      }
    });
  };

export default successNotification