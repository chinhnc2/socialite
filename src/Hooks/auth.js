/* eslint-disable no-restricted-globals */
import { useDispatch, useSelector } from 'react-redux'

import saga from 'Modules/auth/store/saga'
import reducer from 'Modules/auth/store/reducer'
import { loadProfile, login, register, logout, getMyNoti, checkNoti, queryUsers } from 'Modules/auth/store/actions'
import { useInjectSaga, useInjectReducer } from 'Stores'
import { USER_ROLE } from 'Constants/auth'
import { makeSelectAuthentication } from 'Modules/auth/store/selectors'

export const useAuth = () => {
  useInjectSaga({ key: 'auth', saga })
  useInjectReducer({ key: 'auth', reducer })

  const { isLoading, error, authenticated, profile, newNoti, userQueries } = useSelector(
    makeSelectAuthentication()
  )

  const dispatch = useDispatch()
  
  const loadProfileAction = () => dispatch(loadProfile());
  
  const loginAction = (payload) => dispatch(login(payload));

  const logoutAction = (payload) => dispatch(logout(payload));

  const registerAction = (payload) => dispatch(register(payload));

  const searchUserAction = (payload) => dispatch(queryUsers(payload));

  return {
    isLoading,
    error,
    authenticated,
    profile,
    newNoti,
    userQueries,
    //metaData,
    loadProfileAction,
    loginAction,
    logoutAction,
    registerAction,
    searchUserAction
  }
}

export const useNoti = () => {
  useInjectSaga({ key: 'auth', saga })
  useInjectReducer({ key: 'auth', reducer });

  const dispatch = useDispatch();

  const getMyNotifyAction = (payload) => dispatch(getMyNoti(payload));
  const checkNotifyAction = (payload) => dispatch(checkNoti(payload));
  
  return {
    getMyNotifyAction,
    checkNotifyAction,
  }
}
