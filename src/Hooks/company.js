/* eslint-disable no-restricted-globals */
import { useDispatch, useSelector } from 'react-redux'

import saga from 'Modules/company/store/saga'
import reducer from 'Modules/company/store/reducer'

import { useInjectSaga, useInjectReducer } from 'Stores'
import { makeSelectCompany } from 'Modules/company/store/selectors'
import { checkinCompany, checkOutCompany, getAllCompany, getCICO, getCompany, requestToCompany, searchCompany, getAllCICO, allowJoinInCompany, updateAvatarCompany } from 'Modules/company/store/action'



export const useCompany = () => {
    useInjectSaga({ key: 'company', saga })
    useInjectReducer({ key: 'company', reducer });

    const dispatch = useDispatch();

    const { isLoading, error, allCompany, company, queryCompany, cico, allCICO } = useSelector(
        makeSelectCompany()
      )

    const getAllCompanyAction = () => dispatch(getAllCompany());
    
    const getCompanyAction = (payload) => dispatch(getCompany(payload));

    const searchCompanyAction = (payload) => dispatch(searchCompany(payload));

    const joinInCompanyAction = (payload) => dispatch(requestToCompany(payload));

    const getCICOAction = (payload) => dispatch(getCICO(payload));
    const checkInAction = (payload) => dispatch(checkinCompany(payload));
    const checkOutAction = (payload) => dispatch(checkOutCompany(payload));
    const updateAvatarCompanyAction = (payload) => dispatch(updateAvatarCompany(payload))
    const getAllCICOAction = (payload) => dispatch(getAllCICO(payload))
    const allowJoininCompanyAction = (payload) => dispatch(allowJoinInCompany(payload))



    return {
        isLoading,
        error,
        allCompany,
        company,
        queryCompany,
        cico,
        allCICO,
        getAllCompanyAction,
        getCompanyAction,
        searchCompanyAction,
        joinInCompanyAction,
        getCICOAction,
        checkInAction,
        checkOutAction,
        getAllCICOAction,
        updateAvatarCompanyAction,
        allowJoininCompanyAction
    }
}