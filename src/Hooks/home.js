import { useDispatch, useSelector } from 'react-redux'
import saga from 'Modules/home/store/saga'
import reducer from 'Modules/home/store/reducer'
import { loadPost, loadComment, createPost, editPost, delPost, likePost, createComment, getPostId, createStory, loadStory, getPostInCompany, delComment, unmountedPost } from 'Modules/home/store/actions'
import { useInjectSaga, useInjectReducer } from 'Stores'
//import { USER_ROLE } from 'Constants/auth'
import { makeSelectPosts } from 'Modules/home/store/selectors'


export const useHome = () => {
  useInjectSaga({ key: 'home', saga })
  useInjectReducer({ key: 'home', reducer })

  const dispatch = useDispatch()
    
  const { isLoading, error, posts, postSingle, stories } = useSelector(
    makeSelectPosts()
  )
  return {
    isLoading,
    error,
    posts,
    postSingle,
    stories
    //metaData,
  }
}
export const usePost = () => {
    useInjectSaga({ key: 'home', saga })
    useInjectReducer({ key: 'home', reducer });
    const dispatch = useDispatch()
    const loadPostAction = (payload) => dispatch(loadPost(payload));
    const getPostIdAction = (payload) => dispatch(getPostId(payload));
    const createPostAction = (payload) => dispatch(createPost(payload));
    const editPostAction = (payload) => dispatch(editPost(payload)); //payload là 1 post với content là mới.
    const deletePostAction = (payload) => dispatch(delPost(payload));
    const likePostAction = (payload) => dispatch(likePost(payload));
    const createCommentAction = (payload) => dispatch(createComment(payload));
    const deleteCommentAction = (payload) => dispatch(delComment(payload));
    const getPostInCompanyAction = (payload) => dispatch(getPostInCompany(payload));
    const createStoryAction = (payload) => dispatch(createStory(payload));
    const loadStoryAction = (payload) => dispatch(loadStory(payload));
    const unmountedPostAction = (payload) => dispatch(unmountedPost(payload));

    return {
        loadPostAction,
        createPostAction, 
        editPostAction,
        deletePostAction,
        likePostAction,
        createCommentAction,
        deleteCommentAction,
        getPostIdAction,

        getPostInCompanyAction,
        createStoryAction,
        loadStoryAction,
        unmountedPostAction
    }
}

