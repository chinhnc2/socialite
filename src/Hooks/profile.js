import { useDispatch, useSelector } from 'react-redux'
import saga from 'Modules/profile/store/saga'
import reducer from 'Modules/profile/store/reducer'
import { allowRequestFriend, denyRequestFriend, loadMyProfile, loadProfileUser, requestFriend, updateProfile } from 'Modules/profile/store/actions'
import { useInjectSaga, useInjectReducer } from 'Stores'
import { makeSelectProfiles } from 'Modules/profile/store/selector'
export const useProfile = () => {
    useInjectSaga({ key: 'profile', saga })
    useInjectReducer({ key: 'profile', reducer });
    const dispatch = useDispatch();
    const { isLoading, error, profileUser, request  } = useSelector(
        makeSelectProfiles()
    )
    const loadProfileUserAction = (payload) => dispatch(loadProfileUser(payload));
    const updateProfileAction = (payload) => dispatch(updateProfile(payload));
    const getMyProfileAction = (payload) => dispatch(loadMyProfile(payload));
    const sendRequestAddFriendAction = (payload) => dispatch(requestFriend(payload));
    const allowRequestFriendAction = (payload) => dispatch(allowRequestFriend(payload));
    const denyRequestFriendAction = (payload) => dispatch(denyRequestFriend(payload));
    return {
        isLoading,
        error,
        profileUser,
        request,
        loadProfileUserAction,
        updateProfileAction,
        getMyProfileAction,
        sendRequestAddFriendAction,
        allowRequestFriendAction,
        denyRequestFriendAction
    }
}