import React from 'react'
import styled from 'styled-components'
import { useRoot } from '../../Hooks'

import Nav from 'Modules/STORES/Nav/Nav'
import Holder from 'Modules/STORES/Holder/Holder'
import Footer from 'Modules/STORES/Footer/Footer'

const Wrapper = styled.div`
  
`
const Main = styled.main`
  
`
const Content = styled.div`
  
`

const Shop = ({ children }) => {
  const { sidebarCompact } = useRoot()

  return (
    <Wrapper>
      <Nav />
      <Holder />
      <Main>
        <Content>
          {children}
        </Content>
      </Main>
      <Footer />
    </Wrapper>
  )
}
export default Shop
