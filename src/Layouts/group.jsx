/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react'
import styled from 'styled-components'
import { Header, SideBarNew, ListFriendOnl } from "Components"

const Wrapper = styled.div`
  overflow: auto;
//   padding-left: ${({ sidebarCompact }) => (sidebarCompact ? '4rem' : '14rem')};
  margin: 0;
  display: flex;
  flex-direction: column;
  width: 100%;
  background: ${({ theme }) => theme.bg_primary_light};
  height: 100%;
`

const Main = styled.main`
  display: flex;
  display: flex;
  width: 100%;
  top: 70px;
  position: relative;
  overflow: hidden;
  height: calc(100% - 70px);
  padding: 0;
  background: ${({ theme }) => theme.bg_primary_light};
`

const Content = styled.div`
    width: 68%;
    padding: 0px 50px;
    top: 14px;
    display: flex;
    border-radius: 14px;
    position: absolute;
    left: 20%;
    //   background-color: #00ffff36;
    height: 100%;
    overflow: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
`

const friends = [
    {
      name: "thao",
      avt: "http://demo.foxthemes.net/socialitev2.2/assets/images/post/img-4.jpg",
    },
    {
      name: "thang",
      avt: "http://demo.foxthemes.net/socialitev2.2/assets/images/avatars/avatar-3.jpg",
    },
    {
      name: "nhi",
      avt: "http://demo.foxthemes.net/socialitev2.2/assets/images/avatars/avatar-6.jpg",
    },
    {
      name: "thao",
      avt: "http://demo.foxthemes.net/socialitev2.2/assets/images/post/img-4.jpg",
    },
  ];

const GroupLayout = ({ children }) => {
  const [showModal, setShowModal] = useState(false);  
  const [showDropdown, setShowDropdown] = useState(false);

  const handleShowHideModalEnterPost = () => {
    setShowModal(!showModal);
  };
  const handleShowHideDropdown = () => {
    setShowDropdown(!showDropdown);
  };
  const handleHideDropdown = () => {
    setShowDropdown(false);
  };

  return (
    <Wrapper>
      <Header
        showDropdown={showDropdown}
        handleShowHideDropdown={handleShowHideDropdown}
      />
      <Main>  
          <SideBarNew />
          <Content>
              {children}
          </Content>
          <ListFriendOnl friends={friends} />
      </Main>
    </Wrapper>
  )
}

export default GroupLayout