/* eslint-disable react/prop-types */
import React, { useState, useEffect, Children } from "react";
import styled from "styled-components";
import {
  Header,
  SiderBar,
  SideBarNew,
  Footer,
  Comment,
  CardPost,
  CommentInput,
  PostCreate,
  EnterPost,
  Stories,
  Related,
  ListFriendOnl,
} from "Components";
import "antd/dist/antd.css";
import { notification } from "antd";
// import { useRoot } from "Hooks";
import { useRoot, useHome, useNoti, usePost, useAuth , useProfile } from "Hooks";
import StoryReel from "../Components/storyReel"

const Wrapper = styled.div`
  overflow: auto;
  /* padding-left: 14rem; */
  margin: 0;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  position: relative;
  /* -webkit-flex-direction: column; */
  -ms-flex-direction: column;
  /* flex-direction: column; */
  width: 100%;
  background: #f1f1f1;
  height: 100%;
`;
const Main = styled.main`
  display: flex;
  width: 100%;
  top: 70px;
  position: relative;
  overflow: hidden;
  height: calc(100% - 70px);
  padding: 0;
  background: ${({ theme }) => theme.bg_primary_light};
  //   justify-content: center;
`;

const Friend = styled.div`
  position: absolute;
  border-radius: 14px;
  right: 0;
  overflow: hidden;
  top: 25px;
  background-color: blue;
  width: 5%;
`;


const HomeLayout = ({ children }) => {
  const [showModal, setShowModal] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const [showBarNew, setShowBarNew] = useState(false);

  const handleShowHideBarNew = () => {
    setShowBarNew(!showBarNew)
  }

  const { profileUser } = useProfile();
  const { profile} = useAuth();
  const handleShowHideModalEnterPost = () => {
    setShowModal(!showModal);
  };
  const handleShowHideDropdown = () => {
    setShowDropdown(!showDropdown);
  };
  const handleHideDropdown = () => {
    setShowDropdown(false);
  };

  // const { posts, newNoti, noti } = useHome();
  const { newNoti } = useAuth();
  
  const { getMyNotifyAction } = useNoti();

  //Toast Notifications
  const openNotification = (noti) => {
    if (noti.from_id) {
    notification.open({
      message: noti.type,
      description:
        noti.content,
      placement: 'bottomRight'
    });
  }
  };


  
  const friends = profile && profile.friends;
  console.log(friends);

  
  
  useEffect(() => {
    //console.log(newNoti);
    if (newNoti != null) {
      openNotification(newNoti)
    }
  }, [newNoti])
  
  useEffect(() => {
    if (window.location.reload) {
      console.log('trueeeeeeee')
    }
  }, [window.location.reload])
  // useEffect(() => {
  //   getMyNotifyAction();
  // }, [])
  const { sidebarCompact } = useRoot();
  return (
    <Wrapper sidebarCompact={sidebarCompact}>
      <Header
        showDropdown={showDropdown}
        handleShowHideDropdown={handleShowHideDropdown}
        handleShowHideBarNew={handleShowHideBarNew}
      />
      <Main onClick={handleHideDropdown}>
        <SideBarNew 
          showBarNew={showBarNew}
          setShowBarNew={setShowBarNew}
        />
          {children}
        <ListFriendOnl friends={friends} />
        {/* <StoryReel /> */}
      </Main>
    </Wrapper>
  );
};

export default HomeLayout;
