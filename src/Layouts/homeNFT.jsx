/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import styled from 'styled-components'
import { useRoot } from '../Hooks'
// import {Header , Footer , Menuleft} from 'Modules/NFT/layout'
import Header from 'Modules/NFT/layout/Header'
import Footer from 'Modules/NFT/layout/Footer'
import Menuleft from 'Modules/NFT/layout/Menuleft'
const Wrapper = styled.div`
  
`
const Main = styled.main`
 
`
const Content = styled.div`
  
`

const HomeNFT = ({ children }) => {


    const [search,setSearch] = useState("")

    console.log(search)
    const { sidebarCompact } = useRoot()
    return (
        <Wrapper>
            <Header setSearch={setSearch} search={search} />
            <Main>
                {/* <Menuleft /> */}
                <Content>
                    {React.cloneElement( children, { search } ) }
                    {/* React.cloneElement() */}
                    {/* {children(search)} */}
                </Content>
            </Main>
            <Footer />
        </Wrapper>
    )
}

export default HomeNFT
