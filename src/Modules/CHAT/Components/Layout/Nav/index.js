import React from "react";
import { Btn, NavWrapper } from "./styled";
import { BtnConversation } from "../../Pages/Sidebar/Footer/styled";
import AppProvider, { AppContext } from '../../../Context/AppProvider';
import { LinkStyled } from '../../Pages/Chat/ChatRoom/RoomList';
import { Link } from "react-router-dom";


export default function Navleft() {
  const { groups, setSelectedGroupId, setIsAddGroupVisible, selectedGroupId } = React.useContext(AppContext);

  const handleAddGroup = () => {
    setIsAddGroupVisible(true);
  };

  return (
    <NavWrapper>
      <nav className="scrollbar2">
        <ul>
          <div>
            <div className="logo">
              <Link to="/" >
                <BtnConversation title="Trang chủ" >
                  <img
                    className="logo-mini"
                    src="https://icones.pro/wp-content/uploads/2021/03/logo-discord-icone-png-rose.png"
                    alt="logo"
                    ></img>
                </BtnConversation>
              </Link>
            </div>
            <div style={{"margin": "-16px 0 0 10px", "height": "24px"}}>
              <i className="fa-solid fa-window-minimize" style={{"fontSize": "30px", "fontWeight": "100", color: "#40454f"}}></i>
            </div>
            <div className="conversation">
            {groups.map((group) => (
              <LinkStyled key={group.id} onClick={() => setSelectedGroupId(group.id)}>
              <Btn title= {group.name}>
                <span className="ellipsis">  
                  <i style={{fontSize: "22px"}}>{group.name}</i>
                </span>
              </Btn>
              </LinkStyled>
            ))}
              <Btn
                onClick={handleAddGroup}
                title="Thêm nhóm mới"
              >
                <span>                  
                <i className="fa-solid fa-circle-plus"></i>
                </span>
              </Btn>
            </div>
            <div className="add-conversation">
              <div className="item-conversation">
                <Link to="/nft-game">
                  <Btn title="Chơi game" >
                    <span>
                    <i className="fa-solid fa-gamepad"></i>
                    </span>
                  </Btn>
                </Link>
              </div>
            </div>
            <div className="search-conversation">
              <div className="item-conversation">
                <Link to="/store" >
                  <Btn title="Mua sắm" >
                    <span>
                    <i className="fa-solid fa-store"></i>
                    </span>
                  </Btn>
                </Link>
              </div>
            </div>
            <div style={{"margin": "-16px 0 0 10px", "height": "24px"}}>
                <i className="fa-solid fa-window-minimize" style={{"fontSize": "30px", "fontWeight": "100", color: "#40454f"}}></i>
            </div>
            <div className="dow-conversation">
              <div className="item-conversation">
                <Btn title="Tải ứng dụng" >
                  <span>
                    <i className="fa-solid fa-arrow-down"></i>
                  </span>
                </Btn>
              </div>
            </div> 
          </div>
        </ul>
      </nav>
    </NavWrapper>
  );
}