import styled from "styled-components";

export const NavWrapper = styled.div`
  background-color: #202225;
  display: flex;
  /* height: 100vh; */
  .scrollbar2::-webkit-scrollbar-track {
    display: none;
  }

  .scrollbar2::-webkit-scrollbar {
    display: none;
  }

  .scrollbar2::-webkit-scrollbar-thumb {
    display: none;
  }
  nav {
    height: 100vh;
    width: 72px;
    overflow-y: auto;  
    
    ul {
    margin-left: 12px; 
    /* width: 100%; */
    }
  }
`;

export const Btn = styled.button`
  width: 50px;
  height: 50px;
  margin-top: 10px;
  cursor: pointer;
  padding: 0;
  border-radius: 50%;
  background-color: #36393f;

  &:hover {
    span {
      background-color: #3ba55d;
      border-radius: 25%;
      color: white;
      i {
        color: #fff;
      }
    }

    img {
      background-color: #3ba55d;
      border-radius: 25%;
    }
  }

  img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 50%;
  }
  span {
    display: inline-block;
    /* text-align: center; */
    width: 50px;
    height: 50px;

    i {
      display: inline-block;
      margin-top: 15px;
      text-align: center;
      font-size: 22px;
      color: #3ba55d;
    }
  }
`;
