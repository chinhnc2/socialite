import { UserAddOutlined, SendOutlined, UploadOutlined, FileImageOutlined} from '@ant-design/icons';
import React, { useContext, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { Button, Tooltip, Avatar, Form, Input, Alert } from 'antd';

import Video from './UploadVideo';
import Message from './Message';
import Image from './Uploadfile';
import { AppContext } from '../../../../Context/AppProvider';
import { addDocument } from '../../../../firebase/services';
import useFirestore from '../../../../hooks/useFirestore';
import { ref, getDownloadURL, getStorage, uploadBytes } from "firebase/storage";

const HeaderStyled = styled.div`
  display: flex;
  justify-content: space-between;
  height: 56px;
  padding: 0 16px;
  align-items: center;
  border-bottom: 1.5px solid rgba(32, 34, 37, 0.8);

  .header {
    &__info {
      display: flex;
      flex-direction: column;
      justify-content: center;
      width: 100%;
    }

    &__title {
      margin: 0;
      font-weight: bold;
    }

    &__description {
      font-size: 12px;
    }
  }
`;

const ButtonGroupStyled = styled.div`
  display: flex;
  align-items: center;
`;

const WrapperStyled = styled.div`
  height: 100vh;
  /* width: 100%; */
  background-color: #36393f;
`;

const ContentStyled = styled.div`
  height: calc(100% - 56px);
  display: flex;
  flex-direction: column;
  padding: 12px;
  justify-content: flex-end;
`;

const FormStyled = styled(Form)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 20px !important;
  outline: none;
  height: 46px;
  background-color: #40444B;

  .ant-form-item {
    flex: 1;
    margin-bottom: 0;
  }
  .ant-input {
    border-radius: 20px !important;
    /* color: var(--text-light); */
    color: white;
  }
  button {
    background: #40444B;
    font-size: 1.2rem;
    height: 42px;
    border: none;
    border-radius: 20px !important;
  }
`;
const FormInput = styled.div`
  display: flex;

  .file-upload {
    display: flex;
    position: relative;
    background-color: #40444B;
    border-radius: 25px;
    height: 46px;
    max-width: 180px;
    min-width: 152px;
  }
  .line-upload {
    position: absolute;
    width: 10px;
    margin-top: 4px;
    left: 70px;
    i {
      display: inline-block;
      height: 41px;
      width: 10px;
    }
  }
  .form-video {
    position: absolute;
    right: 20px;
    width: 50px;
    font-size: 20px;
    margin-top: 2px;
  }
  .form-image {
    position: absolute;
    left: 0;
    width: 50px;
    font-size: 20px;
  }
    .file-input {
    width: 64px;
    background-color: #40444B;
    border-radius: 18px;
    margin-top: 2px;

    button {
      /* background-color: #ccc; */
      width: 22px;
      &:hover {
        color: white;
      }
    }

    
    .file-input__input {
      width: 0.1px;
      height: 0.1px;
      opacity: 0;
      overflow: hidden;
      position: absolute;
      z-index: -1;
    }
    
    .file-input__label {
      cursor: pointer;
      display: inline-flex;
      align-items: center;
      font-size: 18px;
      font-weight: 600;
      padding: 10px 12px;

      &:hover {
        color: white;
      }
    }
  /* } */
}
`;

const MessageListStyled = styled.div`
  max-height: 100%;
  overflow-y: auto;

`;

export default function ChatWindow({ profile }) {

  const { selectedRoom, members, setIsInviteMemberVisible } = useContext(AppContext);

  // const uid = profile.id
  const uid = profile._id, photoURL = profile.avatar , displayName = profile.name, like = 0 ;
  const [inputValue, setInputValue] = useState('');
  const [form] = Form.useForm();
  const inputRef = useRef(null);
  const messageListRef = useRef(null);
  const { TextArea } = Input;
  const storage = getStorage();

  //submit images

  const formHandler = (e) => {
    e.preventDefault();
    const fileImage = e.target[0].files[0];
    uploadFiles(fileImage);
  };

  const uploadFiles = async (fileImage) => {
    if (!fileImage) return;
    const sotrageRef = ref(storage, `files/${fileImage.name}`);
    await uploadBytes(sotrageRef, fileImage).then((snapshot) => {
      console.log('Uploaded!')
    });

    getDownloadURL(sotrageRef)
    .then((url) => {
      addDocument('images', {
      name: "fileName",
      files: url,
      uid,
      photoURL,
      roomId: selectedRoom.id,
      displayName,
      like
    })
      console.log('rul ne`: ', url)    
    })
    .catch((error) => {
      console.log('error')
    })
  }

  // End submit images

  //Start submit video
  const formHandlerVideo = (e) => {
    e.preventDefault();
    const fileVideo = e.target[0].files[0];
    uploadFileVideo(fileVideo);
  };

  const uploadFileVideo = async (fileVideo) => {
    if (!fileVideo) return;
    const sotrageRef = ref(storage, `files/${fileVideo.name}`);
    await uploadBytes(sotrageRef, fileVideo).then((snapshot) => {
      console.log('Uploaded!')
    });

    getDownloadURL(sotrageRef)
    .then((url) => {
      addDocument('videos', {
      name: "fileName",
      files: url,
      uid,
      photoURL,
      roomId: selectedRoom.id,
      displayName,
      like
    })
      console.log('url video ne`: ', url)    
    })
    .catch((error) => {
      console.log('error')
    })
  }

  //End submit video

  // Start submit messages
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
    console.log(`value: ` ,e.target.value);
  };
  
  const handleOnSubmit = async () => {
    if (!inputValue) return;
    //dk loc null input
    addDocument('messages', {
      text: inputValue,
      uid,
      photoURL,
      roomId: selectedRoom.id,
      displayName,
      like
    });
    form.resetFields(['message']);
    setInputValue('');

    
    // focus để nhập lại sau khi gửi
    if (inputRef?.current) {
      setTimeout(() => {
        inputRef.current.focus();
      });
    }
  };
  // End submit messages

  const condition = React.useMemo(
    () => ({
      fieldName: 'roomId',
      operator: '==',
      compareValue: selectedRoom.id,
    }),
    [selectedRoom.id]
  );

  const messages = useFirestore('messages', condition);
  const images = useFirestore('images', condition); //test up images
  const videos = useFirestore('videos', condition); //test up video
  
  useEffect(() => {
    // cuộn xuống cuối sau khi tin nhắn được thay đổi
    if (messageListRef?.current) {
      messageListRef.current.scrollTop =
      messageListRef.current.scrollHeight + 50;
    }
  }, [messages, images, videos]);


  //Start merged [ merged 2 component( mes, image ) để sắp xếp theo createdAt ]

  // console.log(messages[0].id)
  const toArr = [(messages.map((mes) => ( // gộp các component thành arr 
    <Message
      key={mes.id}
      text={mes.text}
      photoURL={mes.photoURL}
      displayName={mes.displayName}
      createdAt={mes.createdAt}
      uid={mes.uid}
      DocID={mes.id}
      like={mes.like}
    />
  ))),
  (videos.map((video) => (
    <Video
      key={video.id}
      photoURL={video.photoURL}
      displayName={video.displayName}
      createdAt={video.createdAt}
      files={video.files}
      nameFile={video.nameFile}
      DocID={video.id}
      like={video.like}
    />
  ))),
  (images.map((image) => (
    <Image 
    key={image.id}
    files= {image.files}
    nameFile= {image.name}
    createdAt={image.createdAt}
    photoURL={image.photoURL}
    displayName={image.displayName}
    DocID={image.id}
    like={image.like}
    />
  )))]

  // console.log( ` merged id: `, testmerge.sort((a, b) => a.props.createdAt - b.props.createdAt));
  const mergeds = toArr[0].concat(toArr[1], toArr[2]); //merge thành 1 arr
  const sortCreatedAt = mergeds.sort((a, b) => a.props.createdAt - b.props.createdAt);  // Sort by createdAt

  // End merged

  return (
    <WrapperStyled>
      {selectedRoom.id ? (
        <>
          <HeaderStyled>
            <div className='header__info'> 
              <p className='header__title ellipsis'>{selectedRoom.name}</p>
              <span className='header__description ellipsis'>
                {selectedRoom.description}
              </span>
            </div>
            <ButtonGroupStyled>
              <Button
                icon={<UserAddOutlined />}
                type='text'
                onClick={() => setIsInviteMemberVisible(true)}
              >
                Mời
              </Button>
              <Avatar.Group size='small' maxCount={2}>
                {members.map((member) => (
                  <Tooltip title={member.displayName} key={member.id}>
                    <Avatar src={member.photoURL}>
                      {member.photoURL
                        ? ''
                        : member.displayName?.charAt(0)?.toUpperCase()}
                    </Avatar>
                  </Tooltip>
                ))}
              </Avatar.Group>
            </ButtonGroupStyled>
          </HeaderStyled>
          <ContentStyled>
            <MessageListStyled ref={messageListRef} className='scrollbar' >
              { sortCreatedAt }  {/* Content chat */}
            </MessageListStyled>
            {/* from upload images */}
            <FormInput>
              <div className="file-upload">
                <form onSubmit={formHandler} className="form-image" >
                  <div className="file-input">
                    <input
                      type="file"
                      name="file-input"
                      id="file-input"
                      className="file-input__input"
                    />
                    <label className="file-input__label" htmlFor="file-input">
                    <FileImageOutlined />
                    </label>
                    <button type="submit"><UploadOutlined /></button>
                  </div>
                </form>
                {/* upload Video */}
                <div className='line-upload'><i className="fa-solid fa-grip-lines-vertical fa-2x"></i></div>
                <form onSubmit={formHandlerVideo} className="form-video" >
                  <div className="file-input">
                    <input
                      type="file"
                      name="file-input"
                      id="file-inputVideo"
                      className="file-input__input"
                    />
                    <label className="file-input__label" htmlFor="file-inputVideo">
                    <i className="fa-solid fa-film"></i>
                    </label>
                    <button type="submit"><UploadOutlined /></button>
                  </div>
                </form>
              </div>
              {/* from upload messages */}
              <FormStyled style={{width: "86%"}} form={form}>
                <Form.Item name='message'>
                  <Input
                    ref={inputRef}
                    onChange={handleInputChange}
                    // onPressEnter={handleOnSubmit}
                    placeholder='Nhập tin nhắn...'
                    bordered={false}
                    autoComplete='off'
                    // autoSize={{ minRows: 1, maxRows: 4 }}
                  />
                </Form.Item>
                <Button type='primary' onClick={handleOnSubmit} >
                  <SendOutlined />
                </Button>
              </FormStyled>
            </FormInput>
          </ContentStyled>
        </>
      ) : (
        <Alert
          message='Hãy chọn phòng'
          type='info'
          showIcon
          // style={{ margin: 5 }}
          closable
        />
      )}
    </WrapperStyled>
  );
}