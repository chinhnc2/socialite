import React, { useState } from 'react';
import { Avatar, Typography } from 'antd';
import styled from 'styled-components';
import { formatRelative } from 'date-fns/esm';
import { doc, deleteDoc, setDoc } from "firebase/firestore";
import { DeleteOutlined } from '@ant-design/icons';
import { db } from 'Utils/firebase';

export const WrapperStyled = styled.div`
  margin-bottom: 10px;

  .author {
    margin-left: 5px;
    font-weight: bold;
    color: #a7a7a7;

  }

  .date {
    margin-left: 10px;
    font-size: 11px;
    color: #a7a7a7;
  }

  
  .content {
    margin-left: 30px;
    color: #a7a7a7;
  }
  
  img, video {
    max-width: 60vh;
    max-height: 420px;
    /* margin-left: 30px; */
  }

  .btn-del {
    margin-left: 16px;
    padding: 0 6px;
    &:hover {
      color: #BA0F30;
      font-size: 18px;
      animation: shake 0.5s;
      animation-iteration-count: infinite;
    }
  }
  .countLike {
    margin: 6px 0 0 60px;
    display: inline-block;
  }
  .reaction {
    /* margin: 6px 0 0 60px; */
    margin-top: 8px;
    color: #EC0D00;
    padding: 0 8px;
    &:hover {
      font-size: 18px;
      animation: shake 0.5s;
      animation-iteration-count: infinite;
    }
  }

  @keyframes shake {
  0% { transform: rotate(0deg); }
  25% { transform: rotate(10deg); }
  50% { transform: rotate(0deg); }
  75% { transform: rotate(-10deg); }
  100% { transform: rotate(0deg); }
}
`;

export const BtnDel = styled.button`
  margin-left: 16px;
  padding: 0 6px;
  &:hover {
    color: #BA0F30;
    font-size: 18px;
    animation: shake 0.5s;
    animation-iteration-count: infinite;
  }
`;

function formatDate(seconds) {
  let formattedDate = '';

  if (seconds) {
    formattedDate = formatRelative(new Date(seconds * 1000), new Date());

    formattedDate =
      formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
  }

  return formattedDate;
}

export default function Message({ text, displayName, createdAt, photoURL, DocID, like }) {
  
  function deleteFlight(e) {
  deleteDoc(doc(db, "messages", e));
  }
  
  async function updateCountLike(e) {
    like +=1;
    await setDoc(doc(db, "messages", e), {
      like: like
      }, { merge: true }
    );
  }

  return (
    <WrapperStyled className='hover'>
      <div>
        <Avatar size='small' src={photoURL}>
          {photoURL ? '' : displayName?.charAt(0)?.toUpperCase()}
        </Avatar>
        <Typography.Text className='author'>{displayName}</Typography.Text>
        <Typography.Text className='date'>
          {formatDate(createdAt?.seconds)}
        </Typography.Text>
        <BtnDel type='button' onClick={() => deleteFlight(DocID)} >
          <DeleteOutlined />
        </BtnDel>
      </div>
      <div>
          <Typography.Text className='content'>{text}</Typography.Text>
      </div>
       <p className='countLike' >{like !== 0 ? like : '' }</p>
      <button className='reaction'  onClick={() => updateCountLike(DocID)} >
        <i className="fa-solid fa-heart"></i>
      </button>
    </WrapperStyled>
  );
}
