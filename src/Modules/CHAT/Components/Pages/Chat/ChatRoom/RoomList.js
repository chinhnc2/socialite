import React from 'react';
import { Collapse, Typography, Button, Alert } from 'antd';
import styled from 'styled-components';
import { PlusSquareOutlined, DeleteOutlined } from '@ant-design/icons';
import { AppContext } from '../../../../Context/AppProvider';
import { doc, deleteDoc, setDoc } from "firebase/firestore";
import { db } from 'Utils/firebase';
import { BtnDel } from './Message';


const { Panel } = Collapse;
const PanelStyled = styled(Panel)`

&&& {
  .ant-collapse-header,
  p {
    color: white;
  }
  .ant-collapse-content {
    overflow-y: auto;
    overflow-x: hidden;
    max-height: 60vh ;
    .ant-collapse-content-box {
      padding: 0 22px;
    }
  }
  
  .listRoom {
    display: flex;
  }


  .ant-collapse-content::-webkit-scrollbar-track
    {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: #2F3136;
      // border tạo ảo giác margin-right
      border-right: solid 6px #2F3136;
    }

  .ant-collapse-content::-webkit-scrollbar
    {
      // width: 8px ( trừ đi vì border-right)
      width: 14px;
      /* background-color: #fff; */
    }

    .add-room {
      color: white;
      padding: 0;
    }
  }
  .name-room {
    padding-left: 12px;
    min-height: 32px;
  }

  .btn-del {
    margin-left: 16px;
    padding: 0 6px;
    &:hover {
      color: #BA0F30;
      font-size: 18px;
      animation: shake 0.5s;
      animation-iteration-count: infinite;
    }
  }
  `;

export const LinkStyled = styled(Typography.Link)`
  display: block;
  width: 80%;
  color: var(--text-light) !important;
`;

export default function RoomList() {
  const { rooms, setIsAddRoomVisible, selectedGroup, setSelectedRoomId, selectedRoom } =
    React.useContext(AppContext);

  const handleAddRoom = () => {
    setIsAddRoomVisible(true);
  };
  function deleteFlight(e) {
    if(window.confirm('You want to delette ' + e.name + ' ?')) {
      deleteDoc(doc(db, "rooms", e.id));
    }
  }

  return (
    // event groups
    <>
    {selectedGroup.id ? (
    
    <Collapse ghost defaultActiveKey={['1']} style={{position: "relative"}} className='scrollbar' >
      <PanelStyled header='Danh sách các phòng' key='1' className='scrollbar'>
        {rooms.map((room) => (
          <div className='listRoom' key={room.id}>
          <LinkStyled key={room.id} onClick={() => setSelectedRoomId(room.id)} className="hover name-room ellipsis" >
            {room.name}
          </LinkStyled>
            <BtnDel className='btn-del' type='button' onClick={() => deleteFlight(room)} >
              <DeleteOutlined />
            </BtnDel>
          </div>
        ))}
        <Button
          type='text'
          icon={<PlusSquareOutlined />}
          className='add-room'
          onClick={handleAddRoom}
        >
          Thêm phòng
        </Button>
      </PanelStyled>
    </Collapse>
    ) : (
      <Alert
      message='Hãy chọn nhóm'
      type='info'
      showIcon
      // style={{ margin: 5 }}
      closable
    />
    )}
    </>
  );
}
