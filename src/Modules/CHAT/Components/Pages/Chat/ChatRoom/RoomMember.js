import React from 'react'
import styled from 'styled-components';
import { User, BtnConversation } from '../../Sidebar/Footer/styled';
import { WrapperStyled } from './UserInfo';
import { Link } from 'react-router-dom';
import {useAuth} from "Hooks";
import { Avatar, Typography } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { doc, deleteDoc } from "firebase/firestore";
import { db } from 'Utils/firebase';
import { AppContext } from '../../../../Context/AppProvider';

export const MembersGroup = styled.aside`
    /* min-width: 248px; */
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;

    .title {
        margin: 18px 44% 4px 0;
    }
    .list-users{
        /* justify-content: center; */
        display: flex;
        width: 90%;
        section {
            height: 42px;
            width: 100%;
            .user-name {
            width: 80%;
            }
        }
    }
`;

export default function RoomMember() { //{profile}
  const { selectedGroupId, selectedGroup } = React.useContext(AppContext);
  
  const { profile } = useAuth();
  const friendList = profile.friends;

  async function deleteFlight(e) {
    if(window.confirm('You want to delette ' + selectedGroup.name + ' ?')) {
      deleteDoc(doc(db, "groups", e));
    }
  }
  return (
    <MembersGroup className="bgr--medium">
      <WrapperStyled>
          <div className="toolbar">
              <div className="toolbar__user hover" title="Ẩn danh sách thành viên"><i className="fa-solid fa-user-group"></i></div>
              <div className="toolbar__help hover" title="Trợ giúp"><i className="fa-solid fa-circle-question"></i></div>
              <div className="toolbar__help hover" title="Xoá nhóm" onClick={() => deleteFlight(selectedGroupId)} ><DeleteOutlined /></div>
          </div>
      </WrapperStyled>
        <div className="title">
          <p>TRỰC TUYẾN</p>
        </div>
          {friendList.map((friend) => (
          <div className="list-users hover" key={friend._id}>
            <User>
                <Link to={`/u/${friend._id}`} title="Detailed profile" >
                    <Avatar src={friend.avatar}>
                      {friend.avatar ? '' : friend.name?.charAt(0)?.toUpperCase()}
                    </Avatar>
                </Link>
              <div className="user-name mr-l-4">
                <p>{friend.name}</p>
                <p>#1325</p>
              </div>
            </User>
          </div>
          ))}
    </MembersGroup>
  )
}
