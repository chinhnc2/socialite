import React from 'react';
import { Row, Col } from 'antd';
import UserInfo from './UserInfo';
import RoomList from './RoomList';
import styled from 'styled-components';
import Footer from '../../Sidebar/Footer';
import { useAuth } from "Hooks";
import { ContextProvider } from '../../Video/Context';

const SidebarStyled = styled.div`
  background: #2f3136;
  color: white;
  height: 100vh;
  min-width: 240px;
`;

export default function Sidebar() {
    
  const { profile } = useAuth();

  return (
    <SidebarStyled style={{position: "relative"}}>
      <Row>
        <Col span={24}>
          <UserInfo profile={profile} />
        </Col>
        <Col span={24}>
          <RoomList />
        </Col>
      </Row>
      <Row style={{position: "absolute", bottom: "0", width: "100%"}}>
        <Col span={24}>
          <ContextProvider>
            <Footer profile={profile} />
          </ContextProvider>
        </Col>
      </Row>
      {/* <VideoPlayer /> */}
    </SidebarStyled>
  );
}
