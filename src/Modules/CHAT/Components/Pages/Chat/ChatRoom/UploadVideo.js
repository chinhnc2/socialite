import React from 'react';
import { Avatar, Typography } from 'antd';
import { formatRelative } from 'date-fns/esm';
import { WrapperStyled } from './Message';
import { doc, deleteDoc, setDoc } from "firebase/firestore";
import { db } from 'Utils/firebase';
import { DeleteOutlined, LikeOutlined } from '@ant-design/icons';


function formatDate(seconds) {
  let formattedDate = '';

  if (seconds) {
    formattedDate = formatRelative(new Date(seconds * 1000), new Date());

    formattedDate =
      formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
  }

  return formattedDate;
}

export default function Video({ files, nameFile, displayName, createdAt, photoURL, DocID, like }) {

  function deleteFlight(e) {
    deleteDoc(doc(db, "videos", e));
  }
  async function updateCountLike(e) {
    like +=1;
    // }
    await setDoc(doc(db, "videos", e), {
      like: like
      }, { merge: true }
    );
  }

  return (
    <WrapperStyled className='hover'>
      <div>
        <Avatar size='small' src={photoURL}>
          {photoURL ? '' : displayName?.charAt(0)?.toUpperCase()}
        </Avatar>
        <Typography.Text className='author'>{displayName}</Typography.Text>
        <Typography.Text className='date'>
          {formatDate(createdAt?.seconds)}
        </Typography.Text>
        <button className='btn-del' type='button' onClick={() => deleteFlight(DocID)} >
          <DeleteOutlined />
        </button>
      </div>
      <div>
        <video width="480" height="320" controls className='content' >
            <source src={files} type="video/mp4" />
            {/* <source src="movie.ogg" type="video/ogg"> */}
        </video>
      </div>
      <p className='countLike' >{like !== 0 ? like : '' }</p>
      <button className='reaction'  onClick={() => updateCountLike(DocID)} >
        <i className="fa-solid fa-heart"></i>
      </button>
    </WrapperStyled>
  );
}
