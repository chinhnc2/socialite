import React, { useState } from 'react';
import { Avatar, Typography } from 'antd';
import { formatRelative } from 'date-fns/esm';
import { WrapperStyled } from './Message';
import { DeleteOutlined, LikeOutlined } from '@ant-design/icons';
import { db } from 'Utils/firebase';
import { doc, deleteDoc, setDoc } from "firebase/firestore";

function formatDate(seconds) {
  let formattedDate = '';

  if (seconds) {
    formattedDate = formatRelative(new Date(seconds * 1000), new Date());

    formattedDate =
      formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
  }

  return formattedDate;
}
export default function Image({ files, nameFile, displayName, createdAt, photoURL, DocID, like }) {

  function deleteFlight(e) {
    deleteDoc(doc(db, "images", e));
  }
  async function updateCountLike(e) {
    like +=1;
    // }
    await setDoc(doc(db, "images", e), {
      like: like
      }, { merge: true }
    );
  }

  return (
    <WrapperStyled className='hover'>
      <div>
        <Avatar size='small' src={photoURL}>
          {photoURL ? '' : displayName?.charAt(0)?.toUpperCase()}
        </Avatar>
        <Typography.Text className='author'>{displayName}</Typography.Text>
        <Typography.Text className='date'>
          {formatDate(createdAt?.seconds)}
        </Typography.Text>
        <button className='btn-del' type='button' onClick={() => deleteFlight(DocID)} >
          <DeleteOutlined />
        </button>
      </div>
      <div>
        <img src={files} alt={nameFile} className="content" />
      </div>
      <p className='countLike' >{like !== 0 ? like : '' }</p>
      <button className='reaction'  onClick={() => updateCountLike(DocID)} >
        <i className="fa-solid fa-heart"></i>
      </button>
    </WrapperStyled>
  );
}
