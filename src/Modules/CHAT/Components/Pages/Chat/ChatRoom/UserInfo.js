import React from 'react';
import { Avatar, Typography } from 'antd';
import styled from 'styled-components';

export const WrapperStyled = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 56px;
  border-bottom: 1.5px solid rgba(32, 34, 37, 0.8);

  .info-user-chat {
    margin-left: 18px;
  }

  .toolbar {
    display: flex;

    .toolbar__user {
      margin-left: 26px;
    }
    .toolbar__help {
      margin-left: 26px;
    }
  }

  .username {
    color: white;
    margin-left: 5px;
  }
`;

export default function UserInfo({ profile }) {
  const displayName = profile.name , photoURL = profile.avatar ;

  return (
    <WrapperStyled>
      <div className='info-user-chat'>
        <Avatar src={photoURL}>
          {photoURL ? '' : displayName?.charAt(0)?.toUpperCase()}
        </Avatar>
        <Typography.Text className='username'>{displayName}</Typography.Text>
      </div>
    </WrapperStyled>
  );
}
