import React from 'react';
import { Row, Col } from 'antd';
import Sidebar from './Sidebar';
import ChatWindow from './ChatWindow';
import RoomMember from './RoomMember';
import {useAuth} from "Hooks";
import Navleft from 'Modules/CHAT/Components/Layout/Nav';
import styled from 'styled-components';

const WrapperChatWindow = styled.div`
  width: 100%;

`;
export default function ChatRoom() {

  const { profile } = useAuth();
  return (
    <WrapperChatWindow>
      <Row>
        <Col span={1} style={{"minWidth": "72px"}} >
          <Navleft profile={profile} />
        </Col>
        <Col span={4}  >
          <Sidebar profile={profile} />
        </Col>
        <Col span={16}>
          <ChatWindow profile={profile} />
        </Col>
        <Col span={3}  className="set-width" style={{"marginRight": "-9px"}} >
          <RoomMember profile={profile}/>
        </Col>
      </Row>
    </WrapperChatWindow>
  );
}
