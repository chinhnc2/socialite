import React, { useContext } from 'react';
import { Form, Modal, Input } from 'antd';
import { AppContext } from '../../../../Context/AppProvider';
import { addDocument } from '../../../../firebase/services';
import { useAuth } from "Hooks";

export default function AddGroupModal() {
  const { isAddGroupVisible, setIsAddGroupVisible } = useContext(AppContext);

  const { profile } = useAuth();
  const [form] = Form.useForm();
  const uid = profile._id;
  // const uid = "123";

  const handleOk = () => {
    // handle logic
    // add new group to firestore
    addDocument('groups', { ...form.getFieldsValue()
      , members: [uid]
     });

    // reset form value
    form.resetFields();
    setIsAddGroupVisible(false);
  };

  const handleCancel = () => {
    // reset form value
    form.resetFields();
    setIsAddGroupVisible(false);
  };

  return (
    <div>
      <Modal
        title='Tạo nhóm'
        visible={isAddGroupVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form form={form} layout='vertical'>
          <Form.Item label='Tên nhóm' name='name'>
            <Input placeholder='Nhập tên nhóm' />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
