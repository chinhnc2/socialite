import React, { useContext, useState, useRef, useEffect } from 'react';
import { Modal, Avatar } from 'antd';
import { AppContext } from '../../../../Context/AppProvider';
import { db } from '../../../../../../Utils/firebase';
import {useAuth} from "Hooks"
import { useForm, FormProvider } from "react-hook-form";
import { FormInput } from "Components";
import { doc, setDoc, getDocs, collection } from "firebase/firestore";
import {CHAT} from 'Modules/CHAT/styled';
import styled from 'styled-components';
import { addDocument } from 'Modules/CHAT/firebase/services';

const WapperListUser = styled(CHAT)`
  .user_search {
    display: flex;
    width: 100%;
    height: 42px;
    margin-bottom: 8px;
    align-items: center;
    p {
      margin-left: 16px;
    }
  }

  .hover {
    &:hover {
      background-color: #D3ECF8;
      color: #211522;
    }
  }

`;

export default function InviteMemberModal() {

  const {
    isInviteMemberVisible,
    setIsInviteMemberVisible,
    selectedGroupId,
    selectedGroup,
    // selectedRoomId,
    // selectedRoom,
  } = useContext(AppContext);

  const { userQueries, searchUserAction } = useAuth();
  const [isShowSearchBoard, setIsShowSearchBoard] = useState(false);
  const search_boardRef = useRef(null);

  const onHandleChangeSearch = (data) => {
    if (data.queryString.length > 0) {
      searchUserAction(data);
    }
    return data;
  }

  const form = useForm({
    resolver: onHandleChangeSearch ,
    mode: "onChange"
  });
  const { handleSubmit } = form;

  async function haha() {
    const querySnapshot = await getDocs(collection(db, "users"));
    console.log(querySnapshot)
    let a =querySnapshot.docs.map((doc) => {
      // doc.data() is never undefined for query doc snapshots
      return doc.data().uid; //Get all user id
    })
    console.log(a)
  }

  const handleOk = async (e, member) => {
    //Kiểm tra user tồn tại trong nhóm hay chưa
    const checkUserInGroup = selectedGroup.members.find(id => id == member.id);
    if(!checkUserInGroup) {
    // update members in current room
      await setDoc(doc(db, "groups", e), {
        members: [...selectedGroup.members, member.id ]
        }, { merge: true }
      );
    }

    //kiểm tra user mới có trong colection user hay chưa
    const querySnapshot = await getDocs(collection(db, "users"));
    let allUserId = querySnapshot.docs.map((doc) => {
      // doc.data() is never undefined for query doc snapshots
      return doc.data().uid; //Get all user id
    })
    //Dk lọc thêm user vào colection User
    const checkUserinColection = allUserId.find(id => id == member.id);
    if(!checkUserinColection){
      addDocument('users', {
        displayName: member.name,
        photoURL: member.avatar,
        uid: member.id,
      });
    }

    setIsInviteMemberVisible(false);
  };

  const handleCancel = () => {
    setIsInviteMemberVisible(false);
  };

const handleClickOutSearchBoard = (e) => {
    
  if (search_boardRef.current && !search_boardRef.current.contains(e.target)) {
      setIsShowSearchBoard(false);
  }
}
useEffect(() => {
  document.addEventListener("click", handleClickOutSearchBoard, false);
  return () => document.removeEventListener("click", handleClickOutSearchBoard, false);
})

  return (
    <div>
      <Modal
        title='Mời thêm thành viên'
        visible={isInviteMemberVisible}
        // onOk={handleOk}
        onCancel={handleCancel}
        destroyOnClose={true}
      >
        <FormProvider {...form}>
          <form onSubmit={handleSubmit()} name='search__user' >
            <FormInput
            // value= {inputValue}
            name='queryString'
            onFocus={() => setIsShowSearchBoard(true) }
            type="text"
            className="top-bar-search"
            placeholder="Search..."
            />
          </form>
        </FormProvider>
        {isShowSearchBoard && (
          <div className="search_user_board" >
            {userQueries && userQueries.length > 0 && userQueries.map(infoUser =>
              <WapperListUser>
                <div className="user_search hover" key={infoUser.id} onClick={() => handleOk(selectedGroupId , infoUser)} >
                  <Avatar src={infoUser.avatar} ></Avatar>
                  <p>{infoUser.name}</p>
                </div>
              </WapperListUser>
            )}
          </div>
        )}
      </Modal>
    </div>
  );
}
