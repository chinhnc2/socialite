import React, { useState, useContext } from "react";
import { Call, Panel, User, BtnConversation } from "./styled";
import CallVideo from "../../Video/";
import { SocketContext } from '../../Video/Context';
import { Row, Col } from 'antd';


export default function Footer( {profile} ) {

  // const [active, setActive] = useState('false');
  const { active, setActive, callUser,leaveCall } = useContext(SocketContext);
  const onVideo = () => {
    setActive(true)
    leaveCall()
    // callUser()
  }

  const offVideo = () => {
    setActive(false)
    callUser()
    // leaveCall()
  }
  return (
    <Panel>
      {/* <CallVideo /> */}
      {!active === true && <CallVideo />}
        <Call>
        <div className="call-header">
          <div className="info">
            <div className="status">
              <div className="status-icon">
                <i className="fa-solid fa-signal"></i>
              </div>
              <div className="status-info hover-text">
                <p>Đã Kết Nối Giọng Nói</p>
              </div>
            </div>
          </div>
          <button className="close-call hover" onClick={() => offVideo()} >
            <i className="fa-solid fa-phone-slash"></i>
          </button>
        </div>
        <div className="actions">
          <button className="call-video bgr--light actions-btn" onClick={() => onVideo()} >
            <div className="actions-logo">
              <i className="fa-solid fa-video"></i>
            </div>
            <div className="actions-name mr-l-4">
              <p>Video</p>
            </div>
          </button>
          <button className="share-screen bgr--light actions-btn">
            <div className="actions-logo">
              <i className="fa-solid fa-desktop"></i>
            </div>
            <div className="actions-name mr-l-4">
              <p>Màn hình</p>
            </div>
          </button>
        </div>
        </Call>
      <User>
        <div className="user-logo">
          <BtnConversation>
            <img
              className="logo-mini"
              src="https://icones.pro/wp-content/uploads/2021/03/logo-discord-icone-png-rose.png"
              alt="logo"
            ></img>
          </BtnConversation>
        </div>
        <div className="user-name mr-l-16">
          <p className="ellipsis">{profile.name}</p>
          <p>#1325</p>
        </div>
        <div className="user-control">
          <div className="hover user-control-item">
            <i className="fa-solid fa-microphone"></i>
          </div>
          <div className="hover user-control-item">
            <i className="fa-solid fa-headphones"></i>
          </div>
          <div className="hover user-control-item">
            <i className="fa-solid fa-gear"></i>
          </div>
        </div>
      </User>
    </Panel>
  );
};

