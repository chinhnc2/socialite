import styled from "styled-components";

export const Panel = styled.div`
  /* width: 100%; */
  height: 160px;
  /* position: absolute; */
  /* position: fixed; */
  /* bottom: 0; */
  background-color: #292b2f !important;
  color: var(--text-light) !important;
  font-size: 0.8rem;

  .hover-text {
    &:hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;

export const Call = styled.div`
  height: 90px;
  border-bottom: 1.5px solid rgba(32, 34, 37, 0.8);
  /* width: 100%; */
  /* color: #36393F; */
  .call-header {
    display: flex;
    position: relative;
    margin: 12px -2px 6px 8px;

    .info {
      margin-top: 14px;
      width: 80%;
      height: 28px;
      .status {
        display: flex;
        width: 100%;
        font-size: 0.9rem;
        color: #47d47c;

        .status-icon {
        }
        .status-info {
          margin-left: 6px;
          font-size: 16px;
        }
      }
      .name-conversation {
        margin-top: 6px;
        display: flex;
      }
    }
    .close-call {
      position: absolute;
      right: 10px;
      font-size: 1.1rem;
      width: 32px;
      height: 32px;
      margin: 10px 0 0 6px;
    }
  }

  .actions {
    display: flex;
    width: 90%;
    margin-left: 8px;
    justify-content: space-around;

    .actions-btn {
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 5px;
      width: 40%;
      height: 30px;
      font-size: 0.8rem;
      &:hover {
        background-color: #2f3136;
      }
    }
    .call-video {
    }
    .share-screen {
      margin-left: 8px;
    }
  }
`;

export const User = styled.section`
  display: flex;
  align-items: center;
  font-size: 0.75rem;
  height: 52px;
  /* bottom: 0; */
  width: 100%;
  /* left: 73px; */

  span {
    &:hover {
      border-radius: 5px;
    }
  }
    div {
      margin: -2px 4px 0 8px;
      height: 36px;
      width: 36px;
      &:hover {
        img {
          border-radius: 50% !important;
        }
      }
    }

  .user-name {
    width: 30%;
    font-size: 16px;
  }

  .user-control {
    display: flex;
    font-size: 1.2rem;
    margin-left: 6px;
    width: 36%;
    position: absolute;
    right: 0px;

    .user-control-item {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 30px;
      height: 30px;
    }
  }
`;

export const BtnConversation = styled.div`
  width: 50px;
  height: 50px;
  margin-top: 10px;
  cursor: pointer;
  padding: 0;
  border-radius: 50%;
  background-color: #36393f;

  &:hover {
    img {
      background: #a49393;
      border-radius: 25%;
      cursor: pointer;
    }
  }
`;
