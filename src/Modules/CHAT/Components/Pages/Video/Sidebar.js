import React, { useState, useContext } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { SocketContext } from './Context';
import { WapperSidebar } from './styled';
import {useAuth} from "Hooks";

const Sidebar = ({ children }) => {
  const { me, callUser, name, setName } = useContext(SocketContext);
  const [idToCall, setIdToCall] = useState('');
  const { profile } = useAuth();

  return (
    <WapperSidebar>
        <form noValidate autoComplete="off">
          <div>
            <div >
              {/* <input label="Name" value={name} onChange={(e) => setName(e.target.value)} /> */}
              {setName(profile.name) }
              <CopyToClipboard text={me}>
                <button type="button">
                  Copy Your ID {me}
                </button>
              </CopyToClipboard>
            </div>
            <div >
              <input label="ID to call" value={idToCall} onChange={(e) => setIdToCall(e.target.value)} ></input>
                <button type="button" onClick={() => callUser(idToCall)}>
                  Call
                </button>
            </div>
          </div>
        </form>
        {children}
    </WapperSidebar>
  );
};

export default Sidebar;
