import React, { useContext } from 'react';
import { SocketContext } from './Context';
import { ViewVideo } from './styled';
import {useAuth} from "Hooks";

const VideoPlayer = () => {
  const { name, callAccepted, myVideo, userVideo, callEnded, stream, call } = useContext(SocketContext);
  const { profile } = useAuth();

  return (
    <ViewVideo>
      {stream && (
        <div>
          <div>
            <div>{name || profile.name}</div>
            <video playsInline muted ref={myVideo} autoPlay />
          </div>
        </div>
      )}
      {callAccepted && !callEnded && (
        <div>
          <div >
            <div>{call.name || profile.name}</div>
            <video playsInline ref={userVideo} autoPlay />
          </div>
        </div>
      )}
    </ViewVideo>
  );
};

export default VideoPlayer;
