import React, { Component } from 'react';
import VideoPlayer from './VideoPlayer';
import Sidebar from './Sidebar';
import Notifications from './Notifications';
import { ContextProvider } from './Context';
import { Wapper } from './styled';

export default function CallVideo() {

    return (
      <ContextProvider>
        <Wapper >
          <div>video chat</div>
          <VideoPlayer />
          <Sidebar>
            <Notifications />
          </Sidebar>
        </Wapper>
      </ContextProvider>
  );
};