import styled from "styled-components";

export const Wapper = styled.div`
    background-color: black;
    position: fixed;
    width: calc(100% - 72px);
    max-height: 74%;
    z-index: 99999999;
    top: 0;
    
`;

export const ViewVideo = styled.div`
    display: flex;
    justify-content: center;
`;

export const WapperSidebar = styled.div`
    position: absolute;
    top: 86%;

`;