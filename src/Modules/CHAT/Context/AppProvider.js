import React, { useState } from 'react';
import useFirestore from '../hooks/useFirestore';
// import { AuthContext } from './AuthProvider';
import { useAuth } from "Hooks";
export const AppContext = React.createContext();

export default function AppProvider({ children }) {
  const [isAddGroupVisible, setIsAddGroupVisible] = useState(false);
  const [isAddRoomVisible, setIsAddRoomVisible] = useState(false);
  const [isInviteMemberVisible, setIsInviteMemberVisible] = useState(false);
  const [selectedGroupId, setSelectedGroupId] = useState('');
  const [selectedRoomId, setSelectedRoomId] = useState('');
  const { profile } = useAuth();
  const uid = profile._id;
  // const uid = "123";

  const groupsCondition = React.useMemo(() => {
    return {
      fieldName: 'members',
      operator: 'array-contains',
      compareValue: uid,
    };
  }, [uid]);

  const groups = useFirestore('groups', groupsCondition);
  
  const selectedGroup = React.useMemo(
    () => groups.find((group) => group.id === selectedGroupId) || {},
    [groups, selectedGroupId]
    );
    
    const roomsCondition = React.useMemo(() => {
      return {
        fieldName: 'GroupId', //GroupId
        operator: '==', //array-contains
        compareValue: selectedGroupId, //selectedGroupId ..
        // compareValue: selectedGroupId,
      };
    }, 
    [selectedGroupId]
    // [selectedGroupId]
    );
    
  const rooms = useFirestore('rooms', roomsCondition);

  const selectedRoom = React.useMemo(
    () => rooms.find((room) => room.id === selectedRoomId) || {},
    [rooms, selectedRoomId]
  );

  const usersCondition = React.useMemo(() => {
    return {
      fieldName: 'uid',
      operator: 'in',
      compareValue: selectedGroup.members,
    };
  }, [selectedGroup.members]);

  const members = useFirestore('users', usersCondition);
  return (
    <AppContext.Provider
      value={{
        groups,
        rooms,
        members,
        selectedGroup,
        selectedRoom,
        isAddGroupVisible,
        setIsAddGroupVisible,
        isAddRoomVisible,
        setIsAddRoomVisible,
        selectedGroupId,
        selectedRoomId,
        setSelectedGroupId,
        setSelectedRoomId,
        isInviteMemberVisible,
        setIsInviteMemberVisible
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
