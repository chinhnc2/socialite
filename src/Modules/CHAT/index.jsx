import React from 'react';
import GlobalStyle from './Components/Layout/styled';
import { CHAT } from './styled';
import CallVideo from './Components/Pages/Video';
import ChatRoom from './Components/Pages/Chat/ChatRoom';
import AppProvider from './Context/AppProvider';
import AddRoomModal from './Components/Pages/Chat/Modals/AddRoomModal';
import AddGroupModal from './Components/Pages/Chat/Modals/AddGroupModal';
import InviteMemberModal from './Components/Pages/Chat/Modals/InviteMemberModal';

// import { useTranslation } from 'react-i18next'

const ChatScreen = () => {
  return (
    <CHAT>
      <GlobalStyle />
      <AppProvider>
        <ChatRoom />
        <AddRoomModal />
        <AddGroupModal />
        <InviteMemberModal />
      </AppProvider>
      {/* <CallVideo /> */}
    </CHAT>
  )
}

export default ChatScreen;
