 import styled from "styled-components";

 export const CHAT = styled.div`
    width:100%;
    height: 100%;
    display: flex ;
    color: var(--text-light);
    font-weight: 450;
    font-family: Poppins, "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 16px;
    .hover {
    &:hover {
      border-radius: 5px;
      background-color: #393c43;
      color: #fcfcfc;
      cursor: pointer;
        }
    }

    --text-light: #71757C;
    --text-medium: #2F3136;
    --text-dark: #202225;
    
    .border-b-line {
        border-bottom: 1.5px solid rgba(32, 34, 37, 0.3);
    }
    .bgr--light {
        background-color: #393c43;
    }
    .bgr--medium {
        background-color: #2F3136;
    }
    .bgr--dark {
        background-color: #202225;
    }
    .mr-l-4 {
        margin-left: 4px;
    }
    .mr-l-8 {
        margin-left: 8px;
    }
    .mr-l-16 {
        margin-left: 16px;
    }

    .ellipsis {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }

    .scrollbar::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
              box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: var(--text-medium);

      // border tạo ảo giác margin-right
      border-right: solid 6px #36393F;
    }

    .scrollbar::-webkit-scrollbar {
      // width: 8px ( trừ đi vì border-right)
      width: 14px;
      /* background-color: #fff; */
    }

    .scrollbar::-webkit-scrollbar-thumb {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
      box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: var(--text-dark);

      // border tạo ảo giác margin-right
      border-right: solid 6px #36393F;
    }
 `;