import React from 'react';
import { PostCreate, CardPost } from 'Components';
import { useRoot, useHome } from "Hooks";
import "./style.scss"

const HomeGroup = () => {
    const { posts } = useHome();
    
    return (
        <div className="home">
            <div className="home__post">
                <PostCreate />
                {posts && posts.length > 0 && posts.map(post => <CardPost x={post} /> )}
            </div>
            <div className="home__intro">
                <div className="home__intro-about">
                    <h3>About</h3>
                    <div className="home__intro-member">
                        <i className="fa-solid fa-user-group" />
                        <span>122 members</span>
                    </div>
                    <div className="home__intro-status">
                        <i className="fa-solid fa-globe" />
                        <span>
                            Public
                            <h6>Anyone can see who's in the group and what they post.</h6>
                        </span>
                    </div>
                    <div className="home__intro-link">
                        <i className="fa-solid fa-arrow-up-right-from-square" />
                        <a href="facebook.com">http://localhost:3000/group</a>
                    </div>
                    <div className="home__intro-mail">
                        <i className="fa-solid fa-envelope" />
                        <a href="/">bapbap123@gmail.com</a>
                    </div>
                </div>

                <div className="home__intro-relate">
                    <h3>Related groups</h3>
                    <div className="home__intro-relate--group">
                        <a href="/">
                            <img src="http://demo.foxthemes.net/socialitev2.2/assets/images/group/group-3.jpg" alt="" />
                            <div>
                                <h4>Graphic Design</h4>
                                <p>345K Followin</p>
                            </div>
                        </a>
                        <button>Join</button>
                    </div>
                    <div className="home__intro-relate--group">
                        <a href="/">
                            <img src="http://demo.foxthemes.net/socialitev2.2/assets/images/group/group-3.jpg" alt="" />
                            <div>
                                <h4>Graphic Design</h4>
                                <p>345K Followin</p>
                            </div>
                        </a>
                        <button>Join</button>
                    </div>
                    <div className="home__intro-relate--group">
                        <a href="/">
                            <img src="http://demo.foxthemes.net/socialitev2.2/assets/images/group/group-3.jpg" alt="" />
                            <div>
                                <h4>Graphic Design</h4>
                                <p>345K Followin</p>
                            </div>
                        </a>
                        <button>Join</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeGroup