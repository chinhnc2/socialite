import React from 'react';
import Header from './layout/Header';
import Menuleft from './layout/Menuleft';
import Footer from './layout/Footer';
function App(props) {
    return (
        <div>
            <Header/>
            <div className='container'>
              <div className='row'>
                  <Menuleft/>
                  {props.children}
              </div>
            </div>
            <Footer/>      
        </div>
    );
}
    
export default App;