/* eslint-disable no-restricted-globals */
import { useDispatch, useSelector } from 'react-redux'

import saga from '../home/store/saga'
import reducer from '../home/store/reducer'
import {addGame, loadGame, loadGameid} from '../home/store/actions'
import { useInjectSaga, useInjectReducer } from 'Stores'
import { makeSelectGames } from '../home/store/selectors'
import { loadPlayer } from '../player/store/actions'
import { makeSelectPlayer } from '../player/store/selectors'

export const useHome = () => {
  useInjectSaga({ key: 'home', saga })
  useInjectReducer({ key: 'home', reducer })

  const { isLoading, error, gameid,games } = useSelector(
    makeSelectGames()
  )

  const dispatch = useDispatch()
  
  const loadGameAction = (payload) => dispatch(loadGame(payload));
  const loadGameidAction = (id) => dispatch(loadGameid(id));
  const addGameAction = (payload) => dispatch(addGame(payload));
  
  return {
    isLoading,
    error,
    gameid,
    games,
    loadGameAction,
    loadGameidAction,
    addGameAction
  }
}
export const usePlayer = () => {
  useInjectSaga({ key: 'player', saga })
  useInjectReducer({ key: 'player', reducer });
  const { isLoading, error, players } = useSelector(
    makeSelectPlayer()
  )
  const dispatch = useDispatch();
  const loadPlayerAction = (payload) => dispatch(loadPlayer(payload));
  return {
    isLoading,
    error,
    players,
    loadPlayerAction,
  }
}

export const useGame = () => {
  useInjectSaga({ key: 'home', saga })
  useInjectReducer({ key: 'home', reducer });

  const dispatch = useDispatch();
  const loadGameAction = (payload) => dispatch(loadGame(payload));
  const addGameAction = (payload) => dispatch(addGame(payload));

  return {
    loadGameAction,
    addGameAction,
  }
}
