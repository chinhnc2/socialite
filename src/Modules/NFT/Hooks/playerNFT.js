/* eslint-disable no-restricted-globals */
import { useDispatch, useSelector } from 'react-redux'

import saga from '../player/store/saga'
import reducer from '../player/store/reducer'

import { useInjectSaga, useInjectReducer } from 'Stores'
import { gameplayed, getItemdepot, loadPlayer, sellItem } from '../player/store/actions'
import { makeSelectPlayer } from '../player/store/selectors'


export const usePlayer = () => {
  useInjectSaga({ key: 'player', saga })
  useInjectReducer({ key: 'player', reducer });
  const { isLoading, error, players } = useSelector(
    makeSelectPlayer()
  )
  // console.log(players)
  const dispatch = useDispatch();
  const loadPlayerAction = (payload) => dispatch(loadPlayer(payload));
  const gamePlayedAction = (payload) => dispatch(gameplayed(payload));
  const sellItemAction = (payload) => dispatch(sellItem(payload));
  const getItemdepotAction = (payload) => dispatch(getItemdepot(payload))
  return {
    isLoading,
    error,
    players,
    loadPlayerAction,
    sellItemAction,
    gamePlayedAction,
    getItemdepotAction
  }
}

