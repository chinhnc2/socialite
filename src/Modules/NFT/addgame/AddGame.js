import React, { useCallback, useState } from "react";
import './AddGame.css';
import { useForm, FormProvider } from "react-hook-form";
import {FormInput,FormUploadImage } from '../../../Components/index'
import { useHome } from "../Hooks/homeNFT";
import axios from "axios";
import uploadCloudinary from "Utils/cloudinary";
function AddGame(props) {
    const form = useForm();
    const { handleSubmit, reset } = form;
    const [imgURL, setImgURL] = useState(null);
    const [getimgURL, setgImgURL] = useState(null);
    const [success,,setSuccess] = useState("")

    const { loadGameAction , addGameAction } = useHome();

    const handleChangeFile = (e) => {
        let reader = new FileReader();
        console.log(e.target.files[0])
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function (e) {
            setImgURL(reader.result);
            console.log(reader.result)
        };
    }
    const onSubmit = async (data) => {
        console.log(imgURL)
        const data1 = new FormData()
            data1.append('file', imgURL)
            data1.append('upload_preset', 'ml_default');
            data1.append("cloud_name", "bapbapbap123");
            fetch("https://api.cloudinary.com/v1_1/bapbapbap123/auto/upload", {
            method: "post",
            body: data1
            }).then(res => res.json())
                .then(data1 => {
                console.log(data1);
                setgImgURL(data1.url);
                return data1.url;
            }).catch(err => {
                console.log("An Error Occured While Uploading")
        })
        console.log(getimgURL);
        if (getimgURL) {
            const dataPost = {
                game_name : data["name_game"],
                game_link : data["link_game"],
                category : data["category_game"],
                description: data["description_game"],
                user_id :"62314a9f6b4abd5680ca502d",
                game_img: getimgURL
        }
            console.log(dataPost);
            // loadGameAction()
            addGameAction(dataPost)
            document.getElementById("input__imageGame").value = "";
            reset();
            setSuccess("Thêm game thành công")

        }   
        // reset();
      };
    return (
        <div className='addGame__Home'>
            <div className="add__Form">
                <h1 className='add__Form__title'>Thêm Game Mới</h1>
                <FormProvider {...form} >
                    <form className="form-addgame" onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-inputGame">
                            <p>Nhập tên game</p> 
                            <FormInput className="input__nameGame"
                            name="name_game" 
                            required/>
                        </div>
                        <div className="form-inputGame">
                            <p>Nhập đường link game</p> 
                            <FormInput className="input__linkGame"
                            name="link_game" 
                            required/>
                        </div>
                        <div className="form-inputGame">
                            <p>Nhập thể loại game</p> 
                            <FormInput className="input__categoryGame"
                            name="category_game" 
                            required/>
                        </div>
                        <div className="form-inputGame">
                            <p>Nhập mô tả game</p> 
                            <FormInput className="input__descriptionGame"
                            name="description_game" 
                            required/>
                        </div>
                        {/* <div className="form-inputGame">
                            <p>Nhập hình ảnh game</p> 
                            <FormUploadImage className="input__imageGame"
                            name="image_game" 
                            required/>npm 
                        </div> */}
                        <div className="form-inputGame">
                            <p>Nhập hình ảnh game</p> 
                            <input type="file" id="input__imageGame" className="input__imageGame"
                            name="image_game" 
                            required
                            onChange={handleChangeFile}
                            />
                        </div>
                        {imgURL && <img src={imgURL} alt='asd' />}
                        <div className="form-btnGame">
                            <input className="btn-submit" type='submit' value='Thêm game mới' />
                        </div>
                        <h4 className="success__form__addGame">{success}</h4>
                    </form>
                </FormProvider>
            </div>

        </div>
    );
}

export default AddGame;