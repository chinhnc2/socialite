import React, { useCallback, useState, useEffect } from "react";
import './GameDetail.css'
import { useHome } from "../Hooks/homeNFT"
import { useParams } from "react-router-dom";
import {usePlayer} from "../Hooks/playerNFT"
import axios from 'axios';
import Modal from "../Modal/Modal";
import NotiScore from "./NotiScore";
import NotiSuccess from "../items/NotiSuccess";
function GameDetail(props) {

    const [score, setScore] = useState(0);
    const [infoGame, setInfoGame] = useState({});
    const [stateModal, setStateModal] = useState(false)
    const [sumscore, setSumScore] = useState(0);
    const {players,loadPlayerAction,gamePlayedAction } = usePlayer();
        
    const { gameid  , loadGameidAction ,loadGameAction } = useHome()
    let { id } = useParams();
    useEffect(() => {
        loadGameidAction(id)
        const randomID = () => {
            return 1000 + Math.trunc((Math.random() * 9000));
        }
        // loadGameidAction(id)
        // console.log(gameid)
        // setInfoGame(gameid)
       
        const newScore = randomID();
    
        setScore(newScore)
       
    },[])
    useEffect(() => {
        console.log(gameid)
        setInfoGame(gameid)
    }, [gameid])
    function handleSubmitScore(e) {
        e.preventDefault();
        console.log(123)
        const data = {
            "id": id,
            "score" :score ,
            "_id" : "62458066104426474c8a6b50"
        } 
        gamePlayedAction(data)
        setStateModal(true)

        // axios.post("http://localhost:2707/v1/player/info/"+id , data)
        //     .then(res=>{
        //         console.log(res)
        //         if(res.data.ok ==  1){
        //             axios.get("http://localhost:2707/v1/player/info/62458066104426474c8a6b50")
        //             .then(res =>{
        //                 console.log(res.data.score)
        //                 setSumScore(res.data.score)
        //                 setStateModal(true)
        //             })
        //             .catch(err=>{
        //                 console.log(res)
        //             })
        //         }
        //     })
        //     .catch(err=>{
        //         console.log(err)
        //     })
    }
    function offModal(){
        setStateModal(false)
    }

    return (
        <>  
            {stateModal && <Modal>
                <NotiScore offModal={offModal} sumscore={sumscore} score={score}/>
            </Modal>}
            {
                infoGame && Object.keys(infoGame).length > 0 &&<div className='main__gamedetail'>
                <h2 className='gamedetail__name'>{infoGame["game_name"]}</h2>
                <iframe src={infoGame["game_link"]}></iframe>
                <h2 className="yourScore">Your Score : <span>{score}</span></h2>
                <h2 className="description__game">Category :  <span> - {infoGame["category"]} </span>
                </h2>
                <button className="submit_score" onClick={handleSubmitScore}>Submit Score Game</button>
                <div className='gamedetail__info'>
                    <p className='gamedetail__desc'>{infoGame["description"]}</p>
                </div>
            </div>
            }
            

        </>
    );
}

export default GameDetail;