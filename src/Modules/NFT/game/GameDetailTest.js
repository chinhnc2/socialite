import React, { useCallback, useState, useEffect } from "react";
import './GameDetail.css'
import { useHome } from "../Hooks/homeNFT"
import { useParams } from "react-router-dom";
import {usePlayer} from "../Hooks/playerNFT"
import axios from 'axios';
import Modal from "../Modal/Modal";
import NotiScore from "./NotiScore";
import NotiSuccess from "../items/NotiSuccess";
function GameDetailTest(props) {

    const [score, setScore] = useState(0);
    const [infoGame, setInfoGame] = useState({});
    const [stateModal, setStateModal] = useState(false)
    const [sumscore, setSumScore] = useState(0);
    const {players,loadPlayerAction,gamePlayedAction } = usePlayer();
        
    const { gameid  , loadGameidAction ,loadGameAction } = useHome()
    let { id } = useParams();
    useEffect(() => {
        loadGameidAction(id)
        const randomID = () => {
            return 1000 + Math.trunc((Math.random() * 9000));
        }
        // loadGameidAction(id)
        // console.log(gameid)
        // setInfoGame(gameid)
       
        const newScore = randomID();
    
        setScore(newScore)
       
    },[])
    useEffect(() => {
        console.log(gameid)
        setInfoGame(gameid)
    }, [gameid])
    function handleSubmitScore(e) {
        e.preventDefault();
        console.log(123)
        const data = {
            "id": id,
            "score" :score ,
            "_id" : "62458066104426474c8a6b50"
        } 
        gamePlayedAction(data)
        setStateModal(true)
    }
    function offModal(){
        setStateModal(false)
    }

    return (
        <>  
            {stateModal && <Modal>
                <NotiScore offModal={offModal} sumscore={sumscore} score={score}/>
            </Modal>}
            <div className='main__gamedetail'>
                <h2 className='gamedetail__name'>TABLE TENNIS WORLD TOUR</h2>
                <iframe src="https://games.cdn.famobi.com/html5games/f/fruit-ninja/v010/?fg_domain=play.famobi.com&fg_aid=A1000-100&fg_uid=5450f1ec-af9e-4859-832a-69da3715c378&fg_pid=5a106c0b-28b5-48e2-ab01-ce747dda340f&fg_beat=138&original_ref=https%3A%2F%2Fhtml5games.com%2F"></iframe>
                <h2 className="yourScore">Your Score : <span>{score}</span></h2>
                <h2 className="description__game">Category :  <span> Giải Trí </span>
                </h2>
                <button className="submit_score" onClick={handleSubmitScore}>Submit Score Game</button>
                <div className='gamedetail__info'>
                    <p className='gamedetail__desc'>Become a paddle master and play like a champion in this challenging ping pong game with realistic physics! Choose from 20 countries and battle 60 opponents from all over the world. The touch controls are super easy to learn: simply move the table tennis bat and hit the ball in the direction you want. Train spin and power shots to throw your opponent off guard, but watch out as they might do the same to you! Can you collect all trophies and win the World Tour?</p>
                </div>
            </div>
            123  
        </>
    );
}

export default GameDetailTest;