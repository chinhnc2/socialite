import React from 'react';
import { usePlayer } from '../Hooks/playerNFT';
import "./NotiScore.css"
function NotiScore({offModal,sumscore,score}) {
    const {players,loadPlayerAction,gamePlayedAction } = usePlayer();
    let sumScoreNew = players.score + score
    return (
        <div className='submit_Score'>
            <div className='submit_info'>
            <h2 className='score__player'>Bạn đã chơi được  <span>{score}</span> điểm</h2>
            <h2 className='score__player'>Số điểm hiện tại của bạn là : <span>{sumScoreNew} </span></h2>
            <button className='btn__score__ok' onClick={offModal}>OK</button>
            </div>
        </div>
    );
}

export default NotiScore;
