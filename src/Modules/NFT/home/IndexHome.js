import React, { useState, useEffect } from "react";
import { useHome } from "../Hooks/homeNFT"

import style from './IndexHome.module.css';
import { Link } from 'react-router-dom'
function IndexHome({ search }) {

    const { games, loadGameAction } = useHome();
    const [allGame, setAllGame] = useState({})
    const [data, setData] = useState({})
    //const [infoGame,setInfoGame] = useState([])
    console.log(search)
    useEffect(() => {
        console.log(456)
        
        // setInfoGame(games)
        console.log(games)
        setAllGame(games)
        setData(games)
    }, [games]);

    useEffect(() => {
        loadGameAction(10);
    }, [])

    
    useEffect(()=>{
            // loadGameAction(10);
            console.log(123)
            if(data.length>0){
                if(search.length>0 ){
                    let itemgame = data.filter(x=>x["game_name"].toUpperCase().indexOf(search.toUpperCase())>= 0 )
                    console.log(itemgame)
                    setAllGame(itemgame)
                }
                else{
                    setAllGame(data)
                }

            }
    },[search])
    
    function categoryTag(e){
        console.log("aaaaaaa")
        console.log(e)
        let valueTag = e.target.innerHTML;
        console.log(valueTag)
        if(data.length>0){
            let gameTag = data.filter(x=>x.category.join(" ").toUpperCase().indexOf(valueTag.toUpperCase())>=0)
            setAllGame(gameTag)
        }

        // if(valueTag =="Tất cả thể loại"){
        //     setAllGame(games)
        // }




    }


    function renderListGame() {
        console.log(allGame)
            if (allGame?.length > 0) {
                return allGame.map((info, index) => {
                    return (
                        <div key={info["_id"]} className={style.game__item}>
                            <Link target="" to={"/nft-game/" + info["_id"]}><img className={style.game__item__img} alt="GARDEN BLOOM"
                                src={info.game_img} /> </Link>
                            <div className={style.game__info}>
                                <h5 className={style.game__name}>{info.game_name}</h5>
                                <h5 className={style.game__source}>HTML5</h5>
                                <p className={style.game__view}><span>78%</span> 35,368,807 chơi</p>
                            </div>
                        </div>
                    )


                })
            }
    }



    return (
        <>
            <div className={style.main__content}>
                <div className={style.container_container__main}>
                    <div className={style.main}>
                        <div className={style.main__category}>
                            {/* <div className={style.category__list}> */}

                            <ul className={style.category__list}>
                                <li  onClick={categoryTag} value= "Hoạt Hình" >
                                    <Link className={style.category__list__item} >
                                        <i class='bx bx-target-lock'></i>
                                        <div className={style.item__info} >
                                            <h5 className={style.tagCategory}>Hoạt Hình</h5>
                                            <p>1,430 Game</p>
                                        </div>
                                    </Link>
                                </li>
                                <li onClick={categoryTag} value= "3D">
                                    <Link className={style.category__list__item}>
                                        <i class='bx bx-wink-smile' ></i>
                                        <div className={style.item__info}>
                                            <h5 className={style.tagCategory}>3D</h5>
                                            <p>2,639 Game</p>
                                        </div>
                                    </Link>
                                </li>
                                <li onClick={categoryTag} value= "Trí tuệ">
                                    <Link className={style.category__list__item}>
                                        <i class='bx bx-knife' ></i>
                                        <div className={style.item__info}>
                                            <h5 className={style.tagCategory}>Trí tuệ</h5>
                                            <p>233 Game</p>
                                        </div>
                                    </Link>
                                </li>
                                <li onClick={categoryTag} value= "Bóng đá">
                                    <Link className={style.category__list__item}>
                                        <i class='bx bx-football' ></i>
                                        <div className={style.item__info}>
                                            <h5 className={style.tagCategory}>Bóng đá</h5>
                                            <p>1,632 Game</p>
                                        </div>
                                    </Link>
                                </li>

                                <li onClick={categoryTag} value= "Chiến thuật">
                                    <Link className={style.category__list__item}>
                                        <i class='bx bxs-chess' ></i>
                                        <div className={style.item__info}>
                                            <h5 className={style.tagCategory}>Chiến thuật</h5>
                                            <p>250 Game</p>
                                        </div>
                                    </Link>
                                </li>
                                <li onClick={()=>categoryTag()} value= "Đua xe">
                                    <Link className={style.category__list__item}>
                                        <i class='bx bx-car' ></i>
                                        <div className={style.item__info}>
                                            <h5>Đua xe</h5>
                                            <p>753 Game</p>
                                        </div>
                                    </Link>
                                </li>
                                <li onClick={categoryTag}>
                                    <Link className={style.category__list__item}>
                                        <h5 className={style.category_all} >Tất cả thể loại</h5>
                                    </Link>
                                </li>
                            </ul>
                            {/* </div> */}
                        </div>
                        <div className={style.main__hastag}>
                            <ul className={style.hastag__list}>
                                <li className={style.hastag__list__item}>
                                    <h5>2 Người chơi</h5>
                                    <p>564</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Súng</h5>
                                    <p>843</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Nấu ăn</h5>
                                    <p>239</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Thời trang</h5>
                                    <p>2,166</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Chiến tranh</h5>
                                    <p>564</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Zombie</h5>
                                    <p>544</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Trốn thoát</h5>
                                    <p>208</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Bắn tỉa</h5>
                                    <p>108</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Nhiều người chơi</h5>
                                    <p>416</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Xe hơi</h5>
                                    <p>1,111</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Giả lập</h5>
                                    <p>406</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Bom</h5>
                                    <p>293</p>
                                </li>
                                <li className={style.hastag__list__item}>
                                    <h5>Tất cả các thẻ</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={style.main__seachtop}>
                    <div className={style.seachtop}>
                        <h4>TÌM KIẾM HÀNG ĐẦU</h4>
                        <ul className={style.searchtop__list}>
                            <li className={style.searchtop__item__language}>
                                <i class="fa-solid fa-flag"></i>
                                <i class='bx bx-chevron-down' ></i>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>đua xe</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>uno</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>free fire</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>vex</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>pokemon go</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>io</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>fnf</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>bắn súng</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>minecraft</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>papa</h5>
                            </li>
                            <li className={style.searchtop__item}>
                                <h5>among us</h5>
                            </li>
                            <li className={style.searchtop__item__more}>
                                <h5>Nhiều hơn</h5>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className={style.content__game}>
                    <h1 className={style.title__game}>
                        Trò chơi (83,234)
                    </h1>

                    <div className={style.game__list}>
                        {renderListGame()}
                        <div  className={style.game__item}>
                            <Link target="" to={"/nft-game-testid"}><img className={style.game__item__img} alt="GARDEN BLOOM"
                                src="https://img.cdn.famobi.com/portal/html5games/images/tmp/FruitNinjaTeaser.jpg?v=0.2-ac565862" /> </Link>
                            <div className={style.game__info}>
                                <h5 className={style.game__name}>FRUIT NINJA</h5>
                                <h5 className={style.game__source}>HTML5</h5>
                                <p className={style.game__view}><span>78%</span> 35,368,807 chơi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={style.content__selectpage}>
                    <button className={style.back__page}><i className='bx bx-left-arrow-alt' ></i></button>
                    <ul className={style.list__page}>
                        <li className={style.item__page} >1</li>
                        <li className={style.item__page}>2</li>
                        <li className={style.item__page}>3</li>
                        <li className={style.item__page}>4</li>
                        <li className={style.item__page}>5</li>
                        <li className={style.item__page}>...</li>
                        <li className={style.item__page}>196</li>
                        <li className={style.item__page}>197</li>
                        <li className={style.item__page}>198</li>
                        <li className={style.item__page}>199</li>
                        <li className={style.item__page}>200</li>
                    </ul>
                    <button className={style.back__page}><i className='bx bx-right-arrow-alt'></i></button>

                </div>
                <div className={style.content__description}>
                    <h2 > SCEP Games - Các trò chơi Trực tuyến Miễn phí tại SCEP.com</h2>
                    <p>Chơi game miễn phí trên SCEP. Các game hhai người chơi và game trang điểm hàng đầu. Tuy nhiên, game mô phỏng và game nấu ăn cũng rất phổ biến trong các người chơi. Y8 Games cũng hoạt động trên các thiết bị di động và có nhiều game cảm ứng cho điện thoại. Ghé thăm Y8.com và gia nhập với cộng đồng người chơi ngay.</p>
                </div>
            </div>
        </>
    );
}

export default IndexHome;