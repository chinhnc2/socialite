import { REQUEST } from 'Stores'
import {
  LOAD_GAME,
  ADD_GAME,
  LOAD_GAMEID
} from './constants'

export function loadGame(payload) {
  return {
    type: REQUEST(LOAD_GAME),
    payload
  }
}
export function loadGameid(payload){
  return {
    type: REQUEST(LOAD_GAMEID),
    payload
  }
}

export function addGame(payload) {
  return {
    type: REQUEST(ADD_GAME),
    payload
  }
}
