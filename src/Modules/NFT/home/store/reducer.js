import { createReducer, updateObject, REQUEST, SUCCESS, FAILURE } from 'Stores'
import { ADD_GAME, LOAD_GAME, LOAD_GAMEID } from './constants'
import { setLocalStorage, STORAGE } from 'Utils';
import { getGames } from 'APIs/game.api';

export const initialState = {
  isLoading: false,
  error: null,
  games: [],
  gameid: {}
  // games: JSON.parse(localStorage.getItem("games")) || [],
}

//LOAD Game

function loadingGame(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function LoadedGame(state, { payload }) {
  //const { profile, token } = payload
  // console.log(payload)
  // console.log(state)
  //localStorage.setItem("games",JSON.stringify(payload.game))
  return updateObject(state, {
    isLoading: false,
    games: payload.game
  })

}

function LoadGameError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}
//LOADING GAME ID

function loadingGameid(state) {
  console.log(state)
  return updateObject(state, {
    isLoading: true
  })
}

function LoadedGameid(state, { payload }) {
  console.log(payload)
  console.log(state)
  return updateObject(state, {
    isLoading: false,
    gameid: payload.game
  })

}

function LoadGameidError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}
// ADD GaME

function addingGame( state ){
  return updateObject(state, {
    isLoading: true
  })
}
function addedGame(state , {payload}){

  // console.log(data)
  console.log(state)
  console.log(payload)

  const {game} = payload
  const newGame = [...state.games];
  console.log(newGame)
  newGame.push(game)
  console.log(newGame)

  return updateObject(state, {
    isLoading: false,
    // games: payload.game
    games: newGame
    // games: [...state.games, game]
    // games: [...state.payload.game, payload.game]
  })
}


function addGameError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}

// Slice reducer
export default createReducer(initialState, { //Object với các key là các action type và value là hàm xử lý state của type đó.
  [REQUEST(LOAD_GAME)]: loadingGame,
  [SUCCESS(LOAD_GAME)]: LoadedGame,
  [FAILURE(LOAD_GAME)]: LoadGameError,
  [REQUEST(LOAD_GAMEID)]: loadingGameid,
  [SUCCESS(LOAD_GAMEID)]: LoadedGameid,
  [FAILURE(LOAD_GAMEID)]: LoadGameidError,
  [REQUEST(ADD_GAME)]: addingGame,
  [SUCCESS(ADD_GAME)]: addedGame,
  [FAILURE(ADD_GAME)]: addGameError,
})
