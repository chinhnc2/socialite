import { put, takeLatest } from 'redux-saga/effects'

import { REQUEST, SUCCESS, FAILURE } from 'Stores'
import { addGames, getGames, getGamesId} from 'APIs/game.api'
import { getLocalStorage, STORAGE } from 'Utils'
import { ADD_GAME, LOAD_GAME, LOAD_GAMEID } from './constants'

export function* loadGameSaga(action) {
  try {
    const data = yield getGames();
    console.log(data)
    yield put({
      type: SUCCESS(LOAD_GAME),
      payload: {
        game: data
      }
    })
  } catch (error) {
    yield put({
      type: FAILURE(LOAD_GAME),
      error
    })
  }
}
export function* loadGameidSaga(action) {
  console.log(action)
  try {
    const data = yield getGamesId(action.payload);
    console.log(data)
    yield put({
      type: SUCCESS(LOAD_GAMEID),
      payload: {
        game: data
      }
    })
  } catch (error) {
    yield put({
      type: FAILURE(LOAD_GAME),
      error
    })
  }
}

export function* addGameSaga(action) {
  try {
    const data = yield addGames(action.payload);
    yield put({
      type: SUCCESS(ADD_GAME),
      payload: {
        game: data
      }
    })
    const dataload = yield getGames();
    console.log(dataload)
    yield put({
      type: REQUEST(LOAD_GAME),
      payload: {
        game: dataload
      }
    })
  } catch (error) {
    yield put({
      type: FAILURE(ADD_GAME),
      error
    })
  }
}
export default function* gameSaga() {
  yield takeLatest(REQUEST(LOAD_GAME), loadGameSaga);
  yield takeLatest(REQUEST(LOAD_GAMEID), loadGameidSaga);
  yield takeLatest(REQUEST(ADD_GAME), addGameSaga);

}

