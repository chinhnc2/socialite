/* eslint-disable arrow-parens */
/* eslint-disable implicit-arrow-linebreak */

/**
 * The global state selectors
 */

 import { createSelector } from 'reselect'
 import { initialState } from './reducer'
 
 const selectGames = state => state.home || initialState
 
 const makeSelectGames = () =>
   createSelector(
     selectGames,
     state => state
   )

 export {
    selectGames,
   makeSelectGames
 }
 