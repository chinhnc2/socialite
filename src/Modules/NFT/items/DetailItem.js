
import React, { useState , useEffect } from 'react';
import './DetailItem.css'
import axios from 'axios';
import { useParams } from "react-router-dom";
import { usePlayer } from 'Modules/NFT/Hooks/playerNFT';
function DetailItem(props) {
    const {players,loadPlayerAction} = usePlayer()
    const [item,setItem] = useState({})
    let {id} = useParams()
    useEffect(()=>{

        axios.get("http://localhost:2707/v1/items/item/"+id)
        .then(res=>{
                console.log(res.data)
                console.log(typeof(res.data))
                setItem(res.data)
        })
        .catch(err=>{
            console.log(err)
        })
    },[])
    function changeItem(){
        let data = {
            "_id": "62458066104426474c8a6b50",
            "item_id" : item["_id"],
            "score" : players.score - item.price
        }
        console.log(data)
        axios.post("http://localhost:2707/v1/player/changeItem",data)
        .then(res=>{
                console.log(res.data)
                console.log(typeof(res.data))
                if(res.data.ok ==1){
                    console.log(123)
                    loadPlayerAction(123)
                }
        })
        .catch(error=>{
            console.log(error)
        })
    }
    return (
        <div className='detail'>
            <div className='detail__item'>
                <h2 className='detail__item__title'>Chi tiết vật phẩm</h2>
                <div className='info__item'>
                    <h2 className='detail__item__name'>{item["name_item"]}</h2>
                    <div className='detail__item__info'>
                        <div className='review__item'>
                            <img src={item["item_img"]}/>
                            <div className='star__item'>
                                <i className='bx bxs-star' ></i>
                                <i className='bx bxs-star' ></i>
                                <i className='bx bxs-star' ></i>
                                <i className='bx bxs-star' ></i>
                                <i className='bx bxs-star' ></i>
                            </div>
                        </div>
                        <div className='description__item'>
                            <p>{item["description"]}</p>
                            <h5 className='score'>Giá: <span>{item["price"]}</span> điểm</h5>
                            {/* <h5 className='score'>Quantity: <span>{item["quantity"]}</span> </h5> */}
                            <div className='btn__select'>
                                <button className='btn__item__changes' onClick={changeItem}>Đổi</button>
                                <button className='btn__item__cancel'>Hủy</button>
                            </div> 
                        </div>
                    </div>
                                      
                </div>

            </div>
            
        </div>
    );
}

export default DetailItem;