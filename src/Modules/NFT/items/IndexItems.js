import React, { useState , useEffect } from 'react';
import './IndexItem.css'
import IndexChange from './changeItems/IndexChange'
import Modal from '../Modal/Modal';
import {Link} from 'react-router-dom'
import axios from 'axios';
import { usePlayer } from '../Hooks/playerNFT';
import NotiSuccess from './NotiSuccess';


function IndexItems(props) {
    const [stateModal,setStateModal] = useState(false)
    const [item,setItem] = useState({})
    const [player,setPlayer] = useState({})
    const [idItem,setIdItem] = useState({})
    const [successChange, setSuccessChange] = useState(false)
    const {players,loadPlayerAction} = usePlayer()


    useEffect(()=>{
        loadPlayerAction(123)

        axios.get("http://localhost:2707/v1/items/item")
        .then(res=>{
                console.log(res.data)
                console.log(typeof(res.data))
                setItem(res.data)
        })
        .catch(err=>{
            console.log(err)
        })
        console.log(players)
    },[])


    const  showModal = (idItem) =>{
        console.log(idItem)
        setIdItem(idItem)
        setStateModal(true)
        console.log(stateModal)
    }
    const  offModal = () =>{
        setStateModal(false)
        console.log(stateModal)
    }
    const  modalTest = () =>{
        // setStateModal(false)
        console.log("bat modal")
    } 

    const offModalSuccess = () =>{
        setSuccessChange(false)
    }

    function renderPlayer(){
        return(
            <div className='content__infor'>
                        <img src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt=''/>
                        <div className='infor__user'>
                            <p className='user__name'>{players["playerName"]}</p>
                            <p className='user__sex'>Giới tính: <span>{players.sex}</span></p>
                            <p className='user__game'>Số game đã chơi: <span>{Object.keys(players).length >0 ? players.game_id.length : 0 }</span></p>
                            <p className='user_dayplay'> Số ngày đã chơi <span>130</span> days </p>
                            <p className='user__score'>Điểm đang có: <span>{players.score}</span></p>
                            <p className='user__change'>Vật phẩm đã đổi: <span>{Object.keys(players).length>0 ? players.item_id.length : 0 }</span> <i className='bx bxs-cart-alt' ></i></p>
                        </div>
            </div>
        )
    }
    
    function renderItem(){
        if(Object.keys(item).length>0){
            return Object.keys(item).map((key,index)=>{
                return (
                    <li className='items__changes'>
                                        <img src={item[key]["item_img"]} alt='' />
                                        <h5 className='items__name'>{item[key]["name_item"]}</h5>
                                        <p className='items__price'><span>{item[key]["price"]}</span> điểm</p>
                                        <div className='btn__item'>
                                            <button className='btn__item__changes' onClick={()=>showModal(item[key]["_id"])}>Đổi</button>
                                            <button className='btn__item__view '> <Link to={"/nft-game/detailItem/"+item[key]["_id"]}> Xem </Link></button>
                                        </div>
                    </li>
                )
            })
        }
        
    }
    function changeItem(changeItem){
        return changeItem
    }
    

    return (<>
            
            {stateModal && <Modal modalTest={modalTest}>
                <IndexChange idItem={idItem} changeItem={changeItem} stateModal={stateModal}  setStateModal= {setStateModal} offModal={offModal} setSuccessChange={setSuccessChange} successChange={successChange} />
            </Modal> }
            {successChange && <Modal>
                <NotiSuccess offModalSuccess={offModalSuccess}/>
            </Modal>}
            {/* { <Modal>
                <NotiSuccess offModal={offModal}/>
            </Modal>} */}
            <div className='main__items'>
                <div className='content__items'>
                    <h2>Vật Phẩm</h2>
                    {renderPlayer()}
                    <div className='all__items'>
                        <ul className='items__list'>
                            {renderItem()}

                        </ul>
                    </div>
                </div>
            </div>

    </>
    );
}

export default IndexItems;