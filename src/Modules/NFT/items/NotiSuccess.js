import React from 'react';
import './NotiSuccess.css'
function NotiSuccess({offModalSuccess}) {
    return (
        <div className='success_Change'>
            <h2>Đổi vật phẩm thành công</h2>
            <button className='btn__score__ok' onClick={offModalSuccess}>OK</button>
        </div>
    );
}

export default NotiSuccess;