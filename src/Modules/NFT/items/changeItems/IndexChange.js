import React, { useState , useEffect } from 'react';
import './IndexChange.css';
import axios from 'axios';
import { usePlayer } from 'Modules/NFT/Hooks/playerNFT';
// import { useNavigate } from "react-router-dom";

function IndexChange({idItem,stateModal,setStateModal,offModal, setSuccessChange , successChange}) {
    // const navigate = useNavigate()
    const [item,setItem] = useState({})
    const [player,setPlayer] = useState({})
    const [score,setScore] = useState(0);
    const [err,setErr] = useState("")

    console.log(idItem)
    const {players,loadPlayerAction} = usePlayer()
    useEffect(()=>{


        axios.get("http://localhost:2707/v1/items/item/"+idItem)
        .then(res=>{
                console.log(res.data)
                console.log(typeof(res.data))
                setItem(res.data)
        })
        .catch(err=>{
            console.log(err)
        });
    },[])    

    function changeItem(offModal){
        if(players.score - item.price > 0 ){
            console.log("Đổi thành công")
            let data = {
                "_id": "62458066104426474c8a6b50",
                "item_id" : item["_id"],
                "score" : players.score - item.price
            }
            console.log(data)
            axios.post("http://localhost:2707/v1/player/changeItem",data)
            .then(res=>{
                    console.log(res.data)
                    console.log(typeof(res.data))
                    if(res.data.ok ==1){
                        console.log(123)          
                        console.log(offModal)
                        setStateModal(!stateModal)
                        loadPlayerAction(123)
                        setSuccessChange(!successChange)
                    }
            })
            .catch(error=>{
                console.log(error)
            })
        }
        else {
            setErr("Đổi thất bại, Điểm của bạn hiện không đủ!")
        }
        
    }

    function renderChangeItem(){
        let scoreAfterChange = player["score"] - item["price"];
        // setScore(scoreAfterChange)
        return(
            <div className='content___change__detail'>
                    <div className='changeItem__info'>
                        <img src={item["item_img"]} alt='' />
                        <h4 className='changeItem__name'>{item["name_item"]}</h4>
                        <p className='changeItem__store'>{item["description"]}</p>
                    </div>
                    <div className='changeItem__action__price'>
                        <div className='changeItem__price'>
                            <h3 className='score__Item score__item__present'>Giá sản phẩm: <span>{item["price"]} </span> điểm</h3>
                            <h3 className='score__Item score__user__present   '>Điểm đang có: <span>{players["score"]} </span> điểm</h3>
                            <h3 className='score__Item score__user__rest'>Điểm còn lại: <span>{players["score"]-item["price"] } </span> điểm</h3>
                        </div>
                        <div className='btn__action'>
                            <button className='btn__action__change' onClick={()=>changeItem(offModal)} >Đổi</button>
                            <button className='btn__action__cancle' onClick={offModal} >Hủy</button>
                        </div>
                        <h5 className='error__change'>{err}</h5>
                        
                    </div>

                </div>
        )
    }


    return (
        <div className='main__change'>
            <div className='content___change'>
                <h2>Đổi vật phẩm</h2>
                {renderChangeItem()}
            </div>
        </div>
    );
}

export default IndexChange;