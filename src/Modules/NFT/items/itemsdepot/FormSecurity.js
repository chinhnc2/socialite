import React,{useState} from 'react';

import { useForm, FormProvider } from "react-hook-form";
import {FormInput,FormUploadImage } from '../../../../Components/index'
import './FormSecurity.css'

function FormSecurity({modalOff , setRequestModalForm , requestModalForm
     , getDataPass , keyworldSecurity , successPassWord,setSuccessPassWord}) {
    const form = useForm();
    const { handleSubmit, reset } = form;
    const [errorSecurity, setErrorSecurity] = useState("")



    const onSubmit = (data)=>{
        console.log(data)
        if(data.passworld_security == data.passworld2_security){
            localStorage.setItem("passwordDepot",JSON.stringify(data))
            getDataPass(data)
            setRequestModalForm(!requestModalForm)
            setSuccessPassWord(!successPassWord)
        }
        else{
            setErrorSecurity("Mật khẩu không trùng khớp, Nhập lại")
        }
    }

    return (
        <div className='form__password--security'>
                <FormProvider {...form} >
                    <form className="form-security" onSubmit={handleSubmit(onSubmit)}>
                    {/* <form className="form-security" > */}
                         <h1>{keyworldSecurity}</h1>
                        <div className="form-inputPassworld">
                            <p>Nhập mật khẩu</p> 
                            <FormInput className="input__pass"
                            name="passworld_security" 
                            required/>
                        </div>
                        <div className="form-inputPassworld">
                            <p>Xác nhận mật khẩu</p> 
                            <FormInput className="input__pass2"
                            name="passworld2_security" 
                            required/>
                        </div> 
                        <h2 className='error__lock'>{errorSecurity}</h2>
                        <div className='btn__action--security'>
                            <div className="form-btn__accept">
                                <input className="btn-submit" type='submit' value='Xác nhận' />
                            </div> 
                            <div className="form-btn__cancel">
                                <input className="btn-submit" type='submit' value='Hủy' onClick={modalOff} />
                            </div> 
                        </div>

                        
                    </form>
                </FormProvider>
        </div>
    );
}

export default FormSecurity;