import { usePlayer } from 'Modules/NFT/Hooks/playerNFT';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import "./ItemDepot.css"
import Modal from 'Modules/NFT/Modal/Modal';
import DepotNotify from './depotNotify';
import FormSecurity from './FormSecurity';
import { set } from 'react-hook-form';
import LockSecurity from './LockSecurity';



function ItemsDepot(props) {
    const { players, update,loadPlayerAction, sellItemAction , getItemdepotAction} = usePlayer();
    const [requestModal,setRequestModal] = useState(false);
    const [requestModalForm,setRequestModalForm] = useState(false);
    const [requestModalLock,setRequestModalLock] = useState(false);
    const [successPassWord,setSuccessPassWord] = useState(false);
    const [createPass,setCreatePass] = useState(false);
    const [request,setRequest] = useState(" ");
    const [img_item,setImgItem] = useState(" ");
    const [idItemRequest,setidItemRequest] = useState("")
    const [dataRequest,setDataRequest] = useState({})
    const [dataPassword,setDataPassWord] = useState({})
    const [deletePass,setDeletePass] = useState(false)
    const [keyworldSecurity , setKeyworldSecurity] = useState(" ")

    useEffect(() => {
        loadPlayerAction(123);
        let getPassWordDepot = localStorage.getItem("passwordDepot")
        console.log(getPassWordDepot)
        if(getPassWordDepot != null){
            console.log("co pass")
            getPassWordDepot = JSON.parse(getPassWordDepot)
            setDataPassWord(getPassWordDepot)
            setRequestModalLock(true)

        }
        else{
            setRequestModalLock(false)
        }
    }, [])

    useEffect(()=>{
        loadPlayerAction(123);
        // if(dataPassword !=null){
        //     setRequestModalLock(false)
        // }
    }, [dataPassword])

    function renderPlayer() {
        return (
            <div className='content__infor'>
                <img src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt='' />
                <div className='infor__user'>
                    <p className='user__name'>{players["playerName"]}</p>
                    <p className='user__sex'>Giới tính: <span>{players.sex}</span></p>
                    <p className='user__game'>Số game đã chơi: <span>{Object.keys(players).length > 0 ? players.game_id.length : 0}</span></p>
                    <p className='user_dayplay'> Số ngày đã chơi <span>130</span> days </p>
                    <p className='user__score'>Điểm đang có: <span>{players.score}</span></p>
                    <p className='user__change'>Vật phẩm đã đổi: <span>{Object.keys(players).length > 0 ? players.item_id.length : 0}</span> <i className='bx bxs-cart-alt' ></i></p>
                </div>
            </div>
        )
    }
    const  sellItem = async (idItem,score,image_item,name_item)=>{
        //  setidItemRequest(idItem)
        setDataRequest({
            "_id" : "62458066104426474c8a6b50",
            "item_id" : idItem,
            "score": score
            }) 
        setRequest("Bán");
        setImgItem(image_item);
        setRequestModal(true)
        // console.log(score)
      
        // sellItemAction(data)
        // console.log(update)
        // loadPlayerAction(123);

    }
    const getItemDepotPlayer = async(idItem,name_item , image_item) =>{
        setidItemRequest(idItem)
        setRequest("Nhận");
        setImgItem(image_item);
        setRequestModal(true)
        // let data = {
        //     "_id" : "62458066104426474c8a6b50",
        //     "item_id" : idItem,
        // }
        // console.log(idItem)
        // getItemdepotAction(data)
    }


    function renderItem() {
        if (Object.keys(players).length > 0) {
            return players["item_id"].map((value, index) => {
                return (
                    <li key={index}>
                        <img className="depot__item_img" src={value.item_img} alt='' />
                        <h4 className="depot__item_name">{value.name_item}</h4>
                        <p className="depot__item_score">Score: <span>{value.price}</span></p>
                        <div className='btn__action'>
                            {/* Nhận thì sẽ gửi sản phẩm về địa chỉ của player */}
                            <button className='btn__action__change' onClick={()=>getItemDepotPlayer(value._id,value.name_item,value.item_img)}>Nhận</button> 
                            {/* Bán thì xóa item của player và đưa vào store bán*/}
                            <button className='btn__action__cancle' onClick={()=>sellItem(value._id,value.price,value.item_img,value.name_item)}>Bán</button>
                        </div>
                    </li>
                )
            })
        }

    }

    function modalOff(){
        setRequestModal(false)
        setRequestModalForm(false)
        setCreatePass(false)
        setDeletePass(false)
    }


    function formSecurity(){
        setKeyworldSecurity("Tạo mật khẩu")
        setRequestModalForm(true)
    }
    function formSecurityLock(){
        setKeyworldSecurity("Thay đổi mật khẩu")
        if(Object.keys(dataPassword).length>0){
            setRequestModalForm(true)
        }
        else {
             setCreatePass(true)
        }
    }

    function renderButton(){
        console.log(dataPassword)
        if(Object.keys(dataPassword).length>0){
            console.log("disibled")
            return(
                <button className='btn-btn__security' disabled value="Tạo mật khẩu" onClick={formSecurity}>Tạo mật khẩu kho </button>
            )
        }
        else{
            console.log(Object.keys(dataPassword).length)
            return(
                <button className='btn-btn__security'  value="Tạo mật khẩu" onClick={formSecurity}>Tạo mật khẩu kho </button>
            )
        }
    }
    function getDataPass(data){
        console.log(data)
        setDataPassWord(data)
    }
    function deleteSecurity(){
        localStorage.removeItem('passwordDepot')
        setDataPassWord({})
        setDeletePass(true)
    }

    function lockSecurity(){
        if(Object.keys(dataPassword).length>0){
        setRequestModalLock(true)
        }
        else {
            setCreatePass(true)
        }
    }


    return (
        <>
             {requestModal && <Modal>
                <DepotNotify request={request}  modalOff={modalOff} img_item={img_item} 
                idItemRequest={idItemRequest}  setRequestModal={setRequestModal} requestModal={requestModal} 
                dataRequest={dataRequest} successPassWord= {successPassWord} setSuccessPassWord={setSuccessPassWord}/>
            </Modal>}
            {
                requestModalForm && <Modal>
                    <FormSecurity modalOff={modalOff}  setRequestModalForm={setRequestModalForm} requestModalForm={requestModalForm} getDataPass={getDataPass}  keyworldSecurity={keyworldSecurity}/>
                </Modal>
            }

            {
                requestModalLock && <Modal>
                    <LockSecurity  modalOff={modalOff} requestModalLock={requestModalLock} setRequestModalLock={setRequestModalLock} />
                </Modal>
            }
            {
                createPass && <Modal>
                    <div className='create_passSecurity'>
                        <h2>Kho chưa có mật khẩu, vui lòng tạo mật khẩu</h2>
                        <button onClick={modalOff}>Xác nhận</button>
                    </div>
                </Modal>
            }
            {
                deletePass && <Modal>
                    <div className='create_passSecurity'>
                        <h2>Xoát mật khẩu kho thành công</h2>
                        <button onClick={modalOff}>Xác nhận</button>
                    </div>
                </Modal>
            }
             {
                successPassWord && <Modal>
                    <div className='create_passSecurity'>
                        <h2>{keyworldSecurity} thành công</h2>
                        <button onClick={modalOff}>Xác nhận</button>
                    </div>
                </Modal>
            }
            <div className='main__items'>
                <div className='content__items'>
                    <h2>Kho Vật Phẩm</h2>
                    {renderPlayer()}
                    <div className="security__item">
                        <div className='security__item--action'>
                            {renderButton()}
                            <button className='btn-btn__security' onClick={formSecurityLock} value="Thay đổi mật khẩu">Thay đổi mật khẩu kho </button>
                            <button className='btn-btn__security'onClick={lockSecurity} >Khóa kho</button>
                            <button className='btn-btn__security'onClick={deleteSecurity} >Xóa mật khẩu kho</button>
                        </div>
                    </div>
                    <div className='all__items'>
                        <h2 >Đang Sở Hữu</h2>
                        <ul className='items__list__depot'>
                            {renderItem()}
                        </ul>
                    </div>
                </div>
            </div>    
        </>
    );
}

export default ItemsDepot;