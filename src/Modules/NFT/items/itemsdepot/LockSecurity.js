import React, { useEffect, useState } from 'react';
import { useForm, FormProvider } from "react-hook-form";
import {FormInput,FormUploadImage } from '../../../../Components/index'
import './FormSecurity.css'

function LockSecurity({modalOff ,requestModalLock , setRequestModalLock}) {
    const form = useForm();
    const { handleSubmit, reset } = form;
    const [getDataPass, setGetDataPass] = useState({})
    const [dataInput,setDataInput] = useState({})
    const [errorLock, setErrorLock] = useState("")


    const onSubmit = (data)=>{
        let getPassWordDepot = localStorage.getItem("passwordDepot")
        console.log(getPassWordDepot)
        if(getPassWordDepot != null){
            console.log("co pass" + getPassWordDepot)
            getPassWordDepot = JSON.parse(getPassWordDepot)
         
            if(data.passworld_security == getPassWordDepot.passworld_security &&  data.passworld2_security == getPassWordDepot.passworld2_security ){
                setRequestModalLock(!requestModalLock)
            }
            else{
                setErrorLock("Mở khóa thất bại, nhập lại mật khẩu")
            }
        }
        console.log(data.passworld_security)
        console.log(getDataPass)
        
    }
    return (
        <div className='form__password--security'>
                <FormProvider {...form} >
                    <form className="form-security form-security__lock " onSubmit={handleSubmit(onSubmit)}>
                    {/* <form className="form-security" > */} 
                         <h1>Nhập mật khẩu để mở khóa kho</h1>  
                        <div className="form-inputPassworld">
                            <p>Nhập mật khẩu</p> 
                            <FormInput className="input__pass"
                            name="passworld_security" 
                            required/>
                        </div>
                        <div className="form-inputPassworld">
                            <p>Xác nhận mật khẩu</p> 
                            <FormInput className="input__pass2"
                            name="passworld2_security" 
                            required/>
                        </div> 
                        <h3 className='error__lock'>{errorLock}</h3>
                        <div className='btn__action--security'>
                            <div className="form-btn__accept">
                                <input className="btn-submit" type='submit' value='Mở khóa' />
                            </div> 
                        </div>
                        
                    </form>
                </FormProvider>
        </div>
    );
}

export default LockSecurity;