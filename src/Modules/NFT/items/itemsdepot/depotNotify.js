import { usePlayer } from 'Modules/NFT/Hooks/playerNFT';
import React from 'react';
import './depotNotify.css'
function DepotNotify({request ,modalOff, img_item , idItemRequest , setRequestModal , requestModal ,dataRequest}) {

    const { sellItemAction, getItemdepotAction} = usePlayer();

    const acceptRequest = () =>{
        if(request=="Nhận"){
            let data = {
             "_id" : "62458066104426474c8a6b50",
             "item_id" : idItemRequest,
         }
         getItemdepotAction(data)
         setRequestModal(!requestModal)
        }
        if(request=="Bán"){
            sellItemAction(dataRequest)
            setRequestModal(!requestModal)
        }
    }

    return (
        <div className='depotNotify__item'>
            <div className='depotNotify__request'>
                <h5>Bạn có muốn "  <span>{request} </span> "  không ?</h5>
                <div className='depotNotify__request--img'>
                    <img  src={img_item} alt = "img item"/>

                </div>
                <div className='btn__action'>
                    <button className='btn__action__change' onClick={acceptRequest}>Đồng ý</button>
                    <button className='btn__action__cancle' onClick={modalOff}>Hủy</button>
                </div>
            </div>
        </div>
    );
}

export default DepotNotify;