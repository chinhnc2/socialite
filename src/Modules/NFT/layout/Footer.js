import React from 'react';
import './Footer.css'
import imagelogo from '../.././../Assets/images/logo/SCEP_6.png'
function Footer(props) {    
    return (
        <div className='footer-nft'>
            <div className='footer__info'>
                <div className='footer__info__web footer__explain'>
                    <div className='footer__info__icon' >
                        <i class='bx bx-question-mark' ></i>
                    </div>  
                    <div className='footer__info__title'>
                         SCEP  là gì?
                    </div>
                    <div className='footer__info__desc'>
                    SCEP  Games là một đơn vị phát hành và phát triển game.
                     Nền tảng của SCEP  là mạng xã hội với 30 triệu người chơi và đang không ngừng phát triển. 
                     Trang web cũng có videos to watch như hoạt hình, video trò chơi, và hướng dẫn trò chơi. 
                     Danh mục giải trí phát triển hàng ngày new games được phá hành từng giờ. Vì SCEP .com có một lịch sử lâu dài, 
                     chúng tôi đã ghi lại các hiện tượng xã hội trên các game trên trình duyệt.
                      Nội dung này là một phương tiện nghệ thuật quan trọng và có thể cho thấy mọi người đã như thế nào trong những thời gian khác nhau.
                    </div>
                </div>
                <div className='footer__info__web footer__category'>
                    <div className='footer__info__icon' >
                        <i class='bx bxs-component'></i>
                    </div>
                    <div className='footer__info__title'>
                        Thể loại game
                    </div>
                    <div className='footer__info__desc'>
                    Trước đây, SCEP  nổi tiếng với các dòng game như arcade và games cổ điển khi Bubble Shooter là game trình duyệt đình đám nhất. Ngày nay, có nhiều dòng game đã trở nên nổi tiếng. Đáng chú ý là, 2 player games đã trở thành game trình duyệt nổi tiếng cùng với dress up games. Một phần trò chơi quan trọng cuối cùng là multiplayer games, chơi danh mục mở rộng của game mạng xã hội hỗ trợ Internet
                    </div>
                </div>
                <div className='footer__info__web footer_technology'>
                    <div className='footer__info__icon' >
                        <i className='bx bx-laptop'></i>
                    </div>
                    <div className='footer__info__title'>
                        Công nghệ
                    </div>
                    <div className='footer__info__desc'>
                    SCEP .com là ngôi nhà cho mọi game thủ trên bất kỳ thiết bị nào. Chơi phone games hoặc tải đồ họa 3D phong phú trên máy tính bằng cách chơi WebGL Games. Mặt khác, nếu bạn chỉ thích chơi game 2D thông thường, thì HTML5 games sẽ phù hợp với bạn. Nếu bạn muốn nhớ lại những ngày xưa cũ, hãy truy cập kho lưu trữ Flash games để chơi các trò chơi chưa hề có ở những nơi khác. Cuối cùng, đừng quên đăng ký SCEP  Account. Đây là mạng xã hội cộng đồng hỗ trợ người chơi.
                    </div>
                </div>
            </div>
            <div className='footer_more'>
                <div className='footer_more_logo'>
                    <img src={imagelogo} alt='logo'/>
                    <p>© 2022 SCEP.COM.</p>
                    <p>Đã đăng ký bản quyền.</p>
                </div>
                <div className='footer_more_game'>
                    <h4>Game SCEP</h4>
                    <ul>
                        <li>Game mới</li>
                        <li>Tải lên</li>
                        <li>Mới tốt nhất</li>
                        <li>Phổ biến nhất</li>
                        <li>Tải ứng dụng</li>
                    </ul>
                </div>
                <div className='footer_more_update'>
                    <h4>Cập nhập</h4>
                    <ul>
                        <li>Blog</li>
                        <li>Forum</li>
                        <li>Twitter</li>
                        <li>Facebook</li>
                        <li>Instagram</li>
                        <li>Discord</li>
                    </ul>
                </div>
                <div className='footer_more_company'>
                    <h4>Công ty</h4>
                    <ul>
                        <li>Điều khoản sử dụng</li>
                        <li>Chính sách bảo mật</li>
                        <li>Chính sách Cookie</li>
                        <li>Nhà phát hành Game</li>
                        <li>Các Nhà phát triển game</li>
                        <li>Mẫu email của chúng tôi</li>
                        <li>Gửi mail cho chúng tôi</li>
                    </ul>
                </div>
                <div className='footer_more_follow'>
                    <h4>THEO DÕI CHÚNG TÔI</h4>
                    <ul>
                        <li><i className='bx bxl-twitter'></i></li>
                        <li><i className='bx bxl-facebook-circle' ></i></li>
                        <li><i className='bx bxl-instagram-alt' ></i></li>
                        <li><i className='bx bxl-discord-alt' ></i></li>
                    </ul>
                </div>
                <div className='footer_more_img'>
                    <img src='https://img-hws.y8.com/assets/y8/footer_image1-422cedcbdbcd68bb2fbf253eafac355ee154ba97da9ad3c587d2e2052b8d7821.webp' alt='img footer'/>

                </div>

            </div>
        </div>
    );
}

export default Footer;