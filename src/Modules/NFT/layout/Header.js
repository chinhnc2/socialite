import React, { useEffect } from 'react';
import './Header.css';
import imagelogo from '../.././../Assets/images/logo/SCEP_6.png'
import {Link} from 'react-router-dom'
import { usePlayer } from '../Hooks/playerNFT';
function Header({ setSearch , search }) {


    const {players,loadPlayerAction} = usePlayer()
    console.log(Object.keys(players).length)

    useEffect(()=>{
        loadPlayerAction(123);
        console.log(players)
    },[])
    const searchCategory = (e) =>{
        console.log(e.target.value)
        setSearch(e.target.value);
    }

    return (
        <div className='header-nft'>
            <div className='header__main'>
                <div className='header__logo'>
                    <Link to="/nft-game">  <img src={imagelogo} alt='' /></Link>
                   
                </div>
                <div className='header__option'>
                    <h4 className='header__option--game'>Game</h4>
                    <h4 className='header__option--video'>Video</h4>
                </div>
                <div className="header__searchbox">
                    <input type="text" placeholder="Tìm kiếm trò chơi" value={search} onChange={searchCategory}/>
                    <i className="ri fa-solid fa-magnifying-glass"></i>
                </div>
                <div className='header__category'>
                    <div className='header__category--new'>
                        <i className="fa-solid fa-n"></i>
                        <h3 className="header__category--new--h3">Game Mới</h3>
                    </div>
                    <div className='header__category--list'>
                        <i className="fa-solid fa-shapes"></i>
                        <h3>Thể Loại</h3>
                    </div>
                </div>
               
                <div className='header__user'>
                     <div className='header__user'> 
                    <div className="dropdown header__user--avata">
                        <a className="dropdown" data-toggle="dropdown">
                        <img  src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt=''/>
                        </a>
                        <ul className="dropdown-menu user--avata">
                            <li><Link className="dropdown-item name" to="#">tranngoccuong668<p>20 các điểm</p></Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/player">Hồ sơ của tôi</Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/addGame">Thêm trò chơi</Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/indexItems">Đổi quà</Link></li>
                           
                            <li><Link className="dropdown-item" to="/itemDepot">Kho vật phẩm :  {Object.keys(players).length>0 ? players.item_id.length : 0 }  </Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/player">Các trò chơi đã chơi</Link></li>
                            <li><Link className="dropdown-item" to="/">Trang chủ</Link></li>
                        </ul>
                    </div>
                    </div>
                    {/* <div className="nav-item dropdown header__user--avata">
                        <a className="nav-link dropdown" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img  src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt=''/>

                        </a>    
                        <ul className="dropdown-menu user--avata" aria-labelledby="navbarDropdownMenuLink">
                            <li><Link className="dropdown-item name" to="#">tranngoccuong668<p>20 các điểm</p></Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/player">Hồ sơ của tôi</Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/addGame">Thêm trò chơi</Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/indexItems">Đổi quà</Link></li>
                            <li><Link className="dropdown-item" to="/itemDepot">Kho vật phẩm :  {Object.keys(players).length>0 ? players.item_id.length : 0 }  </Link></li>
                            <li><Link className="dropdown-item" to="/nft-game/player">Các trò chơi đã chơi</Link></li>
                            <li><Link className="dropdown-item" to="#">Đăng xuất</Link></li>
                        </ul>
                    </div> */}

                    <div className="dropdown header__user--info">
                        <a className="dropdown" data-toggle="dropdown">
                            <i className='bx bx-male-female'></i>
                        </a>
                        <div className="dropdown-menu user--info" >
                            <h5>Bộ lọc an toàn cho phép phụ huynh chọn những loại trò chơi có thể chơi.
                                Đặt mật khẩu và chọn các dán nhãn nên bị chặn</h5>                 
                            <form action='#'>
                                <p>* Mật khẩu</p>
                                <input type="text" />
                                <p>* Xác nhận mật khẩu</p>
                                <input type="text" />
                                <p>* Nhãn</p>
                                <input type="text" />
                                <button>Cập nhập bộ lọc an toàn</button>
                            </form>
                            
                            <p className='warrning'>
                            Xin lưu ý rằng Y8 là trang an toàn với thiếu niên. 
                            Các bậc phụ huynh có thể kiểm soát để hạn chế hơn nữa các trò chơi có sẵn
                            </p>
                        </div>
                    </div>
                    <div className='header__user--language'>
                        <i className="fa-solid fa-flag"></i>
                    </div>
                    
                </div>
            </div>


        </div>
    );
}

export default Header;