import React, { useState, useEffect } from "react";
import './PlayerInfo.css'
import {usePlayer} from "../Hooks/playerNFT"
// import {usePlayer} from "../Hooks/homeNFT"

function PlayerInfo(props) {

    const {players,loadPlayerAction} = usePlayer()
    console.log(Object.keys(players).length)

    useEffect(()=>{
        loadPlayerAction(123);
        console.log(players)
    },[])
    
    function renderGamePlayed(){
        if(Object.keys(players).length >0){

            return players["game_id"].map((value,index)=>{
                console.log(value)
                return(<li className='game__info__item'>
                <img src={value.game_img} alt='item played' />
                <p> {value.game_name}</p>
                </li>)
            })
        }


    }
    return (
        <div className='main__player'>
            <div className='content__player'>
                <div className='player__info'>
                    <img src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt='avatar__user' />
                    <div className='player__namesex'>
                        <h2>{players.playerName}</h2>
                        <p>id: <span>{players._id}</span></p>
                        <p>Score: <span>{players.score}</span></p>
                        <p>Sex: <span>{players.sex}</span></p>
                        <p>Game Played: <span>{Object.keys(players).length >0 ? players.game_id.length : 0 }</span></p>
                        <p>Item Changed : <span>{Object.keys(players).length>0 ? players.item_id.length : 0 }</span></p>
                    </div>
                </div>
                <div className='row row__result'>
                    <div className='listrowinfo col-xs-12 col-md-8 '>
                        <div className='col-sm-12 user__infor '>
                            <h4 className='user__infor__player'>Thành tích <span>{Object.keys(players).length >0 ? players.game_id.length : 0 }</span></h4>
                            <ul className='game__info__list'>
                                {renderGamePlayed()}
                                {/* <li className='game__info__item'>
                                    <img src='https://img-hws.y8.com/cloud/v2-y8-thumbs-small-thumbnails-001/143792/small.gif' alt='item played' />
                                    <p> Meo va Chuot</p>
                                </li>
                                <li className='game__info__item'>
                                    <img src='https://img-hws.y8.com/cloud/v2-y8-thumbs-small-thumbnails-001/140042/small.gif' alt='item played' />
                                    <p> Mo Mang</p>
                                </li>
                                <li className='game__info__item'>
                                    <img src='https://cdn.y8.com/achievements/56b1b9bcd55930ac06002d7f/8371bab42270a9dfcec6.png' alt='item played' />
                                    <p> Huyen Thoai</p>
                                </li> */}
                            </ul>
                        </div>
                        <div className='col-sm-12 user__infor'>
                            <h4 className='user__infor__player'>Bạn Bè <span>4</span></h4>
                            <ul className='friend__info__list'>
                                <li className='friend__info__item'>
                                    <img src='https://img-hws.y8.com/assets/y8/default_avatar-d594c7c6e605201898e1dadf838c38d27e4ca38361edcff4022d4fab84368136.png' alt='item played' />
                                    <p> cuong 1</p>
                                </li>
                                <li className='friend__info__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/62149ad1b4439cb5cb4976f7-large.jpg' alt='item played' />
                                    <p> cuong 2</p>
                                </li>
                                <li className='friend__info__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/6216a47f87f0fc411f8f7511-large.jpg' alt='item played' />
                                    <p> cuong 3</p>
                                </li>
                                <li className='friend__info__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/6216a0661f76dd53be5d464b-large.jpg' alt='item played' />
                                    <p> cuong 4</p>
                                </li>
                            </ul>
                        </div>
                        <div className='col-sm-12 user__infor' >
                            <h4 className='user__infor__player comment__user'>Các Bình Luận <span>0</span></h4>
                            <ul className='comment__list'>
                                <li className='comment__item'>
                                    <i className='bx bx-message-rounded-dots' ></i>
                                    <p>Không có bình luận nào được thêm vào</p>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div className='col-xs-12 col-md-4  user_info_favori'>
                        <div className='info_favori__video_list'>
                            <div className='info_favori__video'>
                                <h2 className='favori__video__title'>
                                    CÁC TRÒ CHƠI VÀ VIDEO ĐÃ YÊU THÍCH
                                </h2>
                                <p>BẠN CHỈ CÓ THỂ YÊU THÍCH TỐI ĐA 3 TRÒ CHƠI ƯA THÍCH À 3 VIDEO YÊU THÍCH CỦA BẠN.</p>
                                <h4> <i className='bx bx-heart' ></i> <span>Hearted Empty List</span></h4>

                            </div>
                            <div className='favori__category'>
                                <h2 className='favori_category__title'>
                                    CÁC HẠNG MỤC VÀ VIDEO YÊU THÍCH <span> * 2</span>
                                </h2>
                                <ul className='favori_category__list'>
                                    <li className='favori_category__item'>
                                        <img src='https://img-hws.y8.com/cloud/v2-y8-thumbs-small-thumbnails-001/140042/small.gif' alt='favori' />
                                        <div className='favori_category__info'>
                                            <a className='category__item__name'>BloxdHop io</a>
                                            <div className='favori_category__love'>
                                                <i className='bx bxs-heart' ></i>
                                                <p>Yêu điều này</p>
                                            </div>
                                        </div>
                                        <i className='bx bxs-trash' ></i>
                                    </li>
                                    <li className='favori_category__item'>
                                        <img src='https://img-hws.y8.com/cloud/v2-y8-thumbs-small-thumbnails-001/143792/small.gif' alt='favori' />
                                        <div className='favori_category__info'>
                                            <a className='category__item__name'>Parkour Block 2</a>
                                            <div className='favori_category__love'>
                                                <i className='bx bxs-heart' ></i>
                                                <p>Yêu điều này</p>
                                            </div>
                                        </div>
                                        <i className='bx bxs-trash' ></i>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className='infor_user_score'>
                            <div className='user__title__score'>
                                <h2>CÁC ĐIỂM <span>* 20</span></h2>
                                <p>XEM BẢNG XẾP LOẠI</p>
                            </div>
                            <ul className='history__score'>
                                <li>
                                    <span className='score__game__play'>+10</span>
                                    <p className='score__game'>points for 10 minutes of play on <span className='name__game'>EvoWars.Y8</span>
                                     <span className='date_played'>cách đây 3 ngày trước</span></p>
                                </li>
                                <li>
                                    <span className='score__game__play'>+10</span>
                                    <p className='score__game'>	points for 10 minutes of play on <span className='name__game'>EvoWars.Y8</span>
                                     <span className='date_played'>cách đây 3 ngày trước</span></p>
                                </li>
                            </ul>
                            <div className='find__score'>
                                <i className='bx bxs-bullseye' ></i>
                                <p>muốn kiếm thêm điểm</p>
                                <button>Chơi trò chơi SCEP</button>
                            </div>

                        </div>
                        
                        <div className='add_friend'>
                            <h2>CẦN THÊM BẠN BÈ</h2>
                            <ul className='add_friend__list'>
                                <li className='add_friend__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/62149ad1b4439cb5cb4976f7-large.jpg' />
                                    <div className='friend__item__info'>
                                        <h5 className='friend__name'>Motherfan</h5>
                                        <p className='friend__point'>+750 point</p>
                                    </div>
                                </li>
                                <li className='add_friend__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/6213aa3587f0fc42fa8e7f27-large.jpg' />
                                    <div className='friend__item__info'>
                                        <h5 className='friend__name'>QWX</h5>
                                        <p className='friend__point'>+520 point</p>
                                    </div>
                                </li>
                                <li className='add_friend__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/6213d1ac1f76dd517d5c7c13-large.jpg' />
                                    <div className='friend__item__info'>
                                        <h5 className='friend__name'>spidermen</h5>
                                        <p className='friend__point'>+750 point</p>
                                    </div>
                                </li>
                                <li className='add_friend__item'>
                                    <img src='https://cdn.y8.com/profile_pictures/6215affc1f76dd52305cfd18-large.jpg' />
                                    <div className='friend__item__info'>
                                        <h5 className='friend__name'>captain mavel</h5>
                                        <p className='friend__point'>+750 point</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {/* screen <1200px display block */}
                <div className='row row__comment'>
                    <div className='col-sm-9'>
                        <h4 className='user__infor__player comment__user'>Các Bình Luận <span>0</span></h4>
                        <ul className='comment__list'>
                            <li className='comment__item'>
                                <i className='bx bx-message-rounded-dots' ></i>
                                <p>Không có bình luận nào được thêm vào</p>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default PlayerInfo;