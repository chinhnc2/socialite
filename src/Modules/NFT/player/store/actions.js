import { REQUEST } from "Stores";
import {
  GAME_PLAYED,
    GET_ITEMDEPOT,
    LOAD_PLAYER, SELL_ITEM
} from './constants'
export function loadPlayer(payload) {
    // console.log(payload)
    return {
      type: REQUEST(LOAD_PLAYER),
      payload
    }
}
export function gameplayed(payload) {
  // console.log(payload)
  return {
    type: REQUEST(GAME_PLAYED),
    payload
  }
}

export function sellItem(payload){
  console.log(payload)
  return {
    type: REQUEST(SELL_ITEM),
    payload
  }
}
export function getItemdepot(payload){
  console.log(payload)
  return {
    type: REQUEST(GET_ITEMDEPOT),
    payload
  }
}