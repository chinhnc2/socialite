import { createReducer, updateObject, REQUEST, SUCCESS, FAILURE } from 'Stores'
import {GAME_PLAYED, GET_ITEMDEPOT, LOAD_PLAYER, SELL_ITEM } from './constants'
import { setLocalStorage, STORAGE } from 'Utils';
import { getPlayer } from 'APIs/player';

export const initialState = {
  isLoading: false,
  error: null,
  players: [],
  update: []
}

//LOAD player

function loadingPlayer(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function LoadedPlayer(state, { payload }) {
  console.log(payload)
  console.log(state)
  return updateObject(state, {
    isLoading: false,
    players: payload.player
  })

}

function LoadPlayerError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}
//PLAYED GAME
function LoaddingGameplayed(state) {
  return updateObject(state, {
    isLoading: true
  })
}
function LoadedGameplayed(state, { payload }) {
  console.log(payload)
  console.log(state)
  return updateObject(state, {
    isLoading: false,
    update: payload
  })

}

function LoadGameplayedError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}

//sell player

function sellingItem(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function selledItem(state, { payload }) {
  console.log(payload)
  console.log(state)
  return updateObject(state, {
    isLoading: false,
    update: payload
    // players.item_id: payload.player.item_id
  })

}

function sellerrorItem(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}
//get item depot player

function getingItem(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function getedItem(state, { payload }) {
  console.log(payload)
  console.log(state)
  return updateObject(state, {
    isLoading: false,
    update: payload
    // players: payload.player  
  })

}

function getlerrorItem(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}




// Slice reducer
export default createReducer(initialState, { //Object với các key là các action type và value là hàm xử lý state của type đó.
  [REQUEST(LOAD_PLAYER)]: loadingPlayer,
  [SUCCESS(LOAD_PLAYER)]: LoadedPlayer,
  [FAILURE(LOAD_PLAYER)]: LoadPlayerError,
  [REQUEST(GAME_PLAYED)]: LoaddingGameplayed,
  [SUCCESS(GAME_PLAYED)]: LoadedGameplayed,
  [FAILURE(GAME_PLAYED)]: LoadGameplayedError,
  [REQUEST(SELL_ITEM)]: sellingItem,
  [SUCCESS(SELL_ITEM)]: selledItem,
  [FAILURE(SELL_ITEM)]: sellerrorItem,
  [REQUEST(GET_ITEMDEPOT)]: getingItem,
  [SUCCESS(GET_ITEMDEPOT)]: getedItem,
  [FAILURE(GET_ITEMDEPOT)]: getlerrorItem,
})
