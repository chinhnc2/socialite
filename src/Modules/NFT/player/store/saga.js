import { put, takeLatest } from 'redux-saga/effects'

import { REQUEST, SUCCESS, FAILURE } from 'Stores'
import { getItemdepot, getPlayer, playedGame, sellItem } from 'APIs/player'
import { getLocalStorage, STORAGE } from 'Utils'

import {GAME_PLAYED, GET_ITEMDEPOT, LOAD_PLAYER, SELL_ITEM} from './constants'

export function* loadPlayerSaga(action) {
  try {
    const data = yield getPlayer();
    console.log(data)
    yield put({
      type: SUCCESS(LOAD_PLAYER),
      payload: {
        player: data
      }
    })
    
  } catch (error) {
    yield put({
      type: FAILURE(LOAD_PLAYER),
      error
    })
  }
}
export function* gamePlayedSaga(action) {
  try {
    const data = yield playedGame(action.payload);
    console.log(data)
    yield put({
      type: SUCCESS(GAME_PLAYED),
      payload: {
        player: data
      }
    })
    const dataplayer = yield getPlayer();
    console.log(dataplayer)
    yield put({
      type: REQUEST(LOAD_PLAYER),
      payload: {
        player: dataplayer
      }
    })
    
  } catch (error) {
    yield put({
      type: FAILURE(GAME_PLAYED),
      error
    })
  }
}
export function* sellItemsaga(action) {
  try {
    const data = yield sellItem(action.payload);
    console.log(data)
    yield put({
      type: SUCCESS(SELL_ITEM),
      payload: {
        player: data
      }
    })
    const dataplayer = yield getPlayer();
    console.log(dataplayer)
    yield put({
      type: REQUEST(LOAD_PLAYER),
      payload: {
        player: dataplayer
      }
    })
  } catch (error) {
    yield put({
      type: FAILURE(SELL_ITEM),
      error
    })
  }
}

export function* getItemDepotSaga(action) {
  try {
    const data = yield getItemdepot(action.payload);
    console.log(data)
    yield put({
      type: SUCCESS(GET_ITEMDEPOT),
      payload: {
        update: data
      }
    })
    const dataplayer = yield getPlayer();
    console.log(dataplayer)
    yield put({
      type: REQUEST(LOAD_PLAYER),
      payload: {
        player: dataplayer
      }
    })
  } catch (error) {
    yield put({
      type: FAILURE(SELL_ITEM),
      error
    })
  }
}


export default function* playerSaga() {
  console.log(123)
  yield takeLatest(REQUEST(LOAD_PLAYER), loadPlayerSaga);
  yield takeLatest(REQUEST(GAME_PLAYED), gamePlayedSaga);
  yield takeLatest(REQUEST(SELL_ITEM), sellItemsaga);
  yield takeLatest(REQUEST(GET_ITEMDEPOT),getItemDepotSaga)
}

