/* eslint-disable arrow-parens */
/* eslint-disable implicit-arrow-linebreak */

/**
 * The global state selectors
 */

 import { createSelector } from 'reselect'
 import { initialState } from './reducer'
 
 const selectPlayer = state => state.player || initialState
 console.log(selectPlayer)
 const makeSelectPlayer = () =>
   createSelector(
    selectPlayer,
     state => state
   )

 export {
    selectPlayer,
    makeSelectPlayer
 }
 