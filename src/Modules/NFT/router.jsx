import React from 'react'
import { Switch } from 'react-router-dom'
import PrivateRoute from '../../Components/route/privateRoute'
import HomeNFT from 'Layouts/homeNFT'
import IndexHome from './home/IndexHome'
import AddGame from './addgame/AddGame'
import PlayerInfo from './player/PlayerInfo'
import IndexGameAdd from './addgame/IndexGameAdd'
import IndexItems from './items/IndexItems'
import DetailItem from './items/DetailItem'
import IndexChange from './items/changeItems/IndexChange'
import GameDetail from './game/GameDetail'
import ItemsDepot from './items/itemsdepot/ItemsDepot'
import { USER_ROLE } from "Constants/auth";
import GameDetailTest from './game/GameDetailTest'

export const RoutesName = {
    NFT_GAME: '/nft-game',
    ADD_NFT_GAME:'/nft-game/addGame',
    PLAYER_NFT_GAME:'/nft-game/player',
    DETAIL_NFT_GAME: '/nft-game/:id',
    INDEX_NFT_GAME: '/nft-game/indexAdd',
    HOME_ITEMS_NFT : '/nft-game/indexItems',
    DETAIL_ITEM_NFT: '/nft-game/detailItem/:id',
    CHANGE_ITEM_NFT: '/nft-game/changeItem',
    ITEM_NFT_DEPOT: '/itemDepot',
    PLAYGAME_ID: '/nft-game-testid'
}

export const ROUTES = [
  {
    path: RoutesName.NFT_GAME,
    component: IndexHome,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.ADD_NFT_GAME,
    component: AddGame,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.PLAYER_NFT_GAME,
    component: PlayerInfo,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  
  {
    path: RoutesName.INDEX_NFT_GAME,
    component: IndexGameAdd,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.HOME_ITEMS_NFT,
    component: IndexItems,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  }
  ,
  {
    path: RoutesName.DETAIL_ITEM_NFT,
    component: DetailItem,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  //test interface change
  {
    path: RoutesName.CHANGE_ITEM_NFT,
    component: IndexChange,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.DETAIL_NFT_GAME,
    component: GameDetail,
    layout: HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.ITEM_NFT_DEPOT,
    component: ItemsDepot,
    layout:HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.PLAYGAME_ID,
    component: GameDetailTest,
    layout:HomeNFT,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },

]

export default function NFTGameRoutes() {
  return (
    <Switch>
      {ROUTES.map((routeConfig, index) => (
        <PrivateRoute key={index} exact {...routeConfig} />
      ))}
    </Switch>
  )
}
