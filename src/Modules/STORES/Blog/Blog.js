import React from 'react'
import { Wrapper } from './styled-blogS'
import img1 from '../../../Assets/dungvv-img/petBlog1.png';
import img2 from '../../../Assets/dungvv-img/petBlog2.png';
import img3 from '../../../Assets/dungvv-img/petBlog3.png';


export default function Blog() {
    return (
        <Wrapper className='container'>
            <div className="blog">
                <h2 className='blog-title'>Latest From Blog</h2>
                <div className='row'>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div className="tt-blog-thumb respimgsize">
                            <div className="tt-img">
                                <a href='#' alt="">
                                    <img className='lazyload' src={img1} />
                                </a>
                            </div>
                            <div className="tt-title-description">
                                <div className='tt-bg'>
                                    <div className="tt-tag text-uppercase">
                                        <a href="#" title>LADIES</a>
                                    </div>
                                    <div className="tt-title">
                                        <a href="#">CATALOGUE MODE</a>
                                    </div>
                                    <div className="tt-content">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                        </p>
                                    </div>
                                    <div className='tt-meta'>
                                        <div className='tt-author'>
                                            <p>by <a href="#">Diego Lopez</a> on July 07, 2021
                                            </p>
                                        </div>
                                        <div className='tt-comment'>
                                            <i class="fa-regular fa-comment"></i> 0
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div className="tt-blog-thumb respimgsize">
                            <div className="tt-img">
                                <a href='#' alt="">
                                    <img className='lazyload' src={img2} />
                                </a>
                            </div>
                            <div className="tt-title-description">
                                <div className='tt-bg'>
                                    <div className="tt-tag text-uppercase">
                                        <a href="#" title>LADIES</a>
                                    </div>
                                    <div className="tt-title">
                                        <a href="#">COUNTDOWN TIMEZONE</a>
                                    </div>
                                    <div className="tt-content">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                        </p>
                                    </div>
                                    <div className='tt-meta'>
                                        <div className='tt-author'>
                                            <p>by <a href="#">Diego Lopez</a> on July 07, 2021
                                            </p>
                                        </div>
                                        <div className='tt-comment'>
                                            <i class="fa-regular fa-comment"></i> 0
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div className="tt-blog-thumb respimgsize">
                            <div className="tt-img">
                                <a href='#' alt="">
                                    <img className='lazyload' src={img3} />
                                </a>
                            </div>
                            <div className="tt-title-description">
                                <div className='tt-bg'>
                                    <div className="tt-tag text-uppercase">
                                        <a href="#" title>LADIES</a>
                                    </div>
                                    <div className="tt-title">
                                        <a href="#">PURCHASED PROMO</a>
                                    </div>
                                    <div className="tt-content">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                        </p>
                                    </div>
                                    <div className='tt-meta'>
                                        <div className='tt-author'>
                                            <p>by <a href="#">Diego Lopez</a> on July 07, 2021
                                            </p>
                                        </div>
                                        <div className='tt-comment'>
                                            <i class="fa-regular fa-comment"></i> 0
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Wrapper>
    )
}
