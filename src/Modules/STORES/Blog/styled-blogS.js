import styled from 'styled-components'

export const Wrapper = styled.div`
    .blog{
        margin-top: 50px;
        margin-bottom: 50px;
    }

    .blog-title{
        color: #009950;
        text-decoration: none;
        display: inline-block;
        font-size: 30px;
        line-height: 36px;
        font-weight: 700;
        letter-spacing: 0em;
        font-family: Rubik,sans-serif;    
        position: relative;
        text-align: center;
        display: block;
        margin-bottom: 30px;
    }

    .tt-img img{
        border-radius: 15px;
        vertical-align: middle;
        border-style: none;
        width: 100%;
        height: auto;
    }

    .tt-title-description{
        color: red;
        margin-top: -70px;
        position: relative;
        z-index: 99999;
        margin-top: -100px;
        margin-right: 20px;
        margin-left: 20px;
        padding: 22px 15px 10px;
        text-align: center;
        min-height: 80px;
        transition: transform .2s;
        -webkit-transition: all .2s linear;
    }

    .tt-bg{
        border: 1px solid transparent;
        border-radius: 15px;
        /* position: absolute; */
        top: 0;
        left: 0;
        width: 100%;
        height: 200px;
        background: #fff;
    }

    .tt-bg:hover{
        transition: all .2s linear;
        -webkit-transition: all .2s linear;
        transform: scale(1.1);
    }

    .tt-title-description .tt-tag a{
        font-weight: 500;
        color: #009950;
        display: inline-block;
        padding: 2px 5px;
        text-decoration: none;
        outline: none;
    }

    .tt-title-description .tt-title{
        margin-bottom: 25px;
        margin-top: 0px;
    }

    .tt-title-description .tt-title a{
        font-size: 20px;
        line-height: 30px;
        font-weight: 700;
        font-family: Rubik,sans-serif;
        position: relative;
        color: #FA9247;  
    }

    .tt-title-description .tt-content p{
        color: #009950;
        margin-bottom: 15px;
    }

    .tt-title-description .tt-meta{
        color: #fa9247;
        display: flex;
        width: 100%;
        text-align: center;
        margin-top: 10px;
        font-size: 12px;
        align-items: center;
    }

    .tt-title-description .tt-meta .tt-author{
        margin-top: 4px;
        margin-left: 7px;
        color: #FA9247;
    }
    .tt-title-description .tt-meta .tt-author a{
        margin-top: 4px;
        color: #FA9247;
    }

    .tt-title-description .tt-meta .tt-comment{
        cursor: pointer;
        font-size: 15px;
        margin-left: 40px;
    }
`