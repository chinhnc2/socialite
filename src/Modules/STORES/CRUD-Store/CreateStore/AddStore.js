import React, { useEffect, useState } from 'react'
import { Wrapper } from './styled-addStore'

export default function AddStore(props) {

    const [nameStore, setNameStore] = useState("")
    const [nameUser, setNameUser] = useState("")
    const [phone, setPhone] = useState("")
    const [location, setLocation] = useState("")
    const [categori, setCategori] = useState("")
    const [image, setImage] = useState("")

    const handleOnSubmit = async (e) => {
        e.preventDefault();
        let result = await fetch(
            'http://localhost:5000/register', {
            method: "post",
            body: JSON.stringify({ nameStore, nameUser, phone, location, categori, image }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        result = await result.json();
        console.warn(result);
        if (result) {
            alert("You have successfully registered the Store");
            setNameStore("");
            setNameUser("");
            setPhone("");
            setLocation("");
            setCategori("");
            setImage("");
        }
    }

    


    
    return (
        <Wrapper className="container">
            <div className="create-store">
                <div className="form defaul">
                    <div className="wrapper">
                        <div className="title">
                            Create Store
                        </div>
                        <span className="create-title">Create your booth according to your wishes</span>
                        <div className="form">
                            <div className="inputfield">
                                <label>Name Store: </label>
                                <input value={nameStore} onChange={(e) => setNameStore(e.target.value)} type="text" className="input" placeholder="Enter Name Store" required />
                            </div>
                            <div className="inputfield">
                                <label>Name User: </label>
                                <input value={nameUser} onChange={(e) => setNameUser(e.target.value)} type="text" className="input" placeholder="Enter User Name" required />
                            </div>
                            <div className="inputfield">
                                <label>Phone: </label>
                                <input value={phone} onChange={(e) => setPhone(e.target.value)} type="text" className="input" placeholder="Enter Phone" required />
                            </div>
                            <div className="inputfield">
                                <label>Location: </label>
                                <input value={location} onChange={(e) => setLocation(e.target.value)} type="text" className="input" placeholder="Enter Location" required />
                            </div>
                            <div className="inputfield">
                                <label>Categori: </label>
                                <input value={categori} onChange={(e) => setCategori(e.target.value)} type="text" className="input" placeholder="Enter Categori" required />
                            </div>
                            <div className="inputfield">
                                <label>image of store: </label>
                                <input value={image} onChange={(e) => setImage(e.target.value)} type="file" id="img" name="img" accept="image/*" />
                            </div>

                            <hr />
                            <div className="d-flex justify-content-center mt-1">
                                <ul className="social-icons">
                                    <li> <span class="dot"><i class="fa-brands fa-facebook"></i></span> </li>
                                    <li> <span class="dot"><i class="fa-brands fa-twitter"></i></span> </li>
                                    <li> <span class="dot"><i class="fa-brands fa-instagram"></i></span> </li>
                                </ul>
                            </div>
                            <div className="form-group">
                                <button onClick={handleOnSubmit} type="submit" className="btn-create btn-border">CREATE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>

    )
}
