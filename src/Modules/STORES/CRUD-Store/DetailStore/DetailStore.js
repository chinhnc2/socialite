import React, { useEffect } from 'react'
import img1 from '../../../../Assets/dungvv-img/dog1.png';
import img2 from '../../../../Assets/dungvv-img/dog2.png';
import img3 from '../../../../Assets/dungvv-img/dog5.png';
import img4 from '../../../../Assets/dungvv-img/dog6.png';
import logoShop from '../../../../Assets/dungvv-img/icon-shop.png';
import { Link } from 'react-router-dom'
import { Wrapper } from './styled-detailStore'
import { useSelector, useDispatch } from 'react-redux';




export default function DetailStore(data) {

    // const Store = useSelector(state => state.cart._products);
    // const detailStore = Store.find(value => value._id == data.location.idProduct);
    // console.log(detailStore);

    return (
        <Wrapper className="container">
            <div className="detailStore">
                <Link to="/stores/create-store" className="create-product">Create</Link>
                <h2>Store Of Boss</h2>
                <div className="row detail-store">
                    <div className="col-md-6">
                        <img className="logoShop" src={logoShop} alt="" />
                    </div>
                    <div className="col-md-6 shop-information">
                        <h6>shop information</h6>
                        <ul>
                            <li>Address: 02 Đống Đa, Hà Nội</li>
                            <li>Working Time: 9am - 12pm, Monday - Saturday</li>
                            <li>Hotline: +84 334 751 943</li>
                            <li>Fanpage: Petshop</li>
                            <li>Website: https://shop-pet.com</li>
                        </ul>
                    </div>
                </div>


                <div className="row related-product">
                    <h4 className="title-related-product">the shop's products</h4>
                    <div className="cards">
                        <div className="col-md-3">
                            <div className="card" style={{ width: '18rem' }}>
                                <img src={img1} className="card-img-top" alt="" />
                                <div className='icon'>
                                    <i class="fa-regular fa-eye"></i> <br />
                                    <i class="fa-regular fa-heart"></i>
                                    <i class="fa-solid fa-scale-balanced"></i>
                                </div>
                                <div className="card-body">
                                    <span className="card-text">
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                    </span>
                                    <h5 className="card-title">dog Happy</h5>
                                    <p className='sale'>$ 56</p>
                                    <button href="#" className="btn btn-addToCart">add to card</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="card" style={{ width: '18rem' }}>
                                <img src={img2} className="card-img-top" alt="" />
                                <div className='icon'>
                                    <i class="fa-regular fa-eye"></i> <br />
                                    <i class="fa-regular fa-heart"></i>
                                    <i class="fa-solid fa-scale-balanced"></i>
                                </div>
                                <div className="card-body">
                                    <span className="card-text">
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                    </span>
                                    <h5 className="card-title">dog chukaka</h5>
                                    <p className='sale'>$ 59</p>
                                    <button href="#" className="btn btn-addToCart">add to card</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="card" style={{ width: '18rem' }}>
                                <img src={img3} className="card-img-top" alt="" />
                                <div className='icon'>
                                    <i class="fa-regular fa-eye"></i> <br />
                                    <i class="fa-regular fa-heart"></i>
                                    <i class="fa-solid fa-scale-balanced"></i>
                                </div>
                                <div className="card-body">
                                    <span className="card-text">
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                    </span>
                                    <h5 className="card-title">dog yuyu</h5>
                                    <p className='sale'>$ 59</p>
                                    <button href="#" className="btn btn-addToCart">add to card</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <div className="card" style={{ width: '18rem' }}>
                                <img src={img4} className="card-img-top" alt="" />
                                <div className='icon'>
                                    <i class="fa-regular fa-eye"></i> <br />
                                    <i class="fa-regular fa-heart"></i>
                                    <i class="fa-solid fa-scale-balanced"></i>
                                </div>
                                <div className="card-body">
                                    <span className="card-text">
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                        <i class="fa-regular fa-star"></i>
                                    </span>
                                    <h5 className="card-title">dog kiriri</h5>
                                    <p className='sale'>$ 59</p>
                                    <button href="#" className="btn btn-addToCart">add to card</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    )
}
