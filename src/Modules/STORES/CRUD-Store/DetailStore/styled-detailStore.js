import styled from 'styled-components'

export const Wrapper = styled.div`
    .detailStore h2 {
        font-size: 30px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        text-align: center;
        color: #009950;
        margin-top: 30px;
        margin-bottom: 30px;
        text-transform: uppercase;
    }

    .cards {
        margin-top: 20px;
        text-align: center;
    }

    .detail-store img{
        width: 450px;
        height: 370px;
    }

    .cards .icon{
        margin-top: -250px;
    }

    .create-product {
        padding: 10px 20px;
        float: right;
        margin-right: 30px;
        margin-bottom: 30px;
        font-weight: bold;
        background-color: #009950;
        color: #fff;
        transition: .2s;
        border: 1px solid transparent;
    }

    .create-product:hover {
        background-color: #f1c40f;
        color: #009950;
    }

    .shop-information h6{
        text-transform: uppercase;
        font-size: 20px;
        font-weight: bold;
        color: #009950;
        font-family: Rubik, sans-serif;
        margin-bottom: 20px;
    }

    .shop-information ul{
        list-style: none;
    }
    .shop-information ul li{
        margin-top: 20px;
        font-size: 15px;
        font-weight: 500;
        color: #009950;
        font-family: Rubik, sans-serif;
    }

    .title-related-product{
        margin-top: 30px;
        text-transform: uppercase;
        font-size: 20px;
        font-weight: bold;
        color: #009950;
        font-family: Rubik, sans-serif;
    }
    
    .related-product {
        margin-top: 50px;
        margin-bottom: 100px;
    }
    .card img{
        width: 200px;
        height: 250px;
    }

    .card {
        position: relative;
        width: 100%;
        height: 100%;
    }

    .card img {
        width: 220px;
        height: 270px;
    }

    .icon {
        position: absolute;
        opacity: 0;
        color: #009950;
        padding: 10px;
        margin-left: 160px;
        margin-top: -250px;
        transition: .2s ease-in-out;
    }

    .icon i {
        border: 1px solid transparent;
        border-radius: 50%;
        padding: 10px 10px;
        background-color: #dff9fb;
        margin: 3px 0px;
    }

    .icon i:hover {
        background-color: #009950;
        color: #fff
    }

    .icon svg {
        font-size: 10px;
        border: 1px solid transparent;
        background-color: #dff9fb;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        text-align: center;
    }

    .icon svg:hover {
        background-color: #009950;
        color: #fff;
    }

    .card:hover .icon {
        opacity: 1;
        transition: .2s;
        cursor: pointer;
    }

    .container .product-item-shop .card strong {
        background: red;
        width: 30px;
        height: 20px;
        position: absolute;
    }

    .product-item-shop {
        display: flex;
        margin-left: 15px;
    }

    .product-item-shop .card {
        width: 20%;
        margin: 15px 20px;
        border: none;
    }

    .product-item-shop .card .card-text {
        justify-content: center;
        display: flex;
        margin-bottom: 10px;
        margin-left: 10px;
    }


    .card-title{
        color: #009950;
        text-align: center;
        text-transform: uppercase;
    }

    .product-item-shop .card p {
        color: #009950;
        font-weight: bold;
        font-size: 16px;
        font-family: Rubik, sans-serif;
        text-align: center;
    }

    .price {
        text-decoration-line: line-through;
        color: #009950 !important;
    }

    .sale {
        color: red;
        font-size: 16px;
    }

    .btn-addToCart {
        margin-top: 20px;
        margin-left: 30px;
        text-transform: uppercase;
        padding: 12px 20px;
        color: #fff;
        background-color: #009950;
        border: 1px solid transparent;
        border-radius: 15px;
        font-weight: bold;
        text-align: center;
        display: flex;
        justify-content: center;
    }

    .btn-addToCart:hover {
        color: #009950;
        background-color: #f1c40f;
        transition: 0.2s linear;
    }


`