import React, { useEffect, useState } from 'react'
import { Wrapper } from './styled-listStore'
import { Link } from 'react-router-dom';

import axios from 'axios';

export default function ListStore() {



    const [item, setItem] = useState([])

    useEffect(() => {
        axios({
            method: 'GET',
            url: 'http://localhost:5000/store',
            data: null
        }).then(res => {
            setItem(res.data);
        }).catch(err => {
            console.log(err);
        });
    }, [])
    console.log(item);


    return (
        <Wrapper>
            <div className="listStore">
                <h2 className="list-title">List Store</h2>
                <table className="table table-hover">
                    <tbody>
                        {
                            item.map((data) => (
                                <tr>
                                    <td>
                                        <div className="nameStore">
                                            <Link to={{
                                                pathname: `/stores/list-store/detail-store`,
                                                idProduct: data._id
                                            }}><span>{data.nameStore}</span>
                                            </Link>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="logoStore">
                                            <Link to={{
                                                pathname: `/stores/list-store/detail-store`,
                                                idProduct: data._id
                                            }}>
                                                <img src={data.img} alt="store" />
                                            </Link>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="nameBoss">
                                            <Link to="#"><span>{data.nameUser}</span></Link>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="phone">
                                            <span>{data.phone}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="location">
                                            <span>{data.location}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="categori">
                                            <span>{data.categori}</span>
                                        </div>
                                    </td>
                                </tr>
                            ))
                        }

                    </tbody>
                </table>
            </div>
        </Wrapper>
    )
}
