import styled from 'styled-components'

export const Wrapper = styled.div`
    .listStore .list-title {
        font-size: 24px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        text-align: center;
        color: #009950;
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .listStore .table {
        margin-bottom: 60px;
    }

    .nameStore span {
        margin-top: 40px;
        color: #009950;
    }

    .listStore .logoStore img {
        width: 200px;
        height: 150px;
    }

    .nameBoss span {
        margin-top: 40px;
        color: #009950;
        font-weight: 500;
        font-size: 16px;
        font-family: Rubik, sans-serif;
    }

    .listStore .location {
        color: #009950;
        font-weight: 500;
        font-size: 16px;
        font-family: Rubik, sans-serif;
    }

    .listStore .phone {
        color: #009950;
        font-weight: 500;
        font-size: 16px;
        font-family: Rubik, sans-serif;
    }

    .listStore .categori {
        color: #009950;
        font-weight: 500;
        font-size: 16px;
        font-family: Rubik, sans-serif;
    }

`