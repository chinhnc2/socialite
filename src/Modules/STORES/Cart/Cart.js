import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Wrapper } from './styled-cart'

import { postApiToDel, postApiToIncresseNumber } from '../Shopping/action/index';


export default function Cart() {

    const dispatch = useDispatch()
    const carts = useSelector(state => state.cart.Carts);

    const handleClickButtonIncrease = (e, value) => {
        e.preventDefault();
        dispatch(postApiToIncresseNumber(value));
    }

    const handleClickButtonDel = (e, value) => {
        e.preventDefault();
        dispatch(postApiToDel(value));
    }



    const [total, setTotal] = useState("")
    useEffect(() => {
        console.log(carts);
        const sumReduce = (numberList) => {
            return numberList.reduce((sum, number) => {
                return sum + (number.price * number.quantity)
            }, 0);
        }
        setTotal(sumReduce(carts));
    })

    const [title, setTitle] = useState("")
    const [price, setPrice] = useState("")
    const [quatity, setQuatity] = useState("")
    const [sumTotal, setSumTotal] = useState("")

    const handleOnSubmit = async (e) => {
        e.preventDefault();
        let result = await fetch(
            'http://localhost:5000/stored', {
            method: "post",
            body: JSON.stringify({ title, price, quatity, sumTotal }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        result = await result.json();
        console.warn(result);
        if (result) {
            alert("You have successfully registered the Store");
            setTitle("");
            setPrice("");
            setQuatity("");
            setSumTotal("");
        }
    }



    return (
        <Wrapper className="container cart">
            <h2>SHOPPING CART</h2>
            <table className="productCart table-hover">
                <tbody>
                    {
                        carts.map(cart => (
                            <>
                                <tr>
                                    <td>
                                        <div className="product-img">
                                            <img src={cart.img} alt="product" />
                                        </div>
                                    </td>
                                    <td>
                                        <h2 className="product-title">{cart.name}</h2>
                                    </td>
                                    <td>
                                        <div className="product-price">
                                            <span className="money" data-currency-usd="$39">${cart.price}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="quantity-desctope">
                                            <div className="tt-input-counter style-01 input-counter">
                                                <button className="minus-btn" type="button">-</button>
                                                <input className="number" value={cart.quantity} size="5" id="textbox" />
                                                <button className="plus-btn" type="button" onClick={(e) => handleClickButtonIncrease(e,
                                                    {
                                                        id: cart.idProduct,
                                                        quantity: 1,
                                                        name: cart.name,
                                                        image: cart.image,
                                                        price: cart.price
                                                    }
                                                )}>+</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="price-subtotal">
                                            <span className="money" data-currency-usd="$39">$ {cart.price * cart.quantity}</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div className="clean">
                                            <button onClick={(e) => handleClickButtonDel(e, {
                                                id: cart.idProduct,
                                                quantity: 1,
                                                name: cart.name,
                                                image: cart.image,
                                                price: cart.price
                                            })} className='deleteproduct'><i class="fa-solid fa-trash-can"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            </>
                        ))}

                </tbody>
            </table>


            <hr />

            <div className="shopcart-btn">
                <div className="col-left continue">
                    <Link to="/shop" className="btn-link">
                        <i class="fa-solid fa-arrow-left"></i> &nbsp;
                        <span>CONTINUE SHOPPING</span>
                    </Link>
                </div>
                <div className="col-right delete">
                    <Link to="/stores/delete" className="btn-link">
                        <i class="fa-solid fa-trash-can"></i> &nbsp;
                        <span>CLEAR SHOPPING CART</span>
                    </Link>
                </div>
            </div>

            <hr />

            <div className="row payment">
                <div className="col-md-6 note">
                    <h4>NOTE</h4>
                    <span>Add special instructions for your order...</span>
                    <textarea id="review" name="review" rows="9" cols="40"></textarea>
                </div>
                <div className="col-md-6 total">
                    <div className="subtotal">
                        <span>SUBTOTAL</span>
                        <span className="sub-price">$ {total}</span>
                    </div>

                    <br />

                    <div className="grandtotal">
                        <span>GRAND TOTAL</span>
                        <span className="grand-price">$ {total}</span>
                    </div>

                    <br />

                    <div className="check-total">
                        <input type="checkbox" value="" />&nbsp;
                        <span>I agree with the terms and conditions</span>
                    </div>

                    <br />
                    <Link href="/shop" >
                        <button type='submit' onClick={handleOnSubmit} className="btn-total">PROCEED TO CHECKOUT</button>
                    </Link>
                </div>
            </div>
        </Wrapper>
    )
}
