import styled from 'styled-components'

export const Wrapper = styled.div`
    .productCart {
        width: 100%;
        height: 100%;
    }

    input{
        text-align: center;
    }

    h2 {
        text-align: center;
        color: #009950;
        font-weight: bold;
        font-size: 34px;
        margin-top: 20px;
        margin-bottom: 30px;
    }

    img {
        width: 100px;
        height: 100px;
    }

    .productCart .product-title {
        text-decoration: none;
        outline: none;
        color: #009950;
        font-family: Rubik, sans-serif;
        font-size: 16px;
        font-weight: bold;
        margin-left: -50px;
    }

    .product-price {
        color: #009950;
        margin-top: -10px;
        font-weight: bold;
    }

    .money {
        color: #009950;
        font-size: 18px;
        font-weight: bold;
    }


    .deleteproduct {
        background-color: #009950;
        color: #fff;
        padding: 5px;
        border: 1px solid transparent;
        border-radius: 5px;
    }
    .deleteproduct:hover {
        background-color: #FFE11B;
        color: #009950;
    }

    .shopcart-btn {
        display: flex;
        margin-top: 20px;
        margin-bottom: 20px;
    }

    .minus-btn{
        padding: 7px;
        margin: 5px;
        background-color: #009950;
        color: #fff;
        border: 1px solid transparent;
        border-radius: 10px;
    }

    .minus-btn:hover{
        background-color: #FFE11B;
        color: #009950;
    }

    .plus-btn{
        padding: 7px;
        margin: 5px;
        background-color: #009950;
        color: #fff;
        border: 1px solid transparent;
        border-radius: 10px;
    }

    .plus-btn:hover{
        background-color: #FFE11B;
        color: #009950;
    }

    .number{
        padding: 7px;
        border: none;
    }

    .shopcart-btn .continue a {
        text-decoration: none;
        display: inline-block;
        font-size: 14px;
        color: #fa9247;
        text-decoration: none;
        font-weight: 400;
        font-family: Rubik, sans-serif;
        transition: color .2s linear;
    }

    .shopcart-btn .continue a:hover {
        color: #009950;
    }

    .shopcart-btn .delete a {
        margin-left: 780px;
        display: inline-block;
        font-size: 14px;
        color: #fa9247;
        text-decoration: none;
        font-weight: 400;
        font-family: Rubik, sans-serif;
        text-decoration: none;
        transition: color .2s linear;
    }

    .shopcart-btn .delete a:hover {
        color: #009950;
    }

    .payment .note {
        width: 350px;
        height: 300px;
        margin-top: 20px;
        border: 1px solid;
        padding: 10px;
        box-shadow: 5px 10px #888888;
    }

    .payment .total {
        margin-left: 30px;
        margin-top: 20px;
        border: 1px solid;
        padding: 10px;
        box-shadow: 5px 10px #888888;
    }

    .payment .total .subtotal {
        font-size: 20px;
        font-weight: 700;
        padding: 5px 0;
        color: #009950;
        vertical-align: middle;
        font-family: Rubik, sans-serif;
    }

    .payment .total .subtotal .sub-price {
        float: right;
        margin-right: 30px;
    }

    .payment .total .grandtotal {
        font-size: 20px;
        font-weight: 700;
        padding: 5px 0;
        color: #009950;
        vertical-align: middle;
        font-family: Rubik, sans-serif;
    }

    .payment .total .grandtotal .grand-price {
        float: right;
        margin-right: 30px;
        color: #FA9247;
        font-size: 25px;
    }

    .payment .total .check-total {
        font-family: Rubik, sans-serif;
        color: #009950;
        font-weight: 300;
    }

    .btn-total{
        background-color: #009950;
        color: #fff;
        font-family: Rubik, sans-serif;
        font-size: 15px;
        font-weight: bold;
        padding: 10px 25px;
        border: 1px solid transparent;
    }

    .btn-total:hover{
        background-color: #FFE11B;
        color: #009950;
    }

`