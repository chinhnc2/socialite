import React from 'react'
import { Wrapper } from './styled-categories'
import dog from '../../../Assets/dungvv-img/dog.png';
import cat from '../../../Assets/dungvv-img/cat.png';
import fish from '../../../Assets/dungvv-img/fish.png';
import small from '../../../Assets/dungvv-img/smallpet.png';
import rep from '../../../Assets/dungvv-img/rep.png';
import bird from '../../../Assets/dungvv-img/bird.png';

export default function Categories() {
    return (
        <Wrapper className='container'>
            <div className="categories">

                <h2>our categories</h2>
                <div className='row'>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={dog} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>DOGS</a>
                        </div>
                    </div>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={cat} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>CATS</a>
                        </div>
                    </div>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={fish} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>FISH</a>
                        </div>
                    </div>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={small} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>SMALL PETS</a>
                        </div>
                    </div>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={rep} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>REPTILES</a>
                        </div>
                    </div>
                    <div className='col-sm-2 categori-animal'>
                        <div className='card-top'>
                            <img src={bird} alt='' className='img-animal' />
                        </div>
                        <div className='card-bottom'>
                            <a href='#' className='title'>BIRDS</a>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    )
}
