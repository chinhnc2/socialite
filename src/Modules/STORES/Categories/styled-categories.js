import styled from 'styled-components'

export const Wrapper = styled.div`
    .categories {
        margin-top: 50px;
        margin-bottom: 30px;
    }

    .categories h2 {
        margin-bottom: 30px !important;
        text-align: center;
        color: #009950;
        font-size: 30px;
        line-height: 36px;
        font-weight: 700;
        letter-spacing: 0em;
        text-transform: capitalize;
        margin: 0;
        padding: 0;
        display: block;
        position: relative;
        font-family: Rubik, sans-serif;
    }

    .card-top {
        overflow: hidden;
        border-radius: 15px;
        position: relative;
        padding-top: 100%;
    }

    .img-animal {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        /* de hinh khoi meo */
        object-fit: cover;
        vertical-align: middle;
        border-style: none;
        border: 1px solid transparent;
        transition: transform 0.2s;
    }

    .img-animal:hover {
        transform: scale(1.1); 
    }

    .categori-animal .card-bottom {
        justify-content: center;
    }

    .categori-animal .card-bottom a {
        color: rgb(250, 146, 71);
        font-size: 14px;
        line-height: 19px;
        font-family: Rubik, sans-serif;
        font-weight: bold;
        margin-left: 50px;
        margin-top: 20px;
    }

    .categori-animal .card-bottom a:hover{
        color: #009950;
    }

`