import React, { useEffect, useState } from 'react'
import img1 from '../../../Assets/dungvv-img/DogBed.png'
import img2 from '../../../Assets/dungvv-img/f1.png'
import img3 from '../../../Assets/dungvv-img/f2.png'
import img4 from '../../../Assets/dungvv-img/f3.png'
import img5 from '../../../Assets/dungvv-img/f4.png'
import img6 from '../../../Assets/dungvv-img/f5.png'
import img7 from '../../../Assets/dungvv-img/f6.png'
import img8 from '../../../Assets/dungvv-img/f7.png'
import img9 from '../../../Assets/dungvv-img/f8.png'
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import { AddCart, actFetchProductsRequest, actFetchCartRequest } from 'Modules/STORES/Shopping/action/index';

import { Wrapper } from './styled-detailproduct'





export default function Detail(data) {

  const products = useSelector(state => state.cart._products);
  const detailProduct = products.find(value => value._id == data.location.idProduct);
  console.log(detailProduct);

  const dispatch = useDispatch();
  const cart = useSelector(state => state.cart);
  const idUser = useSelector(state => state.auth.profile._id);
  const handleClickAddToCart = (event, payload) => {
    event.preventDefault();
    dispatch(AddCart(payload));
  }


  useEffect(() => {
    actFetchProductsRequest(dispatch);
    actFetchCartRequest(dispatch, idUser);
    
  }, [])




  return (
    <Wrapper>
      <div className="detail bg">
        <Link className="bg-link" to="/stores">Home</Link>
        &nbsp; / &nbsp;
        <Link to="/stores/shop">Pets Supplies</Link> / <Link>{detailProduct.name}</Link>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <img className="img-product" src={detailProduct.img} alt="product" />
          </div>
          <div className="col-md-6">
            <div className="product-single-info">
              <div className="add-info">
                <ul>
                  <li className="availability">
                    <span className="ava">Availability:</span>
                  </li>
                  <li>
                    <span className="many">Many in stock</span>
                  </li>
                </ul>
                <h1 className="detail-product">{detailProduct.name}</h1>
                <div className="price">
                  <span className="sale-price">
                    <span className="money" data-currency-usd="$59" data-currency="USD">$ {detailProduct.price}</span>
                  </span>
                  <span className="old-price">
                    <span className="money" data-currency-usd="$64" data-currency="USD">$64</span>
                  </span>
                </div>
                <div className="review">
                  <div className="rating">
                    <span className="card-text">
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </span>
                    <span className="detail-caption">No Reviews</span>
                  </div>
                </div>
                <div className="detail-wrapper">
                  A great about us block helps builds trust between you and your customers.
                  The more content you provide about you and your business, the more confident people will be when
                  purchasing from your store.
                </div>
                <div className="detail-information">
                  <span className="shipping"><i class="fa-regular fa-heart"></i> SHIPPING</span>
                  <span className="ask"><i class="fa-solid fa-scale-balanced"></i> ASK ABOUT THIS PRODUCT</span>
                </div>
                <div className="row shopify-product-form">
                  <div className="col-6 item">
                    <button className="minus-btn">-</button>
                    <input type="text" name="quantity" value="1" size="7" />
                    <button className="plus-btn">+</button>
                  </div>
                  <div className="col-6 item">
                    <button className="btn-lg btn-addtocart"
                      onClick={(e) => handleClickAddToCart(e,
                        {
                          name: data.name,
                          img: data.img,
                          price: data.price,
                          idUser: idUser,
                          idProduct: data._id
                        }
                      )}
                    ><span>ADD TO CART</span></button>
                  </div>
                </div>
                <div className="checkbox-group prpage-term-conditions-checkbox term-conditions-checkbox-js">
                  <input type="checkbox" id="prpage-cart-term-conditions-checkbox" value="1" />
                  <label className="agree" for="prpage-cart-term-conditions-checkbox">
                    <span className="check"></span>
                    <span className="box"></span>
                    I agree with the terms and conditions
                  </label>
                </div>
                <div className="buyinoneclick" data-buttonname="BUY NOW" data-loading="LOADING" >
                  <div className="shopify-payment-button" data-shopify="payment-button" data-has-selling-plan="false">
                    <button type="button" class="shopify-payment" data-testid="Checkout-button" disabled="disabled">BUY NOW</button>
                  </div>
                </div>
                <div className="detail-list">
                  <ul>
                    <li>
                      <Tippy content='You need to login'>
                        <span><i class="fa-regular fa-heart wishlish"></i> &nbsp; ADD TO WISHLIST</span>
                      </Tippy>
                    </li>
                    <li>
                      <Tippy content='Add to Compare'>
                        <span><i class="fa-solid fa-scale-balanced compare"></i> &nbsp; ADD TO COMPARE</span>
                      </Tippy>
                    </li>
                  </ul>
                </div>
                <div className="detail-add-info">
                  <span>Vendor:</span>
                  <Link to="/shop">Pets Supplies #2</Link>
                  <br />
                  <span>Product Type:</span>
                  <Link to="/shop">Pets Supplies #2</Link>
                </div>

                <div className="panel-group" id="accordion">
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">DESCRIPTION</a>
                      </h4>
                    </div>
                    <div id="collapse1" className="panel-collapse collapse in">
                      <div className="panel-body">  <span>We are proud to present our best premium Shopify theme - Wokiee.</span> <br />
                        This is multi-purpose software that can be used for any type of the store. Great variety of available options will make customization process very easy. <br />
                        Please, take a look at feature list and compare with our competitors. <br />
                        You can buy our theme and start your business online with minimal time investments. <br />
                        Wokiee support DropShipping app Oberlo.
                        Wokiee Shopify theme is powerfool tool to create personal webshop. <br />
                      </div>
                    </div>
                  </div>
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">REVIEWS</a>
                      </h4>
                    </div>
                    <div id="collapse2" className="panel-collapse collapse">
                      <div className="panel-body">
                        <h4>CUSTOMER REVIEWS</h4>
                        <p>No Reviews Yet</p>
                        <a href="/page">Write a Review</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg">
        <Link to="/stores" className="bg-link">Home</Link>
        &nbsp; / &nbsp;
        <Link to="/stores/shop">Product</Link>
      </div>

      <div className="container related">
        <h6>RELATED PRODUCTS</h6>

        <div className="relatedProduct">
          <input type="radio" name="dot" id="one" />
          <input type="radio" name="dot" id="two" />
          <div className="main-card">

            <div className="cards c1">

              <div className="card" >
                <Link to="/product">
                  <img src={img1} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Dog Bed</a></h5>
                  <p className='price'>$ 55</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img2} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Cat Scratcher</a></h5>
                  <p className='price'>$ 35</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img3} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Dog Bowl</a></h5>
                  <p className='price'>$ 39</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
            </div>

            <div className="cards c2">
              <div className="card">
                <Link to="/product">
                  <img src={img4} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Dog Leash</a></h5>
                  <p className='price'>$ 19</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img5} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Leather Belt</a></h5>
                  <p className='price'>$ 12</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img6} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Grooming Glove</a></h5>
                  <p className='price'>$ 43</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
            </div>

            <div className="cards c3">
              <div className="card">
                <Link to="/product">
                  <img src={img7} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Dog Chew Toy</a></h5>
                  <p className='price'>$ 58</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img8} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Dog Cage</a></h5>
                  <p className='price'>$ 28</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
              <div className="card">
                <Link to="/product">
                  <img src={img9} className="card-img-top" alt="" />
                </Link>
                <div className="card-body">
                  <span className="card-text">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </span>
                  <h5 className="card-title"><a href='#'>Cat Collar</a></h5>
                  <p className='price'>$ 40</p>
                  <a href="#" className="btn btn-addToCart">add to card</a>
                </div>
              </div>
            </div>

          </div>
          <div className="button">
            <label htmlFor="one" className=" active one" />
            <label htmlFor="two" className="two" />
          </div>
        </div>

      </div>

      <div className="container recently">
        <h6>RECENTLY VIEWED PRODUCTS</h6>
        <div className="row">
          <dic className="col-3">
            <div className="card" style={{ width: '18rem' }}>
              <img src={img3} className="card-img-top" alt="" />
              <div className='icon'>
                <i class="fa-regular fa-eye"></i> <br />
                <i class="fa-regular fa-heart"></i>
                <i class="fa-solid fa-scale-balanced"></i>
              </div>
              <div className="card-body">
                <span className="card-text">
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                </span>
                <h5 className="card-title"><Link to='/stores/cart'>Dog Bowl</Link></h5>
                <p className='sale'>$ 39</p>
                <button href="#" className="btn btn-addToCart">add to card</button>
              </div>
            </div>
          </dic>
          <dic className="col-3">
            <div className="card" style={{ width: '18rem' }}>
              <img src={img4} className="card-img-top" alt="" />
              <div className='icon'>
                <i class="fa-regular fa-eye"></i> <br />
                <i class="fa-regular fa-heart"></i>
                <i class="fa-solid fa-scale-balanced"></i>
              </div>
              <div className="card-body">
                <span className="card-text">
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                </span>
                <h5 className="card-title"><Link to='/stores/cart'>Dog Leash</Link></h5>
                <p className='sale'>$ 19</p>
                <button href="#" className="btn btn-addToCart">add to card</button>
              </div>
            </div>
          </dic>
          <dic className="col-3">
            <div className="card" style={{ width: '18rem' }}>
              <img src={img6} className="card-img-top" alt="" />
              <div className='icon'>
                <i class="fa-regular fa-eye"></i> <br />
                <i class="fa-regular fa-heart"></i>
                <i class="fa-solid fa-scale-balanced"></i>
              </div>
              <div className="card-body">
                <span className="card-text">
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                </span>
                <h5 className="card-title"><Link to='/stores/cart'>Grooming Glove</Link></h5>
                <p className='sale'>$ 43</p>
                <button href="#" className="btn btn-addToCart">add to card</button>
              </div>
            </div>
          </dic>
          <dic className="col-3">
            <div className="card" style={{ width: '18rem' }}>
              <img src={img7} className="card-img-top" alt="" />
              <div className='icon'>
                <i class="fa-regular fa-eye"></i> <br />
                <i class="fa-regular fa-heart"></i>
                <i class="fa-solid fa-scale-balanced"></i>
              </div>
              <div className="card-body">
                <span className="card-text">
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                  <i class="fa-regular fa-star"></i>
                </span>
                <h5 className="card-title"><Link to='/stores/cart'>Dog Chew Toy</Link></h5>
                <p className='sale'>$ 58</p>
                <button href="#" className="btn btn-addToCart">add to card</button>
              </div>
            </div>
          </dic>
        </div>
      </div>

    </Wrapper>
  )
}
