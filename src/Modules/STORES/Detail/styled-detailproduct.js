import styled from 'styled-components'

export const Wrapper = styled.div`

    img{
        width: 100%;
        height: 100%;
    }

    .product-single-info .add-info ul{
        display: flex;    
    }

    .product-single-info .add-info ul li{
        list-style: none;
        color: #009950;
        margin-top: 20px;
    }

    .availability .ava{
        margin-left: -20px;
    }

    .availability .many{
        margin-left: 10px;
    }

    .detail-product{
        font-size: 30px;
        line-height: 36px;
        letter-spacing: 0px;
        font-weight: 700;
        margin-top: 19px;
        font-family: Rubik,sans-serif;
        color: #009950;
    }

    .price .sale-price .money{
        color: #f8353e;
        margin-right: 7px;
        font-size: 30px;
        font-weight: bold;
    }

    .price .old-price .money{
        color: #009950;
        text-decoration: line-through;
        font-size: 25px;
        font-weight: bold;
    }

    .review .rating .detail-caption{
        color: #FDC57A;
        margin-left: 15px;
    }

    .detail-wrapper{
        margin-top: 20px;
        margin-bottom: 20px;
        font-family: Rubik,sans-serif;
        font-size: 14px;
        font-weight: 400;
        line-height: 17px;
        color: #009950;
    }

    .detail-information{
        margin-bottom: 15px;
        color: #fa9247;
    }

    .detail-information .ask{
        float: right;
    }

    .shopify-product-form{
        margin-bottom: 20px;
    }

    .shopify-product-form .item .minus-btn{
        padding: 10px 5px;
        cursor: pointer;
        border: 1px solid transparent;
        background-color: #F7F8FA;
        border-radius: 10px 0 0 10px;
        margin-left: 10px;
    }

    .shopify-product-form .item .plus-btn{
        padding: 10px 5px;
        cursor: pointer;
        border: 1px solid transparent;
        background-color: #F7F8FA;
        border-radius: 0 10px 10px 0;
    }

    .shopify-product-form .item input{
        height: 40px;
        border: 1px solid transparent;
        background-color: #F7F8FA;
    }

    .btn-addtocart{
        width: 360px;
        height: 40px;
        margin-left: 10px;
        background-color: #009500;
        color: #fff;
        text-align: center;
        font-weight: 500;
        font-family: Rubik,sans-serif;
        border-radius: 15px;
    }

    .btn-addtocart:hover {
        color: #009500;
        background-color: #FFE11B;
        transition: .2s ease-in-out;
    }

    .checkbox-group{
        margin: 20px 0 10px;
    }

    .checkbox-group input{
        border-radius: 50%;
    }

    .agree{
        margin-left: 10px;
    }

    .shopify-payment{
        width: 550px;
        height: 50px;
        background-color: #FBEAA4;
        border: 1px solid #FBEAA4;
        color: #009950;
        font-weight: 500;
        font-family: Rubik,sans-serif;
    }

    button, html [type="button"], [type="reset"], [type="submit"] {
        -webkit-appearance: button;
        border-radius: 15px;
    }

    .detail-list {
        margin-left: -20px;
    }

    .detail-list span{
        color: #fa9247;
        cursor: pointer;
    }

    .detail-list span:hover{
        color: #009500;    
    }

    .detail-list .compare{
        margin-left: 20px;
    }

    .detail-add-info{
        margin-bottom: 20px;
        color: #009500;
    }

    .accordion{
        color: #009500;
        border: none;
    }

    .accordion-button{
        color: #009500;
    }

    .accordion-body a{
        float: right;
        margin-top: -20px;
        color: #fa9247;
        text-decoration: underline;
    }

    .accordion-body a:hover{
        color: #009500;
        text-decoration: none;
    }

    .bg {
        margin-top: 40px;
        background-color: #F7F8FA;
    }

    .bg-link {
        margin-left: 150px;
    }

    .related {
        text-align: center;
    }

    .related img{
        width: 100px;
        height: 150px;
    }

    .related h6 {
        margin-top: 30px;
        font-weight: bold;
        font-size: 24px;
        color: #009500;
    }



    .relatedProduct {
        max-width: 1200px;
        width: 100%;
        overflow: hidden;
        padding: 80px 0;
    }

    .relatedProduct .main-card {
        display: flex;
        justify-content: space-evenly;
        width: 200%;
        transition: 1s;
    }

    #two:checked~.main-card {
        margin-left: -100%;
    }
    
    .cards {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }
    .c2 {
        margin-left: -100px;
    }

    .c3 {
        margin-left: -100px;
    }

    .relatedProduct .cards .card {
        width: calc(100% / 3 - 10px);
        background: #fff;
        border-radius: 12px;
        padding: 28px;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.25);
        transition: all 0.4s ease;
    }

    .main-card .cards .card:hover {
        transform: translateY(-15px);
    }

    .relatedProduct .button {
        width: 100%;
        display: flex;
        justify-content: center;
        margin: 20px;
    }

    .button label {
        height: 15px;
        width: 15px;
        border-radius: 20px;
        background: #009500;
        margin: 0 4px;
        cursor: pointer;
        transition: all 0.5s ease;
    }

    .button label.active {
        width: 35px;
    }

    #one:checked~.button .one {
        width: 35px;
    }

    #one:checked~.button .two {
        width: 15px;
    }

    #two:checked~.button .one {
        width: 15px;
    }

    #two:checked~.button .two {
        width: 35px;
    }


    input[type="radio"] {
        display: none;
    }
    
    .relatedProduct .price{
        color: #009950;
    }
    .relatedProduct .price:hover{
        color: red;
    }

    .relatedProduct .card-title a{
        color: #009950;
        text-align: center;
    }

    .relatedProduct .card-title a:hover{
        color: #f1c40f;
    }

    .relatedProduct .btn-addToCart {
        margin-top: 20px;
        margin-left: 25px;
        text-transform: uppercase;
        padding: 12px 20px;
        color: #fff;
        background-color: #009950;
        border: 1px solid transparent;
        border-radius: 15px;
        font-weight: bold;
        text-align: center;
        display: flex;
        justify-content: center;
    }

    .relatedProduct .btn-addToCart:hover {
        color: #009950;
        background-color: #f1c40f;
        transition: 0.2s linear;
    }





    .recently {
        text-align: center;
    }
    .recently .row{
        display: flex;
    }

    .recently img {
        width: 200px;
    }

    .recently h6 {
        margin-top: 30px;
        font-weight: bold;
        font-size: 24px;
        color: #009500;
    }

    .recently .card {
        position: relative;
        width: 20%;
        height: 100%;
        margin-top: 50px;
        margin-bottom: 50px;
        margin-left: 100px;
    }

    .recently .card img {
        width: 200px;
        height: 250px;
    }

    .recently .icon {
        position: absolute;
        opacity: 0;
        color: #009950;
        padding: 10px;
        margin-left: 160px;
        margin-top: -250px;
        transition: .2s ease-in-out;
    }

    .recently .icon i {
        border: 1px solid transparent;
        border-radius: 50%;
        padding: 10px 10px;
        background-color: #dff9fb;
        margin: 3px 0px;
    }

    .recently .icon i:hover {
        background-color: #009950;
        color: #fff
    }

    /* hover vào img thì nó hiện ra icon ở đây */
    .recently .card:hover .icon {
        opacity: 1;
        transition: .2s;
        cursor: pointer;
    }

    .recently .card-title a{
        color: #009950;
        text-align: center;
    }

    .recently .card .sale {
        color: #f1c40f;
    } 

    .recently .btn-addToCart {
        margin-top: 20px;
        margin-left: 25px;
        text-transform: uppercase;
        padding: 12px 20px;
        color: #fff;
        background-color: #009950;
        border: 1px solid transparent;
        border-radius: 15px;
        font-weight: bold;
        text-align: center;
        display: flex;
        justify-content: center;
    }

    .recently .btn-addToCart:hover {
        color: #009950;
        background-color: #f1c40f;
        transition: 0.2s linear;
    }

`