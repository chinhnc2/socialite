import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { AddCart, actFetchProductsRequest, GetAllProduct, actFetchCartRequest, GetAllCart } from 'Modules/STORES/Shopping/action/index';

import { Wrapper } from './styled-favorite'

export default function Favorites() {
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart);
    const idUser = useSelector(state => state.auth.profile._id);
    const handleClickAddToCart = (event, payload) => {
        event.preventDefault();
        dispatch(AddCart(payload));
    }
    useEffect(() => {
        actFetchProductsRequest(dispatch);
        actFetchCartRequest(dispatch, idUser);
    }, [])
    return (

        <Wrapper className='container'>
            <h1><Link className='title' to='#'>Our Favorites to Care</Link></h1>
            <div class="row product-item">

                {
                    cart._products.map((data, index) => (
                        <div className="card" style={{ width: '18rem' }} key={index}>
                            <Link to={{
                                  pathname: `/stores/detail-product`,
                                  idProduct: data._id
                            }}><img src={data.img} className="card-img-top" alt="" /></Link>
                            <div className='icon'>
                                <i class="fa-regular fa-eye"></i> <br />
                                <i class="fa-regular fa-heart"></i>
                                <i class="fa-solid fa-scale-balanced"></i>
                            </div>
                            <div className="card-body">
                                <span className="card-text">
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                </span>
                                <h5 className="card-title"><Link className="name-product" to='/stores/detail-product'>{data.name}</Link></h5>
                                <p className='sale'>$ {data.price}</p>
                                <button href="#" className="btn btn-addToCart" onClick={(e) => handleClickAddToCart(e,
                                    {
                                        name: data.name,
                                        img: data.img,
                                        price: data.price,
                                        idUser: idUser,
                                        idProduct: data._id
                                    }
                                )}>add to card</button>
                            </div>
                        </div>
                    ))
                }

            </div>


        </Wrapper>
    )
}
