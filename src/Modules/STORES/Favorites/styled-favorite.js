import styled from 'styled-components'

export const Wrapper = styled.div`

    .title {
        color: #009950;
        text-decoration: none;
        display: inline-block;
        font-size: 30px;
        line-height: 36px;
        font-weight: 700;
        letter-spacing: 0px;
        font-family: Rubik, sans-serif;
        margin-top: 50px;
        margin-left: 430px;
    }

    .card {
        position: relative;
        width: 100%;
        height: 100%;
    }

    .card img{
        width: 200px;
        height: 250px;
    }

    .icon {
        position: absolute;
        opacity: 0;
        color: #009950;
        padding: 10px;
        margin-top: -250px;
        margin-left: 140px;
        transition: .2s ease-in-out;
    }

    .icon i {
        border: 1px solid transparent;
        border-radius: 50%;
        padding: 10px 10px;
        background-color: #dff9fb;
        margin: 3px 0px;
    }

    .icon i:hover {
        background-color: #009950;
        color: #fff
    }

    .name-product {
        color: #009950;
    }
    .name-product:hover{
        color: #f1c40f;
    }

    h5{
        margin-left: 50px;
        color: #009950;
    }
    .icon svg {
        font-size: 10px;
        border: 1px solid transparent;
        background-color: #dff9fb;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        text-align: center;
    }

    .icon svg:hover {
        background-color: #009950;
        color: #fff;
    }

    /* hover vào img thì nó hiện ra icon ở đây */
    .card:hover .icon {
        opacity: 1;
        transition: .2s;
    }

    .product-item {
        margin-left: 15px;
        display: flex;
        flex-wrap: wrap;
    }

    .product-item .card {
        width: 20%;
        margin: 15px 20px;
        border: none;
    }

    .product-item .card .card-text {
        justify-content: center;
        margin-bottom: 10px;
        margin-left: 50px;
    }


    .product-item .title-card-product a {
        text-align: center !important;
        color: #009950 !important;
    }

    .product-item .card p {
        color: #009950;
        font-weight: bold;
        font-size: 16px;
        font-family: Rubik, sans-serif;
        text-align: center;
    }

    .product-item .card .price {
        text-decoration-line: line-through;
        color: #009950 !important;
    }

    .product-item .card .sale {
        color: red;
    }
    /* end product item */

    .btn-addToCart {
        margin-top: 20px;
        margin-left: 20px;
        text-transform: uppercase;
        padding: 12px 20px;
        color: #fff;
        background-color: #009950;
        border: 1px solid transparent;
        border-radius: 15px;
        font-weight: bold;
        text-align: center;
        justify-content: center;
    }

    .btn-addToCart:hover {
        color: #009950;
        background-color: #f1c40f;
        transition: 0.2s linear;
    }

`