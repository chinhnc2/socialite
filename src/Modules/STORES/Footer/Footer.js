import React from 'react'
import logo from '../../../Assets/dungvv-img/logoFooter.png';
import pay1 from '../../../Assets/dungvv-img/paypal.png';
import pay2 from '../../../Assets/dungvv-img/visa.png';
import pay3 from '../../../Assets/dungvv-img/master.png';
import pay4 from '../../../Assets/dungvv-img/apple.png';
import pay5 from '../../../Assets/dungvv-img/google.png';
import { Link } from 'react-router-dom';
import {Wrapper} from './styled-footer'

export default function Footer() {
    return (
        <Wrapper className="footer">
            <footer className="text-center text-lg-start">

                <div className="container p-4">

                    <div className="row">

                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase ft-title">ABOUT US</h5>
                            <ul className="list-unstyled mb-0">
                                <span>
                                    A great about us block helps builds trust between you and your customers. The more content you provide about you and your business, the more confident people will be when purchasing from your store.
                                </span>
                            </ul>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase mb-0 ft-title">CATEGORIES</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <Link to="/stores" className="text-dark">Fish</Link>
                                </li>
                                <li>
                                    <Link to="/stores" className="text-dark">Small Pets</Link>
                                </li>
                                <li>
                                    <Link to="/stores" className="text-dark">Reptiles</Link>
                                </li>
                                <li>
                                    <Link to="/stores" className="text-dark">Birds</Link>
                                </li>
                                <li>
                                    <Link to="/stores" className="text-dark">Clearence</Link>
                                </li>
                                <li>
                                    <Link to="/stores" className="text-dark">Gift Cards</Link>
                                </li>
                            </ul>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase ft-title">INFO</h5>
                            <ul className="list-unstyled mb-0">
                                <li>
                                    <Link to="/" className="text-dark">About</Link>
                                </li>
                                <li>
                                    <Link to="/" className="text-dark">Contacts</Link>
                                </li>
                                <li>
                                    <Link to="/" className="text-dark">Faqs</Link>
                                </li>
                                <li>
                                    <Link to="/" className="text-dark">Privacy Policy</Link>
                                </li>
                                <li>
                                    <Link to="/" className="text-dark">Cookie Policy</Link>
                                </li>
                                <li>
                                    <Link to="/" className="text-dark">Terms and Conditions</Link>
                                </li>
                            </ul>
                        </div>

                        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                            <h5 className="text-uppercase mb-0 ft-title">CONTACTS</h5>
                            <ul className="list-unstyled">
                                <span>
                                    + 777 2345 7885 <br />
                                    + 566 4774 9931
                                    <br />
                                    <br />
                                    Monday - Friday: 8am - 9pm <br />
                                    Saturday - Sunday: 8am - 6pm
                                </span>
                            </ul>
                        </div>

                    </div>

                </div>

                <div className="container">
                    <div className="row logo-footer">
                        <div className="col-6 left">
                            <a to="#">
                                <img src={logo} className="footer-logo" />
                            </a>
                            <div className="col-item">
                                © Wokiee 2021. All Rights Reserved
                            </div>
                        </div>
                        <div className="col-6 right">
                            <div className="col-item">
                                <ul className="pay-list">
                                    <li>
                                        <div>
                                            <img src={pay1} alt="pay" />
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <img src={pay2} alt="pay" />
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <img src={pay3} alt="pay" />
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <img src={pay4} alt="pay" />
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <img src={pay5} alt="pay" />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </Wrapper>
    )
}
