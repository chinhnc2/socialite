import styled from 'styled-components'

export const Wrapper = styled.div`
footer {
    height: 400px;
    width: 100%;
    margin-left: 0px;
    margin-top: 30px;
    background-color: #009950;
    font-family: Rubik, sans-serif;
}

.ft-title {
    margin-top: 30px;
    color: #fff;
    font-size: 20px;
    line-height: 24px;
    letter-spacing: 0em;
    font-weight: 700;
    display: block;
    position: relative;
}

.list-unstyled {
    margin-top: 20px;
}

.list-unstyled li {
    margin-top: 5px;
    font-size: 14px;
    font-weight: 400;
}

.list-unstyled span {
    color: #fff;
    font-size: 14px;
    font-family: Rubik, sans-serif;
    text-align: justify;
}

.list-unstyled li .text-dark {
    color: #f1c40f !important;
}

.list-unstyled li .text-dark:hover {
    color: #fff !important;
}


/* footer */
.left {
    display: flex;
}

.footer-logo {
    margin-top: 60px;
}

.left .col-item {
    margin-top: 70px;
    margin-left: 10px;
    color: #fff;
}

.pay-list{
    margin-top: -25px !important;
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
    float: right;
}

.pay-list li{
    margin: 5px;
}
`