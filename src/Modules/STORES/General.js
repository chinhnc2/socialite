import React from 'react'
import PagesContent from './Pages/PagesContent'
import Favorites from './Favorites/Favorites'
import OurFavorites from './OurFavorites/OurFavorites'
import PetNeed from './PetNeeds/PetNeed'
import Healthy from './Healthy/Healthy'
import TellProduct from './TellProduct/TellProduct'
import Blog from './Blog/Blog'
import Letter from './Letter/Letter'
import Categories from './Categories/Categories'

export default function General() {
  return (
    <div className="Sum">
      <PagesContent />
      <Categories />
      <Favorites />
      <OurFavorites />
      <PetNeed />
      <Healthy />
      <TellProduct />
      <Blog />
      <Letter />
    </div>
  )
}
