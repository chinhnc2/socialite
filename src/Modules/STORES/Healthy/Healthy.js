import React from 'react'
import img1 from '../../../Assets/dungvv-img/health1.png';
import img2 from '../../../Assets/dungvv-img/health2.png';
import img3 from '../../../Assets/dungvv-img/health3.png';
import img4 from '../../../Assets/dungvv-img/health4.png';

import { Wrapper } from './styled-healthy'

export default function Healthy() {
    return (
        <Wrapper className='container'>
            <h4 className='healthy-title'>Our Health and Wellness Services</h4>
            <div className='row card-group healthy'>
                <div className='col-3 box'>
                    <div className="card">
                        <img src={img1} className="card-img-top" alt="..." />
                        <div className="card-body bg-1 content-outside">
                            <h4 className="card-title">GROOMING</h4>
                            <p className="card-text">A great about us block helps builds trust between you and your customers. The more content you provide about you.</p>
                        </div>
                    </div>
                </div>

                <div className='col-3 box'>
                    <div className="card">
                        <img src={img2} className="card-img-top" alt="..." />
                        <div className="card-body bg-2 content-inside">
                            <h4 className="card-title">TRAINING</h4>
                            <p className="card-text">A great about us block helps builds trust between you and your customers. The more content you provide about you.</p>
                        </div>
                    </div>
                </div>

                <div className='col-3 box'>
                    <div className="card">
                        <img src={img3} className="card-img-top" alt="..." />
                        <div className="card-body bg-3 content-inside">
                            <h4 className="card-title">VETERINARY</h4>
                            <p className="card-text">A great about us block helps builds trust between you and your customers. The more content you provide about you.</p>
                        </div>
                    </div>
                </div>

                <div className='col-3 box'>
                    <div className="card">
                        <img src={img4} className="card-img-top" alt="..." />
                        <div className="card-body bg-4 content-outside">
                            <h4 className="card-title">PET INSURANCE</h4>
                            <p className="card-text">A great about us block helps builds trust between you and your customers. The more content you provide about you.</p>
                        </div>
                    </div>
                </div>

            </div>
        </Wrapper>
    )
}
