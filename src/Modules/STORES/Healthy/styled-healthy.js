import styled from 'styled-components'

export const Wrapper = styled.div`
    .healthy{
        margin-top: 30px;
        margin-bottom: 70px;
        display: flex;
    }

    .healthy-title{
        font-size: 30px;
        line-height: 36px;
        font-weight: bold;
        color: #009950;
        display: block;
        position: relative;
        font-family: Rubik,sans-serif;
        text-align: center;
        margin-bottom: 30px;
    }

    .healthy .box{
        margin-left: 10px;
    }

    .healthy .card{
        border: 1px solid transparent;
        border-radius: 20px;
    }

    .healthy .card img{
        width: 100%;
        border-radius: 20px 20px 0 0;
    }

    .healthy .card .bg-1{
        background-color: #4DB885;
        border-radius: 0 0 20px 20px;
        height: 150px;
    }
    .healthy .card .bg-2{
        background-color: #EDF8F3;
        border-radius: 0 0 20px 20px;
        height: 150px;
    }
    .healthy .card .bg-3{ 
        background-color: #FFE11B;
        border-radius: 0 0 20px 20px;
        height: 150px;
    }
    .healthy .card .bg-4{
        background-color: #FA9247;
        border-radius: 0 0 20px 20px;
        height: 150px;
    }

    .healthy .card .content-outside{
        color: #ffffff;
        display: block;
        position: relative;
        font-family: Rubik,sans-serif;
        margin-bottom: 0;
        height: 150px;
    }
    .healthy .card .content-outside h4{
        color: #ffffff;
        font-weight: bold;
        text-align: center;
        font-size: 20px;
    }
    .healthy .card .content-outside p{
        margin-top: 15px;
        font-size: 14px;
        line-height: 17px;
        font-weight: 400;
        margin-top: 10px;
        margin-left: 30px;
        margin-right: 20px;
        text-align: inherit;
    }


    .healthy .card .content-inside{
        color: #009950;
        display: block;
        position: relative;
        font-family: Rubik,sans-serif;
        margin-bottom: 0;
    }
    .healthy .card .content-inside h4{
        color: #009950;
        font-weight: bold;
        text-align: center;
        font-size: 20px;
    }
    .healthy .card .content-inside p{
        font-size: 14px;
        line-height: 17px;
        font-weight: 400;
        margin-top: 10px;
        margin-left: 30px;
        margin-right: 20px;
        text-align: inherit;
    }
`