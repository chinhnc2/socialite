import React from 'react'
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css';
import logo from '../../../Assets/dungvv-img/logo.png';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Wrapper } from './styled-holder';

export default function Holder() {
    const cartNumber=useSelector(state=>state.cart.numberCart);
    return (
        <Wrapper className='container holder_banner'>
            <div className="row">

                <div className='col-md-4 obj-logo'>
                    <Link to='/store'><img src={logo} alt="Logo" /></Link>
                </div>

                <div className='col-md-4 header-info'>
                    <p className="holder_title">Call Us: +777 2345 7885</p>
                    <p className="holder_bot">From 8:00 to 21:00 (Mon-Sun) Free by United States</p>
                </div>

                <div className='col-md-4 header-icon'>
                    <span class="tool">
                        <i class="fa-solid fa-scale-balanced"></i>
                        <Tippy content='Compare'>
                            <Link to="#">
                                <span>compare</span>
                            </Link>
                        </Tippy>
                    </span>

                    <span class="tool like">
                        <i class="fa-solid fa-heart"></i>
                        <Tippy content='Wishlist'>
                            <Link to="#">
                                <span>wishlist</span>
                            </Link>
                        </Tippy>
                    </span>

                    <span class="tool">
                        <i class="fa-solid fa-cart-shopping"></i>
                        <Tippy content='Cart'>
                            <Link to="/stores/cart">
                                <span>cart</span>
                                <span>({cartNumber})</span>
                            </Link>
                        </Tippy>
                    </span>
                    
                </div>
            </div>


        </Wrapper>
    )
}
