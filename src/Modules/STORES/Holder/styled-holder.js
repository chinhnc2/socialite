import styled from 'styled-components'

export const Wrapper = styled.div`
    .obj-logo img {
        margin-left: 10px;
    }
    .holder_banner {
        display: flex;
    }

    .header-info{
        margin-bottom: 20px;
    }
    .header-info .holder_title {
        color: #fa9247;
        font-weight: bold;
        margin-left: 190px;
    }
    .header-info .holder_title .holder_bot{
       color: #009950;
    }
    .tool {
        justify-content: center;
        align-items: center;
        height: 100vh;
        width: 100%;
        padding: 5px 8px;
        font-size: 14px;
        line-height: 1;
        color: #009950;
    }

    .tool span {
        color: #009950;
        outline: none;
        text-align: center;
        text-transform: uppercase;
        font-size: 16px;
        line-height: 19px;
        font-weight: bold;
        transition: color 0.2s linear;
        padding: 0 0 10px 5px;
    }




    @media screen and (max-width: 1200px) {
        .holder_banner {
            width: 100%;
        }
        .header-info {
            margin-left: -75px;
        }
    }

    @media screen and (max-width: 378px) {
        .holder_banner {
            width: 100%;
        }
        .row {
            width: 900px;
        }
        .obj-logo img {
            width: 100px;
            height: 100px;

        }
        .header-info {
            font-size: 12px;
            margin-top: -50px;
            margin-left: 20px;
        }
        .header-info .holder_bot {
            margin-left: 80px;
            font-size: 10px;
        }
        .header-icon {
            margin-bottom: 30px;
        }
        
    }


`