import React from 'react'
import {Wrapper} from './styled-letter'

export default function Letter() {
    return (
        <Wrapper className='container '>
            <div className="layout-letter letter">
                <h5 className="newLetter">Newsletter Signup</h5>
                <div className="letter-content">
                    Sign up for our e-mail and be the first who know our special offers!
                </div>
                <div className="form-defaul">
                    <form className="form-detail" method="post">
                        <input type="text" className="inputLetter" placeholder="Your Email" />
                        <button className="btn join" type="submit">Join Us</button>
                    </form>
                </div>
                <div className="text">
                    <p>
                        By clicking the button you agree to the <a href="#">Privacy Policy</a> and <a href="#">Terms and Conditions</a>
                    </p>
                </div>
                
                <div className="social">
                    <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                    <a href="#"><i class="fa-brands fa-twitter"></i></a>
                    <a href="#"><i class="fa-brands fa-instagram"></i></a>
                    <a href="#"><i class="fa-brands fa-pinterest-p"></i></a>
                    <a href="#"><i class="fa-solid fa-square-phone"></i></a>
                </div>
            </div>

        </Wrapper>
    )
}
