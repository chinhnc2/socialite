import styled from 'styled-components'

export const Wrapper = styled.div`
.letter {
    width: 100%;
}

.layout-letter .newLetter {
    font-size: 30px;
    line-height: 36px;
    color: #009950;
    font-weight: 700;
    margin-top: 0;
    display: block;
    position: relative;
    font-family: Rubik, sans-serif;
    text-align: center;
}

.letter-content {
    font-size: 14px;
    line-height: 17px;
    font-weight: 400;
    margin-top: 8px;
    color: #009950;
    text-align: center;
}

.form-defaul {
    justify-content: center;
}

.form-detail {
    width: 600px;
    display: flex;
    margin-top: 30px;
    margin-left: 350px;
}

.inputLetter {
    display: block;
    width: 100%;
    height: 34px;
    padding: 23px;
    font-size: 14px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 5px;
}

.form-detail .join {
    color: #009950;
    background: #009950;
    margin-left: 10px;
    margin-top: 0;
    font-family: Rubik, sans-serif;
    border: none;
    color: #fff;
    font-size: 14px;
    line-height: 1;
    font-weight: 500;
    letter-spacing: 0em;
    position: relative;
    outline: none;
    padding: 6px 31px 4px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    height: 50px;
    border-radius: 20px;
}

.form-detail .join:hover {
    color: #009950;
    background: #ffe11b;
}

.text {
    margin-top: 10px;
    margin-left: 30px;
    text-align: center;
    color: #009950;
}

.text a {
    color: #fa9247;
}
.text a:hover {
    text-decoration: underline;
}

.social {
    text-align: center;
    margin-top: 20px;
}

.social a {
    color: #fa9247;
    margin: 10px;
    font-size: 15px;
    padding: 4px;
    line-height: 26px;
}

.social a:hover {
    color: #009950;
}
`