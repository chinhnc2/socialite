import React from "react";
import {Link, NavLink} from "react-router-dom";

import { Wrapper } from './styled'

export default function Nav() {
  return (
    <Wrapper className="container nav_store">
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container-fluid">
          <div className="menu_store collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="ul_store navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item active" aria-current="page" to="/stores">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/shop">shop</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/contact">pages</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/blog">blog</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/port">portfolio</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/buy">buy theme!</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/create-store">Create Story</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-NavLink list_item" to="/stores/list-store">List Story</NavLink>
              </li>
            </ul>
          </div>
          <div className="signtest">
            <p><NavLink className="sign" to="/stores/sign">SIGN IN &nbsp; </NavLink> or <NavLink className="sign" to="/stores/register">&nbsp;REGISTER</NavLink></p>
          </div>

          <div class="dropdown money">
            <p class="dropbtn">$ (USD) <i class="fa-solid fa-angle-down"></i></p>
            <div class="dropdown-content">
              <NavLink to="#" className="active">$ (USD)</NavLink>
              <NavLink to="#">€ (EUR)</NavLink>
              <NavLink to="#">£ (GBP)</NavLink>
            </div>
          </div>

        </div>

      </nav>
    </Wrapper>
  );
}
