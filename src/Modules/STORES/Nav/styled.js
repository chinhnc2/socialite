import styled from 'styled-components'

export const Wrapper = styled.div`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    list-style: none;
  }

  .container-fluid{
    display: flex;
    list-style: none;
    margin-bottom: 30px;
  }
  
  .nav_store {
    width: 100%;
    margin-top: 30px;
  }

  .menu_store{
    margin-top: 20px;
    /* margin-left: -50px; */
  }

  .list_item {
    font-family: Rubik, sans-serif;
    font-size: 15px;
    font-weight: bold;
    line-height: 17px;
    color: #009950;
    text-transform: uppercase;
    list-style: none;
    margin: 0px 5px;
  }
  
  .menu_store .ul_store .nav-item .list_item:hover {
    color: #fa9247;
  }
  
  .menu_store .ul_store .nav-item .list_item.active {
    color: #fa9247;
  }
  

  .signtest{
    margin-left: 230px;
    margin-top: 20px;
  }

  .signtest .sign {
    color: #009950;
    text-decoration: none;
    background-color: transparent;
    transition: color 0.3s;
    font-family: Rubik, sans-serif;
    font-size: 14px;
    line-height: 17px;
  }
  
  .signtest .sign:hover{
    color: #fa9247;
  }
  
  .money{
    color: #009950;
  }
  
  .money .list_item.active{
    color: #fa9247;
  }
  
  .dropbtn {
    color: #009950;
    padding: 14px;
    margin-top: 5px;
    font-size: 14px;
  }
  
  .dropbtn:hover {
    color: #fa9247;
  }
  
  .dropdown {
    position: relative;
    display: inline-block;
  }
  
  .dropdown-content {
    display: none;
    position: absolute;
    z-index: 999999;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  }
  
  .dropdown-content a {
    color: #009950;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }
  
  .dropdown-content a:hover {
    color: #fa9247;
    background-color: #ddd;
  }
  
  .dropdown:hover .dropdown-content {
    display: block;
  }




  @media screen and (max-width: 1200px) {
    .nav_store {
      width: 100%;
    }
    .signtest p{
      margin-left: -180px;
    }
  }



`