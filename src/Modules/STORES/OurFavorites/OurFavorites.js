import React from 'react'
import img1 from '../../../Assets/dungvv-img/pet1.png';
import img2 from '../../../Assets/dungvv-img/pet2.png';
import img3 from '../../../Assets/dungvv-img/pet3.png';
import img4 from '../../../Assets/dungvv-img/pet4.png';
import { Wrapper } from './styled-ourFavorite';

export default function OurFavorites() {
  return (
    <Wrapper className='container'>
      <div className="ourfavorites">
        <h4 className='title'>Some of Our Favorites to Care</h4>
        <div className='row'>
          <div className='col-sm-6 big'>
            <div className='title-big'>take care <br />of your pet's needs</div>
            <div className='img-cat'>
              <img className='image-card' src={img1} alt='' />
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='row'>
              <div className='col-sm-6 small'>
                <div className='title-small'>Shop Cat</div>
                <div className='img-cat'>
                  <img className='image-small-Left' src={img2} alt='' />
                </div>
              </div>
              <div className='col-sm-6 small'>
                <div className='title-small'>Shop Dog</div>
                <div className='img-cat'>
                  <img className='image-small-Right' src={img3} alt='' />
                </div>
              </div>
            </div>
            <div className='row smallbot'>
              <div className='title-small-bot'>Shop fish</div>
              <div className='img-fish'>
                <img className='image-small-Bot' src={img4} alt='' />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}
