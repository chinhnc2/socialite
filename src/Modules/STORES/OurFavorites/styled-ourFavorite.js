import styled from 'styled-components'

export const Wrapper = styled.div`

    .ourfavorites {
        margin-bottom: 50px;
    }

    .title {
        font-size: 30px;
        line-height: 36px;
        color: #009950;
        font-weight: 700;
        letter-spacing: 0px;
        margin: 0;
        padding: 0;
        display: block;
        position: relative;
        font-family: Rubik, sans-serif;
        margin-top: 70px;
        margin-bottom: 50px;
        text-align: center;
    }

    .big .title-big {
        color: #009950;
        font-size: 24px;
        line-height: 30px;
        font-weight: bold;
        position: absolute;
        z-index: 999999;
        font-family: Rubik, sans-serif;
        text-align: center;
        text-transform: uppercase;
        margin-top: 30px;
        margin-left: 150px;
    }

    .big .title-big:hover {
        text-decoration: underline;
    }

    .img-cat {
        overflow: hidden;
        border-radius: 15px;
        position: relative;
        padding-top: 100%;
    }

    .image-card {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        /* de hinh khoi meo */
        object-fit: cover;
        vertical-align: middle;
        border-style: none;
        border: 1px solid transparent;
        transition: transform 0.2s;
    }

    .image-card:hover {
        transform: scale(1.1);
        transition: 2s;
    }


    .image-small-Left {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        /* de hinh khoi meo */
        object-fit: cover;
        vertical-align: middle;
        border-style: none;
        border: 1px solid transparent;
        transition: transform 0.2s;
    }

    .image-small-Left:hover {
        transform: scale(1.1);
        transition: 2s;
    }

    .image-small-Right {
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        /* de hinh khoi meo */
        object-fit: cover;
        vertical-align: middle;
        border-style: none;
        border: 1px solid transparent;
        transition: transform 0.2s;
    }

    .image-small-Right:hover {
        transform: scale(1.1);
        transition: 2s;
    }



    .img-fish {
        overflow: hidden;
        border-radius: 15px;
        position: relative;
    }

    .image-small-Bot {
        border-radius: 15px;
        margin-top: 10px;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        object-fit: cover;
        vertical-align: middle;
        border-style: none;
        border: 1px solid transparent;
        transition: transform 0.2s;
        position: relative;
    }

    .image-small-Bot:hover {
        transform: scale(1.1);
        transition: 2s;
    }

    .title-small {
        color: #ffe11b;
        font-size: 24px;
        line-height: 30px;
        font-weight: bold;
        position: absolute;
        z-index: 999999;
        font-family: Rubik, sans-serif;
        text-align: center;
        text-transform: uppercase;
        margin-top: 30px;
        margin-left: 70px;
    }

    .title-small:hover {
        text-decoration: underline;
    }

    .title-small-bot {
        color: #ffe11b;
        font-size: 20px;
        line-height: 30px;
        font-weight: bold;
        position: absolute;
        z-index: 999999;
        font-family: Rubik, sans-serif;
        text-align: center;
        text-transform: uppercase;
        margin-top: 30px;
        margin-left: 250px;
    }

    .title-small-bot:hover {
        text-decoration: underline;
    }

`