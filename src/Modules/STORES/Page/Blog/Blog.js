import React from 'react'
import { Wrapper } from './styled-blog'
import img1 from '../../../../Assets/dungvv-img/blog1.png';
import img2 from '../../../../Assets/dungvv-img/blog2.png';
import img3 from '../../../../Assets/dungvv-img/blog3.png';


export default function Blog() {
    return (
        <Wrapper>
            <div className="blog">
                <h2>NEWS</h2>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-sm-4 box">
                            <h5>BLOGS</h5>
                            <hr />
                            <a href="#" className="fakeimg">Code</a>
                            <a href="#" className="fakeimg">News</a>

                            <h5>ABOUT</h5>
                            <hr />
                            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.
                            </p>
                            <a href="#">READ MORE</a>

                            <h5>TAGS</h5>
                            <hr />
                            <a href="#">AUDIO</a>
                            <a href="#">GALLERY</a>
                            <a href="#">POST TYPE</a>
                            <br />

                            <a href="#">PROMO</a>
                            <a href="#">SECTIONS</a>
                            <a href="#">SHOPIFY</a>
                            <br />

                            <a href="#">STANDART</a>
                            <a href="#">VIDEO</a>
                            <a href="#">WOKIEE</a>


                            <h5>RECENT POST</h5>
                            <hr />
                            <a href="#">LADIES</a> <br />
                            <a href="#"><b>Catalogue Mode</b></a>
                            <p>It is a long established fact that by the readable content of a page when looking at its layout.</p>
                            <p>by Diego Lopez on July 12, 2018</p>

                            <h5>NEWSLETTER SIGNUP</h5>
                            <hr />
                            <p>Sign up for our e-mail and be the first who know our special offers! Furthermore, we will give a 15% discount on the next order after you sign up.</p>


                            <hr className="d-sm-none" />
                        </div>
                        <div className="col-sm-8">
                            <div className="row box">
                                <div className="col-sm-4">
                                    <a href="#">
                                        <img src={img2} />
                                    </a>
                                </div>
                                <div className="col-sm-8">
                                    <h3>Standart</h3>
                                    <p>It is a long established fact that by the readable content of a page when looking at its layout.</p>
                                    <p className="time">by Diego Lopez on June 18, 2018 <i class="fa-regular fa-comment"></i> 9</p>
                                    <button className="btn btn-outline-info readmore">READ MORE</button>
                                </div>
                            </div>

                            <div className="row box">
                                <div className="col-sm-4">
                                    <a href="#">
                                        <img src={img3} />
                                    </a>
                                </div>
                                <div className="col-sm-8">
                                    <h3>Gallery</h3>
                                    <p>It is a long established fact that by the readable content of a page when looking at its layout.</p>
                                    <p className="time">by Diego Lopez on June 18, 2018 <i class="fa-regular fa-comment"></i> 9</p>
                                    <button className="btn btn-outline-info readmore">READ MORE</button>
                                </div>
                            </div>

                            <div className="row box">
                                <div className="col-sm-4">
                                    <a href="#">
                                        <img src={img1} />
                                    </a>
                                </div>
                                <div className="col-sm-8">
                                    <h3>Video</h3>
                                    <p>It is a long established fact that by the readable content of a page when looking at its layout.</p>
                                    <p className="time">by Diego Lopez on June 18, 2018 <i class="fa-regular fa-comment"></i> 9</p>
                                    <button className="btn btn-outline-info readmore">READ MORE</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </Wrapper>
    )
}
