import styled from 'styled-components'

export const Wrapper = styled.div`


    .blog h2 {
        font-size: 34px;
        color: #009950;
        margin-left: 650px;
        line-height: 44px;
        font-weight: bold;
        text-transform: uppercase;
        font-family: Rubik, sans-serif;
    }

    .box {
        margin-top: 20px;
    }

    .box img {
        width: 250px;
        height: 200px;
    }

    .box h5 {
        margin-top: 30px;
    }

    .box a {
        margin: 15px 5px 5px 0px;
        color: #009950;
        font-family: Rubik, sans-serif;
    }

    .time {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .readmore {
        background-color: #009950;
        color: #fff;
    }

    .readmore:hover {
        background-color: #ffe11b;
        color: #009950;
    }
`