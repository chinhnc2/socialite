import React from 'react'
import { Link } from 'react-router-dom'

import { Wrapper } from './styled-contact'

export default function PageContact() {
    return (
        <Wrapper className="contact">
            <ul className="contact-nav">
                <li className="sp">
                    <Link className="" to="/">Home</Link>
                </li>
                &nbsp; /
                <li>
                    <Link className="" to="/shop">Contact</Link>
                </li>
            </ul>
            <div className="row">
                <div className="map">
                    <img src="https://images.viblo.asia/full/d5fa8659-fd34-49b8-8dfa-5180621b9367.png" />
                </div>
                <div className="col-sm-4 box">
                    <i class="fa-solid fa-phone fa-3x active"></i>
                    <h6 className="contact-title">LET'S HAVE A CHAT!</h6>
                    <address className="active">
                        +777 2345 7885:
                        <br />
                        +777 2345 7835:
                    </address>
                </div>
                <div className="col-sm-4 box">
                    <i class="fa-solid fa-location-dot fa-3x active"></i>
                    <h6 className="contact-title">VISIT OUR LOCATION</h6>
                    <address className="active">
                        2548 Broaddus Maple Court Avenue, <br />
                        Madisonville KY 4783, <br />
                        United States of America
                    </address>
                </div>
                <div className="col-sm-4 box">
                    <i class="fa-solid fa-clock fa-3x active"></i>
                    <h6 className="contact-title">WORK TIME</h6>
                    <address className="active">
                        7 Days a week <br />
                        from 10 AM to 6 PM
                    </address>
                </div>
            </div>
            <div className="row form">
                <div className="col-6">
                    <div class="form-input">
                        <input type="text"
                            class="form-control" placeholder="Your Name" name="" id="" aria-describedby="helpId" />
                        <input type="text"
                            class="form-control" placeholder="Your Email" name="" id="" aria-describedby="helpId" />
                        <input type="text"
                            class="form-control" placeholder="Your Phone" name="" id="" aria-describedby="helpId" />
                    </div>
                </div>
                <div className="col-6">
                    <div class="form-group">
                        <textarea class="form-control" name="" id="" rows="5"></textarea>
                    </div>
                </div>
                
            </div><button className="btn-info">SEND MESSAGE</button>
        </Wrapper>
    )
}
