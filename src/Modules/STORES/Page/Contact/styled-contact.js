import styled from 'styled-components'

export const Wrapper = styled.div`
    .contact {
        width: 100%;
        text-align: center;
    }

    .contact-nav{
        display: flex;
        list-style: none;
        background: #f7f8fa;
        text-align: center;
        color: #009950;
    }

    .contact-nav .sp{
        margin-left: 150px;
    }

    .contact-nav li {
        margin-left: 30px;
    }

    .contact-nav li:hover {
        color: #fa9247;
    }


    .map img {
        width: 1200px;
        height: 400px;
        margin-left: 150px;
        margin-top: 50px;
        margin-bottom: 50px;
    }

    .contact i {
        margin: 20px;
        font-size: 20px;
        margin-bottom: 20px;
    }

    address {
        color: #777777;
    }
    .box {
        text-align: center;
    }
    h6 {
        margin-bottom: 20px;
        font-weight: 400px;
        margin-top: 20px;
        font-size: 20px;
    }

    .form{
        display: flex;
    }

    .form-input input {
        margin-top: 8px;
        width: 500px;
        margin-left: 200px;
        margin-right: 100px;
        border-radius: 10px;
    }

    textarea {
        margin-top: 10px;
        border-radius: 5px;
        width: 550px;
    }

    .btn-info {
        width: 150px;
        height: 50px;
        border-radius: 20px;
        text-align: center;
        margin-top: 20px;
        margin-bottom: 20px;
        margin-left: 650px;
    }
`