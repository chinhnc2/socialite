import React from 'react'
import {Wrapper} from './styled-portfolio'
import img1 from '../../../../Assets/dungvv-img/por.png';
import img2 from '../../../../Assets/dungvv-img/por2.png';
import img3 from '../../../../Assets/dungvv-img/por3.png';
import img4 from '../../../../Assets/dungvv-img/por4.png';
import img5 from '../../../../Assets/dungvv-img/por5.png';
import img6 from '../../../../Assets/dungvv-img/por6.png';
import img7 from '../../../../Assets/dungvv-img/por7.png';
import img8 from '../../../../Assets/dungvv-img/por8.png';



export default function Portfolio() {
    return (
        <Wrapper className="container port">
            <div className="row">
                <div className="column">
                    <img src={img1} />
                    <img src={img2} />
                    <img src={img3} />
                    <img src={img4} />
                </div>
                <div className="column">
                    <img src={img5} />
                    <img src={img6} />
                    <img src={img7} />
                    <img src={img8} />
                </div>
            </div>
        </Wrapper>
    )
}
