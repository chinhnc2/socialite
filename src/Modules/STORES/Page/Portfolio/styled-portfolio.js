import styled from 'styled-components'

export const Wrapper = styled.div`
.column {
    float: left;
    width: 50%;
    padding: 10px;
}

.column img {
    width: 100%;
    margin-top: 12px;
    transition: 0.5s;
    opacity: 1;   
}

.column img:hover {
    opacity: 0.3;

}

.row:after {
    content: "";
    display: table;
    clear: both;
}
`