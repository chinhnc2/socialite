import React, { useEffect } from 'react'
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { Wrapper } from './styled-product'

import { AddCart, actFetchProductsRequest, actFetchCartRequest } from 'Modules/STORES/Shopping/action/index';

export default function Product() {

    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart);
    const idUser = useSelector(state => state.auth.profile._id);
    const handleClickAddToCart = (event, payload) => {
        event.preventDefault();
        dispatch(AddCart(payload));
    }
    useEffect(() => {
        actFetchProductsRequest(dispatch);
        actFetchCartRequest(dispatch, idUser);
    }, [])
    return (
        <Wrapper className="pagecollection">
            <ul className="product-nav">
                <li className="sp">
                    <Link className="" to="/stores">Home</Link>
                </li>
                &nbsp; /
                <li>
                    <Link className="" to="/stores/shop">Pets supplies</Link>
                </li>
            </ul>

            {/* select menu */}
            <div className="selectMenu">
                <label for="sel1" class="form-label">Select list:</label>
                <select className="form-select" id="sel1" name="sellist1">
                    <option>Featured</option>
                    <option>Name Ascending</option>
                    <option>Name Descending</option>
                    <option>Date Ascending</option>
                    <option>Date Descending</option>
                    <option>Price Ascending</option>
                    <option>Price Descending</option>
                </select>
            </div>



            <div className="pageContent">
                <h2>Pets Supplies #2</h2>

                <div className="container">
                    <div class="row product-item-shop">

                        {
                            cart._products.map((data, index) => (
                                <div className="card" style={{ width: '18rem' }} key={index}>
                                    <Link to={{
                                        pathname: `/stores/detail-product`,
                                        idProduct: data._id
                                    }}><img src={data.img} className="card-img-top" alt="" /></Link>
                                    <div className='icon'>
                                        <i class="fa-regular fa-eye"></i> <br />
                                        <i class="fa-regular fa-heart"></i>
                                        <i class="fa-solid fa-scale-balanced"></i>
                                    </div>
                                    <div className="card-body">
                                        <span className="card-text">
                                            <i class="fa-regular fa-star"></i>
                                            <i class="fa-regular fa-star"></i>
                                            <i class="fa-regular fa-star"></i>
                                            <i class="fa-regular fa-star"></i>
                                            <i class="fa-regular fa-star"></i>
                                        </span>
                                        <h5 className="card-title"><Link to='/stores/detail-product'>{data.name}</Link></h5>
                                        <p className='sale'>$ {data.price}</p>
                                        <button href="#" className="btn btn-addToCart" onClick={(e) => handleClickAddToCart(e,
                                            {
                                                name: data.name,
                                                img: data.img,
                                                price: data.price,
                                                idUser: idUser,
                                                idProduct: data._id
                                            }
                                        )}>add to card</button>
                                    </div>
                                </div>

                            ))
                        }
                    </div>
                </div>
                <button className="btn-noProduct">NO MORE PRODUCTS</button>
            </div>
        </Wrapper>

    )
}
