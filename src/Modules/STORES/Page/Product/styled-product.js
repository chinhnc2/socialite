import styled from "styled-components";

export const Wrapper = styled.div `

    .product-item-shop{
        flex-wrap: wrap;
    }

    .product-nav {
        display: flex;
        list-style: none;
        background: #f7f8fa;
        text-align: center;
        color: #009950;
    }

    .sp {
        margin-left: 190px !important;
    }

    .product-nav li {
        margin-left: 30px;
    }

    .product-nav li:hover {
        color: #fa9247;
    }

    .pageContent {
        margin-top: 20px;
        margin-bottom: 40px;
    }

    .pageContent h2 {
        margin-left: 180px;
        display: block;
        position: relative;
        font-family: Rubik, sans-serif;
        color: #009950;
        font-size: 30px;
        line-height: 36px;
        letter-spacing: 0em;
        font-weight: 700;
    }

    .selectMenu {
        width: 150px;
        float: right;
        margin-right: 280px;
        color: #009950;
    }


    .product-item-shop-shop {
        margin-left: -70px !important;
    }

    .card {
        position: relative;
        width: 100%;
        height: 100%;
    }

    .card img {
        width: 200px;
        height: 250px;
    }

    .icon {
        position: absolute;
        opacity: 0;
        color: #009950;
        padding: 10px;
        margin-left: 160px;
        margin-top: -250px;
        transition: .2s ease-in-out;
    }

    .icon i {
        border: 1px solid transparent;
        border-radius: 50%;
        padding: 10px 10px;
        background-color: #dff9fb;
        margin: 3px 0px;
    }

    .icon i:hover {
        background-color: #009950;
        color: #fff
    }

    .icon svg {
        font-size: 10px;
        border: 1px solid transparent;
        background-color: #dff9fb;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        text-align: center;
    }

    .icon svg:hover {
        background-color: #009950;
        color: #fff;
    }

    /* hover vào img thì nó hiện ra icon ở đây */
    .card:hover .icon {
        opacity: 1;
        transition: .2s;
        cursor: pointer;
    }

    .container .product-item-shop .card strong {
        background: red;
        width: 30px;
        height: 20px;
        position: absolute;
    }

    .product-item-shop {
        display: flex;
        margin-left: 15px;
    }

    .product-item-shop .card {
        width: 20%;
        margin: 15px 20px;
        border: none;
    }

    .product-item-shop .card .card-text {
        justify-content: center;
        display: flex;
        margin-bottom: 10px;
        margin-left: 10px;
    }


    .product-item-shop .card .card-title a{
        color: #009950;
        text-align: center;
        margin-left: 50px;
    }

    .product-item-shop .card p {
        color: #009950;
        font-weight: bold;
        font-size: 16px;
        font-family: Rubik, sans-serif;
        text-align: center;
    }

    .product-item-shop .card .price {
        text-decoration-line: line-through;
        color: #009950 !important;
    }

    .product-item-shop .card .sale {
        color: red;
    }

    /* end product item */

    .btn-addToCart {
        margin-top: 20px;
        margin-left: 20px;
        text-transform: uppercase;
        padding: 12px 20px;
        color: #fff;
        background-color: #009950;
        border: 1px solid transparent;
        border-radius: 15px;
        font-weight: bold;
        text-align: center;
        display: flex;
        justify-content: center;
    }

    .btn-addToCart:hover {
        color: #009950;
        background-color: #f1c40f;
        transition: 0.2s linear;
    }

    /* end btn addtocart */

    .btn-noProduct {
        text-align: center;
        background: #fff;
        color: #009950;
        border: 1px solid #009950;
        border-radius: 15px;
        height: 40px;
        width: 200px;
        margin-top: 40px;
        margin-bottom: 40px;
        margin-left: 700px;
    }

    .btn-noProduct:hover {
        background: #f1c40f;
        color: #009950;
        transition: .2s;
    }

`
