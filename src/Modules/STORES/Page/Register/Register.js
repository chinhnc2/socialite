import React from 'react'
import {Wrapper} from './styled-res'
import { Link } from 'react-router-dom'

export default function Register() {
    return (
        <Wrapper className="container register">
            <h1>CREATE AN ACCOUNT</h1>
            <div className="form defaul">
                <div className="wrapper">
                    <div className="title">
                        PERSONAL INFORMATION
                    </div>
                    <div className="form">
                        <div className="inputfield">
                            <label>First Name</label>
                            <input type="text" className="input" placeholder="Enter First Name" />
                        </div>
                        <div className="inputfield">
                            <label>Last Name</label>
                            <input type="text" className="input" placeholder="Enter Last Name" />
                        </div>
                        <div className="inputfield">
                            <label>Password</label>
                            <input type="password" className="input" placeholder="Enter Password" />
                        </div>
                        <div className="inputfield">
                            <label>Email Address</label>
                            <input type="text" className="input" placeholder="Enter Email" />
                        </div>
                        <div className="inputfield">
                            <label>Phone Number</label>
                            <input type="text" className="input" placeholder="Enter Phone Number" />
                        </div>
                        <div className="inputfield">
                            <label>Address</label>
                            <textarea className="textarea" placeholder="Enter Address ..." defaultValue={""} />
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn-create btn-border">CREATE</button>
                            <span>or <Link className="return" to="/stores">Return to Store</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    )
}
