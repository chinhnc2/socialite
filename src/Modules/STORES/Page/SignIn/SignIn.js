import React from 'react'
import { Link } from 'react-router-dom'
import {Wrapper} from './styled-Sign'

export default function SignIn() {
    return (
        <Wrapper className="container sign">
            <h1>ALREADY REGISTERED?</h1>
            <div className="form defaul">
                <div className="wrapper">
                    <div className="title">
                        LOGIN
                    </div>
                    <span className="sign-title">If you have an account with us, please log in.</span>
                    <div className="form">
                        <div className="inputfield">
                            <label>Email</label>
                            <input type="text" className="input" placeholder="Enter Email" />
                        </div>
                        <div className="inputfield">
                            <label>Password</label>
                            <input type="password" className="input" placeholder="Enter Password" />
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn-sign btn-border">LOGIN</button>
                            <span><Link className="return" to="/stores">Lost your password?</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </Wrapper>
    )
}
