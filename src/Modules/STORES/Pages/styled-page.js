import styled from 'styled-components'

export const Wrapper = styled.div`

    .dropdown-menu{
        opacity: 1;
        width: 600px;
        height: 520px;
        border: 1px solid transparent;
        border-radius: 10px;
        margin-left: 320px;
        margin-top: -525px;
        /* display: block; */
    }

    .tt-dropdown-menu nav>ul .dropdown-menu{
        position: absolute;
        top: 0;
        left: 105%;
        visibility: hidden;
        pointer-events: none;
        opacity: 0;
    }

    .tt-megaMenu{
        font-size: 22px;
        font-weight: bold;
    }

    span {
        color: #009950;
    }

    span:hover{
        color: #f1c40f;
    }

    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -10px;
        margin-left: -10px;
    }

    .categories-btn .col {
        margin-top: 10px;
    }

    .tt-megamenu-submenu li{
        list-style: none;
        font-size: 12px;
    }

    .categories-btn .tt-dropdown-menu ul {
        margin: 0;
        padding: 8px 0 7px;
    }

    .categories-btn .tt-dropdown-menu ul li {
        display: none;
    }

    .categories-btn .tt-dropdown-menu ul {
        display: block;
        list-style: none;
        position: relative;
    }

    .categories-btn .tt-dropdown-menu ul {
        color: #009950;
        font-size: 14px;
        line-height: 22px;
        display: inline-block;
        position: relative;
        padding-top: 1px;
        padding-right: 23px;
        padding-bottom: 1px;
        transition: all .2s linear;
        -ms-transition: all .2s linear;
        -webkit-transition: all .2s linear;
        -o-transition: all .2s linear;
    }

    .logo-sale{
        width: 100%;
        height: 100%;
    }

    .tt-description{
        position: absolute;
        margin-top: -100px;
        display: flex;
        width: 100%;
    }

    .tt-description-wrapper{
        position: absolute;
        font-family: Rubik,sans-serif;
        line-height: 26px;
        font-weight: 700;
    }

    .tt-title-small{
        color: #f1c40f;
        margin-left: 50px;
        font-size: 25px;
        cursor: pointer;
    }
    .tt-title-large{
        color: #fff;
        margin-left: 50px;
        font-size: 25px;
    }

    .tt-promo-02 {
        display: block;
        position: relative;
        overflow: hidden;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* sidebar */
    .sidebar .categori {
        width: 300px;
        height: 40px;
        color: #009950;
        font-size: 20px;
        text-align: center;
        font-weight: bold;
        background-color: #f1c40f;
        border-top: 1px solid #f1c40f;
        border-radius: 20px 20px 0px 0px;
    }

    .tt-sale {
        background-color: red;
        color: #fff;
        padding: 3px;
    }

    #myMenu {
        list-style-type: none;
        padding: 0;
        background-color: #fff9d1;
        width: 300px;
        border: 1px solid transparent;
        border-radius: 20px;
    }

    hr {
        color: #f8a5c2;
        margin: 0px;
    }

    #myMenu li a {
        padding: 12px;
        text-decoration: none;
        color: black;
        display: block;
        text-transform: capitalize;
        font-weight: bold;
        color: #009950;
    }

    #myMenu li:hover a {
        color: #fa9247;
    }

    /* searchBox */
    .carousel {
        margin-left: 50px;
    }

    #myInput {
        background-position: 10px 12px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
    }

    .searchbox {
        border: 1px solid #009950;
        border-radius: 15px;
        height: 50px;
        width: 100%;
        background-color: #7bed9f;
        margin-left: 80px;
    }

    .searchbox:hover {
        background-color: #fff;
    }

    .searchbox input {
        border: none;
        outline: none;
        padding: 10px;
        padding-left: 25px;
        background: transparent;
    }

    .searchbox .ri {
        padding-left: 20px;
        margin-top: 12px;
        right: 20px;
        font-size: 20px;
        color: #009950;
    }

    /* carosel */
    #myCarousel {
        margin-top: 20px;
        margin-left: 80px;
        height: 460px;
        width: 100%;
    }
    .carousel {
        border: 1px solid transparent;
        border-radius: 15px;
    }
    .carousel-inner {
        border-radius: 15px;
    }
    .carousel-inner .item img {
        height: 480px;
        border: 1px solid transparent;
        border-radius: 15px;
    }

    .carousel-inner .carousel-caption h2 {
        text-transform: uppercase;
        text-align: left;
        color: #fff;
        font-size: 40px;
        line-height: 48px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        margin-left: -100px;
        margin-top: -300px;
    }

    .carousel-inner .carousel-caption .special {
        color: #009950;
        text-align: justify;
    }

    .carousel-inner .carousel-caption .title1 {
        color: #ffe11b;
        margin-top: 5px;
        font-size: 30px;
        line-height: 35px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        margin-left: -420px;
    }

    .carousel-inner .carousel-caption .title2 {
        color: #fa9247;
        margin-top: 5px;
        font-size: 30px;
        line-height: 35px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        margin-left: -350px;
    }

    .carousel-inner .carousel-caption .title3 {
        color: #ffe11b;
        margin-top: 5px;
        font-size: 30px;
        line-height: 35px;
        font-weight: bold;
        font-family: Rubik, sans-serif;
        margin-left: -100px;
        text-align: justify;
    }

    .carousel-inner .carousel-caption p {
        text-align: left;
        margin-left: -100px;
    }

    /* bóng mờ không thể bỏ qua */
    .carousel-control {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 15%;
        font-size: 20px;
        color: #fff;
        text-align: center;
        text-shadow: 0 1px 2px rgb(0 0 0 / 60%);
        background-color: rgba(0, 0, 0, 0);
        filter: alpha(opacity=50);
        opacity: 0.5;
    }

    .carousel-control.left {
        background-image: none;
    }

    .carousel-control.right {
        background-image: none;
    }

    .btn-shop {
        padding: 10px 20px;
        background: rgb(255, 225, 27);
        border: 1px solid rgb(255, 225, 27);
        border-radius: 15px;
        color: #009950;
        font-size: 18px;
        font-weight: 500;
        margin-left: -580px;
    }
    .btn-shop:hover {
        background: #fa9247;
        color: #fff;
        transition: 0.2s;
    }

    .btn-shop.active {
        padding: 10px 20px;
        background: rgb(0, 153, 80);
        border: 1px solid transparent;
        border-radius: 15px;
        color: #fff;
        font-size: 18px;
        font-weight: 500;
        margin-left: -580px;
    }
    .btn-shop.active:hover {
        background: #fa9247;
        color: #fff;
        transition: 0.2s;
    }

    .carousel-control .glyphicon-chevron-left {
        left: 50%;
        opacity: 0;
    }

    .carousel-control .glyphicon-chevron-right {
        left: 50%;
        opacity: 0;
    }

    /* @media screen and (max-width: 1200px) {
        .container {
            width: 100%;
        }
    } */
`