import styled from 'styled-components'

export const Wrapper = styled.div`
    .pet{
        background-color: #EDF8F3;
        width: 100;

    }
    .petneed {
        height: 550px;
        font-family: Rubik, sans-serif;
        font-weight: bold;
        line-height: 17px;
        background-color: #EDF8F3;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        margin-top: 50px;
        margin-bottom: 50px;
        padding-top: 40px;
        padding-bottom: 40px;
    }

    .petneed .petneed-title {
        font-size: 30px;
        line-height: 36px;
        padding-bottom: 40px;
        color: #009950;
        font-weight: bold;
        margin-left: 190px;
    }

    .petneed .box {
        width: 100%;
        margin-bottom: 25px;
        margin-left: 180px;
        display: flex;
    }

    .petneed .container {
        margin-top: 50px;
    }

    .petneed .petneed-content {
        margin-left: 30px;
        margin-top: 5px;
    }

    .petneed .petneed-content h3 {
        display: block;
        position: relative;
        font-family: Rubik, sans-serif;
        font-size: 20px;
        font-weight: bold;
        color: #009950;
        margin-bottom: 0;
        padding-bottom: 10px;
    }
    .petneed .petneed-content p{
        color: #009950;
        font-weight: 400;
    }

    .petneed .lazyCat img{
        border-radius: 15px;
    }
`