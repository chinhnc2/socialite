import callApi from '../api'
export const INCREASE_QUANTITY = 'INCREASE_QUANTITY';
export const DECREASE_QUANTITY = 'DECREASE_QUANTITY';
export const GET_ALL_PRODUCT = 'GET_ALL_PRODUCT';
export const GET_NUMBER_CART = 'GET_NUMBER_CART';
export const ADD_CART = 'ADD_CART' ;
export const UPDATE_CART = 'UPDATE_CART';
export const DELETE_CART = 'DELETE_CART';
 
/*GET_ALL_PRODUCT*/
export function GetAllCart(payload){
    return {
        type:"GET_ALL_CART",
        payload
    }
}

// Close
export const actFetchCartRequest = (dispatch,idUser) => {
    return callApi(`cart?idUser=${idUser}`, 'GET', {idUser:idUser}).then(res => {
        dispatch(GetAllCart(res.data));
    });
}

// "Increse number cart API"
export  function postApiToIncresseNumber(payload){
    console.log(payload.id);
    callApi("cart/addcart","POST",{idProduct:payload.id})
    .then(res=>{
        console.log(res);
    })
    return {
        type:'ADD_CART',
        payload
    }
}
// Del product cart API
export function postApiToDel(payload){
    console.log(payload.id);
    callApi("cart/deccart","DELETE",{idProduct:payload.id})
    .then(res=>{
        console.log(res);
    })
    return {
        type:'DEL_QUANTITY',
        payload
    }
}

export function GetAllProduct(payload){
    return{
        type:'GET_ALL_PRODUCT',
        payload
    }
}
export const actFetchProductsRequest = (dispatch) => {
        return callApi('product', 'GET', null).then(res => {
            console.log(res.data)
            dispatch(GetAllProduct(res.data));
        });
}

 
/*GET NUMBER CART*/
export function GetNumberCart(){
    return{
        type:'GET_NUMBER_CART'
    }
}
 
// ADD TO CART
export function AddCart(payload){
    callApi("cart/add","POST",payload).then(res=>{
        console.log(res);
    })
    return {
        type:'ADD_CART',
        payload
    }
}


export function UpdateCart(payload){
    return {
        type:'UPDATE_CART',
        payload
    }
}
export function DeleteCart(payload){
    return{
        type:'DELETE_CART',
        payload
    }
}
 
export function IncreaseQuantity(payload){
    return{
        type:'INCREASE_QUANTITY',
        payload
    }
}

export function DecreaseQuantity(payload){
    return{
        type:'DECREASE_QUANTITY',
        payload
    }
}