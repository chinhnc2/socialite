import axios from 'axios';

let API_URL = 'http://localhost:5000';
export default function callApi(endpoint, method = 'GET', body) {
    return axios({
        method,
        headers:{'Content-Type': 'application/json'},
        url: `${API_URL}/${endpoint}`,
        data: JSON.stringify(body)
    }).catch(err => {
        console.log(err);
    });
}
