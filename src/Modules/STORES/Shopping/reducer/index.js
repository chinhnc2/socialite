import { combineReducers } from 'redux';

const initProduct = {
    numberCart:0,
    Carts:[],
    _products:[]
}

function todoProduct(state = initProduct,action){
    switch(action.type){
        case "GET_ALL_CART":
            var numberCart;
            if(action.payload.length===0){
                numberCart=0;
            }
            else{
                numberCart=action.payload.data.reduce((total,currentValue) => {
                        return total+currentValue.quantity
                },0);
            }
            return{
                ...state,
                numberCart:numberCart,
                Carts:action.payload.data,
            }
        case "GET_ALL_PRODUCT": 
            return{
                ...state,
                _products:action.payload
            }
        case "GET_NUMBER_CART":
                return{
                    ...state
                }
        case "ADD_CART":
            if(state.numberCart==0){
                let cart = {
                    id:action.payload.id,
                    quantity:1,
                    name:action.payload.name,
                    image:action.payload.image,
                    price:action.payload.price
                } 
                state.Carts.push(cart); 
            }
            else{
                let check = false;
                state.Carts.map((item,key)=>{
                    if(item.idProduct==action.payload.id || item.id==action.payload.id ){
                        state.Carts[key].quantity++;
                        check=true;
                    }
                });
                if(check==false){
                    let _cart = {
                        id:action.payload.id,
                        quantity:1,
                        name:action.payload.name,
                        image:action.payload.image,
                        price:action.payload.price
                    }
                    state.Carts.push(_cart);
                }
            }
            return{
                ...state,
                numberCart:state.numberCart+1
            }

            case "DEL_CART":
                if(state.numberCart==0){
                    let cart = {
                        id:action.payload.id,
                        quantity:1,
                        name:action.payload.name,
                        image:action.payload.image,
                        price:action.payload.price
                    } 
                    state.Carts.push(cart); 
                }
                else{
                    let check = false;
                    state.Carts.map((item,key)=>{
                        if(item.idProduct==action.payload.id || item.id==action.payload.id ){
                            state.Carts[key].quantity--;
                            check=true;
                        }
                    });
                    if(check==false){
                        let _cart = {
                            id:action.payload.id,
                            quantity:1,
                            name:action.payload.name,
                            image:action.payload.image,
                            price:action.payload.price
                        }
                        state.Carts.push(_cart);
                    }
                }
                return{
                    ...state,
                    numberCart:state.numberCart-1
                }

        case "INCREASE_QUANTITY":
                state.numberCart++
                state.Carts[action.payload].quantity++;
              
               return{
                   ...state
               }
        case "DECREASE_QUANTITY":
                let quantity = state.Carts[action.payload].quantity;
                if(quantity>1){
                    state.numberCart--;
                    state.Carts[action.payload].quantity--;
                }
              
                return{
                    ...state
        }
        case "DELETE_CART":
                let quantity_ = state.Carts[action.payload].quantity;
                return{
                    ...state,
                    numberCart:state.numberCart - quantity_,
                    Carts:state.Carts.filter(item=>{
                        return item.id!=state.Carts[action.payload].id
                    })
                   
        }
        default:
            return state;
    }
}
export default todoProduct;