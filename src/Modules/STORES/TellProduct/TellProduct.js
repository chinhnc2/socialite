import React from 'react'
import { Wrapper } from './styled-tellProduct'

export default function TellProduct() {
  return (
    <Wrapper className='tell'>
      <div className="TellProduct">
        <div className='tellprodct-title'>
          <h2 className='tt-title'>Tell Us What They Say About Your Products</h2>
        </div>
        <div className='tellproduct-content'>

          <div className="element">
            <div className='box'>
              <div class="basic-card basic-card-dark one">
                <div class="card-content">
                  <span class="card-title">
                    <svg width="118" height="22" viewBox='0 0 118 22' fill="none" className="activestarscolor">
                      <rect width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M11 1L13.3607 8.25735H21L14.8197 12.7426L17.1803 20L11 15.5147L4.81966 20L7.18034 12.7426L1 8.25735H8.63932L11 1Z" fill="white"></path>

                      <rect x="24" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M35 1L37.3607 8.25735H45L38.8197 12.7426L41.1803 20L35 15.5147L28.8197 20L31.1803 12.7426L25 8.25735H32.6393L35 1Z" fill="white"></path>

                      <rect x="48" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M59 1L61.3607 8.25735H69L62.8197 12.7426L65.1803 20L59 15.5147L52.8197 20L55.1803 12.7426L49 8.25735H56.6393L59 1Z" fill="white"></path>

                      <rect x="72" width="22" height="22" fill="#FFBA2E"></rect>
                      <rect x="72" width="11" height="22" fill="#FFBA2E"></rect>
                      <path d="M83 1L85.3607 8.25735H93L86.8197 12.7426L89.1803 20L83 15.5147L76.8197 20L79.1803 12.7426L73 8.25735H80.6393L83 1Z" fill="white"></path>

                      <rect x="96" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M107 1L109.361 8.25735H117L110.82 12.7426L113.18 20L107 15.5147L100.82 20L103.18 12.7426L97 8.25735H104.639L107 1Z" fill="white"></path>
                    </svg>
                    <br />
                    <span>for Customizability</span>
                  </span>
                  <p class="card-text">
                    This is my second Shopify Theme I have bought and I love it! Every step of the way that I have been confused support has been there with the answers with a few hours. The fact that they are doing videos tutorials for the sections of the set up is great. I am excited to launch my store lastclothingstore.com this weekend. Thanks!!!!
                  </p>
                </div>
                <div class="card-link">
                  <p href="#" title="Read Full"><span>Eworld007</span></p>
                  <p href="#" title="Read Full"><span>November 15, 2020</span></p>
                </div>
              </div>
            </div>

            <div className='box'>
              <div class="basic-card basic-card-dark">
                <div class="card-content">
                  <span class="card-title">
                    <svg width="118" height="22" viewBox='0 0 118 22' fill="none" className="activestarscolor">
                      <rect width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M11 1L13.3607 8.25735H21L14.8197 12.7426L17.1803 20L11 15.5147L4.81966 20L7.18034 12.7426L1 8.25735H8.63932L11 1Z" fill="white"></path>

                      <rect x="24" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M35 1L37.3607 8.25735H45L38.8197 12.7426L41.1803 20L35 15.5147L28.8197 20L31.1803 12.7426L25 8.25735H32.6393L35 1Z" fill="white"></path>

                      <rect x="48" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M59 1L61.3607 8.25735H69L62.8197 12.7426L65.1803 20L59 15.5147L52.8197 20L55.1803 12.7426L49 8.25735H56.6393L59 1Z" fill="white"></path>

                      <rect x="72" width="22" height="22" fill="#FFBA2E"></rect>
                      <rect x="72" width="11" height="22" fill="#FFBA2E"></rect>
                      <path d="M83 1L85.3607 8.25735H93L86.8197 12.7426L89.1803 20L83 15.5147L76.8197 20L79.1803 12.7426L73 8.25735H80.6393L83 1Z" fill="white"></path>

                      <rect x="96" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M107 1L109.361 8.25735H117L110.82 12.7426L113.18 20L107 15.5147L100.82 20L103.18 12.7426L97 8.25735H104.639L107 1Z" fill="white"></path>
                    </svg>
                    <br />
                    <span>for Design Quality</span>
                  </span>
                  <p class="card-text">
                    Great theme and very good customer service..
                  </p>
                </div>
                <div class="card-link">
                  <p href="#" title="Read Full"><span>Moaz_kilany</span></p>
                  <p href="#" title="Read Full"><span>October 16, 2020</span></p>
                </div>
              </div>
            </div>

            <div className='box'>
              <div class="basic-card basic-card-dark one">
                <div class="card-content">
                  <span class="card-title">
                    <svg width="118" height="22" viewBox='0 0 118 22' fill="none" className="activestarscolor">
                      <rect width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M11 1L13.3607 8.25735H21L14.8197 12.7426L17.1803 20L11 15.5147L4.81966 20L7.18034 12.7426L1 8.25735H8.63932L11 1Z" fill="white"></path>

                      <rect x="24" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M35 1L37.3607 8.25735H45L38.8197 12.7426L41.1803 20L35 15.5147L28.8197 20L31.1803 12.7426L25 8.25735H32.6393L35 1Z" fill="white"></path>

                      <rect x="48" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M59 1L61.3607 8.25735H69L62.8197 12.7426L65.1803 20L59 15.5147L52.8197 20L55.1803 12.7426L49 8.25735H56.6393L59 1Z" fill="white"></path>

                      <rect x="72" width="22" height="22" fill="#FFBA2E"></rect>
                      <rect x="72" width="11" height="22" fill="#FFBA2E"></rect>
                      <path d="M83 1L85.3607 8.25735H93L86.8197 12.7426L89.1803 20L83 15.5147L76.8197 20L79.1803 12.7426L73 8.25735H80.6393L83 1Z" fill="white"></path>

                      <rect x="96" width="22" height="22" fill="#FFBA2E"></rect>
                      <path d="M107 1L109.361 8.25735H117L110.82 12.7426L113.18 20L107 15.5147L100.82 20L103.18 12.7426L97 8.25735H104.639L107 1Z" fill="white"></path>
                    </svg>
                    <br />
                    <span>for Feature Availability</span>
                  </span>
                  <p class="card-text">
                    Amazing Job Guys!.. <br />
                    I bought this theme for my shop for baby, I am very satisfied with everything including a very good customer support. Good luck and good sales guys.
                  </p>
                </div>
                <div class="card-link">
                  <p href="#" title="Read Full"><span>Garcaspro</span></p>
                  <p href="#" title="Read Full"><span>September 27, 2020</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}
