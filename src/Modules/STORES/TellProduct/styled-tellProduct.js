import styled from 'styled-components'

export const Wrapper = styled.div`
    .TellProduct {
        background-color: #FFF9D1;
        height: 500px;
        width: 100%;
    }

    .tellprodct-title {
        text-align: center;
        padding-bottom: 40px;
    }

    .tt-title {
        font-size: 30px;
        line-height: 36px;
        font-weight: 700;
        color: #009950;
        letter-spacing: 0em;
        margin: 0;
        padding: 0;
        display: block;
        font-family: Rubik, sans-serif;
    }

    .element {
        display: flex;
        text-align: center;
        justify-content: center;
        color: #009950;
    }

    .element .box{
        margin: 20px;
    }

    .element .one{
        width: 400px;
    }

    .basic-card {
        width: 300px;
        position: relative;
        border-radius: 20px;
        -webkit-box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.3);
        -moz-box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.3);
        -o-box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.3);
        box-shadow: 0px 5px 5px 0px rgba(0, 0, 0, 0.3);
    }

    .basic-card .card-content {
        padding: 30px;
    }

    .basic-card .card-title {
        font-size: 25px;
        font-family: 'Open Sans', sans-serif;
    }

    .basic-card .card-text {
        line-height: 1.6;
    }

    .basic-card .card-link {
        padding: 25px;
        width: -webkit-fill-available;
    }

    .basic-card .card-link a {
        text-decoration: none;
        position: relative;
        padding: 10px 0px;
    }

    .basic-card .card-link p:after {
        top: 30px;
        content: "";
        display: block;
        height: 2px;
        left: 50%;
        width: 0;
        -webkit-transition: width 0.3s ease 0s, left 0.3s ease 0s;
        -moz-transition: width 0.3s ease 0s, left 0.3s ease 0s;
        -o-transition: width 0.3s ease 0s, left 0.3s ease 0s;
        transition: width 0.3s ease 0s, left 0.3s ease 0s;
    }
`