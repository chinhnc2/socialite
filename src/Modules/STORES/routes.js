import React from 'react'
import { Switch } from 'react-router-dom'
import Store from 'Layouts/Store/store'
import General from './General'
import Product from './Page/Product/Product'
import Contact from './Page/Contact/PageContact'
import Blog from './Page/Blog/Blog'
import Port from './Page/Portfolio/Portfolio'
import SignIn from './Page/SignIn/SignIn'
import Register from './Page/Register/Register'
import CreateStore from './CRUD-Store/CreateStore/AddStore'
import ListStore from './CRUD-Store/ListStore/ListStore'
import DetailStore from './CRUD-Store/DetailStore/DetailStore'
import Cart from './Cart/Cart'
import Detail from './Detail/Detail'
import { USER_ROLE } from 'Constants/auth'
import PrivateRoute from 'Components/route/privateRoute'

export const RoutesName = {
    STORE: '/stores',

    STORE_SHOP:'/stores/shop',
    STORE_DETAIL: '/stores/detail-product',
    STORE_CONTACT: '/stores/contact',
    STORE_BLOG: '/stores/blog',
    STORE_PORT: '/stores/port',

    STORE_SIGN: '/stores/sign',
    STORE_REGISTER: '/stores/register',

    STORE_PRODUCT: '/stores/detail',
    STORE_CART: '/stores/cart',

    STORE_CREATE: '/stores/create-store',
    STORE_LISTSTORE: '/stores/list-store',
    STORE_DETAILSTORE: '/stores/list-store/detail-store',

}

export const ROUTES = [
    {
        path: RoutesName.STORE,
        component: General,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_SHOP,
        component: Product, // các file component
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_CONTACT,
        component: Contact,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_BLOG,
        component: Blog,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_PORT,
        component: Port,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_SIGN,
        component: SignIn,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_REGISTER,
        component: Register,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_CREATE,
        component: CreateStore,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_LISTSTORE,
        component: ListStore,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_DETAILSTORE,
        component: DetailStore,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_CART,
        component: Cart,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.STORE_DETAIL,
        component: Detail,
        layout: Store,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    }
]


export default function  StoreRoutes() {

    return (
    <Switch>
        {ROUTES.map((routeConfig, index) => (
            <PrivateRoute key={index} exact {...routeConfig} />
        ))}
    </Switch>
    )
}
