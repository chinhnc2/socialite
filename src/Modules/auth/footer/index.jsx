import React from 'react'
import "./styled.scss"

const FooterLogin = () => {
    return (
      <footer className="footer-common">
        <ul className="footer__list">
            <li className="footer__item">
                <a href="#">ABOUT</a>
            </li>
            <li className="footer__item">
                <a href="#">HELP</a>
            </li>
            <li className="footer__item">
                <a href="#">TERM</a>
            </li>
            <li className="footer__item">
                <a href="#">PRIVACY</a>
            </li>
        </ul>
        <div className="footer_copy">
            <p>&copy; Copyright 2022 By BAP STUDIO</p>
        </div>
      </footer>
    )
  }
  
  export default FooterLogin