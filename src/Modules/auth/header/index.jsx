import React from 'react'
import "./styled.scss"
import logo from "../../../Assets/images/logo/SCEP_6.png"
import { Link } from 'react-router-dom'

const HeaderLogin = () => {
    return (
      <div className="header">
        <Link to="/" className="header__logo">
          <div className="top-bar-logo">
              <img src={logo} alt="" className="img-logo" />
          </div>
        </Link>
        <div className="button">
          <div className="button__login">
            <Link to="/auth/login">Login</Link>
          </div>
          <div className="button__register">
            <Link to="/auth/register">Register</Link>
          </div>
        </div>
      </div>

  )
}
  
  export default HeaderLogin
  