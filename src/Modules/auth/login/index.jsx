/* eslint-disable no-useless-escape */
/* eslint-disable jsx-a11y/alt-text */
import React, { useCallback, useEffect } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import HeaderLogin from '../header'
import FooterLogin from '../footer'
import { Checkbox } from 'antd'
import { FormInput } from 'Components'
import { Wrapper, Form, Content, LoginHelp, InputSubmit } from './styled'
import { useAuth } from 'Hooks';
import errorNotification from 'Components/toastNoti/error_noti'
import bg_img_1 from 'Assets/images/bg_img_1.png';
import './login.scss';

const LoginScreen = () => {
  const form = useForm({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const { handleSubmit, reset } = form;

  // useEffect(() => {
  //   register('termOfUse')
  //   setValue('termOfUse', false)
  // }, [])
  const { loginAction, error } = useAuth();
  const onSubmit = useCallback((data) => {
    loginAction(data);
    reset();
  }, []);

  const onChange = () => {};

  useEffect(() => {
    console.log('lỗi');
    //errorNotification({type: 'Error', content: error})
  }, [error])

  return (
    <Wrapper className='LoginScreen'>
      <div className="bg_img">
        <img src={bg_img_1} alt="background_img" />
      </div>
      <HeaderLogin />
      <FormProvider {...form}>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <h1 style={{fontWeight: 700, textShadow: '1px 2px #aaa', fontSize: 40, letterSpacing: 1.75}}>Login</h1>
          <p>Email or Username</p>
          <Content>
            <FormInput
              rules={{
                required: "Email is required",
                pattern: {
                  value:
                    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                  message: "This is not email",
                },
              }}
              name="email"
              type="text"
              placeholder="example@mydomain.com"
            />
            <FormInput
              rules={{ required: "Password is required" }}
              name="password"
              type="password"
              placeholder="***********"
            />
            <LoginHelp>
              <Checkbox onChange={onChange}>Remember Me</Checkbox>
              <a href="/register">Forgot Your Password?</a>
            </LoginHelp>
            <InputSubmit type="submit">Login</InputSubmit>
          </Content>
        </Form>
      </FormProvider>
      <FooterLogin />
    </Wrapper>
  );
};

export default LoginScreen;
