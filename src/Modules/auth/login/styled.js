import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background: rgba(250, 249, 247, 0.6);
  height: 100vh;
  overflow: hidden;
`

const Content = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
`

const Form = styled.form`
  margin: 0 auto;
  width: 45%;
  padding: 60px;

  @media (max-width: 1024px) {
    width: 60%;
    padding: 30px;
  }
  @media (max-width: 576px) {
    width: 80% !important;
    padding: 15px !important;
  }
`
const LoginHelp = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 18px;
  align-items: center;
  margin: 16px 0;
  
  @media (max-width: 576px) {
    flex-direction: column;
    justify-content: start;
  }
`

const InputSubmit = styled.button`
  background-image: linear-gradient(to bottom right, #ec4899, #f87171);
  border: none;
  outline: none;
  padding: 12px 0;
  width: 100%;
  border-radius: 8px;
  font-size: 1.25rem;
  font-weight: 600;
  color: #fff;
  margin-bottom: 16px;
`

const InputName = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 8px;
  > * {
    &:first-child {
       margin-right: 12px;
    }
  }
`

export {
  Wrapper,
  Form,
  Content,
  LoginHelp,
  InputSubmit,
  InputName,
}

