import React, { useCallback, useRef, useState } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import { Checkbox, Radio } from "antd"
import { FormInput  , FormDatePicker } from 'Components'
// import { yupResolver } from '@hookform/resolvers/yup';
// import * as Yup from 'yup';

import HeaderLogin from '../header'
import FooterLogin from '../footer'

import { Wrapper, Form, Content, LoginHelp, InputSubmit, InputName  } from '../login/styled'


import "./register.scss"
import { useAuth } from 'Hooks'

import bg_img from 'Assets/images/bg_img.png';


const RegisterScreen = () => {
  const form = useForm({
    defaultValues: {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      rePassword: '',
    }
  });
  const [gender, setGender] = useState(1);
  const { handleSubmit, watch, reset } = form
  
  const { registerAction } = useAuth()

  const passwordRef = useRef()
  passwordRef.current = watch("password", "");

  const onSubmit = useCallback((data) => {
    console.log(data);
    registerAction(data);
    
  }, [])

  const onChangeGender = e => {
    console.log('radio checked', e.target.value);
    setGender(e.target.value);
  };

  const onChange = () => {

  }
  
  // const test = ['bap', "fsort", "rikei", "smartdev"]
  
  return (
    <Wrapper className='RegisterScreen'>
      <div className="bg_img">
        <img src={bg_img} alt="background_img" />
      </div>
      <HeaderLogin />
      <FormProvider {...form}>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <h1 style={{fontSize: 40, fontWeight: 800, textShadow: '1px 2px #aaa', letterSpacing: 1.75}}>Register</h1>
          <p>Email or Username</p>
          <Content>
            <InputName>
              <FormInput
                rules={{ required: "First name is required"}}
                name="firstname"
                type="text"
                placeholder="First Name"
              />
              <FormInput
                rules={{ required: "Last name is required"}}
                name="lastname"
                type='text'
                placeholder="Last Name"
              />
            </InputName>
            <FormDatePicker
               name="dob"
               rules={{required : "Data of birth is required"}}
               
            />
            <FormInput
              rules={{ required: "Email is required", pattern: {
                value: /^(([^<>()[\]\\.,;:\s@\\"]+(\.[^<>()[\]\\.,;:\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\]\\.,;:\s@\\"]+\.)+[^<>()[\]\\.,;:\s@\\"]{2,})$/i,
                message: "This is not email"
              }}}
              name="email"
              type='text'
              placeholder="Email"
            />
            <FormInput
              rules={{ required: "Password is required", pattern: {
                value: /\d/ || /[a-zA-Z]/, 
                message: "Password must contain at least one letter and one number"
              }}}
              ref={passwordRef}
              name="password"
              type='password'
              placeholder="Password"
            />
            <FormInput
              rules={{ required: "Confirm Password is required", validate: value => value === passwordRef.current || "The password do not match"}}
              name="rePassword"
              type='password'
              placeholder="Confirm Password"
            />
            {/* <FormInput
              rules={{ required: "Your company is required"}}
              name="company"
              type='text'
              placeholder="Select your company"
              list="data1"
            /> */}
            
            <div className="sex">
              <Radio.Group onChange={onChangeGender} value={gender} style={{fontSize: '30px'}}>
                <Radio className="sex__radio" value={1}>Male</Radio>
                <Radio className="sex__radio" value={2}>Female</Radio>
                <Radio className="sex__radio" value={3}>Other</Radio>
              </Radio.Group>
            </div>
            <LoginHelp>
              <Checkbox 
                onChange={onChange}
                style={{fontSize: "18px"}}
              >
                I Agree
              </Checkbox>
              <a href="/">Terms and Conditions</a>
            </LoginHelp>
            <InputSubmit 
              type='submit'
            >Signin</InputSubmit>
          </Content>
        </Form>
      </FormProvider>
      <FooterLogin />
    </Wrapper>

  )
}

export default RegisterScreen
