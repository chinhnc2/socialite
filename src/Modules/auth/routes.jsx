import React, { Suspense } from 'react'
import { Switch } from 'react-router-dom'

import LoginScreen from './login';
import RegisterScreen from './register';

import BlankLayout from 'Layouts/blank'
import { Loading } from 'Components';
import PublicRoute from 'Components/route/publicRoute';

export const RoutesName = {
  AUTH_LOGIN: '/auth/login',
  AUTH_REGISTER: '/auth/register',
}

export const ROUTES = [
  {
    path: RoutesName.AUTH_LOGIN,
    component: LoginScreen,
    layout: BlankLayout
  },
  {
    path: RoutesName.AUTH_REGISTER,
    component: RegisterScreen,
    layout: BlankLayout
  },
]

export default function AuthRoutes() {
  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        {ROUTES.map((routeConfig, index) => (
          <PublicRoute key={index} exact {...routeConfig} />
        ))}
      </Switch>
    </Suspense>
  )
}
