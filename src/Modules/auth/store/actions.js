import { REQUEST } from 'Stores'
import {
    LOAD_PROFILE,
    LOGIN,
    REGISTER,
    LOGOUT,
    GET_MY_NOTI,
    GET_NEW_NOTI,
    CHECK_NOTI,
    QUERY_USER,
    REMOVE_NOTI
} from './constants'

export function loadProfile(payload) {
    return {
        type: REQUEST(LOAD_PROFILE),
        payload
    }
}

export function login(payload) {
    return {
        type: REQUEST(LOGIN),
        payload
    }
}
export function logout() {
    return{
        type : LOGOUT
    }
}

export function register(payload) {
    return {
        type: REQUEST(REGISTER),
        payload
    }
}

export function queryUsers(payload) {
    return {
        type: REQUEST(QUERY_USER),
        payload
    }
}

export function getMyNoti(payload) {
    return {
        type: GET_MY_NOTI,
        payload
    }
}

export function getNewNoti(payload) {
    return {
        type: REQUEST(GET_NEW_NOTI),
        payload
    }
}

export function remove_noti(payload) {
    return {
        type: REQUEST(REMOVE_NOTI),
        payload
    }
}


export function checkNoti(payload) {
    return {
        type: REQUEST(CHECK_NOTI),
        payload
    }
}
