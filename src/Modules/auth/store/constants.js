export const LOAD_PROFILE = 'LOAD_PROFILE';

export const LOGIN = 'LOGIN'; 

export const LOGOUT = 'LOGOUT'; 

export const REGISTER = 'REGISTER';

export const QUERY_USER = 'QUERY_USER';

export const GET_MY_NOTI = 'GET_MY_NOTI';
export const GET_NEW_NOTI = 'GET_NEW_NOTI';
export const SEND_NEW_NOTI = 'SEND_NEW_NOTI';
export const REMOVE_NOTI = 'REMOVE_NOTI';
export const CHECK_NOTI = 'CHECK_NOTI';