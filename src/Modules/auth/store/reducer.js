import { createReducer, updateObject, REQUEST, SUCCESS, FAILURE } from 'Stores'
import { LOAD_PROFILE, LOGIN, REGISTER, LOGOUT, GET_MY_NOTI, GET_NEW_NOTI, CHECK_NOTI, QUERY_USER, REMOVE_NOTI } from './constants'
import { setLocalStorage, STORAGE  , removeLocalStorage} from 'Utils';

export const initialState = {
  isLoading: false,
  error: null,
  authenticated: null,
  token: '',
  profile: {},
  newNoti: null,
  userQueries: []
}

// LOGIN
function loadingAuth(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function Authed(state, { payload }) {
  const { profile, token } = payload
  
  setLocalStorage(STORAGE.USER_TOKEN, token);
  setLocalStorage(STORAGE.META_DATA, JSON.stringify({userID: profile.id}));
  return updateObject(state, {
    isLoading: false,
    authenticated: true,
    token,
    //metaData,
    profile
  })
}

function AuthError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
    authenticated: false
  })
}

//LOGOUT
function Logout(state) {
  
  removeLocalStorage(STORAGE.USER_TOKEN );
  removeLocalStorage(STORAGE.META_DATA );
  

  return updateObject(state, {
        
    authenticated: false,
    token: '',
    profile: {},
    newNoti: null,
    userQueries: []

  })
}


// REGISTER
function loadingRegister(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function registed(state, { payload }) {
  
  const { profile, token } = payload
  
  setLocalStorage(STORAGE.USER_TOKEN, token);
  setLocalStorage(STORAGE.META_DATA, JSON.stringify({userID: profile.id})); 
  return updateObject(state, {
    isLoading: false,
    authenticated: true,
    token,
    //metaData,
    profile
  })
}

function registerError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
    authenticated: false
  })
}

//LOAD PROFILE


function loadProfile(state) {
  return updateObject(state, {
    isLoading: true
  })
}

function profileLoaded(state, { payload }) {
  const { profile, token } = payload
  return updateObject(state, {
    isLoading: false,
    authenticated: true,
    profile,
    token
  })
}

function profileLoadingError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
    authenticated: false
  })
}

//QUERY USER

function queryUser(state) {
  return updateObject(state, {
    //isLoading: true
  })
}

function queryUserDone(state, { payload }) {
  return updateObject(state, {
    isLoading: false,
    userQueries: payload
  })
}

function queryUserError(state, { error }) {
  return updateObject(state, {
    error,
    isLoading: false,
  })
}

// //////NOTI

// function gettingMyNotify(state, { payload }) {
//   return updateObject(state, {
//     isLoading: true
//   })
// };

// function gotMyNotify(state, { payload }) {
//   return updateObject(state, {
//     isLoading: false,
//     noti: payload
//   })
// }

// function getMyNotifyError(state, { error }) {
//   return updateObject(state, {
//     isLoading: false,
//     error
//   })
// }

//CREATE NOTI

function gettingNewNoti(state, { payload }) {
  console.log(payload);
  return updateObject(state, {
    newNoti: payload
  })
}

function gotNewNotify(state, { payload }) {
  return updateObject(state, {
    newNoti: null,
    profile: {...state.profile, noti: [...state.profile.noti, payload]}
  })
}

function getNewNotifyError(state, { error }) {
  return updateObject(state, {
    //
    newNoti: null
  })
}

//Remove Noti when user click their.

function removingNoti(state, { payload }) {
  console.log(payload);
  let index= state.profile.noti.findIndex(x => x == payload);
  console.log(index);
  return updateObject(state, {
    profile: {...state.profile, noti: [...state.profile.noti.slice(0, index), ...state.profile.noti.slice(index + 1)]}
  })
}

function removedNoti(state, { payload }) {
  return updateObject(state, {
  })
}

function removeErrNoti(state, { error }) {
  return updateObject(state, {
    
  })
}

function checkingNoti(state, { payload }) {
  let index= state.profile.noti.findIndex(x => x == payload);
  console.log(index);
  state.profile.noti[index] = {...state.profile.noti[index], seen: true};
  console.log(state.profile.noti[index]);
  return updateObject(state, {
    // profile: {...state.profile, noti: [...state.noti, state.noti[index].seen: true]}
  })
}

function checkedNoti(state, { payload }) {
  return updateObject(state, {
  })
}

function checkErrNoti(state, { error }) {
  return updateObject(state, {
    
  })
}

// Slice reducer
export default createReducer(initialState, { //Object với các key là các action type và value là hàm xử lý state của type đó.
  [REQUEST(LOGIN)]: loadingAuth,
  [SUCCESS(LOGIN)]: Authed,
  [FAILURE(LOGIN)]: AuthError,
  [REQUEST(LOAD_PROFILE)]: loadProfile,
  [SUCCESS(LOAD_PROFILE)]: profileLoaded,
  [FAILURE(LOAD_PROFILE)]: profileLoadingError,
  [REQUEST(REGISTER)]: loadingRegister,
  [SUCCESS(REGISTER)]: registed,
  [FAILURE(REGISTER)]: registerError,

  [LOGOUT] : Logout ,
  [REQUEST(QUERY_USER)]: queryUser,
  [SUCCESS(QUERY_USER)]: queryUserDone,
  [FAILURE(QUERY_USER)]: queryUserError,
  // [REQUEST(GET_MY_NOTI)] : gettingMyNotify,
  // [SUCCESS(GET_MY_NOTI)]: gotMyNotify,
  // [FAILURE(GET_MY_NOTI)]: getMyNotifyError,
  [REQUEST(GET_NEW_NOTI)] : gettingNewNoti,
  [SUCCESS(GET_NEW_NOTI)]: gotNewNotify,
  [FAILURE(GET_NEW_NOTI)]: getNewNotifyError,
  [REQUEST(REMOVE_NOTI)] : removingNoti,
  [SUCCESS(REMOVE_NOTI)]: removedNoti,
  [FAILURE(REMOVE_NOTI)]: removeErrNoti,
  [REQUEST(CHECK_NOTI)] : checkingNoti,
  [SUCCESS(CHECK_NOTI)]: checkedNoti,
  [FAILURE(CHECK_NOTI)]: checkErrNoti
})
