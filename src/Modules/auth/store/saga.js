import { put, takeLatest } from 'redux-saga/effects'

import { REQUEST, SUCCESS, FAILURE } from 'Stores'
import { getProfile } from 'APIs'
import { getLocalStorage, STORAGE } from 'Utils'
import { LOAD_PROFILE, LOGIN, REGISTER, GET_MY_NOTI, GET_NEW_NOTI, CHECK_NOTI, QUERY_USER, REMOVE_NOTI } from './constants'
import { postLogin, postRegister, searchUser } from 'APIs/auth.api'
import { getMyNotis, createNoti, checkedNoti, deleteNoti } from 'APIs/noti.api';
import { history } from 'Stores/reducers';
import successNotification from 'Components/toastNoti/success_noti'
import errorNotification from 'Components/toastNoti/error_noti'

export function* loadProfileSaga() {  
  try {
    const getMetaData = getLocalStorage(STORAGE.META_DATA);
    const metaData = JSON.parse(getMetaData)
    const data = yield getProfile({userId: metaData?.userID})
    yield put({
      type: SUCCESS(LOAD_PROFILE),
      payload: {
        profile: data
      }
    });
    
  } catch (error) {
    console.log(error);
    errorNotification({type: 'Load Profile Authenticated', content: error})
    yield put({
      type: FAILURE(LOAD_PROFILE),
      error
    })
    
  }
}

export function* loginSaga(action) {
    try {
        //const { data: result } = yield getProfile({ userId: metaData?.userId })
        const data = yield postLogin(action.payload.email, action.payload.password);
        yield put({
            type: SUCCESS(LOGIN),
            payload: {
                profile: data.user,
                token: data.token
                //metaData,
                //profile: result.data
            }
        })
        yield put({
            type: REQUEST(LOAD_PROFILE),
        })
        history.push("/")

    } catch (error) {
        errorNotification({type: 'Login', content: error})
        yield put({
            type: FAILURE(LOGIN),
            error
        })
    }
}


export function* registerSaga(action) {
  try {
    //const { data: result } = yield getProfile({ userId: metaData?.userId })
    const data = yield postRegister(action.payload.firstname + ' ' + action.payload.lastname, action.payload.email, action.payload.password, action.payload.gender);
    
    yield put({
      type: SUCCESS(REGISTER),
      payload: {
        profile: data.user,
        token: data.token
        //metaData,
        //profile: result.data
      }
    })
    successNotification({type: 'Register', content: 'Register successed!!!!'});
    yield put({
        type: REQUEST(LOAD_PROFILE),

    })
    history.push("/")
  } catch (error) {
    yield put({
      type: FAILURE(REGISTER),
      error
    })
    errorNotification({type: 'Register', content: error})
  }
}

function* queryUserSaga(action) {
    try {
        if (action.payload.queryString.length > 0) {
            const data = yield searchUser(action.payload.queryString);

            yield put({
                type: SUCCESS(QUERY_USER),
                payload: data
            })
        } else {
            yield put({
                type: SUCCESS(QUERY_USER),
                payload: []
            })
        }
        
    } catch (error) {
        errorNotification({type: 'Search User', content: error})
        yield put({
            type: FAILURE(QUERY_USER),
            error
        })
    }
}


function* getMyNotiSaga(action) {
    try {
        const data = yield getMyNotis(action.payload.userID)

        yield put({
            type: SUCCESS(GET_MY_NOTI),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'My Notifications', content: error})
        yield put({
            type: FAILURE(GET_MY_NOTI),
            error
        })
    }
}

function* createNotiSaga(action) {
    try {
        const data = yield createNoti(action.payload);
        yield put({
            type: SUCCESS(GET_NEW_NOTI),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Create Notification', content: error})
        yield put({
            type: FAILURE(GET_NEW_NOTI),
            error
        })
    }
}

function* removeNotiSaga(action) {
    try {
        const data = yield deleteNoti(action.payload.id || action.payload._id);

        yield put({
            type: SUCCESS(REMOVE_NOTI),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Remove Notification', content: error})
        yield put({
            type: FAILURE(REMOVE_NOTI),
            error
        })
    }
}

function* checkNotiSaga(action) {
    try {
        const data = yield checkedNoti(action.payload._id || action.payload.id);
        yield put({
            type: SUCCESS(CHECK_NOTI),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Check Notification', content: error})
        yield put({
            type: FAILURE(CHECK_NOTI),
            error
        })
    }
}

// export default function* authSaga() {
//   yield takeLatest(REQUEST(LOAD_PROFILE), loadProfileSaga)
// }

export default function* authSaga() {
    yield takeLatest(REQUEST(LOGIN), loginSaga);
    yield takeLatest(REQUEST(LOAD_PROFILE), loadProfileSaga);
    yield takeLatest(REQUEST(REGISTER), registerSaga);

    yield takeLatest(REQUEST(QUERY_USER), queryUserSaga)

    yield takeLatest(REQUEST(GET_MY_NOTI), getMyNotiSaga);
    yield takeLatest(REQUEST(GET_NEW_NOTI), createNotiSaga);
    yield takeLatest(REQUEST(CHECK_NOTI), checkNotiSaga);
    yield takeLatest(REQUEST(REMOVE_NOTI), removeNotiSaga);
}

//   try {

// function* fetchUser(action) {
//      const user = yield call(Api.fetchUser, action.payload.userId);
//      yield put({type: "USER_FETCH_SUCCEEDED", user: user});
//   } catch (e) {
//      yield put({type: "USER_FETCH_FAILED", message: e.message});
//   }
// }

// /*
//  Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
//  Allows concurrent fetches of user.
// */
// function* mySaga() {
//  yield takeEvery("USER_FETCH_REQUESTED", fetchUser);
// }