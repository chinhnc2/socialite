import LineChart from 'Components/chart/line_chart';
import { useAuth, useCompany } from 'Hooks';
import { useForm, Controller } from "react-hook-form";
import uploadCloudinary from 'Utils/cloudinary';
import React, { useEffect, useState } from 'react';
import { history } from 'Stores/reducers';
import './manage_company.scss';
import dog from 'Assets/images/dog.png';
import ModalCustom from 'Components/modal_custom';

const ManageCompanyScreen = () => {

    const form = useForm();
    const { handleSubmit, reset, watch, register, control } = useForm();

    const { profile } = useAuth();
    const { company, allCICO, getAllCICOAction, allowJoininCompanyAction, updateAvatarCompanyAction } = useCompany();

    const [adminAvail, setAdminAvail] = useState([]);
    const [managerAvail, setManagerAvail] = useState([]);
    const [memberAvail, setMemberAvail] = useState([]);
    const [requestAvail, setRequestAvail] = useState([]);
    const [compiledAllCICO, setCompiledAllCICO] = useState([]);
    const [imgURL, setImgURL] = useState(null);
    const [check, setCheck] = useState([]);

    const handleChangeFile = (e) => {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function (e) {
            setImgURL(reader.result);
        };
    }

    const onSubmitUpload = async data => {
        let resURL = await uploadCloudinary(imgURL);
        console.log(resURL);
        if (resURL) {
            if (company) {
                updateAvatarCompanyAction({avatar: resURL, companyID: (company._id || company.id)})
            }
            setImgURL(null);
        }
        reset();
    }


    const totalCICO = (arr) => {
        let sum = 0
        arr.forEach((x) => {
          sum += (new Date(x.updatedAt).getTime() - new Date(x.createdAt).getTime()) / 3600000
        })
        return sum
      }


    useEffect(() => {
        if (profile.company && company) {
            if (company.managers && company.managers.length > 0 && company.managers.findIndex(x => x._id === profile._id)) {
                history.replace('/');
            }
        }
    }, [profile, company, window.location.pathname])

    useEffect(() => {
        if (company) {
            getAllCICOAction(company._id);
            setAdminAvail(company.admin ? [...company.admin.slice(0, 3)] : []);
            setManagerAvail(company.managers ? [...company.managers.slice(0, 3)] : [])
            setMemberAvail(company.members ? [...company.members.slice(0,5)] : [])
            setRequestAvail(company.request ? [...company.request.slice(0,3)] : [])
        }
        
    }, [company]);
    

    useEffect(() => {
        if (allCICO && allCICO.length > 0) {
          let temp = [...allCICO.map((x) => ({ ...x, total: totalCICO(x.cico) }))]
          setCompiledAllCICO(temp)
        }
      }, [allCICO])
    return(
        <div className="ManageCompanyScreen">
            {company && <div className='container_manage'>
                <div className="header_company">
                    <img src={imgURL ? imgURL : company.avatar} alt="avtar" />
                    <div className="content_header_company">
                        <h3>{company.name}</h3>
                        <p>{company.address}</p>
                        <p>{company.email}</p>
                        <p>{company.phone}</p>
                        <form onSubmit={handleSubmit(onSubmitUpload)}>
                            <div className="btn_upload">
                                <Controller
                                    name="media"
                                    control={control}
                                    render={({ field }) => {
                                        // sending integer instead of string.
                                        return <input type='file'  name='media'
                                        {...field} onChange={(e) => handleChangeFile(e)} />;
                                    }}
                                />
                                {!imgURL && <button>Update avatar for company</button> }
                                
                            </div>
                            {imgURL && <button className='submit_upload' type='submit'>save</button>}
                        </form>
                    </div>
                </div>
            
                
                <div className="main_company">
                    <div className="list_admin list_item">
                        <div>
                            <h5>Admin in company:</h5>
                        </div>
                        {adminAvail.length > 0 && adminAvail.map(x => <div className='item_admin item_data'>
                            <img src={dog} alt="avt" />
                            <p>{x.name}</p>
                        </div>)}
                        {company.admin && company.admin.length > 3 && <button onClick={() => setCheck(company.admin)}>more</button>}
                    </div>
                    <div className="list_manager list_item">
                        <div>
                            <h5>Managers in company:</h5>
                        </div>
                        {managerAvail.length > 0 && managerAvail.map(x =>  <div className='item_manager item_data'>
                            <img src={x.avatar} alt="avatar" />
                            <p>{x.name}</p>
                        </div>)}
                        {company.manager && company.manager.length > 3 && <button onClick={() => setCheck(company.managers)}>more</button>}    
                    </div>
                    <div className="list_member list_item">
                        <div>
                            <h5>Members in company:</h5>
                        </div>
                        {memberAvail.length > 0 && memberAvail.map(x =>  <div className='item_member item_data'>
                            <img src={x.avatar} alt="avatar" />
                            <p>{x.name}</p>
                        </div>)}
                        {company.members && company.members.length > 3 && <button onClick={() => setCheck(company.members)}>more</button>}    
                    </div>
                    <div className="list_request list_item">
                        <div>
                            <h5>Request join in company:</h5>
                        </div>
                        {requestAvail.length > 0 && requestAvail.map(x =>  <div className='item_request item_data' onClick={() => allowJoininCompanyAction({companyID: company._id, userID: profile._id})}>
                            <img src={x.avatar} alt="avatar" />
                            <p>{x.name}</p>   
                        </div>)}
                        {company.request && company.request.length > 0 && <button onClick={() => setCheck(company.request)}>more</button>}
                    </div>
                </div>
                <div className="all_cico">
                    <div className="chart_data">
                        <LineChart labels={compiledAllCICO.length > 0 && [...compiledAllCICO.map((x) => x.user && x.user.name).slice(0, 10)]}
                            data={compiledAllCICO.length > 0 && [...compiledAllCICO.map((x) => x.total).slice(0, 10)]}
                            title="Chart about top user hardest"
                        />
                    </div>
                    <div className="list_table">
                        <div className="title_table_data">
                            <h5>Top 10 staff hardest:</h5>
                        </div>
                        {compiledAllCICO.length > 0 && [...compiledAllCICO.sort((a, b) => b.total - a.total)].map((x) => (
                        <div className="each_cico">
                            <div className="main_content">
                                <p>{[...compiledAllCICO.sort((a, b) => b.total - a.total)].indexOf(x) + 1}</p>
                                <p className='name_each'>{x.user && x.user.name}</p>
                                <p className="total_time">{x.total.toFixed(2)} hours</p>
                                <p className="item_move">Tháng {x.cico && x.cico.length > 0 && new Date(x.cico[0].createdAt).getMonth() + 1}</p>
                            </div>

                        </div>
                        ))}
                    </div>
                    
                </div>
            </div>}
            {check.length > 0 && <ModalCustom func={() => setCheck('')}>
                <div className="list_data" style={{display: 'flex', flexDirection: 'column', maxHeight: 500, overflowX: 'hidden', width: 500, overflowY: 'scroll'}}>
                    {check.map(x => <div style={{display: 'flex', flexDirection: 'row', margin: 10}}>
                        <img src={x.avatar || dog} alt="avt" style={{width: 30, height: 30, objectFit: 'cover', overflow: 'hidden', borderRadius: 100000}} />
                        <p>{x.name}</p>
                    </div>)}
                </div>
            </ModalCustom> }
        </div>
    )
}

export default ManageCompanyScreen;