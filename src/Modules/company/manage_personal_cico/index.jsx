import LineChart from 'Components/chart/line_chart';
import { useAuth, useCompany } from 'Hooks';
import React, { useEffect, useState } from 'react';
import { history } from 'Stores/reducers';

import './manage_personal_cico.scss';

const ManagePersonalCICO = () => {
    
    const [status, setStatus] = useState(-1) // -1: chưa hiện hộp thoại, 0: chưa checkin + checkout, 1: đã checkin + chưa checkout, 2: đã checkin + checkout
    const [sortedCICO, setSortedCICO] = useState([]);


    const { profile } = useAuth();
    const { company, cico, getCICOAction, checkInAction, checkOutAction }  = useCompany();


    useEffect(() => {
        if (window.location.pathname.split('/')[3] != (profile.id || profile._id)) {
            history.replace('/')
        }
    }, []);

    useEffect(() => {
        if (profile.company) {
            getCICOAction(profile.id || profile._id);
        }
    }, [profile]);

    useEffect(() => {
        if (cico.length > 0) {
            let check = ((new Date(Date.now())).getDate() - (new Date(cico[0].createdAt)).getDate() >= 1)? 0
            : !cico[0].checkOut ? 1 : 2;
            setStatus(check);
        } else if (profile.company) {
            setStatus(0);
        }
    }, [cico]);

    useEffect(() => {
        if (cico.length> 0 ) {
            setSortedCICO(cico.filter(x => new Date(x.createdAt).getMonth() == new Date(Date.now()).getMonth()));
        }
    }, [cico])
    useEffect(() => {
        if (cico.length > 0) {
            console.log(new Date(cico[0].createdAt).getTime());
            console.log(Date.now());
        }
    }, [cico])
    return(
        <div className="ManagePersonalCICO">
            <div className="title_company">
                <p>{company && company.name}</p>
            </div>
            {profile.company && <div className="head">
                <div className="button_cico" onClick={() => status == 0 ? checkInAction(profile.id || profile._id) 
                : status == 1 ? checkOutAction({userID: profile.id || profile._id, CIID: cico[0]._id }) : () => console.log('aaaa') }>
                    <p>{status === 0 ? 'Check In' : status === 1 ? 'Check Out' : 'You checked in & checked out'}</p>
                    {(status >= 0 && status != 2) && <i class='bx bx-fingerprint'></i>}

                </div>
                {status >= 0 && <h2>{status == 0 ? '0%' 
                : (status == 1 ? 
                (
                cico[0].createdAt ? Math.floor(((Date.now() - (new Date(cico[0].createdAt)).getTime())/32400000)*100) + '%' : '1%'
                )
                : (Math.floor((((new Date(cico[0].updatedAt)).getTime()) - ((new Date(cico[0].createdAt)).getTime()))/32400000)*100 + '%')) 
                }</h2>}
                <p className='text_cico'>{status == 0 ? 
                'Hôm nay bạn vẫn chưa check in' 
                : status == 1 ? 
                <>
                {'Đã check in lúc ' + (cico[0].createdAt ? 
                new Date(cico[0].createdAt).toLocaleString('en-US', { timeZone: 'Asia/Jakarta' }) : 'vừa mới') }
                </>
                : 'Đã check in & check out'}</p>
            </div>}
            <div className="chart_cico">
                <h3>Check In & Check Out Statistic:</h3>
                    {sortedCICO.length > 0 && <LineChart labels={[...sortedCICO].map(x => new Date(x.createdAt).getDate()).reverse()} 
                    data={[...sortedCICO].map(x => (new Date(x.updatedAt).getTime() - new Date(x.createdAt).getTime())/3600000).reverse()}
                    title='Biểu độ thể hiện thời gian check in và checkout mỗi ngày'
                    />}
            </div>
            <div className="statistic_data">
                <h3>Thống kê CICO tháng {sortedCICO.length > 0 && new Date(sortedCICO[0].createdAt).getMonth() + 1}</h3>
                <div className="all_cico item_data">Tổng số ngày đã checkin: {sortedCICO.length}</div>
                <div className="late_cico item_data">
                    <p>Những ngày Checkin trễ:</p>
                    {sortedCICO.filter(x => new Date(x.createdAt).toLocaleString('en-US', { timeZone: 'Asia/Jakarta' }))
                .map(element => <p>{new Date(element.createdAt).getDate()}</p>)
                }</div>
                <div className="shortage_cico item_data">
                    <p>Những ngày quên checkout: </p>
                    {sortedCICO.filter(x => !x.checkOut).map(element => <p>{new Date(element.createdAt).getDate()}</p>)}</div>
            </div>
        </div>
    )
}

export default ManagePersonalCICO;