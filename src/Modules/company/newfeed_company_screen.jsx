import React, { useEffect, useState } from 'react';
import { useHome, useAuth, usePost, useCompany } from 'Hooks';
import {
    CardPost,
    PostCreate,
    EnterPost,
  } from "Components";

  import styled from 'styled-components';
  import { Link } from 'react-router-dom';
  import frame4 from 'Assets/images/frame4.png';
  import history from 'Stores/reducers'
import './newfeed.scss';

const ContentCenter = styled.div`
    position: relative;
    width: 63%;
    top: 0;
    overflow: scroll;
    left: 0;
    padding-right: 25px;
    padding-bottom: 100px;
    //   background-color: bisque;
    height: fit-content;
  `;

  const NewfeedCompanyScreenDiv = styled.div`
    width: 75%;
    padding: 0px 50px;
    top: 25px;
    display: flex;
    overflow: scroll;
    border-radius: 14px;
    position: absolute;
    left: 20%;
    //   background-color: #00ffff36;
    height: 100vh;
  `
  const RightSideBar = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    position: relative;
    overflow: hidden;
    width: 35%;
  `

  const ImgAvtCompany = styled.img`
    width: 60%;
    border: 1px solid #aaa;
    border-radius: 100000px;
    object-fit: cover;
    overflow: hidden;
    margin: 15px 0;
  `
  const ContainerCompany = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    oveflow: hidden;
    position: relative;
    margin: 15px 0;
    
  `

  const NavigateCICO = styled.div`
    color: #5687a6;
    font-size: 18px;
    font-weight: 500;
  `
  
  const MemberInCompanyContainer = styled.div`
    display: flex;
    width: 100%;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    position: relative;
  `
  const ImgAvtMemberContainer = styled.div`
    cursor: pointer;
    width: 30px;
    height: 30px;
    object-fit: cover;
    overflow: hidden;
    border: 1px solid #aaa;
    border-radius: 10000px;
    margin: 0 5px;
  `
  const ImgAvtMember = styled.img`
    object-fit: cover;
    width: 100%;
    height: 100%;
  `
  const MoreMemberContainer = styled.div`
    cursor: pointer;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 1000px;
    background-color: rgba(120, 120, 120, 0.7);
    color: white;
    font-size: 25px;
    overflow: hidden;
  `


const NewfeedCompanyScreen = () => {

    const { posts } = useHome();
    const { getPostInCompanyAction } = usePost();
    const { profile } = useAuth();
    const { company } = useCompany();

    const [showModal, setShowModal] = useState(false);
    const [showDropdown, setShowDropdown] = useState(false);

    const handleShowHideModalEnterPost = () => {
        setShowModal(!showModal);
    };
    const handleShowHideDropdown = () => {
        setShowDropdown(!showDropdown);
    };
    const handleHideDropdown = () => {
        setShowDropdown(false);
    };

    const handleNavigateProfileMember = (x) => {
        history.replace(`/u/${x}`)
    }

    useEffect(() => {
        getPostInCompanyAction({amount: 10, begin: 0, companyID: profile.company._id});
    }, [])

    return(
            <NewfeedCompanyScreenDiv className="HomeScreen">
                <ContentCenter className="ContentCenter-Company">
                    {showModal && (
                    <EnterPost type='company' 
                    handleShowHideModalEnterPost={handleShowHideModalEnterPost}
                    />
                    )}
                    <PostCreate type='company'
                    handleShowHideModalEnterPost={handleShowHideModalEnterPost}
                    />
                    {posts && posts.length > 0 && posts.map(post => <CardPost key={post.id || post._id} x={post} /> )}
                </ContentCenter>
                <RightSideBar className="right_sideBar">
                    
                    <NavigateCICO className="cico_navigate">
                        <Link to={`/c/cico/${profile.id || profile._id}`} >My Check In & Check Out</Link>
                    </NavigateCICO>
                    <ContainerCompany className="my_company">
                        <Link to={`/c/manage/${company && company._id}`} style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}} >
                          <ImgAvtCompany src={company && company.avatar || frame4} alt="avt_company" />
                          <h5 style={{color: '#77e916', fontWeight: '600', fontSize: 20, letterSpacing: 1.5, textTransform: 'uppercase'}}>{company && company.name}</h5>
                        </Link>

                    </ContainerCompany>
                    <p style={{fontSize: 13, color: '#aaaa'}}>Danh sách thành viên:</p>
                    <MemberInCompanyContainer className="member_in_company">
                        
                        {company && company.members.length > 0 && [...company.members.slice(0, 7)].map(x => 
                        <Link to={`/u/${x._id}`}>
                            <ImgAvtMemberContainer className="each_member" >
                            <ImgAvtMember src={x.avatar} alt="avt_member" />
                        </ImgAvtMemberContainer>
                        </Link>)}
                        <MoreMemberContainer className="more_meber"><p>+</p></MoreMemberContainer>
                    </MemberInCompanyContainer>
                </RightSideBar>
            </NewfeedCompanyScreenDiv>
    )
}

export default NewfeedCompanyScreen 