import React, { Suspense, useEffect } from 'react'
import { Switch } from 'react-router-dom'

import PrivateRoute from 'Components/route/privateRoute'

import { USER_ROLE } from 'Constants/auth'
import HomeScreen from 'Modules/home';

import BlankLayout from 'Layouts/blank'
import HomeLayout from 'Layouts/home';
import { Loading } from 'Components';
import UnjoinedCompanyScreen from './unjoined_company_screen';
import NewfeedCompanyScreen from './newfeed_company_screen';
import ManagePersonalCICO from './manage_personal_cico';
import { useCompany, useAuth } from 'Hooks';
import { history } from 'Stores/reducers';
import ManageCompanyScreen from './manage_company';

export const RoutesName = {
    COMPANY: '/c/source',
    MY_COMPANY: '/c/nf/:id',
    ABOUT_COMPANY: '/c/about/:id',
    MY_CICO: '/c/cico/:id',
    MANAGE_COMPANY: '/c/manage/:id'
}

export const ROUTES = [
  {
    path: RoutesName.COMPANY,
    component: UnjoinedCompanyScreen,
    layout: HomeLayout,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.MY_COMPANY,
    component: NewfeedCompanyScreen,
    layout: HomeLayout,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.ABOUT_COMPANY,
    component: HomeScreen,
    layout: BlankLayout,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.MY_CICO,
    component: ManagePersonalCICO,
    layout: HomeLayout,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
  {
    path: RoutesName.MANAGE_COMPANY,
    component: ManageCompanyScreen,
    layout: HomeLayout,
    rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
  },
]

export default function CompanyRoutes() {

  const { getCompanyAction } = useCompany();
  const { profile } = useAuth();

  useEffect(() => {
    console.log(profile);
      if (profile && profile.company?._id) {
        getCompanyAction(profile.company._id);
      }
    
  }, [profile])

  useEffect(() => {
    if (window.location.pathname.split('/')[2] == 'source' && profile?.company) {
      history.replace(`/c/${profile.company._id}`);
    }
  }, [profile])


  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        {ROUTES.map((routeConfig, index) => (
          <PrivateRoute key={index} exact {...routeConfig} />
        ))}
      </Switch>
    </Suspense>
  )
}
