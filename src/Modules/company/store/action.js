import { REQUEST } from 'Stores';
import { GET_ALL_COMPANY, GET_COMPANY, REQUEST_TO_COMPANY, QUERY_COMPANY, LEAVE_COMPANY, CHECK_IN_COMPANY, CHECK_OUT_COMPANY, GET_CICO, GET_ALL_CICO, ALLOW_JOININ_COMPANY, UPDATE_AVATAR_COMPANY } from './constants';


export function getAllCompany(payload) {
    return {
        type: REQUEST(GET_ALL_COMPANY),
        payload
    }
}

export function searchCompany(payload) {
    return {
        type: REQUEST(QUERY_COMPANY),
        payload
    }
}

export function getCompany(payload) {
    return {
        type: REQUEST(GET_COMPANY),
        payload
    }
}

export function requestToCompany(payload) {
    return {
        type: REQUEST(REQUEST_TO_COMPANY),
        payload
    }
}

export function leaveCompany(payload) {
    return {
        type: REQUEST(LEAVE_COMPANY),
        payload
    }
}

export function checkinCompany(payload) {
    return {
        type: REQUEST(CHECK_IN_COMPANY),
        payload
    }
}

export function checkOutCompany(payload) {
    return {
        type: REQUEST(CHECK_OUT_COMPANY),
        payload
    }
}

export function getCICO(payload) {
    return {
        type: REQUEST(GET_CICO),
        payload
    }
}

export function getAllCICO(payload) {
    return {
        type: REQUEST(GET_ALL_CICO),
        payload
    }
}

export function allowJoinInCompany(payload) {
    return {
        type: REQUEST(ALLOW_JOININ_COMPANY),
        payload
    }
}

export function updateAvatarCompany(payload) {
    return {
        type: REQUEST(UPDATE_AVATAR_COMPANY),
        payload
    }
}