import { FAILURE, REQUEST, SUCCESS, updateObject, createReducer } from "Stores"
import { GET_ALL_COMPANY, GET_COMPANY, REQUEST_TO_COMPANY, QUERY_COMPANY, LEAVE_COMPANY, GET_CICO, CHECK_IN_COMPANY, CHECK_OUT_COMPANY, GET_ALL_CICO, ALLOW_JOININ_COMPANY, UPDATE_AVATAR_COMPANY } from './constants';

export const initialState = {
    allCompany: [],
    company: null,
    isLoading: false,
    error: null,
    queryCompany: [],
    posts: [],
    cico: [],
    allCICO: [],
}

function gettingAllCompany(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function gotAllCompany(state, { payload }) {
    console.log(payload);
    return updateObject(state, {
        isLoading: false,
        allCompany: payload
    })
}

function getAllCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function gettingCompany(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function gotCompany(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        company: payload
    })
}

function getCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function loadQueryCompany(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function queryCompanySuccess(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        queryCompany: payload
    })
}

function queryCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function loadRequestToCompany(state) {
    return updateObject(state, {
        isLoading: true,
    })
}

function requestToCompanySuccess(state) {
    return updateObject(state, {
        isLoading: false,
    })
}

function getCICOloading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function getCICOSuccess(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        cico: payload
    })
}

function getCICOError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function checkInLoading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function checkInSuccess(state, { payload }) {
    console.log(payload);
    return updateObject(state, {
        isLoading:false,
        cico: [payload, ...state.cico]
    })
}

function checkInEror(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function checkOutLoading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function checkOutError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function getAllCICOLoading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function getAllCICOSuccess(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        allCICO: payload
    })
}

function getAllCICOError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function allowJoininCompanyLoading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function allowJoininCompanySuccess(state) {
    return updateObject(state, {
        isLoading: false
    })
}

function allowJoinInCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function updateAvatarCompanyLoading(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function updateAvatarCompanySuccess(state) {
    return updateObject(state, {
        isLoading: false
    })
}

function updateAvatarCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

// function loadLeaveCompany(state) {
//     return updateObject(state, {
//         isLoading: true
//     })
// }

export default createReducer(initialState, {
    [REQUEST(GET_ALL_COMPANY)]: gettingAllCompany,
    [SUCCESS(GET_ALL_COMPANY)]: gotAllCompany,
    [FAILURE(GET_ALL_COMPANY)]: getAllCompanyError,

    [REQUEST(GET_COMPANY)]: gettingCompany,
    [SUCCESS(GET_COMPANY)]: gotCompany,
    [FAILURE(GET_COMPANY)]: getCompanyError,

    [REQUEST(QUERY_COMPANY)]: loadQueryCompany,
    [SUCCESS(QUERY_COMPANY)]: queryCompanySuccess,
    [FAILURE(QUERY_COMPANY)]: queryCompanyError,

    [REQUEST(REQUEST_TO_COMPANY)]: loadRequestToCompany,
    [SUCCESS(REQUEST_TO_COMPANY)]: requestToCompanySuccess,

    [REQUEST(GET_CICO)]: getCICOloading,
    [SUCCESS(GET_CICO)]: getCICOSuccess,
    [FAILURE(GET_CICO)]: getCICOError,

    [REQUEST(CHECK_IN_COMPANY)]: checkInLoading,
    [SUCCESS(CHECK_IN_COMPANY)]: checkInSuccess,
    [FAILURE(CHECK_IN_COMPANY)]: checkInEror,

    [REQUEST(CHECK_OUT_COMPANY)]: checkOutLoading,
    [FAILURE(CHECK_OUT_COMPANY)]: checkOutError,

    [REQUEST(GET_ALL_CICO)]: getAllCICOLoading,
    [SUCCESS(GET_ALL_CICO)]: getAllCICOSuccess,
    [FAILURE(GET_ALL_CICO)]: getAllCICOError,

    [REQUEST(ALLOW_JOININ_COMPANY)]: allowJoininCompanyLoading,
    [SUCCESS(ALLOW_JOININ_COMPANY)]: allowJoininCompanySuccess,
    [FAILURE(ALLOW_JOININ_COMPANY)]: allowJoinInCompanyError,

    [REQUEST(UPDATE_AVATAR_COMPANY)]: updateAvatarCompanyLoading,
    [SUCCESS(UPDATE_AVATAR_COMPANY)]: updateAvatarCompanySuccess,
    [FAILURE(UPDATE_AVATAR_COMPANY)]: updateAvatarCompanyError
})
