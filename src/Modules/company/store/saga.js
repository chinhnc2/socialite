import { getAllCompany, getCompany, searchCompany, joinInCompany, getCICOByUser, checkInApi, checkOutApi, updateAvatarCompany, getAllCICO, allowJoinInCompany } from "APIs/company.api";
import { put, takeLatest } from "redux-saga/effects";
import { SUCCESS, FAILURE,REQUEST } from "Stores";
import { GET_ALL_COMPANY, GET_COMPANY, QUERY_COMPANY, REQUEST_TO_COMPANY, CHECK_IN_COMPANY, CHECK_OUT_COMPANY, GET_CICO, UPDATE_AVATAR_COMPANY, GET_ALL_CICO, ALLOW_JOININ_COMPANY } from "./constants";
import errorNotification from "Components/toastNoti/error_noti";

function* getAllCompanySaga() {
    try {
        const data = yield getAllCompany();
        yield put({
            type: SUCCESS(GET_ALL_COMPANY),
            payload: data   
        })
    } catch (error) {
        errorNotification({type: 'Get Companies', content: error})
        yield put({
            type: FAILURE(GET_ALL_COMPANY),
            error
        })
    }
}

function* getCompanySaga(action) {
    try {
        const data = yield getCompany(action.payload);

        yield put({
            type: SUCCESS(GET_COMPANY),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Get Company', content: error})
        yield put({
            type: FAILURE(GET_COMPANY),
            error
        })
    }
}

function* queryCompanySaga(action) {
    try {
        const data = yield searchCompany(action.payload.query_string);

        yield put({
            type: SUCCESS(QUERY_COMPANY),
            payload: data
        })
    } catch (error) {
        yield put({
            type: FAILURE(QUERY_COMPANY),
            error
        })
    }
}

function* joinInCompanySaga(action) {
    try {
        const data = yield joinInCompany(action.payload.companyID, action.payload.userID);

        yield put({
            type: SUCCESS(REQUEST_TO_COMPANY),
            payload: data
        })
        yield put({
            type: REQUEST(GET_ALL_COMPANY)
        })
    } catch (error) {
        errorNotification({type: 'Request Join In Company', content: error})
        yield put({
            type: FAILURE(REQUEST_TO_COMPANY),
            error
        })
    }
}

function* getCICOsaga(action) {
    try {
        const data  = yield getCICOByUser(action.payload);
        yield put({
            type: SUCCESS(GET_CICO),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Load Check In & Check Out', content: error})
        yield put({
            type: FAILURE(GET_CICO),
            payload: error
        })
    }
}

function* checkInSaga(action) {
    try {
        const data = yield checkInApi(action.payload);
        yield put({
            type: SUCCESS(CHECK_IN_COMPANY),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Check In', content: error})
        yield put({
            type: FAILURE(CHECK_OUT_COMPANY),
            error
        })
    }
}

function* checkOutSaga(action) {
    try {
        const data = yield checkOutApi(action.payload.userID, action.payload.CIID);
        yield put({
            type: REQUEST(GET_CICO),
            payload: action.payload.userID
        })
    } catch (error) {
        errorNotification({type: 'Check Out', content: error})
        yield put({
            type: FAILURE(CHECK_OUT_COMPANY)
        })
    }
}

function* updateAvatarCompanySaga(action) {
    try {
        const data = yield updateAvatarCompany(action.payload);
        yield put({
            type: SUCCESS(UPDATE_AVATAR_COMPANY)
        })
        yield put({
            type: REQUEST(GET_COMPANY),
            payload: action.payload.companyID
        })
    } catch (error) {
        errorNotification({type: 'Update avatar company error', content: error})
        yield put({
            type: FAILURE(UPDATE_AVATAR_COMPANY),
            error
        })
    }
}

function* getAllCICOCompanySaga(action) {
    try {
        const data = yield getAllCICO(action.payload);
        yield put({
            type: SUCCESS(GET_ALL_CICO),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Get all cico in company fail', content: error});
        yield put({
            type: FAILURE(GET_ALL_CICO)
        })
    }
}

function* allowRequestToCompanySaga(action) {
    try {
        const data = yield allowJoinInCompany(action.payload);
        yield put({
            type: SUCCESS(ALLOW_JOININ_COMPANY)
        })
        yield put({
            type: REQUEST(GET_COMPANY),
            payload: action.payload.companyID
        })
    } catch (error) {
        errorNotification({type: 'Allow request fail', content: error})
        yield put({
            type: FAILURE(ALLOW_JOININ_COMPANY),
            error
        })
    }
}


export default function* companySaga() {
    yield takeLatest(REQUEST(GET_ALL_COMPANY), getAllCompanySaga);
    yield takeLatest(REQUEST(GET_COMPANY), getCompanySaga);
    yield takeLatest(REQUEST(REQUEST_TO_COMPANY), joinInCompanySaga);

    yield takeLatest(REQUEST(QUERY_COMPANY), queryCompanySaga);

    yield takeLatest(REQUEST(GET_CICO), getCICOsaga);
    yield takeLatest(REQUEST(CHECK_IN_COMPANY), checkInSaga);
    yield takeLatest(REQUEST(CHECK_OUT_COMPANY), checkOutSaga);

    yield takeLatest(REQUEST(UPDATE_AVATAR_COMPANY), updateAvatarCompanySaga);
    yield takeLatest(REQUEST(GET_ALL_CICO), getAllCICOCompanySaga);
    yield takeLatest(REQUEST(ALLOW_JOININ_COMPANY), allowRequestToCompanySaga);

}