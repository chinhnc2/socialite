import { FormInput } from 'Components';
import { useAuth, useCompany } from 'Hooks';
import React, { useEffect } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { history } from 'Stores/reducers';
import './unjoined_company_screen.scss';

const UnjoinedCompanyScreen = () => {

    const form = useForm();
    const { handleSubmit, reset } = form;

    const { profile } = useAuth();

    const { isLoading, allCompany, joinInCompanyAction, getAllCompanyAction } = useCompany();

    useEffect(() => {
        getAllCompanyAction();
    }, [])

    useEffect(() => {
        if (profile && profile.company && profile.company._id) {
            console.log('redirect company');
            history.replace(`/c/${profile.company._id}`)
        }
    }, [profile])

    // useEffect(() => {
    //     console.log(allCompany);
    // }, [allCompany])

    return(
        <div className="UnjoinedCompanyScreen">
            <FormProvider {...form}>
                <form onSubmit={handleSubmit(() => {})}>
                    <FormInput type='text' name='search' />
                </form>
            </FormProvider>
            <div className="container_all_company">
                {allCompany && allCompany.length > 0 && allCompany.map(x => <div className='each_company' onClick={() => joinInCompanyAction({companyID: x._id, userID: (profile.id || profile._id)})}>
                    <p>{x.name}</p>
                    <p>{profile && x && x.request.length > 0 && x.request.findIndex(x => x._id ==  (profile.id || profile._id)) >= 0 ? 'Đã gửi yêu cầu' : 'Tham gia'}</p>
                </div>)}
            </div>
        </div>
    )
}

export default UnjoinedCompanyScreen;