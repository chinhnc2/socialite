/* eslint-disable max-len */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState , useCallback} from 'react'
import { Wrapper } from './styled'
import styled from 'styled-components'
import {
  CardPost,
  CommentInput,
  PostCreate,
  EnterPost,
  Stories,
  Related,
  ListFriendOnl,
  CreateStory
} from "Components";
import { useRoot, useHome, usePost, useCompany, useAuth } from "Hooks";
import InfiniteScroll from 'react-infinite-scroll-component';
import { Link } from 'react-router-dom';

const ContentCenter = styled.div`
  width: 65%;
  position: relative;
  top: 0;
  left: 0;
  padding-right: 25px;
  padding-bottom: 100px;
  height: fit-content;

  @media only screen and (max-width: 480px){
    width: 100%;
    padding-right: 0;

    > * {
      width: 94%;
      margin-left: auto;
      margin-right: auto;
    }
  }

  @media only screen and (min-width: 768px) and (max-width: 1024px) {
    width: 70%;
  }
`;

const HomeScreenDiv = styled.div`
  width: 75%;
  padding: 0px 50px;
  top: 25px;
  display: flex;
  overflow: scroll;
  border-radius: 14px;
  position: absolute;
  left: 20%;
  height: 100vh;

  @media only screen and (max-width: 480px) {
    width: 100%;
    padding: 0;
    left: 0;
    right: 0;
    top: 0;
  }

  @media only screen and (min-width: 768px) and (max-width: 1024px) {
    padding: 0px;
    top: 5px;
    left 23%;
    width: 76%;
  }
`

const RightSideBar = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  align-items: center;
  position: relative;
  overflow: hidden;
  width: 35%;

  @media only screen and (min-width: 768px) and (max-width: 1024px) {
    position: fixed;
    height: 100%;
    top: 70px;
    right: 0;
    width: 25%;
  }

  @media only screen and (max-width: 480px) {
    display: none;
  }

  > a {
    color: #fff;
    font-size: 18px;
  }
`


const HomeScreen = () => {
  
  const [showModal, setShowModal] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const [showAddStory, setShowAddStory] = useState(false);
  const [curentPage, setCurrentPage] = useState(1);

  const handleShowHideModalEnterPost = () => {
    setShowModal(!showModal);
  };

  const handleShowHideDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const handleHideDropdown = () => {
    setShowDropdown(false);
  };

  const handleShowAddStory = () => {
    setShowAddStory(!showAddStory);
  }

  const { profile } = useAuth();
  const { posts } = useHome();
  const { loadPostAction, unmountedPostAction, loadStoryAction } = usePost();
  const { cico, getCICOAction } = useCompany();

  useEffect(() => {
    
    loadPostAction({ amount: 10, begin: 0 });
    loadStoryAction()
    console.log('KAKAKKAKAK')
  }, []);

  useEffect(() => {
    if (profile.company) {
      getCICOAction(profile.id || profile._id);
    }
  }, [profile])

  const fechMore = () => {
    console.log('loading');
    loadPostAction({amount: 10, begin: posts.length});
  }
  
    //   useEffect(() => {
    //     console.log(posts.length);
    //   }, [posts])

  useEffect(() => {
    return () => unmountedPostAction();
  }, [])

  return (
    <HomeScreenDiv className="HomeScreen" id="scrollableDiv" >
      <ContentCenter  >
        <Stories 
          handleShowAddStory={handleShowAddStory}
        />
        {showModal && (
          <EnterPost
            handleShowHideModalEnterPost={handleShowHideModalEnterPost}
          />
        )}

        {showAddStory && (
          <CreateStory 
            handleShowAddStory={handleShowAddStory}
          />
        )}

        {showModal && (
          <EnterPost
            handleShowHideModalEnterPost={handleShowHideModalEnterPost}
            setShowModal={setShowModal}
            showModal={showModal}
          />
        )}
        <PostCreate
          handleShowHideModalEnterPost={handleShowHideModalEnterPost}
                  //profileUser={profileUser}
        />
        {profile.friends && profile.friends.length > 0 ? 
        <InfiniteScroll
          dataLength={posts.length}
          hasMore={true}
          loader={<h5>Loading...</h5>}
          scrollableTarget="scrollableDiv"
          next={fechMore}
        >
          {posts && posts.length > 0 && posts.map(post => post && <CardPost key={post.id || post._id} x={post} /> )}
        </InfiniteScroll> :<div style={{margin: '30px 15px'}}>
        <h4>You dont have friend, let add friend more</h4>
        </div>}

      </ContentCenter>

      <RightSideBar>
      {(profile.company && cico && cico.length > 0 && new Date(Date.now()).getHours() >= 8 &&  // Kiểm tra user có gia nhập company và danh sách cico lớn hơn 0, kiểm tra thời điểm hiện tại có quá 8h sáng không
      <>
        {((new Date(Date.now())).getDate() -(new Date(cico[0]?.createdAt)).getDate() >= 1 ) ? // Kiểm tra xem cico mới nhất là hôm nay hay là các hôm trước
        <div className='cico_noti'  style={{padding: '20px'}}> {/* Nếu là các hôm trước thì sẽ hiện thông báo Check In cho hôm nay */}
          <Link to={`/c/cico/${profile.company && (profile.id || profile._id)}`} style={{padding: '8px 12px', borderRadius: '10px', backgroundColor: '#91ff71'}}>Check In</Link>
        </div>
        : 
        <>
        {
        (!cico[0]?.checkOut && (Date.now() - (new Date(cico[0]?.createdAt)).getTime() >= 3600000*9 )) ? //Nếu không (cico mới nhất là hôm nay(đã checkIn)), kiểm tra có checkOut hay không nếu không có và nếu thời gian hiện tại trừ đi thời gian checkIn là 9 tiếng thì
        <div className='checkin_noti'  style={{padding: '20px'}}> {/* Hiển thị noti checkout */}
          <Link to={`/c/cico/${profile.id || profile._id}`} style={{padding: '8px 12px', borderRadius: '10px', backgroundColor: '#91ff71'}}>Check out</Link>
        </div>
        : <div></div> //Nếu không (đã checkIn, checkOut), thì không hiện gì.
        }
        </>
        }
      </>
      )}
      {profile.company && cico && cico.length == 0 && 
        <div className='cico_noti' style={{padding: '20px'}}>
          <Link to={`/c/cico/${profile.company && (profile.id || profile._id)}`} style={{padding: '8px 12px', borderRadius: '10px', backgroundColor: '#91ff71'}}>Check In first</Link>
        </div>}
      {
          posts && <Related posts={posts} />
      }
      </RightSideBar>
      
    </HomeScreenDiv>
)}

export default HomeScreen
