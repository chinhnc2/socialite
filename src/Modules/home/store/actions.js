import { REQUEST } from 'Stores'
import {
    LOAD_POST,
    CREATE_POST,
    EDIT_POST,
    LIKE_POST,
    DEL_POST,
    LOAD_COMMENT,
    CREATE_COMMENT,
    EDIT_COMMENT,
    LIKE_COMMENT,
    DEL_COMMENT,
    GET_POSTID,
    CREATE_STORY,
    LOAD_STORY,
    GET_POST_IN_COMPANY,
    UNMOUTED_POST
} from './constants'

export function loadPost(payload) {
    return {
        type: REQUEST(LOAD_POST),
        payload
    }
}

export function createPost(payload) {
    return {
        type: REQUEST(CREATE_POST),
        payload
    }
}

export function editPost(payload) {
    return {
        type: REQUEST(EDIT_POST),
        payload
    }
}

export function likePost(payload) {
    return {
        type: REQUEST(LIKE_POST),
        payload
    }
}

export function delPost(payload) {
    return {
        type: REQUEST(DEL_POST),
        payload
    }
}

export function loadComment(payload) {
    return {
        type: REQUEST(LOAD_COMMENT),
        payload
    }
}

export function createComment(payload) {
    return {
        type: REQUEST(CREATE_COMMENT),
        payload
    }
}

export function editComment(payload) {
    return {
        type: REQUEST(EDIT_COMMENT),
        payload
    }
}

export function likeComment(payload) {
    return {
        type: REQUEST(LIKE_COMMENT),
        payload
    }
}

export function delComment(payload) {
    return {
        type: REQUEST(DEL_COMMENT),
        payload
    }
}

export function getPostId(payload) {
    return {
        type: REQUEST(GET_POSTID),
        payload
    }
}

export function createStory(payload) {
    return {
        type: REQUEST(CREATE_STORY),
        payload
    }
}

export function loadStory(payload) {
    return {
        type: REQUEST(LOAD_STORY),
        payload
    }
}

export function getPostInCompany(payload) {
    return {
        type: REQUEST(GET_POST_IN_COMPANY),
        payload
    }
}

export function unmountedPost(payload) {
    return {
        type: (UNMOUTED_POST),
        payload
    }
}