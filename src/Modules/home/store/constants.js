export const LOAD_POST = 'LOAD_POST';

export const LOAD_COMMENT = 'LOAD_COMMENT';

export const CREATE_POST = 'CREATE_POST';
export const EDIT_POST = 'EDIT_POST';
export const LIKE_POST = 'LIKE_POST';
export const DEL_POST = 'DEL_POST';

export const CREATE_COMMENT = 'CREATE_COMMENT';
export const EDIT_COMMENT = 'EDIT_COMMENT';
export const LIKE_COMMENT = 'LIKE_COMMENT';
export const DEL_COMMENT = 'DEL_COMMENT';

export const GET_POSTID = 'GET_POSTID'

export const CREATE_STORY = 'CREATE_STORY'
export const LOAD_STORY = 'LOAD_STORY'

export const GET_POST_IN_COMPANY = 'GET_POST_IN_COMPANY';

export const UNMOUTED_POST = 'UNMOUNTED_POST';

