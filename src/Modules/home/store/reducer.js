import { createReducer, updateObject, REQUEST, SUCCESS, FAILURE } from 'Stores'
import { LOAD_POST, LOAD_COMMENT, CREATE_POST, EDIT_POST, DEL_POST, LIKE_POST, CREATE_COMMENT, EDIT_COMMENT, DEL_COMMENT, GET_POST_IN_COMPANY, GET_POSTID, CREATE_STORY, LOAD_STORY, UNMOUTED_POST ,CHECK_NOTI,GET_NEW_NOTI} from './constants'
import { setLocalStorage, STORAGE } from 'Utils';
import { loadProfile } from 'Modules/auth/store/actions';

export const initialState = {
  isLoading: false,
  error: null,
  posts: [],
  postSingle: null,
  stories: []
}

//LOAD POST

function loadingPost(state) {
    console.log('KAKAKSKDAKSKDAKS')
    return updateObject(state, {
        isLoading: true
    })
}

function LoadedPost(state, { payload }) {
    //const { profile, token } = payload
    
    return updateObject(state, {
        isLoading: false,
        posts: [...state.posts, ...payload.posts]
    })
}

function LoadPostError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}


// GET_POSTID
function gettingPostId(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function getedPostId(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        postSingle: payload
    })
}

function getPostIdError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}



//CREATE POST

function creatingPost(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function createdPost(state, { payload }) {
    // console.log(payload);
    const { post, user } = payload;
    Object.assign(post, { user })
    return updateObject(state, {
        isLoading: false,
        posts: [post, ...state.posts]
    })

}

function createPostError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

function editingPost(state) {
    return updateObject(state, {
        isLoading: true
    })
}

//EDIT
function editedPost(state, { payload }) {

    //const { profile, token } = payload
    let index = state.posts.findIndex(x => x._id == payload._id);
    return updateObject(state, {
        isLoading: false,
        posts: [...state.posts.slice(0, index), payload, ...state.posts.slice(index + 1)]
    })
}

function editPostError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

//DELETE
function deletingPost(state, { payload }) {
    return updateObject(state, {
        isLoading: true,
    }) //index = 2
}

function deletedPost(state, { payload }) {
    console.log("payloaddddđ", payload)
    const index = state.posts.findIndex(x => x._id == payload._id);
    return updateObject(state, {
        isLoading: false,
        posts: [...state.posts.slice(0, index), ...state.posts.slice(index + 1)]
    })
}

function deletePostError(state, { error, payload }) {
    return updateObject(state, {
        error,
        isLoading: false,
        posts: [...state.posts, payload].sort((a, b) => a.createAt - b.createAt)
    })
}

// LIKE
function likingPost(state, { payload }) {
    if (payload.type === 'single') {
        return updateObject(state, {
            isLoading: true
        })
    }
    console.log(state.posts)
  let index = state.posts.findIndex(x =>  {return (x._id || x.id) === payload.postID});
  console.log(index)
  let indexLike = state.posts[index] && state.posts[index].likes.findIndex(x => { 
    console.log(x);
    
    return x ?  (x._id || x.id) == (payload.profile.id || payload.profile._id) : -1});
  console.log(indexLike)
  let likeUpdate;
  if (indexLike < 0) {
    likeUpdate = [...state.posts[index].likes, payload.profile];
  } else {
    likeUpdate = [...state.posts[index].likes.slice(0, indexLike), ...state.posts[index].likes.slice(indexLike + 1)];
  }
  state.posts[index] = {...state.posts[index], likes: likeUpdate}
  return updateObject(state, {
    isLoading: true,
    //posts: [...state.posts.slice(0, index), {...state.posts[index], likes: likeUpdate}, ...state.posts.slice(index+1)]
  })
}


function likedPost(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
    })
}

function likePostError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })

}
//LOAD MORE COMMENT

function loadingComment(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function loadedComment(state, { payload }) {
    //const { profile, metaData } = payload
    return updateObject(state, {
        isLoading: false,
        post: payload
    })
}

function loadCommentError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

//EDIT COMMENT


function editingComment(state, { payload }) {
    return updateObject(state, {
        isLoading: true
    })
}

function editedComment(state, { payload }) {
    //const { profile, metaData } = payload
    let index = state.posts.findIndex(x => x.id == payload.post_id);
    let indexCmt = state.posts[index].comment.findIndex(x => x.id == payload.id);
    state.posts[index] = { ...state.posts[index], comment: [...state.posts[index].comment.slice(0, indexCmt), payload, ...state.posts[index].comment.slice(index + 1)] };
    return updateObject(state, {
        isLoading: false,
        posts: state.posts
    })
}

function editCommentError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

//DELETE COMMENT


function deletingComment(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function deletedComment(state, { payload }) {
    //const { profile, metaData } = payload
    console.log(payload);
    let index = state.posts.findIndex(x => (x.id || x._id) == payload.post_id);
    console.log(state.posts[index]);
    let indexCmt = state.posts[index].comment.findIndex(x => (x.id || x._id) == (payload.id || payload._id));
    console.log(indexCmt)
    state.posts[index] = { ...state.posts[index], comment: [...state.posts[index].comment.slice(0, indexCmt), ...state.posts[index].comment.slice(indexCmt + 1)] };
    return updateObject(state, {
        isLoading: false,
        posts: state.posts
    })
}

function deleteCommentError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}
// COMMENT
function creatingComment(state) {
    return updateObject(state, {
        isLoading: false,
    })
}

function createdComment(state, { payload }) {
    if (payload.type && payload.type === 'single') {
        return updateObject(state, {
            isLoading: true
        })
    }
    console.log(payload);
    const { comment, user } = payload;
    Object.assign(comment, { user });
    const index = state.posts.findIndex(x => (x._id || x.id) == comment.post_id);
    console.log(index);
    let postUpComment;
    if (state.posts[index]?.comment) {
        postUpComment = { ...state.posts[index], comment: [...(state.posts[index]?.comment), comment] }
    } else {
        postUpComment = { ...state.posts[index], comment: [comment] }
    }
    return updateObject(state, {
        isLoading: false,
        posts: [...state.posts.slice(0, index), postUpComment, ...state.posts.slice(index+1)]
    })
}

function createCommentError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}


function gettingPostInCompany(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function gotPostInCompany(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        posts: payload
    })
}

function getPostInCompanyError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

// CreateStory

function creatingStory(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function createdStory(state, { payload }) {
    // console.log(payload);
    const { story, user } = payload;
    Object.assign(story, { user })
    return updateObject(state, {
        isLoading: false,
        stories: [story, ...state.stories]
    })

}

function createStoryError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

// LoadStory

function loadingStory(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function loadedStory(state, { payload }) {
    //const { profile, token } = payload
    return updateObject(state, {
        isLoading: false,
        stories: payload.stories
    })
}

function loadStoryError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

function unmountedPost(state, { error }) {
    return updateObject(state, {
        posts: []
    })
}

// Slice reducer
export default createReducer(initialState, { //Object với các key là các action type và value là hàm xử lý state của type đó.

    [REQUEST(LOAD_POST)]: loadingPost,
    [SUCCESS(LOAD_POST)]: LoadedPost,
    [FAILURE(LOAD_POST)]: LoadPostError,

    [REQUEST(CREATE_POST)]: creatingPost,
    [SUCCESS(CREATE_POST)]: createdPost,
    [FAILURE(CREATE_POST)]: createPostError,
    [REQUEST(EDIT_POST)]: editingPost,
    [SUCCESS(EDIT_POST)]: editedPost,
    [FAILURE(EDIT_POST)]: editPostError,
    [REQUEST(DEL_POST)]: deletingPost,
    [SUCCESS(DEL_POST)]: deletedPost,
    [FAILURE(DEL_POST)]: deletePostError,
    [REQUEST(LIKE_POST)]: likingPost,
    [SUCCESS(LIKE_POST)]: likedPost,
    [FAILURE(LIKE_POST)]: likePostError,

    [REQUEST(LOAD_COMMENT)]: loadingComment,
    [SUCCESS(LOAD_COMMENT)]: loadedComment,
    [FAILURE(LOAD_COMMENT)]: loadCommentError,
    [REQUEST(EDIT_COMMENT)]: editingComment,
    [SUCCESS(EDIT_COMMENT)]: editedComment,
    [FAILURE(EDIT_COMMENT)]: editCommentError,
    [REQUEST(DEL_COMMENT)]: deletingComment,
    [SUCCESS(DEL_COMMENT)]: deletedComment,
    [FAILURE(DEL_COMMENT)]: deleteCommentError,
    [REQUEST(CREATE_COMMENT)]: creatingComment,
    [SUCCESS(CREATE_COMMENT)]: createdComment,
    [FAILURE(CREATE_COMMENT)]: createCommentError,

    [REQUEST(GET_POSTID)]: gettingPostId,
    [SUCCESS(GET_POSTID)]: getedPostId,
    [FAILURE(GET_POSTID)]: getPostIdError,

    [REQUEST(GET_POST_IN_COMPANY)]: gettingPostInCompany,
    [SUCCESS(GET_POST_IN_COMPANY)]: gotPostInCompany,
    [FAILURE(GET_POST_IN_COMPANY)]: getPostInCompanyError,

    [REQUEST(CREATE_STORY)]: creatingStory,
    [SUCCESS(CREATE_STORY)]: createdStory,
    [FAILURE(CREATE_STORY)]: createStoryError,
    [REQUEST(LOAD_STORY)]: loadingStory,
    [SUCCESS(LOAD_STORY)]: loadedStory,
    [FAILURE(LOAD_STORY)]: loadStoryError,

    [UNMOUTED_POST]: unmountedPost
})
