import { put, takeLatest, take, call, fork, select, all } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
//import { getDatabase, ref, set, onValue } from 'firebase/database';
import { getFirestore, collection, addDoc, setDoc, doc, onSnapshot, deleteDoc } from "firebase/firestore";
import { app } from 'Utils/firebase'
// import * as firebase from 'firebase';
import { selectAuthentication } from '../../auth/store/selectors';

import { REQUEST, SUCCESS, FAILURE } from 'Stores'
import { getProfile, getPosts, editPost, delPost, likePost, createComment, createPost, getPostId, createStory, getStories } from 'APIs'
import { getLocalStorage, STORAGE } from 'Utils'
import { LOAD_POST, LOAD_COMMENT, CREATE_POST, EDIT_POST, DEL_POST, LIKE_POST, CREATE_COMMENT, GET_POSTID, DEL_COMMENT, CREATE_STORY, LOAD_STORY, GET_POST_IN_COMPANY } from './constants';
import {  GET_NEW_NOTI } from '../../auth/store/constants';

import { getPostInCompany } from 'APIs/post.api';
import { pullAllBy } from 'lodash';
import { deleteComment } from 'APIs/comment.api';
import successNotification from 'Components/toastNoti/success_noti';
import errorNotification from 'Components/toastNoti/error_noti';


//const db = getDatabase(app);
const db = getFirestore(app);

function* loadPostSaga(action) {
    try {
        const data = yield getPosts(action.payload.amount, action.payload.begin);
        yield put({
            type: SUCCESS(LOAD_POST),
            payload: {
                posts: data
            }
        })
    } catch (error) {
        errorNotification({type: 'Load Post', content: error})
        yield put({
            type: FAILURE(LOAD_POST),
            error
        })
    }
}

function* getPostInCompanySaga(action) {
    try {
        const data = yield getPostInCompany(action.payload.amount, action.payload.begin, action.payload.companyID);
        yield put({
            type: SUCCESS(GET_POST_IN_COMPANY),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Load Post In Company', content: error})
        yield put({
            type: FAILURE(GET_POST_IN_COMPANY),
            error
        })
    }
}

function* getPostIdSaga(action) {
    try {
        const data = yield getPostId(action.payload);
        yield put({
            type: SUCCESS(GET_POSTID),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Detail Post', content: error})
        yield put({
            type: FAILURE(GET_POSTID),
            error
        })
    }
}

function* loadCommentSaga(action) {
    try {
        const { data: result } = yield getProfile(action.payload.amount);
        //* put = dispatch
        yield put({
            type: SUCCESS(LOAD_COMMENT),
            payload: {
                posts: result.data
            }
        })
    } catch (error) {
        yield put({
            type: FAILURE(LOAD_COMMENT),
            error
        })
    }
}

function* createPostSaga(action) {
    try {
        // const getMetaData = getLocalStorage(STORAGE.META_DATA)
        // const metaData = JSON.parse(getMetaData)

        const data = yield createPost(action.payload);
        const stateAuth = yield select(selectAuthentication);
        yield put({
            type: SUCCESS(CREATE_POST),
            payload: {
                post: data,
                user: stateAuth.profile
            }
        })
    } catch (error) {
        errorNotification({type: 'Create Post', content: error})
        yield put({
            type: FAILURE(CREATE_POST),
            error
        })
    }
}

function* editPostSaga(action) {
    try {
        const data = yield editPost(action.payload._id || action.payload.id, action.payload.post_content);

        yield put({
            type: SUCCESS(EDIT_POST),
            payload: action.payload
        })
        successNotification({type: 'Edit Post', content: 'Edit Post successed!!!'})
    } catch (error) {
        errorNotification({type: 'Edit Post', content: error})
        yield put({
            type: FAILURE(EDIT_POST),
            error
        })
    }
}

function* deletePostSaga(action) {
    try {
        const data = yield delPost(action.payload._id || action.payload.id);
        yield put({
            type: SUCCESS(DEL_POST),
            payload: action.payload
        })
        successNotification({type: 'Delete Post', content: 'Delete Post successed'})
    } catch (error) {
        errorNotification({type: 'Register', content: error})
        yield put({
            type: FAILURE(DEL_POST),
            error,
            payload: action.payload
        })
    }
}

function* likePostSaga(action) {
    try {
        //const statePosts = yield select(selectPosts);
        //let index = statePosts.posts.findIndex(x => x._id == action.payload.postID);
        if (action.payload.post.likes.findIndex(x => x._id == action.payload.profile._id) < 0 && action.payload.post.user_id != action.payload.profile._id) {
            yield call(handleEmitSocketNoti, { from_id: action.payload.postID, to_id: action.payload.post.user_id, type: 'post', content: `${action.payload.profile.name} liked your post` })
        }
        const data = yield likePost(action.payload.postID, action.payload.profile._id || action.payload.profile.id);
        if (action.payload.type !== 'single') {
            yield put({
                type: SUCCESS(LIKE_POST),
                payload: data

            })
        } else {
            yield put({
                type: REQUEST(GET_POSTID),
                payload: action.payload.postID
            })
        }

        
        
    } catch (error) {
        errorNotification({type: 'Like Post', content: error})
        yield put({
            type: FAILURE(LIKE_POST),
            error
        })
    }
}

function* createCommentSaga(action) {

  try {
      const stateAuth = yield select(selectAuthentication);
      if (action.payload.post.user_id != (stateAuth.profile._id || stateAuth.profile.id)) {
        yield call(handleEmitSocketNoti, {from_id: action.payload.post_id, to_id: action.payload.post.user_id, type: 'post', content: `${stateAuth.profile.name} commented your post`})
      }
      
      const data = yield createComment(action.payload);
        console.log(action.payload)
        if (action.payload.type && action.payload.type === 'single') {
            yield put({
                type: SUCCESS(CREATE_COMMENT),
                payload: action.payload
            })
            yield put({
                type: REQUEST(GET_POSTID),
                payload: action.payload.post_id
            })
        } else {
            yield put({
                type: SUCCESS(CREATE_COMMENT),
                payload: {
                    comment: data,
                    user: stateAuth.profile
                }
            })
        }
      
  } catch (error) {
    errorNotification({type: 'Create Comment', content: error})
      yield put({
          type: FAILURE(CREATE_COMMENT),
          error
      })
  }
}

function* deleteCommentSaga(action) {
    try {
        const data = yield deleteComment(action.payload._id || action.payload.id);
        yield put({
            type: SUCCESS(DEL_COMMENT),
            payload: action.payload
        })
    } catch (error) {
        errorNotification({type: 'Delete Comment', content: error})
        yield put({
            type: FAILURE(DEL_COMMENT),
            error
        })
    }
}




function* handleEmitSocketNoti(noti) {
  let endpoint;
  let subpoint;
  if (noti.type === 'post') {
    subpoint = 'notiPost';
    endpoint = `${noti.from_id}`; // Gửi thông báo đến bài post mà chủ bài post đó đã đăng ký nhận noti
  } else {
    subpoint = 'notiFriend';
    endpoint = `${noti.to_id}`; // Gửi thông báo đến friend/user_id_request mà người nhận request add friend này đã dăng ký để nhận noti của họ
  }
  
  const newNotiRef = collection(db, subpoint);
  return setDoc(doc(newNotiRef, endpoint), noti);
} 

function createEventChanel(item) {
    if (item.friend) {
        const listener = eventChannel(
            emit => {
                const notiRef = collection(db, `notiFriend`);
                const unSubscribe = onSnapshot(doc(notiRef, item.id || item._id), (snapshot) => {
                    const data = snapshot.data();
                    if (data && data != null) emit(data);
                    deleteDoc(doc(notiRef, item.id || item._id));
                })
                return () => unSubscribe();
                //return () => notiRef.off(listener);
            }
        )
        return listener;
    }

    const listener = eventChannel(
        emit => {
            const notiRef = collection(db, `notiPost`);
            const unSubscribe = onSnapshot(doc(notiRef, item.id || item._id), (snapshot) => {
                const data = snapshot.data();
                if (data && data != null) emit(data);
                deleteDoc(doc(notiRef, item.id || item._id));
            })
            return () =>  unSubscribe();
            //return () => notiRef.off(listener);
        }
    )
    return listener;
}

function* updateNotiSaga() {
    const stateAuth = yield select(selectAuthentication);
    //let updateChannel1 = createEventChanel({friend: true, _id: (stateAuth.profile._id || stateAuth.profile.id)})

    let arr = stateAuth.profile.posts && [{ friend: true, _id: (stateAuth.profile._id || stateAuth.profile.id) }, ...stateAuth.profile.posts,]
    yield all(arr.length > 0 && arr.map(x => {
        let updateChanel = createEventChanel(x);
        return fork(handleRequest, updateChanel, stateAuth.profile);

    }))

}

function* handleRequest(updateChanel) {
    while (true) {
        const item = yield take(updateChanel);
        yield put({
            type: REQUEST(GET_NEW_NOTI),
            payload: item
        });
    }
}

function* createStorySaga(action) {
    try {
        // const getMetaData = getLocalStorage(STORAGE.META_DATA)
        // const metaData = JSON.parse(getMetaData)

        const data = yield createStory(action.payload);
        const stateAuth = yield select(selectAuthentication);
        yield put({
            type: SUCCESS(CREATE_STORY),
            payload: {
                story: data,
                user: stateAuth.profile
            }
        })
    } catch (error) {
        errorNotification({type: 'Create Story', content: error})
        yield put({
            type: FAILURE(CREATE_STORY),
            error
        })
    }
}

function* loadStorySaga(action) {
    try {
        const data = yield getStories(action.payload);
        yield put({
            type: SUCCESS(LOAD_STORY),
            payload: {
                stories: data
            }
        })
    } catch (error) {
        errorNotification({type: 'Load Story', content: error})
        yield put({
            type: FAILURE(LOAD_STORY),
            error
        })
    }
}



export default function* postSaga() {
  //yield fork(emitSocketNotiSaga);
    yield fork(updateNotiSaga); //mở
    
    yield takeLatest(REQUEST(LOAD_POST), loadPostSaga);
    yield takeLatest(REQUEST(GET_POST_IN_COMPANY), getPostInCompanySaga)
    yield takeLatest(REQUEST(LOAD_COMMENT), loadCommentSaga);
    yield takeLatest(REQUEST(CREATE_POST), createPostSaga);
    yield takeLatest(REQUEST(EDIT_POST), editPostSaga);
    yield takeLatest(REQUEST(DEL_POST), deletePostSaga);
    yield takeLatest(REQUEST(LIKE_POST), likePostSaga); //mở
    yield takeLatest(REQUEST(CREATE_COMMENT), createCommentSaga);
    yield takeLatest(REQUEST(DEL_COMMENT), deleteCommentSaga);

    yield takeLatest(REQUEST(GET_POSTID), getPostIdSaga);
    

    yield takeLatest(REQUEST(CREATE_STORY), createStorySaga);
    yield takeLatest(REQUEST(LOAD_STORY), loadStorySaga)
}

