/* eslint-disable arrow-parens */
/* eslint-disable implicit-arrow-linebreak */

/**
 * The global state selectors
 */

import { createSelector } from 'reselect'
import { initialState } from './reducer'

const selectPosts = state => state.home || initialState

const makeSelectPosts = () =>
    createSelector(
        selectPosts,
        state => state
    )

export {
    selectPosts,
    makeSelectPosts
}
