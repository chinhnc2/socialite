
import React, { useState, useCallback, useEffect , useRef  } from "react";
import { useAuth , useProfile , usePost} from "Hooks";
import "./postProfile.scss";
import { useHistory, Link } from "react-router-dom";
import styled from "styled-components";
import { TimeSince } from 'Components';
import { FormProvider, useForm } from "react-hook-form";
import { CommentInput, Comment, FormTextArea } from "Components";




const LikeBtn = styled.div`
    color: ${(props) => props.liked ? '#ee0a73f2' : '#656c76'};
    display: flex;
    justify-content: center;
    align-items: center;
  `
const ListLikeModalContent = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  width: 500px;
  height: 400px;
  overflow: scroll;
`
const EachDetailLike = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: start;
  align-items: center;
  width: 100%;
  padding: 15px 30px;
  border-bottom: 1px solid #aaa;
  font-size: 15px;
  color: black;
`
const BtnCancel = styled.button`
  position: absolute;
  right: 10px;
  top: 0px;
`


const CardPostProfile = ({ x }) => {
const [editingMode, setEditingMode] = useState(false);
const form = useForm();
    
    const { profile} = useAuth();
    const {profileUser , loadProfileUserAction} = useProfile();
    const { deletePostAction ,editPostAction } = usePost();
    const [isDropdownOption, setIsDropdownOption] = useState(false);
    const [postProfileCurrent , setPostProfileCurrent] = useState(null);
    let history = useHistory();
    const { handleSubmit, reset } = form;
    const [initialHeight, setInitialHeight] = useState("");
    const [height, setHeight] = useState("");
    const [showTextMore, setShowTextMore] = useState(false);
    
    
   

    const avatar = 'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg';

    const handleNavigatePostSingle = (id) => {
        console.log('id', id)
        if (id) {
          history.push({
            pathname: `/post/${id}`,
            state: { 
              notiFromId: id
            }
          });
        } else {
          history.push("/")
        }
      }
    const handleSetIsDropdownOption = (post) => {
        console.log('post current', post);
        setIsDropdownOption( prev => !prev);
        setPostProfileCurrent(post);
        
    }
    const handleDeletePostProfile = (x) => {
        deletePostAction(x);
        loadProfileUserAction({userId : x.user._id});
    }

  
    const onSubmitEditPostProfile = (data) => {
        
        editPostAction({ ...postProfileCurrent , post_content: data.editPost });
        loadProfileUserAction({userId : postProfileCurrent.user._id});
        setEditingMode(false);
    };
    const hanleEditingPostProfile = () => {
       
        setEditingMode(prev => !prev);
        
    }
    const textContentRef = useRef(null);
    const handleShowTextMore = () => {
        setHeight(initialHeight);
        setShowTextMore(false)
    }
    // useEffect(() => {
    //     const heightRef = textContentRef?.current?.clientHeight;
    //     setInitialHeight(heightRef);
    //     if (heightRef > 250) {
    //         setHeight(250);
    //         setShowTextMore(true)
    //     } 
    //     else {
    //         handleShowTextMore();
    //     }
    // }, [])
    return (
    <>
    {
        profile && profileUser && profile._id !== profileUser._id ?
        // của user
            <>
            {   
                
                profileUser.posts && profileUser.posts.length > 0
                ? profileUser.posts.map(postUser =>
                    (
                        <div className="CardPostProfile" key={postUser._id || postUser.id} >
                            <div className="header_post">
                                <div className="avt_user_post">
                                    <img src={postUser.user.avatar || avatar} alt="avt_user_post" />
                                    {/* <img src="https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg" alt="" /> */}
                                </div>
                                <div className="main_header">
                                    <div className="name_user_post">
                                        <p>{postUser.user.name}</p>
                                    </div>
                                    <div className="create_post">
                                        <p>{TimeSince( new Date(postUser.createdAt))}</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div className="content_post">
                                {
                                    postUser.isPhoto  ? 
                                    <>
                                    {
                                        postUser.post_content.split("/")[4] === "video" ? 
                                        <video controls="controls">
                                            <source src={postUser.post_content}></source>
                                        </video> : <img src={postUser.post_content} alt="image_post" />
                                    }
                                    </>
                                    : 
                                    <>
                                       <p ref={textContentRef} style={{ height: height, overflow: 'hidden', textOverflow: 'ellipsis' }} >{postUser.post_content}</p>
                                       <div style={{
                                            color: '#5b059a',
                                            marginLeft: '8px', marginTop: '-6px',
                                        }}> {showTextMore && <button style={{ border: 'none', backgroundColor: 'transparent' }}
                                            onClick={handleShowTextMore}>...load more</button>}
                                       </div>
                                    </>
                                }
                            </div>
                            <div className="tool_post">
                                <div className="btn_favorite" onClick={() => handleNavigatePostSingle(postUser._id)}>
                                    <i className='bx bxs-heart'></i>
                                    <span>like</span>
                                </div>
                                <div className="btn_comment" onClick={() => handleNavigatePostSingle(postUser._id)}>
                                    <i className="fa-solid fa-comment" />
                                    <span>Comment</span>
                                </div>
                                <div className="btn_share" onClick={() => handleNavigatePostSingle(postUser._id)}>
                                    <i className="fa-solid fa-share" />
                                    <span>Share</span>
                                </div>
                            </div>
                        </div >
                    )) 
                :  
                    <h3>chưa có bài viết</h3>
            }
            </>
            :
        // của mình
            <>  
            {
                profileUser.posts && profileUser.posts.length > 0
                    ? 
                    <>
                        {
                            profileUser.posts.map(post => {
                                console.log("post", post);
                                return (
                                    <div className="CardPostProfile"  key={post._id || post.id}>
                                        <div className="header_post">
                                            <div className="avt_user_post">
                                                <img src={post.user.avatar || avatar} alt="avt_user_post" />
                                                {/* <img src="https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg" alt="" /> */}
                                            </div>
                                            <div className="main_header">
                                                <div className="name_user_post">
                                                    <p>{post.user.name}</p>
                                                </div>
                                                <div className="create_post">
                                                    <p>{TimeSince( new Date(post.createdAt))}</p>
                                                </div>
                                                <div className="option" onClick={() => handleSetIsDropdownOption(post)} >
                                                    <box-icon name='dots-horizontal-rounded'></box-icon>
                                                </div>
                                                {isDropdownOption &&  postProfileCurrent &&  postProfileCurrent._id === post._id &&
                                                    <div className="dropdownOption">
                                                        <div className="item_option" onClick={() => handleDeletePostProfile(postProfileCurrent)}>
                                                            <p>Delete this post</p>
                                                        </div>
                                                        <div className="item_option" onClick={() => hanleEditingPostProfile()}>
                                                            <p>Edit this post</p>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        {
                                            !editingMode ? 
                                            <div className="content_post">
                                            {
                                               post.isPhoto  ? 
                                               <>
                                               {
                                                   post.post_content.split("/")[4] === "video" ? 
                                                   <video controls="controls" style={{    width: '100%' , height: '100%' }}>
                                                       <source src={post.post_content}></source>
                                                   </video> : <img src={post.post_content} alt="image_post" />
                                               }
                                               </>
                                               : 
                                               <>
                                                   <p ref={textContentRef} style={{ height: height, overflow: 'hidden', textOverflow: 'ellipsis' }} >{post.post_content}</p>
                                                   <div style={{
                                                        color: '#5b059a',
                                                        marginLeft: '8px', marginTop: '-6px',
                                                    }}> {showTextMore && <button style={{ border: 'none', backgroundColor: 'transparent' }}
                                                        onClick={handleShowTextMore}>...load more</button>}
                                                    </div>
                                               </>
                                            }
                                            </div> :
                                                <>
                                                {
                                                    postProfileCurrent && postProfileCurrent._id === post._id ?
                                                    <FormProvider {...form}>
                                                        <form onSubmit={handleSubmit(onSubmitEditPostProfile)}>
                                                            <FormTextArea name='editPost' required placeholder={post.post_content} style={{ height: '270px'}}/>
                                                            <div style={{    padding: '6px 0px 14px 0px'}}>
                                                                <input type="submit"  style={{ padding: '6px 10px' , border : 'none', margin : '0px 8px', borderRadius : '13px', backgroundColor : '#ec4899', color : 'white' }}/>
                                                                <button onClick={() => hanleEditingPostProfile()} style={{ padding: '5px 14px' , border : 'none', borderRadius : '13px', backgroundColor : 'cornflowerblue', color : 'white' }}>Cancel</button>
                                                            </div>
                                                            
                                                        </form>
                                                    </FormProvider> : 
                                                    <>
                                                        {
                                                            post.isPhoto ?
                                                            <>
                                                            {
                                                                post.post_content.split("/")[4] === "video" ? 
                                                                <video controls="controls" style={{    width: '100%' , height: '100%' }}>
                                                                    <source src={post.post_content}></source>
                                                                </video> : <img src={post.post_content} alt="image_post" />
                                                            }
                                                            </>
                                                            : 
                                                            <>
                                                                <p ref={textContentRef} style={{ height: height, overflow: 'hidden', textOverflow: 'ellipsis' }} >{post.post_content}</p>
                                                                <div style={{
                                                                     color: '#5b059a',
                                                                     marginLeft: '8px', marginTop: '-6px',
                                                                 }}> {showTextMore && <button style={{ border: 'none', backgroundColor: 'transparent' }}
                                                                     onClick={handleShowTextMore}>...load more</button>}
                                                                 </div>
                                                            </>
                                                        }
                                                    </>
                                                }
                                                </>        
                                        }
                                        
                                        <div className="tool_post">
                                            <div className="btn_favorite" onClick={() => handleNavigatePostSingle(post._id)}>
                                                <i className='bx bxs-heart'></i>
                                                <span>like</span>
                                            </div>
                                            <div className="btn_comment" onClick={() => handleNavigatePostSingle(post._id)}>
                                                <i className="fa-solid fa-comment" />
                                                <span>Comment</span>
                                            </div>
                                            <div className="btn_share" onClick={() => handleNavigatePostSingle(post._id)}>
                                                <i className="fa-solid fa-share" />
                                                <span>Share</span>
                                            </div>
                                        </div>
                                    </div >
                                )
                            })
                            
                        }
                    </>
                    :
                    <>
                        <h3>chưa có bài viết</h3>
                    </>   
            }   
                    
            </>
        
    }
    </>

        
       
    );
};
export default CardPostProfile;
