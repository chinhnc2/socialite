import React, { useCallback , useEffect , useState , useRef } from 'react';
import './inforUser.scss';
import {
    useForm, FormProvider, Controller  } from "react-hook-form";
import moment from 'moment'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Radio } from 'antd';
import { useProfile } from "Hooks";

import {
    Submit,
    ClickAble,
    Input,
    Text,
    FormInput,
    FormTextArea,
    FormDatePicker,
    
} from "Components";


const FormInforUser = ({ profileUser , profile  , setshowFormInfo })  =>{
    
    const form = useForm();
    const { updateProfileAction } = useProfile();
    // const {  profile } = useAuth();
    const { handleSubmit, reset , setValue , control , register, formState : {errors}} = form;
    // const [ nameState , setNameState ] = useState(profileUser.name);
    // const [ about_meState , setAbout_meState] = useState(profileUser.about_me);
    // const [dobState , setDobState] = useState(profileUser.dob);
    // const [gender, setGender] = useState("Male");
    // const [startDate, setStartDate] = useState(profileUser.dob || new Date());
    const [editingMode , setEditingMode] =  useState(false);

    const onSubmitUpdateProfile = useCallback((data) =>  {
        // console.log(moment(data.dob).format('L'))
        console.log("data update", data);
        updateProfileAction({ userID :  profileUser._id , updateBody : data });
    },[])
   
    
    
    useEffect(() => {
        if (profileUser) {
            setValue('name', profileUser.name);
            setValue('about_me', profileUser.about_me);
            setValue('dob', profileUser ? new Date(profileUser.dob) : new Date());
            //setValue('gender', profileUser.gender);   
            setValue('address', profileUser.address);
        }
    }, [profileUser])

    return (
        <div id="InforUser">
            {
                editingMode === false ?
                <>
                    {
                        //của họ
                        profile._id !== profileUser._id ?
                        <div className="inf-curent">
                            
                            <div className="your-infor-list">
                                <div className="inf-curent-item">
                                    <h3>Name:</h3>
                                    <p>{profileUser.name}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>Gender:</h3>
                                    <p>{profileUser.gender}</p>
                                </div >
                                <div className="inf-curent-item">
                                    <h3>Email:</h3>
                                    <p>{profileUser.email}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>About me:</h3>
                                    <p>{profileUser.about_me ? profileUser.about_me : "not set value"}</p>
                                </div>   
                                <div className="inf-curent-item">
                                    <h3>Company:</h3>
                                    <p>{profileUser.company && profileUser.company.name}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>Address:</h3>
                                    <p>{profileUser.address ? profileUser.address : "not set value"}</p>
                                </div>
                                
                                <div className="inf-curent-item">
                                    <h3>Date of birth:</h3>
                                    <p>{ profileUser.dob ? moment(profileUser.dob).format('L') : "not set value"}</p>
                                </div>
                            </div>
                            <button onClick={() => setshowFormInfo(false)}style={{    backgroundColor: 'cornflowerblue' ,border: 'none', padding: '8px 8px',color: 'white',borderRadius: '8px',}}>Return</button>
                        </div>
                        :
                        // của mình
                        <div className="inf-curent">
                            <h3 style={{color: '#ec4899' ,marginLeft: '12px'}}>Your infor</h3>
                            <div className="your-infor-list">
                                <div className="inf-curent-item">
                                    <h3>Name:</h3>
                                    <p>{profileUser.name}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>Gender:</h3>
                                    <p>{profileUser.gender}</p>
                                </div >
                                <div className="inf-curent-item">
                                    <h3>Email:</h3>
                                    <p>{profileUser.email}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>About me:</h3>
                                    <p>{profileUser.about_me ? profileUser.about_me : "not set value"}</p>
                                </div>   
                                <div className="inf-curent-item">
                                    <h3>Company:</h3>
                                    <p>{profileUser.company && profileUser.company.name}</p>
                                </div>
                                <div className="inf-curent-item">
                                    <h3>Address:</h3>
                                    <p>{profileUser.address ? profileUser.address : "not set value"}</p>
                                </div>
                                
                                <div className="inf-curent-item">
                                    <h3>Date of birth:</h3>
                                    <p>{ profileUser.dob ? moment(profileUser.dob).format('L') : "not set value"}</p>
                                </div>
                            </div>
                            <div style={{display : 'flex'}} className="profile-info-button">
                                <button style={{backgroundColor: 'rgb(236 72 153)'}} onClick={() => setEditingMode(true)}><i class='bx bx-edit' ></i><p style={{marginLeft : '3px'}}>Edit</p></button>
                                <button onClick={() => setshowFormInfo(false)}style={{    backgroundColor: 'cornflowerblue'}}>Return</button>
                            </div>
                            

                        </div>
                    }
                </>
                   
                    :
                    <>
                    <FormProvider {...form}>
                        <form onSubmit={handleSubmit(onSubmitUpdateProfile)} >
                            <div>
                                <h3>Change your information</h3>
                            </div>
                            <div className='name'>
                                <section>
                                    <label style={{    fontWeight: '500', fontSize: '16px' ,color: '#777777d9' }}>Full name</label>
                                    <FormInput 
                                        name="name"  
                                        onChange={(e) => setValue( 'name', e.target.value)}
                                        rules={{ required: "Name is required"}}
                                    />
                                </section>
                                <section>
                                
                                <Radio.Group onChange={ e => setValue( 'gender', e.target.value)} style={{fontSize: '30px'}} name="gender">
                                    <Radio className="sex__radio" value='Male' style={{color: '#777777d9'}}>Male</Radio>
                                    <Radio className="sex__radio" value='Famale' style={{color: '#777777d9'}}>Female</Radio>
                                    <Radio className="sex__radio" value='Other' style={{color: '#777777d9'}}>Other</Radio>
                                </Radio.Group>     
                                </section>
                            </div>
                            <div className="input-inf">
                                <label style={{    fontWeight: '500', fontSize: '16px'}}>Address</label>
                                <FormTextArea 
                                    name="address" 
                                    onChange={e => setValue( 'address', e.target.value)}
                                />
                            </div>
                            <div className="input-inf">
                                <label style={{    fontWeight: '500', fontSize: '16px'}}v>About me</label>
                                <FormTextArea 
                                    name="about_me" 
                                    onChange={(e) => setValue( 'about_me', e.target.value)}
                                />
                            </div>
                            <div className="input-inf">
                                <label>Date of births</label>
                                    <Controller
                                        control={control}
                                        name="dob"
                                        render={({ field }) => (
                                            <DatePicker
                                                placeholderText="Select date"
                                                onChange={(date) => field.onChange(date)}
                                                selected={field.value}
                                            />
                                        )}
                                    />
                                {errors.dateInput && <span>This field is required</span>}     
                            </div>
                            <input type="submit" className="form-submit"/>
                            <button onClick={() => setEditingMode(false)}>Cancel</button>
                        </form>
                    </FormProvider>
                    </>
                    
            }
        </div>
    );
}

export default FormInforUser;