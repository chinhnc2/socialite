import React, { useState , useEffect} from "react";
import "./ProfileContent.scss";
import classNames from "classnames";
import CardPostProfile from "./CardPostProfile";
// import CardPostProfileUser from "./CardPostProfileUser";
import { useHistory, Link } from "react-router-dom";

import { useProfile } from "Hooks";
const ProfileContent = ({ profile, profileUser  }) => {
    let history  = useHistory();
    const [showContent, setShowContent] = useState("album");
    // const {profileUser} = useProfile();
    const arrContainPhoto = profileUser && profileUser.posts && profileUser.posts.filter(x => x.isPhoto);
    // useEffect(() => {
    //     console.log("profileUser",profileUser);
    //     console.log("arrContainPhoto", arrContainPhoto);
    // },[])
    const handleNavigateAlbumItem = (id) => {
        //console.log('id', id)
        if (id) {
          history.push({
            pathname: `/post/${id}`,
            state: { 
              notiFromId: id
            }
          });
        } else {
          history.push("/")
        }
      }
    return (
    <div id="profile-content">
        <div class="profile-content-title">
            <div
                className={classNames("content-title-ab", {
                    pinky: showContent === "album",
                })}
            >
                <h3 onClick={() => setShowContent("album")}>Album</h3>
            </div>
            <div
                className={classNames("content-title-ab", {
                    pinky: showContent === "post",
                })}
            >
                <h3 onClick={() => setShowContent("post")}>Post</h3>
            </div>
        </div>
           
        <div className="profile-content-body">
            { showContent === "album" ? 
                <> 
                    {   
                        arrContainPhoto && arrContainPhoto.map(y => (
                                <div className="content-body-album" key={y._id} >
                                    <div className="al-img-item" onClick={() => handleNavigateAlbumItem(y._id)}>
                                        {   
                                           
                                            y.post_content.split('/')[4] === 'video' 
                                            &&  <video controls="controls" style={{    width: '100%',
                                                height: '100%'}}>
                                                    <source src={y.post_content}></source>
                                                </video> ||  <img src={y.post_content}alt=""/> 
                                           
                                            
                                        }
                                        <div className="al-overlay">
                                            <ul className="al-overlay-list">
                                                <li>
                                                    <i className="bx bxs-heart"></i> <p>{y.likes.length}</p>
                                                </li>
                                                <li>
                                                    <i className="bx bx-message-dots"></i> <p>{y.comment.length}</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                        )) 
                    }
                </>
                : <>
                    
                       <CardPostProfile /> 
                    
                  </>
                
               
            }
        </div>
        
    </div>
);
};

export default ProfileContent;
