import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import "./ProfileIntro.scss";
import uploadCloudinary from "Utils/cloudinary";
import { useAuth, useProfile } from "Hooks";
const ProfileIntro = ({ profileUser, handleShowHideFormInf, profile }) => {
    const [isOverlayAvt, setIsOverlayAvt] = useState(false);
    const [imgURL, setImgURL] = useState(null);
    const [showDropdownProfile, setShowDropdownProfile] = useState(false);
    const { updateProfileAction, sendRequestAddFriendAction, request, isFriend } = useProfile();
    // const { profile } = useAuth();
    const handleShowDrProfile = () => {
        setShowDropdownProfile(!showDropdownProfile);
    }
    const form = useForm();
    const { handleSubmit, reset, watch, register, control } = useForm();

    let friendsProfile = profile && profileUser && profileUser.friends.find(x => x._id === profile._id) ? true : false;
    //console.log("friendsProfile",friendsProfile)

    // useEffect(() => {
    //     console.log("profile " , profile);
    //     console.log("profile user" , profileUser);
    // }, [])
    const handleChangeFile = (e) => {
        let reader = new FileReader();
        //console.log(e.target.files[0])
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function (e) {
            setImgURL(reader.result);
        };
    }


    const onSubmitUpload = async data => {
        let resURL = await uploadCloudinary(imgURL);
        console.log(resURL);
        if (resURL) {
            console.log(resURL);
            updateProfileAction({ userID: profile._id, updateBody: { avatar: resURL } })
        }
        reset();
    }

    const handleAddFriend = () => {
        //console.log(profile._id);
        sendRequestAddFriendAction({ userID: (profile.id || profile._id), friendID: (profileUser.id || profileUser._id) })
    }

    const default_avt = profileUser.avatar || "https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg";
    return (
        <div id="profile-intro" onMouseLeave={() => setShowDropdownProfile(false)} >
            <div className="pIntro-avatar" onMouseEnter={() => setIsOverlayAvt(true)} onMouseLeave={() => setIsOverlayAvt(false)}>
                <div>
                    <img
                        src={imgURL || default_avt}
                        alt=""
                    />
                </div>

                {(profile.id || profile._id) == (profileUser.id || profileUser._id) && isOverlayAvt && <div className="overlay">
                    <form className='form_upload_avt' onSubmit={handleSubmit(onSubmitUpload)}>
                        <div className="btn_upload">
                            <Controller
                                name="media"
                                control={control}
                                render={({ field }) => {
                                    // sending integer instead of string.
                                    return <input type='file' name='media'
                                        {...field} onChange={(e) => handleChangeFile(e)} />;
                                }}
                            />
                        </div>
                    </form>
                    <p>Upload</p>
                </div>}
            </div>
            {imgURL && <button className='btn_submit_upload' onClick={onSubmitUpload}>Save</button>}
            <div className="pIntro-text">
                <div className="pIntro-name">
                    <h3>{profileUser.name}</h3>
                </div>
                <div id="pIntro" >
                    <div className="intro-description">
                        <i class='bx bxs-quote-left' style={{ fontSize: '13px', color: '#ec4899' }}></i>
                        <p style={{ marginLeft: '20px', fontSize: '14px', marginRight: '20px' }}>{profileUser.about_me ? profileUser.about_me : "not set value"}</p>
                        <i class='bx bxs-quote-right' style={{ fontSize: '13px', color: '#ec4899' }}></i>
                    </div>
                    <div className="pIntro-address">
                        <i class='bx bxs-business' style={{ fontSize: '25px', color: 'rgb(9 109 217 / 82%)' }}></i>
                        <p style={{ marginLeft: '8px', fontSize: '14px', color: '#2f1414', display: 'flex', alignItems: 'center' }}>Work at <p style={{
                            fontWeight: '600', color: 'darkslategray',
                            marginLeft: '5px'
                        }}>
                            {profileUser.company ? profileUser.company.name : "not set value"}</p> </p>
                    </div>
                    <div className="pIntro-address">
                        <i class='bx bxs-home' style={{ fontSize: '25px', color: 'rgb(255 58 58 / 78%)' }}></i>
                        <p style={{ marginLeft: '8px', fontSize: '14px', color: 'grey' }}>Live in {profileUser.address ? profileUser.address : "not set value"}</p>
                    </div>
                </div>
                

                <ul className="pIntro-nav">
                    {profile && profileUser && profile._id !== profileUser._id ?

                        <>
                            {
                                <>
                                    {
                                        friendsProfile ?
                                            <li className="addfriend">
                                                <p>Friend</p>
                                            </li>
                                            :
                                            <>
                                                {
                                                    request ?
                                                        <li className="addfriend">
                                                            <p>You have request</p>
                                                        </li>
                                                        :
                                                        <li className="addfriend" onClick={() => handleAddFriend()}>
                                                            <p>Add Friend</p>
                                                        </li>
                                                }
                                            </>
                                    }

                                </>
                            }

                            {/* <li className="sendmess">
                                <p>Send message</p>
                            </li> */}
                            <li className="sendmess" onClick={() => handleShowHideFormInf()}>
                                <p>Infor</p>
                            </li>
                            <li className="iconz" onClick={() => handleShowDrProfile()}>
                                <i class="bx bx-chevron-down"></i>
                            </li>
                            {
                                showDropdownProfile &&
                                <div className="dropdown-profile">
                                    <ul>
                                        <li>
                                            <i class='bx bxs-user-x'></i>
                                            Unfriend
                                        </li>
                                        <li>
                                            <i class='bx bx-hide' ></i>
                                            Hide story
                                        </li>
                                        <li>
                                            <i class='bx bx-block' ></i>
                                            Block
                                        </li>
                                    </ul>
                                </div>
                            }
                        </>
                        :
                        <>
                            <ul>
                                <li className="sendmess" onClick={() => handleShowHideFormInf()}>
                                    <i class='bx bxs-edit'></i>
                                    <p>Edit</p>
                                </li>
                            </ul>

                        </>
                    }
                </ul>
            </div>
        </div>
    );
};

export default ProfileIntro;
