import React, {useEffect, useState} from "react";
import "./style.scss";
import ProfileIntro from "./ProfileIntro";
import ProfileContent from "./ProfileContent";
import { useProfile, useAuth} from "Hooks";
import FormInforUser from './FormInforUser';
const Profile = () => {
    const { loadProfileUserAction, getMyProfileAction, updateProfileAction, request, profileUser } = useProfile();
    const { profile } = useAuth();
    const [showFormInfo, setshowFormInfo] = useState(false);
    const handleShowHideFormInf = () => {
        setshowFormInfo(!showFormInfo);
    }
    useEffect(() => {
        console.log(window.location.pathname.split('/'));
        if (window.location.pathname.split('/') != (profile.id || profile._id)) {
          loadProfileUserAction({userId: window.location.pathname.split('/')[2]});
        } else {
          getMyProfileAction(profile);
        }
        //loadProfileAction(profile.id || profile._id);
    },[window.location.pathname]);

    
  return (

    <div id="profile">
          {profileUser && <ProfileIntro profileUser={profileUser} profile={profile}  handleShowHideFormInf={handleShowHideFormInf} />}
          {showFormInfo && profileUser  ? <FormInforUser profileUser={profileUser} profile={profile} setshowFormInfo={setshowFormInfo}/> : 
              <ProfileContent profile={profile} profileUser={profileUser} />
          }
    </div>

  );
};

export default Profile;