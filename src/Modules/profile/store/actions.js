import { REQUEST } from 'Stores'
import { LOAD_PROFILE_USER, UPDATE_PROFILE, REQ_ADD_FRIEND, ALLOW_REQ_FRIEND } from "./constants";

export function loadProfileUser(payload) {
    // console.log(123);
    return {
        type: REQUEST(LOAD_PROFILE_USER),
        payload
    }
}

export function loadMyProfile(payload) {
    return {
        type: LOAD_PROFILE_USER,
        payload
    }
}

export function updateProfile(payload) {
    return {
        type: REQUEST(UPDATE_PROFILE),
        payload
    }
}

export function requestFriend(payload) {
    return {
        type: REQUEST(REQ_ADD_FRIEND),
        payload
    }
}

export function allowRequestFriend(payload) {
    return {
        type: REQUEST(ALLOW_REQ_FRIEND),
        payload
    }
}

export function denyRequestFriend(payload) {
    return {
        type: REQUEST(ALLOW_REQ_FRIEND),
        payload
    }
}
