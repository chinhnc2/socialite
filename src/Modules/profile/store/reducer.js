import { createReducer, updateObject, REQUEST, SUCCESS, FAILURE } from 'Stores';
import { setLocalStorage, STORAGE } from 'Utils';
import { ALLOW_REQ_FRIEND, DENY_REQ_FRIEND, LOAD_PROFILE_USER, REQ_ADD_FRIEND, UPDATE_PROFILE } from './constants';

export const initialState = {
    isLoading: false,
    error: null,
    profileUser: null,
    request: false,
   
}

//LOAD PROFILE

function loadingProfileUser(state) {
    return updateObject(state, {
        isLoading: true
    })
}

function LoadedProfileUser(state, { payload }) {
    //const { profile, token } = payload
    console.log(payload);
    return updateObject(state, {
        isLoading: false,
        profileUser: payload
    })
}

function LoadProfileUserError(state, { error }) {
    return updateObject(state, {
        error,
        isLoading: false,
    })
}

function loadedMyProfile(state, { payload }) {
    console.log(payload);
    return updateObject(state, {
        profileUser: payload,
        request: false
    })
}

function updatingProfileUser(state, { payload }) {
    return updateObject(state, {
        isLoading: true
    })
}

function updatedProfileUser(state, { payload }) {
    return updateObject(state, {
        isLoading: false,
        profileUser: payload
    })
}

function updateProfileUserError(state, { error }) {
    return updateObject(state, {
        isLoading: false,
        error
    })
}

function sendingRequestFriend(state, { payload }) {
    return updateObject(state, {
        request: true
    })
}

function sentRequestFriend(state, { payload }) {
    return updateObject(state, {
        //profile: payload
    })
}

function sendRequestFriendError(state, { error }) {
    return updateObject(state, {
        request: false,
        error
    })
}

function allowingRequestFriend(state, { payload }) {
    console.log(state.profileUser);
    return updateObject(state, {
        request: (state.profileUser && (state.profileUser.id || state.profileUser._id)) == payload.to_id ? false : true,
        
    })
}

function denyingRequestFriend(state, { payload }) {
    return updateObject(state, {
        request: (state.profileUser && (state.profileUser.id || state.profileUser._id)) == payload.to_id ? false : true,
        
    })
}

export default createReducer(initialState, { //Object với các key là các action type và value là hàm xử lý state của type đó.

    [REQUEST(LOAD_PROFILE_USER)]: loadingProfileUser,
    [SUCCESS(LOAD_PROFILE_USER)]: LoadedProfileUser,
    [FAILURE(LOAD_PROFILE_USER)]: LoadProfileUserError,
    [LOAD_PROFILE_USER]: loadedMyProfile,

    [REQUEST(UPDATE_PROFILE)]: updatingProfileUser,
    [SUCCESS(UPDATE_PROFILE)]: updatedProfileUser,
    [FAILURE(UPDATE_PROFILE)]: updateProfileUserError,

    [REQUEST(REQ_ADD_FRIEND)]: sendingRequestFriend,
    [SUCCESS(REQ_ADD_FRIEND)]: sentRequestFriend,
    [FAILURE(REQ_ADD_FRIEND)]: sendRequestFriendError,

    [REQUEST(ALLOW_REQ_FRIEND)]: allowingRequestFriend,
    [REQUEST(DENY_REQ_FRIEND)]: denyingRequestFriend

});
