import { put, takeLatest, call, select } from 'redux-saga/effects'

import { REQUEST, SUCCESS, FAILURE } from 'Stores'
import { getProfile, updateProfle } from 'APIs'
import { getLocalStorage, STORAGE } from 'Utils'
import { ALLOW_REQ_FRIEND, DENY_REQ_FRIEND, LOAD_PROFILE_USER, REQ_ADD_FRIEND, UPDATE_PROFILE } from './constants'
import { allowRequestFriend, denyRequestFriend, removeRequestFriend, sendRequestFriend } from 'APIs/profile.api'
//import { getDatabase, ref, set, onValue,  } from 'firebase/database';
import { getFirestore, collection, addDoc, setDoc, doc, onSnapshot, deleteDoc } from "firebase/firestore";
import { app } from 'Utils/firebase'
import { selectAuthentication } from '../../auth/store/selectors';
import { REMOVE_NOTI } from 'Modules/auth/store/constants'
import successNotification from 'Components/toastNoti/success_noti'
import errorNotification from 'Components/toastNoti/error_noti'



//const db = getDatabase(app);
const db = getFirestore(app);

function* loadProfileUserSaga(action) {
    try {
        //console.log(action.payload);
        console.log(action.payload);
        const data = yield getProfile(action.payload);
        console.log(data);
        yield put({
            type: SUCCESS(LOAD_PROFILE_USER),
            payload: data
        })
    } catch (error) {
        errorNotification({type: 'Load Profile User', content: error})
        yield put({
            type: FAILURE(LOAD_PROFILE_USER),
            error
        })
    }
}

function* updateProfileSaga(action) {
    try {
        const data = yield updateProfle(action.payload.userID, action.payload.updateBody);

        // yield put({
        //     type: SUCCESS(UPDATE_PROFILE),
        //     payload: data
        // })
        console.log(action.payload.userID);
        yield put({
            type: REQUEST(LOAD_PROFILE_USER),
            payload: { userId: action.payload.userID }
        })
        successNotification({type: 'Update Profile', content: 'Update Profile successed'})
    } catch (error) {
        errorNotification({type: 'Update Profile', content: error})
        yield put({
            type: FAILURE(UPDATE_PROFILE),
            error
        })
    }
}

function* sendRequestFriendSaga(action) {
    try {
        const stateAuth = yield select(selectAuthentication);
        console.log(action.payload);
        //const data = yield sendRequestFriend(action.payload.userID, action.payload.friendID);
        yield call(handleEmitSocketNoti, { from_id: action.payload.userID, to_id: action.payload.friendID, type: 'friend', content: `${stateAuth.profile.name} has just sent request add friend with you` })
        yield put({
            type: SUCCESS(REQ_ADD_FRIEND),
            payload: action.payload
        })
    } catch (error) {
        errorNotification({type: 'Request Add Friend', content: error})
        yield put({
            type: FAILURE(REQ_ADD_FRIEND),
            error
        })
    }
}

function handleEmitSocketNoti(noti) {
    // let endpoint;
    // if (noti.type === 'post') {
    //   endpoint = `post/${noti.from_id}`; // Gửi thông báo đến bài post mà chủ bài post đó đã đăng ký nhận noti
    // } else {
    //   endpoint = `friend/${noti.to_id}`; // Gửi thông báo đến friend/user_id_request mà người nhận request add friend này đã dăng ký để nhận noti của họ
    // }
    
    // const newNotiRef = ref(db, endpoint);
    // console.log('OKK')
    // return set(newNotiRef, noti);
    let endpoint;
    let subpoint;
    if (noti.type === 'post') {
        subpoint = 'notiPost';
        endpoint = `${noti.from_id}`; // Gửi thông báo đến bài post mà chủ bài post đó đã đăng ký nhận noti
    } else {
        subpoint = 'notiFriend';
        endpoint = `${noti.to_id}`; // Gửi thông báo đến friend/user_id_request mà người nhận request add friend này đã dăng ký để nhận noti của họ
    }
    
    const newNotiRef = collection(db, subpoint);
    console.log('OKK')
    return setDoc(doc(newNotiRef, endpoint), noti);
  }

function* allowRequestFriendSaga(action) {
     // to_id : người đc nhận lời mời kb
     // from_id : người gửi đi lời mời
    try {
        console.log(action.payload);
        const data = yield allowRequestFriend(action.payload.from_id, action.payload.to_id);
        yield put({
            type: REQUEST(REMOVE_NOTI),
            payload: action.payload
        })
        yield put({
            type: SUCCESS(ALLOW_REQ_FRIEND),
            payload: data
        })
        // khi người user accecpt thì sẽ load lại profile để cập nhật danh sách bạn bè
        // còn người send request phải f5 lại 
        yield put({
            type: REQUEST(LOAD_PROFILE_USER),
            payload: action.payload.to_id
           
        })
    } catch (error) {
        errorNotification({type: 'Allow Request', content: error})
        yield put({
            type: FAILURE(ALLOW_REQ_FRIEND),
            error
        })
    }
}

function* denyRequestFriendSaga(action) {
    try {
        //const data = yield removeRequestFriend(action.payload._id);
        yield put({
            type: REQUEST(REMOVE_NOTI),
            payload: action.payload
        })
        yield put({
            type: SUCCESS(DENY_REQ_FRIEND),
            payload: action.payload
        })
    } catch (error) {
        errorNotification({type: 'Deny Request', content: error})
        yield put({
            type: FAILURE(DENY_REQ_FRIEND),
            error
        })
    }
}

export default function* profleSaga() {

    yield takeLatest(REQUEST(LOAD_PROFILE_USER), loadProfileUserSaga)
    yield takeLatest(REQUEST(UPDATE_PROFILE), updateProfileSaga)
    yield takeLatest(REQUEST(REQ_ADD_FRIEND), sendRequestFriendSaga)
    yield takeLatest(REQUEST(ALLOW_REQ_FRIEND), allowRequestFriendSaga)
    yield takeLatest(REQUEST(DENY_REQ_FRIEND), denyRequestFriendSaga)
}