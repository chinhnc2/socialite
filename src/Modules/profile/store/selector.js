/* eslint-disable arrow-parens */
/* eslint-disable implicit-arrow-linebreak */

/**
 * The global state selectors
 */

import { createSelector } from 'reselect'
import { initialState } from './reducer'

const selectProfiles = state => state.profile || initialState

const makeSelectProfiles = () =>
    createSelector(
        selectProfiles,
        state => state
    )

export {
    selectProfiles,
    makeSelectProfiles
}
