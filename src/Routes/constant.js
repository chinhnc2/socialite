import BlankLayout from 'Layouts/blank'
import HomeLayout from 'Layouts/home'
import HomeScreen from 'Modules/home'
import ChatScreen from 'Modules/CHAT'
import Profile from '../Modules/profile/index.jsx'
import StoryReel from '../Components/storyReel'
import PostSingle from '../Components/postSingle'
import NotiPage from '../Components/notiPage'
import { USER_ROLE } from 'Constants/auth'
const RoutesName = {
    HOME: '/',
    COURSE: '/course-management',
    MY_PAGE: '/my-page',
    REGISTER: '/register',
    CHAT: '/chat', // luong
    PROFILE: '/profile',
    STORYREEL: '/storyreel',
    PROFILE: '/u/:id',
    POST: '/post/:id',
    NOTI: '/noti'
}

export const ROUTES = [
    {
        path: RoutesName.HOME,
        component: HomeScreen,
        layout: HomeLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.PROFILE,
        component: Profile,
        layout: HomeLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    { 
        path: RoutesName.STORYREEL,
        component: StoryReel,
        layout: BlankLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    { 
        path: RoutesName.POST,
        component: PostSingle,
        layout: HomeLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    {
        path: RoutesName.CHAT,
        component: ChatScreen,
        layout: BlankLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    },
    { 
        path: RoutesName.NOTI,
        component: NotiPage,
        layout: BlankLayout,
        rules: [USER_ROLE.NISSHOKEN_SUPER_ADMIN, USER_ROLE.NISSHOKEN_ADMIN, USER_ROLE.COMPANY_ADMIN]
    }
]

export default RoutesName
