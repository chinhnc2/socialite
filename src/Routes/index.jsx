import React, { Suspense, useEffect } from "react";
import { Switch } from "react-router-dom";
import { isNil } from 'lodash'

import Loading from 'Components/loading'
import { ROUTES } from './constant'
import NFTGameRoutes from 'Modules/NFT/router'
import StoreRoutes from 'Modules/STORES/routes'
import AuthRoutes from 'Modules/auth/routes';
import { useAuth } from 'Hooks';
import CompanyRoutes from "Modules/company/routes";
import PrivateRoute from "Components/route/privateRoute";


export default function AppRoutes() {
  const { loadProfileAction, authenticated , error} = useAuth();

  useEffect(() => {
    if (isNil(authenticated)) {
      loadProfileAction();
    }
  }, [authenticated])

  return (
    <Suspense fallback={<Loading />}>
      
      <Switch>
        {ROUTES.map((routeConfig, index) => (
          <PrivateRoute key={index} exact {...routeConfig} />
        ))}
      </Switch>
      <AuthRoutes />
      <StoreRoutes />
      <CompanyRoutes />
      <NFTGameRoutes />
    </Suspense>
  );
}
