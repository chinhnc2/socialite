// // Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

// // Your web app's Firebase configuration
// // For Firebase JS SDK v7.20.0 and later, measurementId is optional
// const firebaseConfig = {
//   apiKey: "AIzaSyAlFHIS91AtGYt6AJPDd3GG09RKVPRDxok",
//   authDomain: "socialte-3e8a1.firebaseapp.com",
//   projectId: "socialte-3e8a1",
//   storageBucket: "socialte-3e8a1.appspot.com",
//   messagingSenderId: "138057832544",
//   appId: "1:138057832544:web:4598b174304f2be9444b05",
//   measurementId: "G-PQHV7HXFEC",
//   databaseURL: "https://socialte-3e8a1-default-rtdb.asia-southeast1.firebasedatabase.app"
// };

// // Initialize Firebase
// export const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

// // const database = getDatabase(app);


// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import "firebase/compat/analytics"
import "firebase/auth"
import 'firebase/compat/firestore';
import firebase from 'firebase/compat/app';
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA-NmSxoBxum7cX5VkiB7Z6pcVXsCsZ3LQ",
  authDomain: "socialite-chat.firebaseapp.com",
  projectId: "socialite-chat",
  storageBucket: "socialite-chat.appspot.com",
  messagingSenderId: "375360585886",
  appId: "1:375360585886:web:aa025d2aeee3aad3267b8f",
  measurementId: "G-6Z0J5VM9PL",
  databaseURL: "https://socialite-chat-default-rtdb.asia-southeast1.firebasedatabase.app/"
};

// Initialize Firebase
export const app = firebase.initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
firebase.analytics();

// const database = getDatabase(app);


const storage = getStorage(app);
// const auth = firebase.auth();
const db = firebase.firestore();

export { db, storage };

export default firebase;
