const setupData = (labels, data, type, title) => {
    let backgroundColor;
    let borderColor;
      if (type == 'circle') {
        backgroundColor= [
          'rgba(60, 237, 118, 1)',
          'rgba(60, 175, 214, 1)',
          'rgba(149, 91, 214, 1)',
          'rgba(149, 224, 0, 1)',
          'rgba(255, 147, 131, 1)',
          'rgba(121, 147, 131, 1)',
          'rgba(255, 35, 16, 1)'
        ]
        borderColor=[];
      } else if (type == 'line') {
        backgroundColor=[
          'rgba(149, 91, 214, 1)',
          'rgba(149, 224, 0, 1)',
          'rgba(255, 147, 131, 1)',
          'rgba(121, 147, 131, 1)',
          'rgba(255, 35, 16, 1)',
          'rgba(60, 237, 118, 1)',
          'rgba(60, 175, 214, 1)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(160, 35, 16, 1)',
          'rgba(160, 151, 16, 1)',
          "rgba(160, 151, 235, 0.6)",
          "rgba(52, 151, 235, 0.5)",
          "rgba(52, 221, 173, 1)",
          "rgba(189, 114, 173, 1)",
          "rgba(0, 0, 0, 1)",
          " rgba(0, 0, 255, 0.7)",
          "rgba(0, 255, 0, 0.7)",
          "rgba(240, 234, 266, 1)",
          'rgba(60, 237, 118, 1)',
          'rgba(60, 175, 214, 1)',
          'rgba(149, 91, 214, 1)',
          'rgba(149, 224, 0, 1)',
          'rgba(255, 147, 131, 1)',
          'rgba(121, 147, 131, 1)',
          'rgba(255, 35, 16, 1)'
        ];
        borderColor = [];
      } else {
        backgroundColor =  [
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)',
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          
        ];
  
        borderColor =  [
          'rgb(75, 192, 192)',
          'rgb(54, 162, 235)',
          'rgb(153, 102, 255)',
          'rgb(201, 203, 207)',
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          
        ]
      }
      return {
  
        labels,
        datasets: [{
          label: title,
          data,
          backgroundColor,
          borderColor,
          borderWidth: 1
        }],
        options: (type == 'circle') ? {
          responsive: true,
          maintainAspectRatio: false,
          cutoutPercentage: 80,
          tooltips: {
            callbacks: {
              label: function(tooltipItem, data) {
                return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
              }
            }
          }
        } : (type == 'line') ? {
            plugins: {
                title: {
                    display: true,
                    text: 'Custom Chart Title'
                }
          }
        } : {}
    
        }
    };
  
    export default setupData;